# Navigation Shifts in MX
## For users of mc

The legacy Nereus / Sentry mission controller ```mc``` sepcified missions in a geo-referenced
cartesian coordinate frame.  To correct for navigation errors during the mission, it allowed
uses to shift the lat/lon origin of that frame by a specified number of meters during a dive.
In addition to correcting for navigation error, this feature is sometimes used by operators 
to move mission elements around to avoid dangerous obstacles or acoustically re-task 
pre-programmed missions to areas of interest.

Given how useful this feature has proven, MX provides similar functionality.  In keeping with
MX's philosophy of always operating with global lat/lon, the navigation and re-tasking roles
have been separated.  Three possible shifts may be specified.

1. Any navigation shift, used exclusively to correct for navigational errors, is handled by a
separate ```ds_nav_shift``` node.
1. Most complex tasks, including any in the standard library, accept ```global_shift_east```
```global_shift_north``` parameters that should be set to shared parameters with those names.
1. These same tasks include ```local_shift_east``` and ```local_shift_north``` parameters that
may be used to shift just those mission elements.

## Why bother with all these different shifts?

This design is intended to accomplish three goals.  First, move navigation corrections outside
the mission and into the navigation system, where more sophisticated sensor-fusion techniques
may eventually replace user-initiated shifts.  This may prove especially useful when operating
a vehicle remotely via gateway ASV (i.e., a waveglider).  Second, preserve the ability to apply
mission-wide retasking shifts.  Third, provide a mechanism for operators to move individual
mission elements without requiring an operator to send a command to reverse a shift between 
mission elements.  This is also expected to be useful during periods of limited communication
with the AUV.  This could easily arise during remote vehicle operations or if the AUV is 
supported by a ship that frequently breaks off to conduct other operations.

It should be noted that separating navigation shifts and command shifts is an essential
first step to useful application of any real-time data fusion techniques that automatically
or semi-automatically compute a navigation shift from USBL data in real-time.

## For Sentry

As an example implementation, here's how Sentry's various shifts are implemented:

Standard navigation output from ```ds_deadreck``` and the nav aggregator is broadcast to
the shift node.  The shift node is named ```/sentry/nav/shifted``` and uses the following
topics:
* ```/sentry/nav/shifted/navagg```: Shifted AggregatedNav message
* ```/sentry/nav/shifted/navstate```: Shifted NavState message
* ```/sentry/nav/shifted/current_shift```: Current shift, in meters. Forwarded topside via
acoustic modem to provide operator feedback
* ```/sentry/nav/shifted/new_shift```: Messages broadcast to this topic are used to set a new
shift.  Currently, these are provided by operator via acoustic modem.
