/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//

#include <ds_mx_stdtask/tracksfile.h>
#include <ds_mxcore/ds_mxeventlog.h>

#include <pluginlib/class_list_macros.h>
#include <sstream>

namespace ds_mx_stdtask {

Tracksfile::Tracksfile() :
center_pt(params, "center_pt", ds_mx::ParameterFlag::DYNAMIC),
point_list(params, "point_list", ds_mx::ParameterFlag::STATIC)
{
}

Tracksfile::~Tracksfile() = default;

void Tracksfile::updatePattern() {
  ds_mx::GeoPoint center = center_pt.get();

  ds_trackline::PatternTracksfile::WaypointListT waypoints;

  std::stringstream str(point_list.get());
  while (!str.eof()) {
    double x, y;
    if ((str >> x) && (str >> y)) {
      waypoints.push_back(ds_trackline::PatternTracksfile::WaypointT(x,y));
    }
  }

  pattern = ds_trackline::PatternTracksfile(center.x, center.y, waypoints);
  applyBaseParams(); // apply anny parameters from our base class
}


} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Tracksfile, ds_mx::MxTask)