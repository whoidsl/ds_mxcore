/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "ds_mx_stdtask/ascent.h"

#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

Ascent::Ascent() : depth_min(params, "depth_min", ds_mx::ParameterFlag::DYNAMIC),
                   depth_hits(params, "depth_hits", 10),
                   ascent_rate_min(params, "ascent_rate_min", ds_mx::ParameterFlag::DYNAMIC),
                   ascent_rate_hits(params, "ascent_rate_hits", 600),
                   disable_rate_check(params, "disable_rate_check", false, ds_mx::ParameterFlag::OPTIONAL),
                   subtask_can_finish(params, "subtask_can_finish", false, ds_mx::ParameterFlag::STATIC) {}

Ascent::~Ascent() = default;

void Ascent::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  // load our single, solitary subtask
  primary = loadSubtask(compiler, "primary", config["primary"]);
}

void Ascent::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state);

  // reset our state stuff
  depth_hits.reset();
  ascent_rate_hits.reset();

  startSubtask(state, primary);
}

bool Ascent::validate() const {
  bool rc = true;

  if (depth_min.get() < 0) {
    ROS_ERROR_STREAM("Ascent Task Exit Depth (" <<depth_min.get() <<") is above sea surface!");
    rc = false;
  }

  // let the ascent rate be whatever
  if (depth_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("Ascent Task ExitHits must have threshold > 0, not " <<depth_hits.getHitThreshold());
    rc = false;
  }

  if (ascent_rate_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("Ascent Task AscentRateHits must have threshold > 0, not " <<ascent_rate_hits.getHitThreshold());
    rc = false;
  }

  // also check our subtask
  return primary->validate() && rc;
}

void Ascent::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  primary->getDisplay(state, display);
}

ds_mx::TaskReturnCode Ascent::subtaskDone(const ds_nav_msgs::NavState& state,
                                          ds_mx::TaskReturnCode subtaskCode) {
  if (subtask_can_finish.get()) {
    return subtaskCode;
  }
  return ds_mx::TaskReturnCode::FAILED;
}

ds_mx::TaskReturnCode Ascent::onTick(const ds_nav_msgs::NavState& state) {

  // check the depth limit
  if (state.down < depth_min.get()) {
    depth_hits.goodHit();
  } else {
    depth_hits.badHit();
  }

  if (depth_hits) {
      MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_SUCCEED, "Depth hits " <<depth_hits.getHits()
                                                                 <<" >= minimum hits required ("
                                                                 <<depth_hits.getHitThreshold() <<")");
    return ds_mx::TaskReturnCode::SUCCESS;
  }

  // increment or decrement the hits counter
  if (!disable_rate_check.get()) {
    if (-state.heave_w < ascent_rate_min.get()) {
      ascent_rate_hits.goodHit();
    } else {
      ascent_rate_hits.badHit();
    }

    // check the hits
    if (ascent_rate_hits) {
      MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "ascent failed because ascent rate "
          << -state.heave_w << "m/s up has been below threshold " << ascent_rate_min.get()
          << " more than " << ascent_rate_hits.getHits() << " >= " << ascent_rate_hits.getHitThreshold() << " times!");
      return ds_mx::TaskReturnCode::FAILED;
    }
  }

  return ds_mx::TaskReturnCode::RUNNING;
}

int Ascent::getDepthHits() const {
  return depth_hits.getHits();
}

int Ascent::getAscentRateHits() const {
  return ascent_rate_hits.getHits();
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Ascent, ds_mx::MxTask)