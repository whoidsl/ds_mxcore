/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/28/19.
//

#include <limits>

#include <ds_mx_stdtask/lawnmower.h>
#include <ds_mxcore/ds_mxeventlog.h>

#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

Lawnmower::Lawnmower() :
center(params, "center_pt", ds_mx::ParameterFlag::DYNAMIC),
survey_heading(params, "survey_heading", ds_mx::ParameterFlag::DYNAMIC),
width(params, "width", ds_mx::ParameterFlag::DYNAMIC),
length(params, "length", ds_mx::ParameterFlag::DYNAMIC),
spacing(params, "spacing", ds_mx::ParameterFlag::DYNAMIC),
cross_offset(params, "cross_offset", ds_mx::ParameterFlag::DYNAMIC),
cross_skip(params, "cross_skip", ds_mx::ParameterFlag::DYNAMIC),
flip_width(params, "flip_width", ds_mx::ParameterFlag::DYNAMIC),
flip_length(params, "flip_length", ds_mx::ParameterFlag::DYNAMIC)
{
}

Lawnmower::~Lawnmower() = default;

void Lawnmower::updatePattern() {
  // create a new pattern by reading all our parameters
  ds_mx::GeoPoint center_pt = center.get();
  pattern = ds_trackline::PatternLawnmow(center_pt.x, center_pt.y,
      survey_heading.get(), width.get(), length.get(), spacing.get(), cross_offset.get(),
      cross_skip.get(), flip_width.get(), flip_length.get());

  applyBaseParams(); // apply any parameters from our base class
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Lawnmower, ds_mx::MxTask)
