/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/29/20.
//

#include <ds_mx_stdtask/goto.h>
#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

Goto::Goto() : center_pt(params, "center_pt", ds_mx::ParameterFlag::DYNAMIC),
               current_trackline(0, 0, 0, 0),
               local_shift_east(params,
                                "local_shift_east",
                                0.0,
                                ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
               local_shift_north(params,
                                 "local_shift_north",
                                 0.0,
                                 ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
               global_shift_east(params,
                                 "global_shift_east",
                                 0.0,
                                 ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
               global_shift_north(params,
                                  "global_shift_north",
                                  0.0,
                                  ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL) {
  // do nothing
}

Goto::~Goto() = default;

void Goto::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  tracklineTemplate = loadSubtask(compiler, "trackline", config["trackline"]);
}

void Goto::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxComplexTask::onStart(state);

  updateTemplate(makeTrackline(state));

  startSubtask(state, tracklineTemplate);
}

bool Goto::validate() const {
  const auto &templateParams = tracklineTemplate->getParameters();

  bool rc = true;
  if (!(templateParams.has<ds_mx::GeoPointParam>("start_pt")
      && templateParams.has<ds_mx::GeoPointParam>("end_pt"))) {
    ROS_ERROR_STREAM("TracklinePattern task \"" << name << "\" of type \"" << type
                                                << "\" has a trackline template that does not support "
                                                << "either start_pt or end_pt"
                                                << " Template has type: " << tracklineTemplate->getType());

    rc = false;
  }
  return validateSubtasks() && rc;
}

void Goto::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  double speed = std::numeric_limits<double>::quiet_NaN();
  auto &templateParams = tracklineTemplate->getParameters();
  if (templateParams.has<ds_mx::DoubleParam>("speed")) {
    speed = templateParams.get<ds_mx::DoubleParam>("speed").get();
  } else {
    ROS_WARN_STREAM("Template parameter for survey " << name << " does not have speed!  Assuming 0...");
  }

  // draw our goal trackline
  ds_trackline::Trackline line = makeTrackline(state);

  ds_mx_msgs::MissionElementDisplay elem;
  elem.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING;
  elem.wellknowntext = line.getWktLL();
  std::copy(uuid.begin(), uuid.end(), elem.task_uuid.begin());
  if (!std::isnan(speed)) {
    state.header.stamp += ros::Duration(line.getLength() / speed);
  }
  display.elements.push_back(elem);

  auto goal = line.getEndLL();

  state.lon = goal(0);
  state.lat = goal(1);
}

ds_trackline::Trackline Goto::makeTrackline(const ds_nav_msgs::NavState &state) const {

  auto pt = center_pt.get();

  // create a new projection centered at our nominal goal position
  ds_trackline::Projection proj(pt.x, pt.y);

  // Start from our current position
  ds_trackline::Projection::VectorLL state_ll(state.lon, state.lat);
  ds_trackline::Projection::VectorEN start_en = proj.lonlat2projected(state_ll);

  // New destination is (0,0), but we have to ADD the shift-- but since 0 + shift is just shift, we'll
  // keep it simple-- just use the shift

  // actually set trackline
  return ds_trackline::Trackline(start_en(0), start_en(1), getShiftEast(), getShiftNorth(), proj);
}

void Goto::updateTemplate(const ds_trackline::Trackline& trackline) {
  current_trackline = trackline;
  auto line_start_ll = current_trackline.getStartLL();
  auto line_end_ll = current_trackline.getEndLL();
  ROS_INFO_STREAM("Drawing trackline from " <<line_start_ll(0) <<" " <<line_start_ll(1) <<" --> "
                      <<line_end_ll(0) <<" " <<line_end_ll(1));

  // actually update the trackline
  auto &templateParams = tracklineTemplate->getParameters();
  templateParams.get<ds_mx::GeoPointParam>("start_pt").set(ds_mx::GeoPoint(line_start_ll(0), line_start_ll(1)));
  templateParams.get<ds_mx::GeoPointParam>("end_pt").set(ds_mx::GeoPoint(line_end_ll(0), line_end_ll(1)));

  tracklineTemplate->resetTimeout(ros::Time::now());
}

ds_mx::TaskReturnCode Goto::subtaskDone(const ds_nav_msgs::NavState &state,
                                        ds_mx::TaskReturnCode subtaskCode) {
  if (subtaskCode == ds_mx::TaskReturnCode::SUCCESS) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_SUCCEED, "GOTO reached goal point!");
    return ds_mx::TaskReturnCode::SUCCESS;
  }

  MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "GOTO failed to reach goal point!");
  return ds_mx::TaskReturnCode::FAILED;
}

void Goto::parameterChanged(const ds_nav_msgs::NavState &state) {

  // this is handled differently if we're currently executing
  if (isRunning()) {

  // we're going to stop the current subtask, update the trackline, and restart it
  stopActiveSubtask(state);

  // just draw trackline from current position
  updateTemplate(makeTrackline(state));

  // start the newly-updated task
  startSubtask(state, tracklineTemplate);

  } else {
    // just update the template, don't worry about starting the task or anything
    updateTemplate(makeTrackline(state));
  }
}

double Goto::getShiftNorth() const {
  return local_shift_north.get() + global_shift_north.get();
}

double Goto::getShiftEast() const {
  return local_shift_east.get() + global_shift_east.get();
}

} // namespace ds_mx_stdtask

// export so this task is discoverable by pluginlib
PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Goto, ds_mx::MxTask);
