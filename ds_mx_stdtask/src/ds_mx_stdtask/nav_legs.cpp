/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "ds_mx_stdtask/nav_legs.h"

#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

NavLegs::NavLegs() : center_pt(params, "center_pt", ds_mx::ParameterFlag::DYNAMIC),
                     length(params, "length", 10, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
                     heading(params, "heading", 90, ds_mx::ParameterFlag::DYNAMIC),
                     right_first(params,
                                 "right_first",
                                 false,
                                 ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL)
{
  updatePattern();
}

NavLegs::~NavLegs() = default;

void NavLegs::updatePattern() {

  // actually update the pattern
  ds_mx::GeoPoint center = center_pt.get();

  pattern = ds_trackline::PatternNavLegs(center.x, center.y, heading.get(), length.get(), right_first.get());

  // apply any parameters from our base class
  applyBaseParams();
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::NavLegs, ds_mx::MxTask)

