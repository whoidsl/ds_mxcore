/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/13/19.
//

#include "ds_mx_stdtask/decktest_transect.h"
#include <pluginlib/class_list_macros.h>
#include <ds_libtrackline/TransformUtils.h>

namespace ds_mx_stdtask {

DecktestTransect::DecktestTransect() : trackline(0, 0, 0, 0.1),
                                       heading_offset(params, "heading_offset", 0, ds_mx::ParameterFlag::OPTIONAL | ds_mx::ParameterFlag::DYNAMIC),
                                       crosstrack_offset(params, "crosstrack_offset", 0, ds_mx::ParameterFlag::OPTIONAL | ds_mx::ParameterFlag::DYNAMIC),
                                       line_length(params, "line_length", 100, ds_mx::ParameterFlag::OPTIONAL | ds_mx::ParameterFlag::DYNAMIC)
{}

DecktestTransect::~DecktestTransect() = default;

void DecktestTransect::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);
  timeout_code = ds_mx::TaskReturnCode::SUCCESS;

  tracklineTemplate = loadSubtask(compiler, "trackline", config["trackline"]);
}

void DecktestTransect::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state);

  updateLine(state);
  startSubtask(state, tracklineTemplate);
}

bool DecktestTransect::validate() const {
  const auto &templateParams = tracklineTemplate->getParameters();

  bool rc = true;
  if (!(templateParams.has<ds_mx::GeoPointParam>("start_pt")
      && templateParams.has<ds_mx::GeoPointParam>("end_pt"))) {
    ROS_ERROR_STREAM("TracklinePattern task \"" << name << "\" of type \"" << type
                                                << "\" has a trackline template that does not support "
                                                << "either start_pt or end_pt"
                                                << " Template has type: " << tracklineTemplate->getType());

    rc = false;
  }
  return validateSubtasks() && rc;
}

void DecktestTransect::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  // just pass on the display
  tracklineTemplate->getDisplay(state, display);
}

ds_mx::TaskReturnCode DecktestTransect::subtaskDone(const ds_nav_msgs::NavState& state,
                                    ds_mx::TaskReturnCode subtaskCode) {
  return subtaskCode;
}

ds_mx::TaskReturnCode DecktestTransect::onTick(const ds_nav_msgs::NavState& state) {
  updateLine(state);
  // there really isn't an end criterion
  return ds_mx::TaskReturnCode::RUNNING;
}

void DecktestTransect::updateLine(const ds_nav_msgs::NavState& state) {
  // update trackline based on current state
  double hdg = state.heading + heading_offset.get();
  ds_trackline::Projection tl_proj(state.lon, state.lat);
  ds_trackline::Trackline::VectorEN tl_start, tl_end;

  // we build these as along/across track
  tl_start <<0,crosstrack_offset.get();
  tl_end <<line_length.get(),crosstrack_offset.get();

  // and then rotate to the desired heading
  Eigen::Matrix2d tform = ds_trackline::makeAlongtrackTransform(
      state.heading + heading_offset.get(), false, false);

  trackline = ds_trackline::Trackline(tform*tl_start, tform*tl_end, tl_proj);

  // update trackline template based on new trackline
  auto start_pt = trackline.getStartLL();
  auto end_pt = trackline.getEndLL();
  auto &templateParams = tracklineTemplate->getParameters();
  templateParams.get<ds_mx::GeoPointParam>("start_pt").set(ds_mx::GeoPoint(start_pt(0), start_pt(1)));
  templateParams.get<ds_mx::GeoPointParam>("end_pt").set(ds_mx::GeoPoint(end_pt(0), end_pt(1)));
}


} //namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::DecktestTransect, ds_mx::MxTask)