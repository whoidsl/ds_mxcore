/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "ds_mx_stdtask/run_above_depth.h"
#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

RunAboveDepth::RunAboveDepth() : depth_max(params, "depth_max", ds_mx::ParameterFlag::DYNAMIC),
                                 depth_hits(params, "depth_hits", 5),
                                 descent_rate_min(params, "descent_rate_min", 0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
                                 descent_rate_hits(params, "descent_rate_hits", 10),
                                 subtask_can_finish(params, "subtask_can_finish", false, ds_mx::ParameterFlag::STATIC),
                                 disable_rate_check(params, "disable_rate_check", false, ds_mx::ParameterFlag::OPTIONAL) {}

RunAboveDepth::~RunAboveDepth() = default;

void RunAboveDepth::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  // load our single, solitary subtask
  subtask = loadSubtask(compiler, "subtask", config["subtask"]);
}

void RunAboveDepth::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state);

  depth_hits.reset();

  previous_depth = state.down;
  previous_depth_time = state.header.stamp;
  descent_rate_hits.reset();

  startSubtask(state, subtask);
}

bool RunAboveDepth::validate() const {
  bool rc = true;
  if (depth_max.get() < 0) {
    ROS_ERROR_STREAM("RunAboveDepth Task has invalid target depth (" <<depth_max.get() <<" < 0)");
    rc = false;
  }

  if (depth_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("RunAboveDepth task has an invalid depth_hits threshold (" <<depth_hits.getHitThreshold() <<" <= 0)");
    rc = false;
  }

  if (descent_rate_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("RunAboveDepth task has an invalid threshold for descent_rate_hits (" <<descent_rate_hits.getHitThreshold() <<" <= 0)");
    rc = false;
  }

  bool subtask_rc = ds_mx::MxComplexTask::validateSubtasks();
  return rc && subtask_rc;
}

void RunAboveDepth::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  return subtask->getDisplay(state, display);
}

ds_mx::TaskReturnCode RunAboveDepth::subtaskDone(const ds_nav_msgs::NavState& state,
                                    ds_mx::TaskReturnCode subtaskCode) {
  if (subtask_can_finish.get()) {
    return subtaskCode;
  }
  return ds_mx::TaskReturnCode::FAILED;
}

ds_mx::TaskReturnCode RunAboveDepth::onTick(const ds_nav_msgs::NavState& state) {

  // check for reaching the depth limit
  if (state.down > depth_max.get()) {
    depth_hits.goodHit();
  } else {
    depth_hits.badHit();
  }

  if (depth_hits) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_SUCCEED, "Depth hits " <<depth_hits.getHits()
                                                                 <<" >= minimum hits required ("
                                                                 <<depth_hits.getHitThreshold() <<")");
    return ds_mx::TaskReturnCode::SUCCESS;
  }

  // now check the minimum descent rate
  if (!disable_rate_check.get()) {
    ros::Duration dt = state.header.stamp - previous_depth_time;
    if (dt.toSec() > 0.010) {
      double ddepth_dt = (state.down - previous_depth) / dt.toSec();

      // increment or decrement the hits counter
      if (ddepth_dt < descent_rate_min.get()) {
        descent_rate_hits.goodHit();
      } else {
        descent_rate_hits.badHit();
      }

      // check the hits
      if (descent_rate_hits) {
        MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "run_above_depth failed because descent rate "
            << ddepth_dt << " has been below threshold " << descent_rate_min.get()
            << " more than " << descent_rate_hits.getHits() << " >= " << descent_rate_hits.getHitThreshold()
            << " times!");
        return ds_mx::TaskReturnCode::FAILED;
      }

      // update the previous values
      previous_depth = state.down;
      previous_depth_time = state.header.stamp;
    }
  }
  // if the timestamp is too small, just ignore this state message for descent rate computation.  It's also how
  // we handle the uninitialized case.  Since onStart is always called before the first tick,
  // dt.toSec will be exactly zero for the first sample.  And since not dividing by zero is always smart when
  // computing derivatives, this gets us two checks for the price of one

  return ds_mx::TaskReturnCode::RUNNING;
}

int RunAboveDepth::getDepthHits() const {
  return depth_hits.getHits();
}

int RunAboveDepth::getDescentRateHits() const {
  return descent_rate_hits.getHits();
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::RunAboveDepth, ds_mx::MxTask)