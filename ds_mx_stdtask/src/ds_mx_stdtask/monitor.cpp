/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/28/19.
//

#include <ds_mx_stdtask/monitor.h>

#include <pluginlib/class_list_macros.h>
#include <ds_mxcore/ds_mxeventlog.h>

namespace ds_mx_stdtask {

Monitor::Monitor() : event_handler(events, "event", "/fault/*", &Monitor::event_detected, this) {}

Monitor::~Monitor() = default;

void Monitor::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  // load our single, solitary subtask
  subtask = loadSubtask(compiler, "subtask", config["subtask"]);
}

void Monitor::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state);

  startSubtask(state, subtask);
}

ds_mx::TaskReturnCode Monitor::subtaskDone(const ds_nav_msgs::NavState &state, ds_mx::TaskReturnCode subtaskCode) {
  // just pass our subtask's return code through
  return subtaskCode;
}

bool Monitor::validate() const {
  // nothing to check
  return validateSubtasks();
}

bool Monitor::event_detected(ds_mx_msgs::MxEvent event) {
  next_tick_return_code = ds_mx::TaskReturnCode::FAILED;
  return false;
}

void Monitor::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  subtask->getDisplay(state, display);
}

}

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Monitor, ds_mx::MxTask)