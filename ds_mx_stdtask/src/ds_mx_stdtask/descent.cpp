/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "ds_mx_stdtask/descent.h"

#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {
Descent::Descent() :
  altitude_goal(params, "altitude_goal", ds_mx::ParameterFlag::DYNAMIC),
  depth_floor(params, "depth_floor", ds_mx::ParameterFlag::DYNAMIC),
  beam_quality_threshold(params, "beam_quality_threshold", ds_mx::ParameterFlag::DYNAMIC),
  beam_num_min(params, "beam_num_min", ds_mx::ParameterFlag::DYNAMIC),
  descent_rate_min(params, "descent_rate_min", ds_mx::ParameterFlag::DYNAMIC),
  subtask_can_finish(params, "subtask_can_finish", false, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
  depth_floor_returns_success(params, "depth_floor_returns_success", false, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
  disable_rate_check(params, "disable_rate_check", false, ds_mx::ParameterFlag::OPTIONAL),
  altitude_hits(params, "altitude_hits", 1000, 100, 0, -1),
  depth_hits(params, "depth_hits", 10),
  descent_rate_hits(params, "descent_rate_hits", 10)
{}

Descent::~Descent() = default;

void Descent::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  // load our single, solitary subtask
  descent = loadSubtask(compiler, "descent", config["descent"]);
}

void Descent::init_ros(ros::NodeHandle& nh) {
  ds_mx::MxComplexTask::init_ros(nh);
  ranges_sub_ = nh.subscribe("ranges", 10, &Descent::rangeMessageCallback, this);
}

void Descent::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state);

  altitude_hits.reset();
  depth_hits.reset();
  descent_rate_hits.reset();

  startSubtask(state, descent);
}

bool Descent::validate() const {
  bool rc = true;

  if (altitude_goal.get() <= 0) {
    ROS_ERROR_STREAM("Descent: Altitude goal must be positive, not " <<altitude_goal.get());
    rc = false;
  }
  if (depth_floor.get() <= 0) {
    ROS_ERROR_STREAM("Descent: Depth floor must be positive, not " <<depth_floor.get());
    rc = false;
  }
  if (descent_rate_min.get() <= 0) {
    ROS_ERROR_STREAM("Descent: Descent rate minimum must be positive, not " <<descent_rate_min.get());
    rc = false;
  }
  if (beam_num_min.get() < 0 || beam_num_min.get() > 4) {
    ROS_ERROR_STREAM("Descent: beam_num_min should be between 0 and 4, not " <<beam_num_min.get());
    rc = false;
  }
  if (altitude_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("Descent: altitude hit threshold should be > 0, not " <<altitude_hits.getHitThreshold());
    rc = false;
  }
  if (depth_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("Descent: depth hit threshold should be > 0, not " <<depth_hits.getHitThreshold());
    rc = false;
  }
  if (descent_rate_hits.getHitThreshold() <= 0) {
    ROS_ERROR_STREAM("Descent: descent rate hit threshold should be > 0, not " <<descent_rate_hits.getHitThreshold());
    rc = false;
  }
  bool subtask_rc = ds_mx::MxComplexTask::validateSubtasks();
  return rc && subtask_rc;
}

void Descent::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  descent->getDisplay(state, display);
}

void Descent::rangeMessageCallback(const ds_sensor_msgs::Ranges3D &msg) {
  // Only do stuff if we're running
  if (!isRunning()) {
    return;
  }

  // we COULD use a TF to look straight down, but we'll stick with looking along the z-axis.  Although
  // recovering a transform to body-frame might be a good idea...
  // Also don't bother with a sound-speed correction... at least, not yet
  size_t good_beams = 0;
  double altitude = 0.0;
  for (size_t i=0; i<msg.ranges.size(); i++) {
    if (msg.ranges[i].range_validity == ds_sensor_msgs::Range3D::RANGE_VALID
      && msg.ranges[i].range_quality >= beam_quality_threshold.get()) {
      good_beams++;
      altitude += msg.ranges[i].range.point.z;
    }
  }
  if (good_beams > 0 && good_beams >= beam_num_min.get()) {
    altitude /= good_beams; // we're using an average

    // altitude is negative, as the standard TF tree uses FPU
    if (-altitude <= altitude_goal.get()) {
      altitude_hits.goodHit();
      ROS_INFO_STREAM("Altitude " <<altitude <<" incremented hits counter to " <<altitude_hits.getHits());
      // checking the hit counter happens in the tick function
    }
  }
}

ds_mx::TaskReturnCode Descent::subtaskDone(const ds_nav_msgs::NavState& state,
                                    ds_mx::TaskReturnCode subtaskCode) {
  if (subtask_can_finish.get()) {
    if (subtaskCode == ds_mx::TaskReturnCode::SUCCESS) {
      MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_SUCCEED, "Descent process exiting because subtask reports success");
    } else {
      MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_FAILED, "Descent process exiting because subtask failed");
    }
    return subtaskCode;
  }
  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_FAILED, "Descent process exiting because subtask finished");
  return ds_mx::TaskReturnCode::FAILED;
}

ds_mx::TaskReturnCode Descent::onTick(const ds_nav_msgs::NavState& state) {

  // check the depth floor limit
  if (state.down > depth_floor.get()) {
    ROS_INFO_STREAM("Depth " <<state.down <<" was below depth floor of " <<depth_floor.get());
    depth_hits.goodHit();
  } else {
    depth_hits.badHit();
  }

  if (depth_hits) {
    if (depth_floor_returns_success.get()) {
      MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_SUCCEED, "Depth floor reached");
      return ds_mx::TaskReturnCode::SUCCESS;
    } else {
      MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Descent failed because depth floor was "
          << "exceeded " << depth_hits.getHits() << " times >= threshold of "
          << depth_hits.getHitThreshold() << " times.");
      return ds_mx::TaskReturnCode::FAILED;
    }
  }

  // check the descent rate
  if (!disable_rate_check.get()) {
    // increment or decrement the hits counter
    if (state.heave_w < descent_rate_min.get()) {
      descent_rate_hits.goodHit();
      ROS_INFO_STREAM("Descent rate of " << state.heave_w << " was below minimum of " << descent_rate_min.get()
                                         << " at depth " << state.down << ", hits now " << descent_rate_hits.getHits());
    } else {
      descent_rate_hits.badHit();
    }

    // check the hits
    if (descent_rate_hits) {
      MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Descent failed because descent rate "
          << state.heave_w << "m/s has been below threshold " << descent_rate_min.get()
          << " more than " << descent_rate_hits.getHits() << " >= " << descent_rate_hits.getHitThreshold()
          << " times!");
      return ds_mx::TaskReturnCode::FAILED;
    }
  }

  // altitude hit counter is incremented separately; we decay it and check for results
  if (altitude_hits) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_SUCCEED, "Descent successfully exited with a final hit counter of "
        <<altitude_hits.getHits());
    return ds_mx::TaskReturnCode::SUCCESS;
  }
  altitude_hits.doDecay();

  return ds_mx::TaskReturnCode::RUNNING;
}

int Descent::getAltitudeHits() const {
  return altitude_hits.getHits();
}

int Descent::getDepthHits() const {
  return depth_hits.getHits();
}

int Descent::getDescentRateHits() const {
  return descent_rate_hits.getHits();
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Descent, ds_mx::MxTask)