/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/23/19.
//

#include <ds_mx_stdtask/contingency.h>
#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

Contingency::~Contingency() = default;

void Contingency::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  // load our subtasks
  nominal = loadSubtask(compiler, "nominal", config["nominal"]);
  backup = loadSubtask(compiler, "backup", config["backup"]);
}

void Contingency::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state);

  startSubtask(state, nominal);
}

bool Contingency::validate() const {
  // this Task really doesn't do anything, so just validate our subtasks
  return validateSubtasks();
}

void Contingency::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  // only display the nominal
  nominal->getDisplay(state, display);
}

ds_mx::TaskReturnCode Contingency::subtaskDone(const ds_nav_msgs::NavState& state,
                                  ds_mx::TaskReturnCode subtaskCode) {

  if (active_subtask == nominal && subtaskCode == ds_mx::TaskReturnCode::FAILED) {
    MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_UPDATE, "Contingency Task's primary subtask failed; switching to backup");
    switchSubtasks(state, backup);
    return ds_mx::TaskReturnCode::RUNNING;
  }

  return subtaskCode;
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Contingency, ds_mx::MxTask);