/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/25/19.
//

#include <ds_mx_stdtask/abort_root.h>

#include <pluginlib/class_list_macros.h>

#include <ds_mxcore/ds_mxevent.h>
#include <ds_mxcore/ds_mxtask.h>
#include <ds_mxcore/ds_mxeventlog.h>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mx_stdtask {

AbortRoot::~AbortRoot() = default;

void AbortRoot::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);


  // load our subtasks
  mission = loadSubtask(compiler, "mission", config["mission"]);
  abort = loadSubtask(compiler, "abort", config["abort"]);
  trap = loadSubtask(compiler, "trap", config["trap"]);
}

void AbortRoot::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxComplexTask::onStart(state);
  startSubtask(state, mission);
}

ds_mx::TaskReturnCode AbortRoot::tick(const ds_nav_msgs::NavState &state) {
  // make sure we have an active subtask
  assertActiveSubtask();

  // DON'T check the timeout; it only applies to the mission anyway

  // check for any parameter changes and respond accordingly
  if (params->anyChanged()) {
    parameterChanged(state);
    params->resetChanged();
  }

  // tick our subtask
  ds_mx::TaskReturnCode rc = active_subtask->tick(state);

  // Only the mission task can timeout; check it though
  if (active_subtask == mission && isTimeoutExpired(state.header.stamp)) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Mission Timeout");
    return subtaskDone(state, ds_mx::TaskReturnCode::FAILED);
  }

  if (rc == ds_mx::TaskReturnCode::RUNNING) {
    // subtask is still running, so call our
    // own handler and return its result
    return onTick(state);
  }

  return subtaskDone(state, rc);
}

ds_mx::TaskReturnCode AbortRoot::subtaskDone(const ds_nav_msgs::NavState &state, ds_mx::TaskReturnCode subtaskCode) {

  // here, the subtask isn't running; what we do depends on the active state
  if (active_subtask == mission) {
    switchSubtasks(state, abort);
    MX_LOG_EVENT_STREAM(this, MX_LOG_ABORT, "Mission aborting...");
  } else if (active_subtask == abort) {
    switchSubtasks(state, trap);
  } else if (active_subtask == trap) {
    switchSubtasks(state, trap);
  } else {
    ROS_ERROR_STREAM("AbortRoot task \"" <<name <<"\" somehow not in any of the normal states!");
    switchSubtasks(state, trap);
  }

  // This task should ALWAYS return running; so do that here
  return ds_mx::TaskReturnCode::RUNNING;
}

bool AbortRoot::validate() const {
  // insist all our subtasks are defined
  if (!mission) {
    ROS_ERROR_STREAM("Node " <<name <<" of type " <<type <<" has invalid mission subtask");
  }

  if (!abort) {
    ROS_ERROR_STREAM("Node " <<name <<" of type " <<type <<" has invalid abort subtask");
  }

  if (!trap) {
    ROS_ERROR_STREAM("Node " <<name <<" of type " <<type <<" has invalid trap subtask");
  }

  if (! (mission && abort && trap) ) {
    return false;
  }

  return validateSubtasks();
}

void AbortRoot::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  ds_mx_msgs::MissionElementDisplay launch_pt_disp;
  launch_pt_disp.role = ds_mx_msgs::MissionElementDisplay::ROLE_POINT_LAUNCH;
  launch_pt_disp.label = "Launch";
  auto pt = launch_pt.get();
  launch_pt_disp.wellknowntext = ds_trackline::wktPointLL(pt.x, pt.y);
  std::copy(uuid.begin(), uuid.end(), launch_pt_disp.task_uuid.begin());
  display.elements.push_back(launch_pt_disp);

  // we iterate through all our subtasks
  mission->getDisplay(state, display);
  abort->getDisplay(state, display);
  trap->getDisplay(state, display);
}

}

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::AbortRoot, ds_mx::MxTask)