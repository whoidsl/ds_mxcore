/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/8/19.
//

#include <ds_mx_stdtask/reactive_survey.h>

#include <pluginlib/class_list_macros.h>
#include <ds_mxcore/ds_mxcore.h>

namespace ds_mx_stdtask {

ReactiveSurvey::ReactiveSurvey() : detection_event(events, "detection_event", "/detection",
    &ReactiveSurvey::detect_detected, this), event_detected(false) {};

ReactiveSurvey::~ReactiveSurvey() = default;

void ReactiveSurvey::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  main_survey = loadSubtask(compiler, "main_survey", config["main_survey"]);
  detail_survey = loadSubtask(compiler, "detail_survey", config["detail_survey"]);
}

void ReactiveSurvey::onStart(const ds_nav_msgs::NavState& state) {
  MxComplexTask::onStart(state);

  event_detected = false;
  startSubtask(state, main_survey);
}

ds_mx::TaskReturnCode ReactiveSurvey::onTick(const ds_nav_msgs::NavState &state) {

  if (event_detected) {
    if (active_subtask == detail_survey) {
      MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_ERROR, "Event detected but already running detail survey...");
    } else {
      detail_survey->getParameters().get<ds_mx::GeoPointParam>("center_pt").set(
          ds_mx::GeoPoint(state.lon, state.lat));
      detail_survey->resetState();
      MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_UPDATE, "Setting detail survey lon/lat to " <<state.lon <<", " <<state.lat);
      switchSubtasks(state, detail_survey);
    }
    event_detected = false;
  }

  return ds_mx::TaskReturnCode::RUNNING;
}

ds_mx::TaskReturnCode ReactiveSurvey::subtaskDone(const ds_nav_msgs::NavState &state,
                                                  ds_mx::TaskReturnCode subtaskCode) {
  if (active_subtask == detail_survey) {
    // if we're doing a detail survey, go back to the main survey
    switchSubtasks(state, main_survey);
    return ds_mx::TaskReturnCode::RUNNING;
  }
  return subtaskCode;
}

bool ReactiveSurvey::validate() const {
  bool rc = true;
  if (! detail_survey->getParameters().has<ds_mx::GeoPointParam>("center_pt") ) {
    ROS_ERROR_STREAM("ReactiveSurvey Task \"" <<name <<"\" of type \"" <<type
                                              <<"\" requires that the detail_survey subtask "
                                              <<"has a center_pt parameter!");
    rc = false;
  }
  return validateSubtasks() && rc;
}

bool ReactiveSurvey::detect_detected(ds_mx_msgs::MxEvent event) {
  event_detected = true;
  MX_LOG_EVENT_TEXT(this, MX_LOG_DETECTION, event.eventid);
  return true;
}

void ReactiveSurvey::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  main_survey->getDisplay(state, display);
}

}

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::ReactiveSurvey, ds_mx::MxTask)