/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//


#include <ds_mx_stdtask/tracksfile3d.h>
#include <ds_mxcore/ds_mxeventlog.h>

#include <pluginlib/class_list_macros.h>
#include <sstream>

namespace ds_mx_stdtask {

Tracksfile3d::Tracksfile3d() :
center_pt(params, "center_pt", ds_mx::ParameterFlag::DYNAMIC),
point_list(params, "point_list", ds_mx::ParameterFlag::STATIC)
{
}

Tracksfile3d::~Tracksfile3d() = default;

bool Tracksfile3d::validate() const {
  // add a check for depth parameters in the template
  return tracklineTemplate->getParameters().has<ds_mx::DoubleParam>("start_depth")
      &&  tracklineTemplate->getParameters().has<ds_mx::DoubleParam>("end_depth")
          && TracklinePattern<ds_trackline::PatternTracksfile>::validate();
}

void Tracksfile3d::updateTemplate(const ds_trackline::Trackline &line) {
  // start with the base class
  TracklinePattern<ds_trackline::PatternTracksfile>::updateTemplate(line);

  auto &templateParams = tracklineTemplate->getParameters();

  double start_depth = 0;
  double end_depth = 0;
  if (running_line_to_start) {
    start_depth = depth_waypoints[0];
    end_depth = depth_waypoints[0];
    templateParams.get<ds_mx::DoubleParam>("start_depth").set(start_depth);
    templateParams.get<ds_mx::DoubleParam>("end_depth").set(end_depth);
  } else if (trackline_idx+1 < depth_waypoints.size()) {
    start_depth = depth_waypoints[trackline_idx];
    end_depth = depth_waypoints[trackline_idx + 1];
    templateParams.get<ds_mx::DoubleParam>("start_depth").set(start_depth);
    templateParams.get<ds_mx::DoubleParam>("end_depth").set(end_depth);
  }

  // if somehow we're past the end, don't bother updating tracklines
}

void Tracksfile3d::updatePattern() {
  ds_mx::GeoPoint center = center_pt.get();

  ds_trackline::PatternTracksfile::WaypointListT waypoints;

  std::stringstream str(point_list.get());
  while (!str.eof()) {
    double x, y, z;
    if ((str >> x) && (str >> y) && (str >> z)) {
      waypoints.push_back(ds_trackline::PatternTracksfile::WaypointT(x,y));
      depth_waypoints.push_back(z);
    }
  }

  pattern = ds_trackline::PatternTracksfile(center.x, center.y, waypoints);
  applyBaseParams(); // apply anny parameters from our base class
}


} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Tracksfile3d, ds_mx::MxTask)