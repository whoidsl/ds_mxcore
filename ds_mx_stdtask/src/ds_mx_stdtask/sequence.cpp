/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/25/19.
//

#include <ds_mx_stdtask/sequence.h>

#include <pluginlib/class_list_macros.h>

#include <ds_mxcore/ds_mxevent.h>
#include <ds_mxcore/ds_mxtask.h>
#include <ds_mxcore/ds_mxeventlog.h>

namespace ds_mx_stdtask {

Sequence::Sequence() : continue_on_failure(params, "continue_on_failure", true, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL){
  // do nothing
}

Sequence::~Sequence() = default;

void Sequence::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  const Json::Value& tasklist = config["subtasks"];
  for (int i=0; i<tasklist.size(); i++) {
    // side effect: adds tasks, in order, to subtasks
    loadSubtask(compiler, std::to_string(i), tasklist[i]);
  }

  // set the index of the subtask to run
  subtask_idx = 0;
}

void Sequence::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxComplexTask::onStart(state); // base class, ALWAYS first!

  if (subtasks.size() < 1) {
    ROS_ERROR_STREAM("Sequence Task \"" <<name <<"\" has no subtasks defined! Will error on first tick...");
    return;
  }

  // not setting subtask_idx to 0 allows this to resume
  startSubtask(state, subtasks[subtask_idx]);
}

ds_mx::TaskReturnCode Sequence::subtaskDone(const ds_nav_msgs::NavState& state,
                                    ds_mx::TaskReturnCode subtaskCode) {

  // if configured to do so, respect when subtasks fail
  if (!continue_on_failure.get() && subtaskCode == ds_mx::TaskReturnCode::FAILED) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Sequence subtask reports FAILED!");
    return ds_mx::TaskReturnCode::FAILED;
  }

  // move on to the next subtask
  subtask_idx++;

  if (subtask_idx >= subtasks.size()) {
    // we're out of subtasks
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_SUCCEED, "Sequence finished last subtask");
    return ds_mx::TaskReturnCode::SUCCESS;
  }

  switchSubtasks(state, subtasks[subtask_idx]);

  return ds_mx::TaskReturnCode::RUNNING;
}

bool Sequence::validate() const {
  if (subtasks.size() < 1) {
    ROS_ERROR_STREAM("Sequence node \"" <<name <<"\" of type \"" <<type <<"\" has no subtasks!");
    return false;
  }
  return validateSubtasks();
}

Json::Value Sequence::toJson() {
  Json::Value obj = MxTask::toJson();

  saveParameters(obj);
  events.saveJson(obj);

  // we save our parameters as a list, which is a little different from normal-- so
  // we have to do our own custom thing here
  obj["subtasks"] = Json::Value(Json::arrayValue);
  for (std::shared_ptr<MxTask>& subtask : subtasks) {
    obj["subtasks"].append(subtask->toJson());
  }

  return obj;
}

void Sequence::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  for (const std::shared_ptr<MxTask>& subtask : subtasks) {
    subtask->getDisplay(state, display);
  }
}

}

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::Sequence, ds_mx::MxTask)