/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/14/19.
//

#include "ds_mx_stdtask/signal_abort.h"
#include <pluginlib/class_list_macros.h>

namespace ds_mx_stdtask {

SignalAbort::SignalAbort() {
  abort_signaled = false;
}

SignalAbort::~SignalAbort() = default;

void SignalAbort::init_ros(ros::NodeHandle &nh) {
  ds_mx::MxComplexTask::init_ros(nh);

  abort_srv_ = nh.serviceClient<ds_hotel_msgs::AbortCmd>("abort_cmd");
  // 2020-03-31 SS - the timeout to wait for service was set at 30 seconds using
  //                 ros::Duration. However when running
  //                 in fastsim that time is not enough for the abort node to start
  //                 and advertise the service, so MX was always crashing here in
  //                 fastsim. Increased this to ros::Duration(300) gave enough
  //                 time for the nodes to spin up in fastsim. This is not a viable
  //                 solution for realtime though. This timeout should really
  //                 be a wall clock and not a ros clock to deal with this issue.
  //                 Unfortunately waitForExistence does not accept a ros::WallDuration
  //                 as an argument
  //if (!abort_srv_.waitForExistence(ros::Duration(30))) {
  if (!abort_srv_.waitForExistence(ros::Duration(300))) {
    ROS_FATAL_STREAM("Unable to connect to abort service! STOPPING");
    ROS_BREAK();
  };
}

void SignalAbort::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxPayloadTask::onStart(state);
  abort_signaled = false;

  ds_hotel_msgs::AbortCmd msg;
  msg.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ABORT;
  if (!abort_srv_.call(msg)) {
    ROS_FATAL_STREAM("Unable to call abort service!  We need some retry semantics here...");
  } else {
    abort_signaled = true;
  }
}

void SignalAbort::onStop(const ds_nav_msgs::NavState& state) {
  // just use the standard implementation
  ds_mx::MxComplexTask::onStop(state);
}

ds_mx::TaskReturnCode SignalAbort::onTick(const ds_nav_msgs::NavState &state) {
  if (!abort_signaled) {
    ds_hotel_msgs::AbortCmd msg;
    msg.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ABORT;
    if (!abort_srv_.call(msg)) {
      ROS_FATAL_STREAM("Unable to call abort service!  We need some retry semantics here...");
    } else {
      abort_signaled = true;
    }
  }

  return ds_mx::TaskReturnCode::RUNNING;
}

bool SignalAbort::validate() const {
  return validateSubtasks();
}

} // namespace ds_mx_stdtask

PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::SignalAbort, ds_mx::MxTask)
