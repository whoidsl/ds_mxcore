/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/8/19.
//

#include "ds_mx_stdtask/std_payload.h"

namespace ds_mx_stdtask {

StdPayload::StdPayload() : ds_mx::MxPayloadTask(),
payload_params(params, "payload_params", ds_mx::ParameterFlag::DYNAMIC ),
config_only(params, "config_only", false, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL )
{
}

StdPayload::~StdPayload() = default;

void StdPayload::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPayloadTask::init(config, compiler);

  // link our child's parameters so that parameters like center point, etc,
  // can propagate down.
  params->linkCollection(subtask->getParametersPtr());
}

void StdPayload::onStart(const ds_nav_msgs::NavState& state) {
  ds_mx::MxPayloadTask::onStart(state);

  ds_mx_msgs::StdPayloadCommand msg;
  msg.stamp = state.header.stamp;
  if (config_only.get()) {
    msg.command = ds_mx_msgs::StdPayloadCommand::COMMAND_CONFIGONLY;
    if (payload_params.get().dict.size() == 0) {
      return;
    }
  } else {
    msg.command = ds_mx_msgs::StdPayloadCommand::COMMAND_START;
  }
  fillInParams(msg, payload_params);

  cmd_pub.publish(msg);
}

void StdPayload::onStop(const ds_nav_msgs::NavState& state) {
  if (!config_only.get()) {
    ds_mx_msgs::StdPayloadCommand msg;
    msg.stamp = state.header.stamp;
    msg.command = ds_mx_msgs::StdPayloadCommand::COMMAND_STOP;

    cmd_pub.publish(msg);
  }

  ds_mx::MxComplexTask::onStop(state); // payload task doesn't define this, so base class
}

void StdPayload::parameterChanged(const ds_nav_msgs::NavState &state) {
  if (isRunning()) {
    ds_mx_msgs::StdPayloadCommand msg;
    msg.stamp = state.header.stamp;
    msg.command = ds_mx_msgs::StdPayloadCommand::COMMAND_CONFIGONLY;
    fillInParams(msg, payload_params);

    cmd_pub.publish(msg);
  }
}

void StdPayload::fillInParams(ds_mx_msgs::StdPayloadCommand& msg, const ds_mx::KeyValueListParam& p) {
  msg.config.clear();
  for (const auto & param : p.get().dict) {
    ds_core_msgs::KeyString v;
    v.key = param.first;
    v.value = param.second;
    msg.config.push_back(v);
  }
}

} // namespace ds_mx_stdtask
