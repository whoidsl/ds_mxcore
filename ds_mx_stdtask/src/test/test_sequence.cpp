/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/8/19.
//

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mx_stdtask/sequence.h>


namespace ds_mx_stdtask {
namespace test {

class SequenceTest : public ::testing::Test {

 protected:

  static const char* JSON_STR;

  void SetUp() override {

    // prepare our subtask
    subtask1 = std::make_shared<ds_mx::MxMockTask>();
    subtask2 = std::make_shared<ds_mx::MxMockTask>();
    subtask3 = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask1", subtask1);
    compiler->addSubtask("subtask2", subtask2);
    compiler->addSubtask("subtask3", subtask3);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<ds_mx_stdtask::Sequence>();
    underTest->init(config, compiler);
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask1;
  std::shared_ptr<ds_mx::MxMockTask> subtask2;
  std::shared_ptr<ds_mx::MxMockTask> subtask3;
  std::shared_ptr<ds_mx_stdtask::Sequence> underTest;
  ds_nav_msgs::NavState state;
};

const char* SequenceTest::JSON_STR = R"( {
  "type": "sequence",
  "subtasks":
  [
    "subtask1",
    "subtask2",
    "subtask3"
  ],
})";

TEST_F(SequenceTest, VerifyOnInit) {
    EXPECT_EQ(1, subtask1->initCalls);
    EXPECT_EQ(1, subtask2->initCalls);
    EXPECT_EQ(1, subtask3->initCalls);
}

TEST_F(SequenceTest, VerifyOnStart) {
    underTest->onStart(state);
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(0, subtask1->ticks);
    EXPECT_EQ(0, subtask2->ticks);
    EXPECT_EQ(0, subtask3->ticks);

    EXPECT_EQ(0, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);
}

TEST_F(SequenceTest, VerifyTick) {
    subtask1->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->ticks);
    EXPECT_EQ(0, subtask2->ticks);
    EXPECT_EQ(0, subtask3->ticks);

    EXPECT_EQ(0, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);
}

TEST_F(SequenceTest, VerifyOnStop) {
    underTest->onStart(state);
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(0, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    underTest->onStop(state);

    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    EXPECT_EQ(0, subtask1->ticks);
    EXPECT_EQ(0, subtask2->ticks);
    EXPECT_EQ(0, subtask3->ticks);
}

TEST_F(SequenceTest, SubtaskStops) {
    subtask1->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
    subtask1->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    subtask2->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
    subtask2->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    underTest->onStart(state);
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    // first, tick the mission twice
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(0, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    // second tick should cause the abort node to tick over to the "abort" subtask
    // but the abort_task ITSELF is still running
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    // Run two more ticks
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    // Second tick should case the abort subtask to exit and
    // start the "trap" subtask
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(1, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(1, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    // Finally, some cleanup
    EXPECT_EQ(2, subtask1->ticks);
    EXPECT_EQ(2, subtask2->ticks);
}

TEST_F(SequenceTest, AllTasksReturn) {
    // first, put the test case in the trap state by having each subtask immediately return
    subtask1->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);
    subtask2->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);
    subtask3->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    underTest->onStart(state);
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    // first tick: transition from subtask1 to subtask2
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(0, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(0, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    // second tick: transition from subtask2 to subtask3
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(1, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(1, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls);

    // third tick: subtask3 finishes, return code generated
    EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

    // but Trap Subtask will have been restarted
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(1, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->ticks);
    EXPECT_EQ(1, subtask2->ticks);
    EXPECT_EQ(1, subtask3->ticks);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(1, subtask2->onStopCalls);
    EXPECT_EQ(0, subtask3->onStopCalls); // not called until onstop...

    underTest->onStop(state);

    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(1, subtask2->onStartCalls);
    EXPECT_EQ(1, subtask3->onStartCalls);

    EXPECT_EQ(1, subtask1->ticks);
    EXPECT_EQ(1, subtask2->ticks);
    EXPECT_EQ(1, subtask3->ticks);

    EXPECT_EQ(1, subtask1->onStopCalls);
    EXPECT_EQ(1, subtask2->onStopCalls);
    EXPECT_EQ(1, subtask3->onStopCalls);
}

TEST_F(SequenceTest, ContinueOnFailTest) {
  // first, put the test case in the trap state by having each subtask immediately return
  subtask1->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);
  subtask2->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);
  subtask3->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);

  underTest->onStart(state);
  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(0, subtask2->onStartCalls);
  EXPECT_EQ(0, subtask3->onStartCalls);

  // first tick: transition from subtask1 to subtask2
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(1, subtask2->onStartCalls);
  EXPECT_EQ(0, subtask3->onStartCalls);

  EXPECT_EQ(1, subtask1->onStopCalls);
  EXPECT_EQ(0, subtask2->onStopCalls);
  EXPECT_EQ(0, subtask3->onStopCalls);

  // second tick: transition from subtask2 to subtask3
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(1, subtask2->onStartCalls);
  EXPECT_EQ(1, subtask3->onStartCalls);

  EXPECT_EQ(1, subtask1->onStopCalls);
  EXPECT_EQ(1, subtask2->onStopCalls);
  EXPECT_EQ(0, subtask3->onStopCalls);

  // third tick: subtask3 finishes, return code generated
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // but Trap Subtask will have been restarted
  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(1, subtask2->onStartCalls);
  EXPECT_EQ(1, subtask3->onStartCalls);

  EXPECT_EQ(1, subtask1->ticks);
  EXPECT_EQ(1, subtask2->ticks);
  EXPECT_EQ(1, subtask3->ticks);

  EXPECT_EQ(1, subtask1->onStopCalls);
  EXPECT_EQ(1, subtask2->onStopCalls);
  EXPECT_EQ(0, subtask3->onStopCalls); // not called until onstop...

  underTest->onStop(state);

  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(1, subtask2->onStartCalls);
  EXPECT_EQ(1, subtask3->onStartCalls);

  EXPECT_EQ(1, subtask1->ticks);
  EXPECT_EQ(1, subtask2->ticks);
  EXPECT_EQ(1, subtask3->ticks);

  EXPECT_EQ(1, subtask1->onStopCalls);
  EXPECT_EQ(1, subtask2->onStopCalls);
  EXPECT_EQ(1, subtask3->onStopCalls);
}

TEST_F(SequenceTest, DontContinueOnFailTest) {

  underTest->getParameters().get<ds_mx::BoolParam>("continue_on_failure").setFromString("false");

  // first, put the test case in the trap state by having each subtask immediately return
  subtask1->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);
  subtask2->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);
  subtask3->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);

  underTest->onStart(state);
  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(0, subtask2->onStartCalls);
  EXPECT_EQ(0, subtask3->onStartCalls);

  // first tick: transition from subtask1 to subtask2
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(0, subtask2->onStartCalls);
  EXPECT_EQ(0, subtask3->onStartCalls);

  EXPECT_EQ(0, subtask1->onStopCalls);  // doesn't increase until onStop
  EXPECT_EQ(0, subtask2->onStopCalls);
  EXPECT_EQ(0, subtask3->onStopCalls);

  EXPECT_EQ(1, subtask1->ticks);
  EXPECT_EQ(0, subtask2->ticks);
  EXPECT_EQ(0, subtask3->ticks);

  underTest->onStop(state);

  EXPECT_EQ(1, subtask1->onStartCalls);
  EXPECT_EQ(0, subtask2->onStartCalls);
  EXPECT_EQ(0, subtask3->onStartCalls);

  EXPECT_EQ(1, subtask1->ticks);
  EXPECT_EQ(0, subtask2->ticks);
  EXPECT_EQ(0, subtask3->ticks);

  EXPECT_EQ(1, subtask1->onStopCalls);
  EXPECT_EQ(0, subtask2->onStopCalls);
  EXPECT_EQ(0, subtask3->onStopCalls);
}

TEST_F(SequenceTest, displayTest) {
    boost::uuids::uuid uuid_res_1, uuid_res_2, uuid_res_3;

    // Setup the subtask's displays
    ds_mx_msgs::MissionElementDisplay displayElements;
    displayElements.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
    displayElements.label = "label 1";
    displayElements.wellknowntext = "WKT (1)";
    subtask1->displayElement = displayElements;

    displayElements.label = "label 2";
    displayElements.wellknowntext = "WKT (2)";
    subtask2->displayElement = displayElements;

    displayElements.label = "label 3";
    displayElements.wellknowntext = "WKT (3)";
    subtask3->displayElement = displayElements;

    // Actually get the display
    ds_mx_msgs::MissionDisplay result;
    underTest->getDisplay(state, result);

    ASSERT_EQ(3, result.elements.size());

    // Order should be:
    // 1. Element from subtask 1
    size_t idx=0;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_1.begin());
    EXPECT_EQ(subtask1->getUuid(), uuid_res_1);
    EXPECT_EQ("WKT (1)", result.elements[idx].wellknowntext);
    EXPECT_EQ("label 1", result.elements[idx].label);

    // 2. Element from subtask 2
    idx++;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_2.begin());
    EXPECT_EQ(subtask2->getUuid(), uuid_res_2);
    EXPECT_EQ("WKT (2)", result.elements[idx].wellknowntext);
    EXPECT_EQ("label 2", result.elements[idx].label);

    // 3. Element from subtask 3
    idx++;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_3.begin());
    EXPECT_EQ(subtask3->getUuid(), uuid_res_3);
    EXPECT_EQ("WKT (3)", result.elements[idx].wellknowntext);
    EXPECT_EQ("label 3", result.elements[idx].label);

    // Finally, let's make sure we're actually setting UUIDs correctly
    EXPECT_NE(uuid_res_1, uuid_res_2);
    EXPECT_NE(uuid_res_1, uuid_res_3);
    EXPECT_NE(uuid_res_2, uuid_res_3);
}

TEST_F(SequenceTest, ValidateSuccessTest) {
    subtask1->validateReturn = true;
    subtask2->validateReturn = true;
    subtask3->validateReturn = true;

    EXPECT_TRUE(underTest->validate());
}

TEST_F(SequenceTest, ValidateFailTest) {
    subtask1->validateReturn = false;
    subtask2->validateReturn = true;
    subtask3->validateReturn = true;
    EXPECT_FALSE(underTest->validate());

    subtask1->validateReturn = true;
    subtask2->validateReturn = false;
    subtask3->validateReturn = true;
    EXPECT_FALSE(underTest->validate());

    subtask1->validateReturn = true;
    subtask2->validateReturn = true;
    subtask3->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(SequenceTest, TimeoutTest) {
    subtask1->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    // set a overall-task-max timeout
    ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
    max_timeout.set(5.0);

    // start the task.  This *should* set the timeout
    underTest->onStart(state);
    EXPECT_EQ(1, subtask1->onStartCalls);
    EXPECT_EQ(0, subtask1->ticks);
    EXPECT_EQ(0, subtask1->onStopCalls);

    EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

    // advance time 5 seconds, and amke sure timeout actually fires
    state.header.stamp += ros::Duration(5.001);
    EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

} // namespace test
} // namespace ds_mx_stdtask