/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/14/19.
//


#include <gtest/gtest.h>

#include <ds_mxutils/ds_test_message_queue.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mx_stdtask/signal_abort.h>
#include <ds_hotel_msgs/AbortCmd.h>

namespace ds_mx_stdtask {
namespace test {

class SignalAbortRostest : public ::testing::Test {

 protected:
  static const char* JSON_STR;

  SignalAbortRostest() {
    node_handle.reset(new ros::NodeHandle());
    abort_srv_ = node_handle->advertiseService<ds_hotel_msgs::AbortCmd::Request, ds_hotel_msgs::AbortCmd::Response>(
        "abort_cmd",
        boost::bind(&SignalAbortRostest::handle_abort_cmd, this, _1, _2));
  }

  bool handle_abort_cmd(ds_hotel_msgs::AbortCmd::Request req, ds_hotel_msgs::AbortCmd::Response resp) {
    if (req.command == ds_hotel_msgs::AbortCmdRequest::ABORT_CMD_ABORT) {
      std::unique_lock<std::mutex>(_mutex);
      abort_reqs++;
      if (abort_reqs == 3) {
        return false;
      }
    }
    return true;
  }



  void SetUp() override {
    // prepare our subtask
    subtask = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask", subtask);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest.reset(new ds_mx_stdtask::SignalAbort());
    underTest->init(config, compiler);
    underTest->init_ros(*node_handle); // run the ROS setup, since this is the
    // rostest edition!

    // fill in our test state with some basic values
    state.header.stamp = ros::Time::now();
    state.lat = 1;
    state.lon = 2;
    state.down = 1000;
    state.heading = 0.5;
    state.roll = 0.01;
    state.pitch = 0.02;

    abort_reqs = 0;
  }

 protected:
  std::shared_ptr<ds_mx::MxMockTask> subtask;
  std::shared_ptr<ds_mx_stdtask::SignalAbort> underTest;
  ds_nav_msgs::NavState state;
  int abort_reqs;
  std::mutex _mutex;
  ros::ServiceServer abort_srv_;

 private:
  std::unique_ptr<ros::NodeHandle> node_handle;
};

const char* SignalAbortRostest::JSON_STR = R"( {
  "type": "signal_abort",
  "subtask": "subtask",
})";

TEST_F(SignalAbortRostest, TestStartStop) {
  EXPECT_EQ(0, abort_reqs);

  // start the task
  underTest->onStart(state);

  // get the next message
  ros::Duration(0.10).sleep();
  {
    std::unique_lock<std::mutex>(_mutex);
    EXPECT_EQ(1, abort_reqs);
  }

  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  // tick (done)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // call on-stop
  state.header.stamp += ros::Duration(10);
  underTest->onStop(state);

  // check to make sure the subtask was asked to do the right stuff
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(SignalAbortRostest, TestRetry) {
  {
    std::unique_lock<std::mutex>(_mutex);
    abort_reqs = 2; // special value
  }
  // start the task
  underTest->onStart(state);

  // get the next message
  ros::Duration(0.10).sleep();
  {
    std::unique_lock<std::mutex>(_mutex);
    EXPECT_EQ(3, abort_reqs);
    // this FAILED, so we should retry on tick
  }

  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  ros::Duration(0.10).sleep();

  {
    std::unique_lock<std::mutex>(_mutex);
    EXPECT_EQ(4, abort_reqs);
  }

  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  ros::Duration(0.10).sleep();

  {
    std::unique_lock<std::mutex>(_mutex);
    EXPECT_EQ(4, abort_reqs);
  }

  // tick (done)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // call on-stop
  state.header.stamp += ros::Duration(10);
  underTest->onStop(state);

  // check to make sure the subtask was asked to do the right stuff
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(SignalAbortRostest, ValidateGood) {
  subtask->validateReturn = true;
  EXPECT_TRUE(underTest->validate());
}

TEST_F(SignalAbortRostest, ValidateBad) {
  subtask->validateReturn = false;
  EXPECT_FALSE(underTest->validate());
}

} // namespace test
} // namespace ds_mx_stdtask

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_signal_abort");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(4); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}
