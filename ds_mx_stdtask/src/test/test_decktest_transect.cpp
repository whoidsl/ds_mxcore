/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/13/19.
//

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/decktest_transect.h>


namespace ds_mx_stdtask {
namespace test {

class DecktestTransectTest : public ::testing::Test {

 protected:

  static const char *JSON_STR;

  void SetUp() override {

      // prepare our subtask
      subtask_trackline = std::make_shared<ds_mx::MxMockTask>();

      // prepare our compiler
      std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
      compiler->addSubtask("subtask_trackline", subtask_trackline);

      // read the config
      Json::Value config;
      Json::Reader reader;
      reader.parse(JSON_STR, config);

      // setup our test
      underTest = std::make_shared<ds_mx_stdtask::DecktestTransect>();
      underTest->init(config, compiler);

      // initialize state
      state.lon = 1.00001;
      state.lat = 2.00001;
  }

  // setup any parameters required of our subtasks
  void SetUpParameters() {
      trackline_start.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                     "start_pt", ds_mx::DYNAMIC));
      trackline_end.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                   "end_pt", ds_mx::DYNAMIC));
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_trackline;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_start;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_end;
  std::shared_ptr<ds_mx_stdtask::DecktestTransect> underTest;
  ds_nav_msgs::NavState state;
  static std::vector<ds_mx::GeoPoint> expected_waypoints;
};

// verified in Matlab as correct
std::vector<ds_mx::GeoPoint> DecktestTransectTest::expected_waypoints = {
    ds_mx::GeoPoint(1.0000100000000, 2.000010000000), // initial state
    ds_mx::GeoPoint(1.0000000000000, 1.999990000000), // start point
    ds_mx::GeoPoint(1.0000000000000, 2.000000000000), // end point
};


const char* DecktestTransectTest::JSON_STR = R"( {
  "type": "decktest_transect",
  "heading_offset": 0,
  "line_length": 1000,
  "trackline": "subtask_trackline"
})";

TEST_F(DecktestTransectTest, VerifyOnInit) {
    EXPECT_EQ(1, subtask_trackline->initCalls);
}

TEST_F(DecktestTransectTest, VerifyOnStart) {
    SetUpParameters();
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(DecktestTransectTest, VerifyTick) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(DecktestTransectTest, SubtaskSuceeeds) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(DecktestTransectTest, SubtaskFails) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    // a trackline finishes causes the trackline to be re-started with new
    // start/end points
    EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(DecktestTransectTest, VerifyOnStop) {
    SetUpParameters();
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    underTest->onStop(state);

    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(DecktestTransectTest, displayTest) {
  boost::uuids::uuid uuid_res;

  // Setup the subtask's displays
  ds_mx_msgs::MissionElementDisplay displayElements;
  displayElements.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
  displayElements.label = "task label";
  displayElements.wellknowntext = "WKT_subtask";
  subtask_trackline->displayElement = displayElements;

  // Actually get the display
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  // The result should be simply the subtask's stuff
  size_t idx=0;
  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
  std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
  EXPECT_EQ(subtask_trackline->getUuid(), uuid_res);
  EXPECT_EQ("WKT_subtask", result.elements[idx].wellknowntext);
  EXPECT_EQ("task label", result.elements[idx].label);
}

TEST_F(DecktestTransectTest, ValidateMissingParamTest) {
  subtask_trackline->validateReturn = true;

  EXPECT_FALSE(underTest->validate());
}

TEST_F(DecktestTransectTest, ValidateSuccessTest) {
  SetUpParameters();
  subtask_trackline->validateReturn = true;

  EXPECT_TRUE(underTest->validate());
}

TEST_F(DecktestTransectTest, ValidateFailTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(DecktestTransectTest, PatternDirectionTest) {
  SetUpParameters();
  // we have a 1km trackline.  2.0e-7 is
  // ~2cm, depending on latitude
  // Note that error seems to be driven by latitude comparisons, which
  // makes a certain amount of sense
  const double TOL=2.0e-7;

  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;


  // start
  underTest->onStart(state);

  // ok, we're going to step our way around the compass on cardinal directions,
  // checking that the start point matches our location and that the end point
  // is in the right direction-- all while moving start locations

  // NORTH!
  state.lat = 12.1;
  state.lon = -34.5;
  state.heading = 0;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_NEAR(state.lon, trackline_end->get().x, TOL);
  EXPECT_LT(state.lat, trackline_end->get().y);

  // EAST!
  state.lat = 12.2;
  state.lon = -34.6;
  state.heading = 90;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_LT(state.lon, trackline_end->get().x);
  EXPECT_NEAR(state.lat, trackline_end->get().y, TOL);

  // SOUTH!
  state.lat = 12.3;
  state.lon = -34.7;
  state.heading = 180;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_NEAR(state.lon, trackline_end->get().x, TOL);
  EXPECT_GT(state.lat, trackline_end->get().y);

  // WEST!
  state.lat = 12.4;
  state.lon = -34.8;
  state.heading = 270;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_GT(state.lon, trackline_end->get().x);
  EXPECT_NEAR(state.lat, trackline_end->get().y, TOL);
}

TEST_F(DecktestTransectTest, PatternOffsetDirectionTest) {
  SetUpParameters();
  const double TOL=2.0e-7;

  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->getParameters().get<ds_mx::DoubleParam>("heading_offset").setFromString("45.0");

  // start
  underTest->onStart(state);

  // ok, we're going to step our way around the compass on cardinal directions,
  // checking that the start point matches our location and that the end point
  // is in the right direction-- all while moving start locations

  // NORTH!
  state.lat = 12.1;
  state.lon = -34.5;
  state.heading = 315;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_NEAR(state.lon, trackline_end->get().x, TOL);
  EXPECT_LT(state.lat, trackline_end->get().y);

  // EAST!
  state.lat = 12.2;
  state.lon = -34.6;
  state.heading = 45;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_LT(state.lon, trackline_end->get().x);
  EXPECT_NEAR(state.lat, trackline_end->get().y, TOL);

  // SOUTH!
  state.lat = 12.3;
  state.lon = -34.7;
  state.heading = 135;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_NEAR(state.lon, trackline_end->get().x, TOL);
  EXPECT_GT(state.lat, trackline_end->get().y);

  // WEST!
  state.lat = 12.4;
  state.lon = -34.8;
  state.heading = 225;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_GT(state.lon, trackline_end->get().x);
  EXPECT_NEAR(state.lat, trackline_end->get().y, TOL);
}

TEST_F(DecktestTransectTest, PatternOffsetCrosstrackTest) {
  SetUpParameters();
  const double TOL = 2.0e-7;

  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->getParameters().get<ds_mx::DoubleParam>("crosstrack_offset").setFromString("45.0");

  underTest->onStart(state);

  // NORTH!
  state.lat = 12.1;
  state.lon = -34.5;
  state.heading = 0;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_LT(state.lon, trackline_start->get().x);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_LT(state.lon, trackline_end->get().x);
  EXPECT_LT(state.lat, trackline_end->get().y);
  EXPECT_NEAR(trackline_start->get().x, trackline_end->get().x, TOL);

  // SOUTH!
  state.lat = 12.1;
  state.lon = -34.5;
  state.heading = 180;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_GT(state.lon, trackline_start->get().x);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);
  EXPECT_GT(state.lon, trackline_end->get().x);
  EXPECT_GT(state.lat, trackline_end->get().y);
  EXPECT_NEAR(trackline_start->get().x, trackline_end->get().x, TOL);

  // start
  underTest->onStart(state);
}

TEST_F(DecktestTransectTest, TimeoutTest) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    // set a overall-task-max timeout
    ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
    max_timeout.set(5.0);

    // start the task.  This *should* set the timeout
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

    // advance time 5 seconds, and amke sure timeout actually fires
    state.header.stamp += ros::Duration(5.001);
    EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
}

} // namespace test
} // namespace ds_mx_stdtask