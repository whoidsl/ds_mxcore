/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/trackline_pattern.h>

namespace ds_mx_stdtask {
namespace test {

class MockPattern : public ds_trackline::AbstractPattern {
 public:
  MockPattern() = default;
  virtual ~MockPattern() = default;


  std::vector<ds_trackline::Trackline>& lines() {
      return tracklines;
  }

  const std::vector<ds_trackline::Trackline>& lines() const {
      return tracklines;
  }

  void setProj(ds_mx::GeoPoint origin) {
      proj = ds_trackline::Projection(origin.x, origin.y);
  }

 protected:
  // required, but do nothing
  void generateLines() override {
    // pattern is just a pre-canned box
    tracklines.clear();
    tracklines.push_back(ds_trackline::Trackline(0.0, 0.0, 0.1, 0.0));
    tracklines.push_back(ds_trackline::Trackline(0.1, 0.0, 0.1, 0.1));
    tracklines.push_back(ds_trackline::Trackline(0.1, 0.1, 0.0, 0.1));

    //ROS_INFO_STREAM("Mock Tracklines Applying shift " <<shift_east <<", " <<shift_north);
    ds_trackline::Trackline::VectorEN shift;
    shift <<shift_east, shift_north;

    for (size_t i=0; i<tracklines.size(); i++) {
      // recover original trackline
      auto start_en = tracklines[i].getStartEN();
      auto end_en = tracklines[i].getEndEN();

      // apply shfit
      start_en += shift;
      end_en += shift;

      // build a new trackline with the shift applied
      tracklines[i] = ds_trackline::Trackline(start_en, end_en, tracklines[i].getProjection());
    }
  }
};

// In order to test TracklinePattern we have to subclass it
class MockPatternTask : public TracklinePattern<MockPattern> {
 public:
  MockPatternTask() {
    updatePattern();
    resetState();
  }

  const MockPattern& getPattern() const { return pattern; }
  MockPattern& getPattern() { return pattern; }

 protected:
  void updatePattern() override {
    // do nothing, just apply the base
    applyBaseParams();
  }

};

class TracklinePatternTest : public ::testing::Test {

 protected:

  static const char *JSON_STR;

  void SetUp() override {

      // prepare our subtask
      subtask_trackline = std::make_shared<ds_mx::MxMockTask>();

      // prepare our compiler
      std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
      compiler->addSubtask("subtask_trackline", subtask_trackline);

      // read the config
      Json::Value config;
      Json::Reader reader;
      reader.parse(JSON_STR, config);

      // setup our test
      underTest = std::make_shared<MockPatternTask>();
      underTest->init(config, compiler);

      state.lon = 0.01;
      state.lat = 0.01;
  }

  // setup any parameters required of our subtasks
  void SetUpParameters() {
      trackline_start.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                     "start_pt", ds_mx::DYNAMIC));
      trackline_end.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                   "end_pt", ds_mx::DYNAMIC));
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_trackline;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_start;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_end;
  std::shared_ptr<MockPatternTask> underTest;
  ds_nav_msgs::NavState state;
  static std::vector<ds_mx::GeoPoint> expected_waypoints;
};

std::vector<ds_mx::GeoPoint> TracklinePatternTest::expected_waypoints = {
    ds_mx::GeoPoint(0.01, 0.01),
    ds_mx::GeoPoint(0.0, 0.0),
    ds_mx::GeoPoint(0.1, 0.0),
    ds_mx::GeoPoint(0.1, 0.1),
    ds_mx::GeoPoint(0.0, 0.1),
};

const char* TracklinePatternTest::JSON_STR = R"( {
  "type": "mock_pattern",
  "backup_dist": 15,
  "trackline": "subtask_trackline"
})";

TEST_F(TracklinePatternTest, VerifyOnInit) {
    EXPECT_EQ(1, subtask_trackline->initCalls);
}

TEST_F(TracklinePatternTest, VerifyOnStart) {
    SetUpParameters();
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(TracklinePatternTest, VerifyTick) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(TracklinePatternTest, SubtaskSuceeeds) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    // a trackline finishes causes the trackline to be re-started with new
    // start/end points
    EXPECT_EQ(2, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(TracklinePatternTest, SubtaskFails) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    // a trackline finishes causes the trackline to be re-started with new
    // start/end points
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(2, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(TracklinePatternTest, VerifyOnStop) {
    SetUpParameters();
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    underTest->onStop(state);

    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(TracklinePatternTest, displayTest) {
    SetUpParameters();
    boost::uuids::uuid uuid_res;

    // Actually get the display
    state.lon = 0.0001;
    state.lat = 0.0002;
    ds_mx::GeoPoint current_pos(state.lon, state.lat);
    ds_mx_msgs::MissionDisplay result;
    underTest->getDisplay(state, result);

    const std::string LINESTRING  = "LINESTRING";

    // Got through the list of elements
    for (size_t idx=0; idx<result.elements.size(); idx++) {
      if (idx == 0) {
          EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[idx].role);
      } else {
          EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, result.elements[idx].role);
      }
      std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
      EXPECT_EQ(underTest->getUuid(), uuid_res);
      // just make sure everything is a linestring
      EXPECT_EQ(0, result.elements[idx].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));
    }
}

TEST_F(TracklinePatternTest, ValidateMissingParamTest) {
    subtask_trackline->validateReturn = true;

    EXPECT_FALSE(underTest->validate());
}

TEST_F(TracklinePatternTest, ValidateSuccessTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = true;
    EXPECT_TRUE(underTest->validate());
}

TEST_F(TracklinePatternTest, ValidateFailTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(TracklinePatternTest, PatternGenerationTest) {
    SetUpParameters();
    const double TOL=1.0e-9;
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    for (size_t i=1; i<expected_waypoints.size()-1; i++) {
        // tracklines should be from i to i+1
        ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
        EXPECT_EQ(i+1, subtask_trackline->onStartCalls);
        EXPECT_EQ(i, subtask_trackline->ticks);
        EXPECT_EQ(i, subtask_trackline->onStopCalls);

        EXPECT_NEAR(expected_waypoints[i].x, trackline_start->get().x, TOL);
        EXPECT_NEAR(expected_waypoints[i].y, trackline_start->get().y, TOL);
        EXPECT_NEAR(expected_waypoints[i+1].x, trackline_end->get().x, TOL);
        EXPECT_NEAR(expected_waypoints[i+1].y, trackline_end->get().y, TOL);
    }
    EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
}

TEST_F(TracklinePatternTest, TimeoutTest) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    // set a overall-task-max timeout
    ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
    max_timeout.set(5.0);

    // start the task.  This *should* set the timeout
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

    // advance time 5 seconds, and amke sure timeout actually fires
    state.header.stamp += ros::Duration(5.001);
    EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

const double MDEG_LON = 111320.700;
const double STOP_LON = 0.05;
const double STOP_LAT = 0.20; // don't stop ON the pattern!
const double STOP_TOL = 1.0e-7; // approx 1cm


TEST_F(TracklinePatternTest, RestartTest) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

    const double BACKUP_DIST = underTest->getParameters().get<ds_mx::DoubleParam>("backup_dist").get();

    // we're going from higher lon to lower lon, so backup up means ADDING
    const double RESUME_LON = STOP_LON + BACKUP_DIST / MDEG_LON;
    const double RESUME_LAT = expected_waypoints[3].y;

    underTest->onStart(state);
    // ship the first few lines
    ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // connecting line
    ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // line 1
    ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // line 2

    // ok, now its on the first actual trackline.  Set a state somewhere on there
    state.lon = STOP_LON;
    state.lat = STOP_LAT;

    // stop this task
    underTest->onStop(state);

    // check that its supposed to resume back to the starting point
    ds_mx::GeoPoint resumePt = underTest->getOnstartPt();

    EXPECT_NEAR(RESUME_LON, resumePt.x, STOP_TOL);
    EXPECT_NEAR(RESUME_LAT, resumePt.y, STOP_TOL);

    // resume, and check that the resulting trackline points to the right place
    state.lon = -0.01;
    state.lat = -0.01;
    underTest->onStart(state);

    EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
    EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

    EXPECT_NEAR(RESUME_LON, trackline_end->get().x, STOP_TOL);
    EXPECT_NEAR(RESUME_LAT, trackline_end->get().y, STOP_TOL);
}

TEST_F(TracklinePatternTest, MoveBeforeTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

  underTest->getParameters().get<ds_mx::DoubleParam>("global_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  underTest->onStart(state);

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

  EXPECT_NEAR(0.00089831391890035639, trackline_end->get().x, STOP_TOL);
  EXPECT_NEAR(9.0436811513625043e-05, trackline_end->get().y, STOP_TOL);
}

TEST_F(TracklinePatternTest, MoveDuringConnectorTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->onStart(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  underTest->getParameters().get<ds_mx::DoubleParam>("global_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

  // originally, the trackline started at 0/0.  So find the difference between the ORIGINAL
  // trackline start and the current start.
  ds_trackline::Projection::VectorLL orig_start_ll, current_start_ll;
  orig_start_ll << 0,0;
  current_start_ll <<trackline_end->get().x, trackline_end->get().y;

  auto orig_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(orig_start_ll);
  auto current_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(current_start_ll);
  auto change = current_start_en - orig_start_en;

  EXPECT_NEAR(change(0), 100, 0.000001);
  EXPECT_NEAR(change(1), 10, 0.000001);
}

TEST_F(TracklinePatternTest, MoveOnLineTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

  underTest->onStart(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // connector
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // first trackline
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->getParameters().get<ds_mx::DoubleParam>("global_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);


  // This one is tricky, since the resume point isn't a fixed offset from even the current state.
  // instead, let's just check against a pre-canned position
  EXPECT_NEAR(0.10089831529791997, trackline_end->get().x, 0.000001);
  EXPECT_NEAR(0.01009044936659193, trackline_end->get().y, 0.000001);

  // Run ONE more trackline, and confirm the shifts
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  ds_trackline::Projection::VectorLL orig_start_ll, current_start_ll;
  ds_trackline::Projection::VectorLL orig_end_ll, current_end_ll;
  orig_start_ll <<0.1, 0.0;
  current_start_ll <<trackline_start->get().x, trackline_start->get().y; // we're on a real line, so start is actually start
  orig_end_ll <<0.1, 0.1;
  current_end_ll <<trackline_end->get().x, trackline_end->get().y;
  auto orig_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(orig_start_ll);
  auto current_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(current_start_ll);
  auto orig_end_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(orig_end_ll);
  auto current_end_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(current_end_ll);

  auto change_start = current_start_en - orig_start_en;

  EXPECT_NEAR(change_start(0), 100, 0.00000001);
  EXPECT_NEAR(change_start(1), 10, 0.00000001);

  auto change_end = current_end_en - orig_end_en;

  EXPECT_NEAR(change_end(0), 100, 0.00000001);
  EXPECT_NEAR(change_end(1), 10, 0.00000001);
}

TEST_F(TracklinePatternTest, MoveOnLineTooLongTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

  state.lat = -0.01; // we need to be off the end of this line
  underTest->onStart(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // connector
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // first trackline
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->getParameters().get<ds_mx::DoubleParam>("global_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

  // originally, the trackline started at 0/0.  So find the difference between the ORIGINAL
  // trackline start and the current start.
  ds_trackline::Projection::VectorLL orig_start_ll, current_start_ll;
  orig_start_ll << 0.1, 0;
  current_start_ll <<trackline_end->get().x, trackline_end->get().y; // we're on the connector

  std::cout <<orig_start_ll.transpose() <<" -> " <<current_start_ll.transpose() <<std::endl;
  auto orig_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(orig_start_ll);
  auto current_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(current_start_ll);
  std::cout <<orig_start_en.transpose() <<" -> " <<current_start_en.transpose() <<std::endl;
  auto change = current_start_en - orig_start_en;

  EXPECT_NEAR(change(0), 100, 0.000001);
  EXPECT_NEAR(change(1), 10, 0.000001);

  // Run ONE more trackline, and confirm the shifts
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  ds_trackline::Projection::VectorLL orig_end_ll, current_end_ll;
  orig_start_ll <<0.1, 0.0;
  current_start_ll <<trackline_start->get().x, trackline_start->get().y; // we're on a real line, so start is actually start
  orig_end_ll <<0.1, 0.1;
  current_end_ll <<trackline_end->get().x, trackline_end->get().y;
  orig_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(orig_start_ll);
  current_start_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(current_start_ll);
  auto orig_end_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(orig_start_ll);
  auto current_end_en = underTest->getPattern().lines()[0].getProjection().lonlat2projected(current_start_ll);

  auto change_start = current_start_en - orig_start_en;

  EXPECT_NEAR(change_start(0), 100, 0.000001);
  EXPECT_NEAR(change_start(1), 10, 0.000001);

  auto change_end = current_end_en - orig_end_en;

  EXPECT_NEAR(change_end(0), 100, 0.000001);
  EXPECT_NEAR(change_end(1), 10, 0.000001);
}

} // namespace test
} // namespace ds_mx_stdtask