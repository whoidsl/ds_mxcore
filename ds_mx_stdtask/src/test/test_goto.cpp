/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/29/20.
//

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <ds_libtrackline/WktUtil.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/goto.h>

namespace ds_mx_stdtask {
namespace test {

class GotoTest : public ::testing::Test {

 protected:

  static const char *JSON_STR;

  void SetUp() override {
    // prepare our subtask
      subtask_trackline = std::make_shared<ds_mx::MxMockTask>();

      // prepare our compiler
      std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
      compiler->addSubtask("subtask_trackline", subtask_trackline);

      // read the config
      Json::Value config;
      Json::Reader reader;
      reader.parse(JSON_STR, config);

      // setup our test
      underTest = std::make_shared<ds_mx_stdtask::Goto>();
      underTest->init(config, compiler);

      // initialize state
      state.lon = 1.00001;
      state.lat = 2.00001;
  }

  // setup any parameters required of our subtasks
  void SetUpParameters() {
    trackline_start.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                   "start_pt", ds_mx::DYNAMIC));
    trackline_end.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                 "end_pt", ds_mx::DYNAMIC));
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_trackline;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_start;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_end;
  std::shared_ptr<ds_mx_stdtask::Goto> underTest;
  ds_nav_msgs::NavState state;
};

const char* GotoTest::JSON_STR = R"( {
  "type": "goto",
  "trackline": "subtask_trackline",
  "center_pt": "POINT(1.0 2.0) "
})";


TEST_F(GotoTest, VerifyOnInit) {
  EXPECT_EQ(1, subtask_trackline->initCalls);
}

TEST_F(GotoTest, VerifyOnStart) {
  SetUpParameters();
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(GotoTest, VerifyTick) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(GotoTest, SubtaskSuceeeds) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  // a trackline finishes causes the trackline to be re-started with new
  // start/end points
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(GotoTest, SubtaskFails) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  // a trackline finishes causes the trackline to be re-started with new
  // start/end points
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(GotoTest, VerifyOnStop) {
  SetUpParameters();
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  underTest->onStop(state);

  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(GotoTest, displayTest) {
  SetUpParameters();
  boost::uuids::uuid uuid_res;

  // Actually get the display
  state.lon = 0.0001;
  state.lat = 0.0002;
  ds_mx::GeoPoint current_pos(state.lon, state.lat);
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  const std::string LINESTRING  = "LINESTRING";

  // Got through the list of elements
  ASSERT_EQ(1, result.elements.size());
  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[0].role);
  std::copy(result.elements[0].task_uuid.begin(), result.elements[0].task_uuid.end(), uuid_res.begin());
  EXPECT_EQ(underTest->getUuid(), uuid_res);
  // just make sure everything is a linestring
  EXPECT_EQ(0, result.elements[0].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));
  EXPECT_EQ(ds_trackline::wktLineLL(0.0001, 0.0002, 1.0, 2.0), result.elements[0].wellknowntext);
}

// This test shifts the entire pattern 0.1 degrees east via the same mechanism the NavG GUI plugin does
TEST_F(GotoTest, changeDisplayTest) {
  SetUpParameters();
  boost::uuids::uuid uuid_res;

  underTest->getParameters().get<ds_mx::GeoPointParam>("center_pt").setFromString("POINT(1.1 2.0)");

  // Actually get the display
  ds_mx::GeoPoint current_pos(state.lon, state.lat);
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  const std::string LINESTRING  = "LINESTRING";

  // Got through the list of elements
  ASSERT_EQ(1, result.elements.size());
  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[0].role);
  std::copy(result.elements[0].task_uuid.begin(), result.elements[0].task_uuid.end(), uuid_res.begin());
  EXPECT_EQ(underTest->getUuid(), uuid_res);
  // just make sure everything is a linestring
  EXPECT_EQ(0, result.elements[0].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));
  EXPECT_EQ(ds_trackline::wktLineLL(1.00001, 2.00001, 1.1, 2.0), result.elements[0].wellknowntext);
}

TEST_F(GotoTest, ValidateMissingParamTest) {
  subtask_trackline->validateReturn = true;

  EXPECT_FALSE(underTest->validate());
}

TEST_F(GotoTest, ValidateSuccessTest) {
  SetUpParameters();
  subtask_trackline->validateReturn = true;
  EXPECT_TRUE(underTest->validate());
}

TEST_F(GotoTest, ValidateFailTest) {
  SetUpParameters();
  subtask_trackline->validateReturn = false;
  EXPECT_FALSE(underTest->validate());
}

TEST_F(GotoTest, PatternGenerationTest) {
  SetUpParameters();
  const double TOL=1.0e-9;
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  EXPECT_NEAR(state.lon, trackline_start->get().x, TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, TOL);

  EXPECT_NEAR(1.0, trackline_end->get().x, TOL);
  EXPECT_NEAR(2.0, trackline_end->get().y, TOL);

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
}

TEST_F(GotoTest, TimeoutTest) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  // set a overall-task-max timeout
  ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
  max_timeout.set(5.0);

  // start the task.  This *should* set the timeout
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

  // advance time 5 seconds, and amke sure timeout actually fires
  state.header.stamp += ros::Duration(5.001);
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

const double MDEG_LON = 111320.700;
const double STOP_LON = 0.05;
const double STOP_LAT = 0.20; // don't stop ON the pattern!
const double STOP_TOL = 1.0e-7; // approx 1cm


TEST_F(GotoTest, RestartTest) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->onStart(state);
  // ship the first few lines
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // line 2

  // ok, now its on the first actual trackline.  Set a state somewhere on there
  state.lon = STOP_LON;
  state.lat = STOP_LAT;

  // stop this task
  underTest->onStop(state);

  // resume, and check that the resulting trackline points to the right place
  state.lon = -0.01;
  state.lat = -0.01;
  underTest->onStart(state);

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);
  EXPECT_NEAR(1.0, trackline_end->get().x, STOP_TOL);
  EXPECT_NEAR(2.0, trackline_end->get().y, STOP_TOL);
}

TEST_F(GotoTest, MoveBeforeTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->getParameters().get<ds_mx::GeoPointParam>("center_pt").set(ds_mx::GeoPoint(1.2, 2.2));

  underTest->onStart(state);

  ds_nav_msgs::NavState orig_state = state;
  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);
  EXPECT_NEAR(1.2, trackline_end->get().x, STOP_TOL);
  EXPECT_NEAR(2.2, trackline_end->get().y, STOP_TOL);

  state.lon -= 0.001;
  state.lat -= 0.001;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(orig_state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(orig_state.lat, trackline_start->get().y, STOP_TOL);
  EXPECT_NEAR(1.2, trackline_end->get().x, STOP_TOL);
  EXPECT_NEAR(2.2, trackline_end->get().y, STOP_TOL);
}

TEST_F(GotoTest, MoveDuringTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->onStart(state);

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);
  EXPECT_NEAR(1.0, trackline_end->get().x, STOP_TOL);
  EXPECT_NEAR(2.0, trackline_end->get().y, STOP_TOL);

  underTest->getParameters().get<ds_mx::GeoPointParam>("center_pt").set(ds_mx::GeoPoint(1.2, 2.2));

  state.lon -= 0.001;
  state.lat -= 0.001;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);
  EXPECT_NEAR(1.2, trackline_end->get().x, STOP_TOL);
  EXPECT_NEAR(2.2, trackline_end->get().y, STOP_TOL);
}

TEST_F(GotoTest, ShiftBeforeTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

  underTest->getParameters().get<ds_mx::DoubleParam>("global_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  underTest->onStart(state);

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

  // compute how much the trackline has changed
  ds_trackline::Projection::VectorLL orig_goal_ll, current_goal_ll;
  orig_goal_ll << 1,2;
  current_goal_ll <<trackline_end->get().x, trackline_end->get().y;
  ds_trackline::Projection proj(orig_goal_ll);
  auto current_goal_en = proj.lonlat2projected(current_goal_ll);

  // Use a projection with the original endpoint as the origin to see that we've moved the requested amount
  EXPECT_NEAR(current_goal_en(0), 100, 0.000001);
  EXPECT_NEAR(current_goal_en(1), 10, 0.000001);
}

TEST_F(GotoTest, ShiftDuringTest) {

  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  underTest->onStart(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  underTest->getParameters().get<ds_mx::DoubleParam>("global_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  state.lon += 0.0001;
  state.lat += 0.0001;

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
  EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

  // compute how much the trackline has changed
  ds_trackline::Projection::VectorLL orig_goal_ll, current_goal_ll;
  orig_goal_ll << 1,2;
  current_goal_ll <<trackline_end->get().x, trackline_end->get().y;
  ds_trackline::Projection proj(orig_goal_ll);
  auto current_goal_en = proj.lonlat2projected(current_goal_ll);

  // Use a projection with the original endpoint as the origin to see that we've moved the requested amount
  EXPECT_NEAR(current_goal_en(0), 100, 0.000001);
  EXPECT_NEAR(current_goal_en(1), 10, 0.000001);
}

} // namespace test
} // namespace ds_mx_stdtask