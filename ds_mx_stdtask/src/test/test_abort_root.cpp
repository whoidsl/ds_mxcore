/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/19.
//

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/abort_root.h>


namespace ds_mx_stdtask {
namespace test {

class AbortRootTest : public ::testing::Test {

 protected:

  static const char* JSON_STR;

  void SetUp() override {

    // prepare our subtask
    subtask_mission = std::make_shared<ds_mx::MxMockTask>();
    subtask_abort = std::make_shared<ds_mx::MxMockTask>();
    subtask_trap = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask_mission", subtask_mission);
    compiler->addSubtask("subtask_abort", subtask_abort);
    compiler->addSubtask("subtask_trap", subtask_trap);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<ds_mx_stdtask::AbortRoot>();
    underTest->init(config, compiler);
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_mission;
  std::shared_ptr<ds_mx::MxMockTask> subtask_abort;
  std::shared_ptr<ds_mx::MxMockTask> subtask_trap;
  std::shared_ptr<ds_mx_stdtask::AbortRoot> underTest;
  ds_nav_msgs::NavState state;
};

const char* AbortRootTest::JSON_STR = R"( {
  "type": "abort_root",
  "launch_pt": "POINT(1 2) ",
  "mission": "subtask_mission",
  "abort": "subtask_abort",
  "trap": "subtask_trap"
})";

TEST_F(AbortRootTest, VerifyOnInit) {
    EXPECT_EQ(1, subtask_mission->initCalls);
    EXPECT_EQ(1, subtask_abort->initCalls);
    EXPECT_EQ(1, subtask_trap->initCalls);
}

TEST_F(AbortRootTest, VerifyOnStart) {
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(0, subtask_mission->ticks);
    EXPECT_EQ(0, subtask_abort->ticks);
    EXPECT_EQ(0, subtask_trap->ticks);

    EXPECT_EQ(0, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);
}

TEST_F(AbortRootTest, VerifyTick) {
    subtask_mission->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->ticks);
    EXPECT_EQ(0, subtask_abort->ticks);
    EXPECT_EQ(0, subtask_trap->ticks);

    EXPECT_EQ(0, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);
}

TEST_F(AbortRootTest, VerifyOnStop) {
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(0, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    underTest->onStop(state);

    // Unlike virtually every Task, Root Nodes DO NOT pass On Stop to their
    // subtasks.
    EXPECT_EQ(0, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    // In fact, we should still be able to tick:
    subtask_mission->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    EXPECT_EQ(0, subtask_mission->ticks);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->ticks);
    EXPECT_EQ(0, subtask_abort->ticks);
    EXPECT_EQ(0, subtask_trap->ticks);
}

TEST_F(AbortRootTest, SubtaskStops) {
    subtask_mission->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
    subtask_mission->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    subtask_abort->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
    subtask_abort->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    underTest->onStart(state);
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    // first, tick the mission twice
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(0, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    // second tick should cause the abort node to tick over to the "abort" subtask
    // but the abort_task ITSELF is still running
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(1, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    // Run two more ticks
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(1, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    // Second tick should case the abort subtask to exit and
    // start the "trap" subtask
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(1, subtask_abort->onStartCalls);
    EXPECT_EQ(1, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->onStopCalls);
    EXPECT_EQ(1, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    // Finally, some cleanup
    EXPECT_EQ(2, subtask_mission->ticks);
    EXPECT_EQ(2, subtask_abort->ticks);
}

TEST_F(AbortRootTest, TrapStops) {
    // first, put the test case in the trap state by having each subtask immediately return
    subtask_mission->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);
    subtask_abort->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    underTest->onStart(state);
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(0, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(1, subtask_abort->onStartCalls);
    EXPECT_EQ(0, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->onStopCalls);
    EXPECT_EQ(0, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(1, subtask_abort->onStartCalls);
    EXPECT_EQ(1, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->onStopCalls);
    EXPECT_EQ(1, subtask_abort->onStopCalls);
    EXPECT_EQ(0, subtask_trap->onStopCalls);

    // Next, let's see what happens when the Trap Task returns
    subtask_trap->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    // Abort Task should continue running...
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    // but Trap Subtask will have been restarted
    EXPECT_EQ(1, subtask_mission->onStartCalls);
    EXPECT_EQ(1, subtask_abort->onStartCalls);
    EXPECT_EQ(2, subtask_trap->onStartCalls);

    EXPECT_EQ(1, subtask_mission->onStopCalls);
    EXPECT_EQ(1, subtask_abort->onStopCalls);
    EXPECT_EQ(1, subtask_trap->onStopCalls);
}

TEST_F(AbortRootTest, launchStateTest) {
    auto result = underTest->getLaunchState();
    EXPECT_EQ(0.0, result.down);
    EXPECT_EQ(1.0, result.lon);
    EXPECT_EQ(2.0, result.lat);
}

TEST_F(AbortRootTest, displayTest) {
    boost::uuids::uuid uuid_res_root, uuid_res_mission, uuid_res_abort, uuid_res_trap;

    // Setup the subtask's displays
    ds_mx_msgs::MissionElementDisplay displayElements;
    displayElements.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
    displayElements.label = "mission label";
    displayElements.wellknowntext = "WKT_mission";
    subtask_mission->displayElement = displayElements;

    displayElements.label = "abort label";
    displayElements.wellknowntext = "WKT_abort";
    subtask_abort->displayElement = displayElements;

    displayElements.label = "trap label";
    displayElements.wellknowntext = "WKT_trap";
    subtask_trap->displayElement = displayElements;

    // Actually get the display
    auto launch_state = underTest->getLaunchState();
    ds_mx_msgs::MissionDisplay result;
    underTest->getDisplay(launch_state, result);

    ASSERT_EQ(4, result.elements.size());

    // Order should be:
    // 1. Launch Point
    size_t idx=0;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_POINT_LAUNCH, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_root.begin());
    EXPECT_EQ(underTest->getUuid(), uuid_res_root);
    EXPECT_EQ("POINT (1.000000000 2.000000000)", result.elements[idx].wellknowntext);

    // 2. Element from mission task
    idx++;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_mission.begin());
    EXPECT_EQ(subtask_mission->getUuid(), uuid_res_mission);
    EXPECT_EQ("WKT_mission", result.elements[idx].wellknowntext);
    EXPECT_EQ("mission label", result.elements[idx].label);

    // 3. Element from abort task
    idx++;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_abort.begin());
    EXPECT_EQ(subtask_abort->getUuid(), uuid_res_abort);
    EXPECT_EQ("WKT_abort", result.elements[idx].wellknowntext);
    EXPECT_EQ("abort label", result.elements[idx].label);

    // 4. Element from trap task
    idx++;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_trap.begin());
    EXPECT_EQ(subtask_trap->getUuid(), uuid_res_trap);
    EXPECT_EQ("WKT_trap", result.elements[idx].wellknowntext);
    EXPECT_EQ("trap label", result.elements[idx].label);

    // Finally, let's make sure we're actually setting UUIDs correctly
    EXPECT_NE(uuid_res_root, uuid_res_mission);
    EXPECT_NE(uuid_res_root, uuid_res_abort);
    EXPECT_NE(uuid_res_root, uuid_res_trap);
    EXPECT_NE(uuid_res_mission, uuid_res_abort);
    EXPECT_NE(uuid_res_mission, uuid_res_trap);
    EXPECT_NE(uuid_res_abort, uuid_res_trap);
}

TEST_F(AbortRootTest, ValidateSuccessTest) {
    subtask_mission->validateReturn = true;
    subtask_abort->validateReturn = true;
    subtask_trap->validateReturn = true;

    EXPECT_TRUE(underTest->validate());
}

TEST_F(AbortRootTest, ValidateFailTest) {
    subtask_mission->validateReturn = false;
    subtask_abort->validateReturn = true;
    subtask_trap->validateReturn = true;
    EXPECT_FALSE(underTest->validate());

    subtask_mission->validateReturn = true;
    subtask_abort->validateReturn = false;
    subtask_trap->validateReturn = true;
    EXPECT_FALSE(underTest->validate());

    subtask_mission->validateReturn = true;
    subtask_abort->validateReturn = true;
    subtask_trap->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(AbortRootTest, TimeoutTest) {
  subtask_mission->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  // set a overall-task-max timeout
  ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
  max_timeout.set(5.0);

  // start the task.  This *should* set the timeout
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_mission->onStartCalls);
  EXPECT_EQ(0, subtask_mission->ticks);
  EXPECT_EQ(0, subtask_mission->onStopCalls);

  EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

  // advance time 5 seconds, and make sure timeout actually fires
  state.header.stamp += ros::Duration(5.001);
  // Root task ALWAYS runs!
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  EXPECT_EQ(1, subtask_mission->onStartCalls);
  EXPECT_EQ(1, subtask_mission->ticks);
  EXPECT_EQ(1, subtask_mission->onStopCalls);
  EXPECT_EQ(1, subtask_abort->onStartCalls);
  EXPECT_EQ(0, subtask_abort->ticks);

  // verify timeout hasn't changed
  EXPECT_EQ(state.header.stamp - ros::Duration(0.001), underTest->timeoutTime());

  // Next task should also run, despite the timeout
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask_abort->ticks);

}

} // namespace test
} // namespace ds_mx_stdtask