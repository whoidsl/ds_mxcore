/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>
#include <ds_libtrackline/WktUtil.h>

#include <ds_mx_stdtask/nav_legs.h>

namespace ds_mx_stdtask {
namespace test {

class NavLegsTest : public ::testing::Test {

 protected:

  static const char *JSON_STR;

  void SetUp() override {

      // prepare our subtask
      subtask_trackline = std::make_shared<ds_mx::MxMockTask>();

      // prepare our compiler
      std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
      compiler->addSubtask("subtask_trackline", subtask_trackline);

      // read the config
      Json::Value config;
      Json::Reader reader;
      reader.parse(JSON_STR, config);

      // setup our test
      underTest = std::make_shared<ds_mx_stdtask::NavLegs>();
      underTest->init(config, compiler);

      // initialize state
      state.lon = 1.00001;
      state.lat = 2.00001;
  }

  // setup any parameters required of our subtasks
  void SetUpParameters() {
      trackline_start.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                     "start_pt", ds_mx::DYNAMIC));
      trackline_end.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                   "end_pt", ds_mx::DYNAMIC));
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_trackline;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_start;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_end;
  std::shared_ptr<ds_mx_stdtask::NavLegs> underTest;
  ds_nav_msgs::NavState state;
  static std::vector<ds_mx::GeoPoint> expected_waypoints;
};

// verified in Matlab as correct
std::vector<ds_mx::GeoPoint> NavLegsTest::expected_waypoints = {
    ds_mx::GeoPoint(1.0000100000000, 2.000010000000),
    ds_mx::GeoPoint(0.9993258556131, 1.999999999861),
    ds_mx::GeoPoint(0.9997752852044, 1.999999999985),
    ds_mx::GeoPoint(0.9997752851428, 2.000452179191),
    ds_mx::GeoPoint(1.000224714857, 2.000452179191),
    ds_mx::GeoPoint(1.000224714796, 1.999999999985),
    ds_mx::GeoPoint(1.000674144387, 1.999999999861)
};


const char* NavLegsTest::JSON_STR = R"( {
  "type": "navlegs",
  "center_pt": "POINT(1.0 2.0) ",
  "length": 50,
  "heading": 90,
  "right_first": false,
  "backup_dist": 15,
  "trackline": "subtask_trackline"
})";

TEST_F(NavLegsTest, VerifyOnInit) {
    EXPECT_EQ(1, subtask_trackline->initCalls);
}

TEST_F(NavLegsTest, VerifyOnStart) {
    SetUpParameters();
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(NavLegsTest, VerifyTick) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(NavLegsTest, SubtaskSuceeeds) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    // a trackline finishes causes the trackline to be re-started with new
    // start/end points
    EXPECT_EQ(2, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(NavLegsTest, SubtaskFails) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    // a trackline finishes causes the trackline to be re-started with new
    // start/end points
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(2, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->ticks);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(NavLegsTest, VerifyOnStop) {
    SetUpParameters();
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    underTest->onStop(state);

    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(NavLegsTest, displayTest) {
    SetUpParameters();
    boost::uuids::uuid uuid_res;

    // Actually get the display
    ds_mx::GeoPoint current_pos(state.lon, state.lat);
    ds_mx_msgs::MissionDisplay result;
    underTest->getDisplay(state, result);

    const std::string LINESTRING  = "LINESTRING";

    // Got through the list of elements
    for (size_t idx=0; idx<result.elements.size(); idx++) {
      if (idx == 0) {
          EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[idx].role);
      } else {
          EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, result.elements[idx].role);
      }
      std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
      EXPECT_EQ(underTest->getUuid(), uuid_res);
      // just make sure everything is a linestring
      EXPECT_EQ(0, result.elements[idx].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));
          EXPECT_EQ(ds_trackline::wktLineLL(expected_waypoints[idx].x, expected_waypoints[idx].y,
                                      expected_waypoints[idx+1].x, expected_waypoints[idx+1].y),
              result.elements[idx].wellknowntext);
    }
}

// This test shifts the entire pattern 0.1 degrees east via the same mechanism the NavG GUI plugin does
TEST_F(NavLegsTest, changeDisplayTest) {
  SetUpParameters();
  boost::uuids::uuid uuid_res;

  underTest->getParameters().get<ds_mx::GeoPointParam>("center_pt").setFromString("POINT(1.1 2.0)");
  state.lon += 0.1; // shift start position 0.1 degrees east to match change in center point & keep the first line
                    // matching the expected_waypoints

  // Actually get the display
  ds_mx::GeoPoint current_pos(state.lon, state.lat);
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  const std::string LINESTRING  = "LINESTRING";

  // Got through the list of elements
  for (size_t idx=0; idx<result.elements.size(); idx++) {
    if (idx == 0) {
      EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[idx].role);
    } else {
      EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, result.elements[idx].role);
    }
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
    EXPECT_EQ(underTest->getUuid(), uuid_res);
    // just make sure everything is a linestring
    EXPECT_EQ(0, result.elements[idx].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));

    EXPECT_EQ(ds_trackline::wktLineLL(expected_waypoints[idx].x + 0.1, expected_waypoints[idx].y,
                                      expected_waypoints[idx + 1].x + 0.1, expected_waypoints[idx + 1].y),
              result.elements[idx].wellknowntext);
  }
}

TEST_F(NavLegsTest, ValidateMissingParamTest) {
    subtask_trackline->validateReturn = true;

    EXPECT_FALSE(underTest->validate());
}

TEST_F(NavLegsTest, ValidateSuccessTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = true;
    EXPECT_TRUE(underTest->validate());
}

TEST_F(NavLegsTest, ValidateFailTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(NavLegsTest, PatternGenerationTest) {
    SetUpParameters();
    const double TOL=1.0e-9;
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    for (size_t i=0; i<expected_waypoints.size()-1; i++) {
        // tracklines should be from i to i+1
        EXPECT_EQ(i+1, subtask_trackline->onStartCalls);
        EXPECT_EQ(i, subtask_trackline->ticks);
        EXPECT_EQ(i, subtask_trackline->onStopCalls);

        EXPECT_NEAR(expected_waypoints[i].x, trackline_start->get().x, TOL);
        EXPECT_NEAR(expected_waypoints[i].y, trackline_start->get().y, TOL);
        EXPECT_NEAR(expected_waypoints[i+1].x, trackline_end->get().x, TOL);
        EXPECT_NEAR(expected_waypoints[i+1].y, trackline_end->get().y, TOL);

        if (i == expected_waypoints.size()-2) {
            EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
        } else {
            ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
        }
    }
}

TEST_F(NavLegsTest, TimeoutTest) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    // set a overall-task-max timeout
    ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
    max_timeout.set(5.0);

    // start the task.  This *should* set the timeout
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

    // advance time 5 seconds, and amke sure timeout actually fires
    state.header.stamp += ros::Duration(5.001);
    EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

const double MDEG_LAT = 110568.605;
const double STOP_LON = 1.0000;
const double STOP_LAT = 2.0003;
const double STOP_TOL = 0.00000001;

TEST_F(NavLegsTest, RestartTest) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

    const double BACKUP_DIST = underTest->getParameters().get<ds_mx::DoubleParam>("backup_dist").get();

    const double RESUME_LON = expected_waypoints[2].x;
    const double RESUME_LAT = STOP_LAT - BACKUP_DIST / MDEG_LAT;

    underTest->onStart(state);
    // ship the first few lines
    ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // connecting line
    ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // first line going east
    // now on the offset line going east

    // ok, now its on the first actual trackline.  Set a state somewhere on there
    state.lon = STOP_LON;
    state.lat = STOP_LAT;

    // stop this task
    underTest->onStop(state);

    // check that its supposed to resume back to the starting point
    ds_mx::GeoPoint resumePt = underTest->getOnstartPt();

    EXPECT_NEAR(RESUME_LON, resumePt.x, STOP_TOL);
    EXPECT_NEAR(RESUME_LAT, resumePt.y, STOP_TOL);

    // resume, and check that the resulting trackline points to the right place
    state.lon = 1.0;
    state.lat = 1.99996;
    underTest->onStart(state);

    EXPECT_NEAR(state.lon, trackline_start->get().x, STOP_TOL);
    EXPECT_NEAR(state.lat, trackline_start->get().y, STOP_TOL);

    EXPECT_NEAR(RESUME_LON, trackline_end->get().x, STOP_TOL);
    EXPECT_NEAR(RESUME_LAT, trackline_end->get().y, STOP_TOL);
}

TEST_F(NavLegsTest, ChangeParam) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;

  underTest->onStart(state);

  // skip the first few line
  ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // connecting line

  // ok, now its on the first actual trackline.  let's apply a shift
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_east").set(100);
  underTest->getParameters().get<ds_mx::DoubleParam>("local_shift_north").set(10);

  ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state)); // finish connecting line

  // we're now on the primary trackline.  Check to see if the actual trackline has been shifted by
  // the correct amount
  ds_trackline::Projection::VectorLL orig_start_ll, current_start_ll;
  ds_trackline::Projection::VectorLL orig_end_ll, current_end_ll;
  orig_start_ll << 0.9993258556131, 1.999999999861;
  current_start_ll
      << trackline_start->get().x, trackline_start->get().y; // we're on a real line, so start is actually start
  orig_end_ll << 0.9997752852044, 1.999999999985;
  current_end_ll << trackline_end->get().x, trackline_end->get().y;
  ds_trackline::Projection proj(orig_start_ll);
  auto orig_start_en = proj.lonlat2projected(orig_start_ll);
  auto current_start_en = proj.lonlat2projected(current_start_ll);
  auto orig_end_en = proj.lonlat2projected(orig_end_ll);
  auto current_end_en = proj.lonlat2projected(current_end_ll);

  auto change_start = current_start_en - orig_start_en;

  std::cout.precision(16);
  std::cout <<trackline_start->get().x <<" " <<trackline_start->get().y <<"\n";
  std::cout <<trackline_end->get().x <<" " <<trackline_end->get().y <<std::endl;

  // 1mm is good enough
  EXPECT_NEAR(change_start(0), 100, 0.001);
  EXPECT_NEAR(change_start(1), 10, 0.001);

  auto change_end = current_end_en - orig_end_en;

  EXPECT_NEAR(change_end(0), 100, 0.001);
  EXPECT_NEAR(change_end(1), 10, 0.001);
}

} // namespace test
} // namespace ds_mx_stdtask
