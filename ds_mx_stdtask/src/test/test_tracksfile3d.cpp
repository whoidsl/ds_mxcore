/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <ds_libtrackline/WktUtil.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/tracksfile3d.h>

namespace ds_mx_stdtask {
namespace test {

class Tracksfile3dTest : public ::testing::Test {
 protected:

  static const char* JSON_STR;

  void SetUp() override {

          // prepare our subtask
      subtask_trackline = std::make_shared<ds_mx::MxMockTask>();

      // prepare our compiler
      std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
      compiler->addSubtask("subtask_trackline", subtask_trackline);

      // read the config
      Json::Value config;
      Json::Reader reader;
      reader.parse(JSON_STR, config);

      // setup our test
      underTest = std::make_shared<ds_mx_stdtask::Tracksfile3d>();
      underTest->init(config, compiler);

      // initialize state
      state.lon = 1.00001;
      state.lat = 2.00001;

  }
  // setup any parameters required of our subtasks
  void SetUpParameters() {
      trackline_start.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                     "start_pt", ds_mx::DYNAMIC));
      trackline_end.reset(new ds_mx::GeoPointParam(subtask_trackline->getParametersPtr(),
                                                   "end_pt", ds_mx::DYNAMIC));
      depth_start.reset(new ds_mx::DoubleParam(subtask_trackline->getParametersPtr(),
                                               "start_depth", ds_mx::DYNAMIC));
      depth_end.reset(new ds_mx::DoubleParam(subtask_trackline->getParametersPtr(),
                                             "end_depth", ds_mx::DYNAMIC));
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_trackline;
  // start/end of CURRENT trackline
  std::shared_ptr<ds_mx::GeoPointParam> trackline_start;
  std::shared_ptr<ds_mx::GeoPointParam> trackline_end;
  std::shared_ptr<ds_mx::DoubleParam> depth_start;
  std::shared_ptr<ds_mx::DoubleParam> depth_end;
  std::shared_ptr<ds_mx::GeoPointParam> pattern_center;
  std::shared_ptr<ds_mx_stdtask::Tracksfile3d> underTest;
  ds_nav_msgs::NavState state;
  static std::vector<ds_mx::GeoPoint> expected_waypoints;
  static std::vector<double> expected_depths;

}; // class Tracksfile3dTest

// verified in Matlab as correct
std::vector<ds_mx::GeoPoint> Tracksfile3dTest::expected_waypoints = {
    ds_mx::GeoPoint(1.0000100000000, 2.000010000000),
    ds_mx::GeoPoint(1.0000179771838, 2.0000135653761),
    ds_mx::GeoPoint(1.0000000000000, 2.0000271307525),
    ds_mx::GeoPoint(0.9999820228162, 2.0000135653761),
    ds_mx::GeoPoint(1.0000179771835, 1.9999864346237),
    ds_mx::GeoPoint(1.0000000000000, 1.9999728692475),
    ds_mx::GeoPoint(0.9999820228165, 1.9999864346237)
};
std::vector<double> Tracksfile3dTest::expected_depths = {
    0,
    0,
    1,
    2,
    3,
    4,
    5
};

const char* Tracksfile3dTest::JSON_STR = R"( {
  "type": "tracksfile",
  "center_pt": "POINT(1.0 2.0) ",
  "point_list": "2.00 1.50 0.00 0.00 3.00 1.00 -2.00 1.50 2.00 2.00 -1.50 3.00 0.00 -3.00 4.00 -2.00 -1.50 5.00",
  "trackline": "subtask_trackline"
})";

TEST_F(Tracksfile3dTest, VerifyOnInit) {
  EXPECT_EQ(1, subtask_trackline->initCalls);
}

TEST_F(Tracksfile3dTest, VerifyOnStart) {
  SetUpParameters();
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(Tracksfile3dTest, VerifyTick) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);
}

TEST_F(Tracksfile3dTest, SubtaskSuceeeds) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  // a trackline finishes causes the trackline to be re-started with new
  // start/end points
  EXPECT_EQ(2, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->ticks);
  EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(Tracksfile3dTest, SubtaskFails) {
  SetUpParameters();
  subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->ticks);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  // a trackline finishes causes the trackline to be re-started with new
  // start/end points
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->ticks);
  EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(Tracksfile3dTest, VerifyOnStop) {
  SetUpParameters();
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(0, subtask_trackline->onStopCalls);

  underTest->onStop(state);

  EXPECT_EQ(1, subtask_trackline->onStartCalls);
  EXPECT_EQ(1, subtask_trackline->onStopCalls);
}

TEST_F(Tracksfile3dTest, displayTest) {
  SetUpParameters();
  boost::uuids::uuid uuid_res;

  // Actually get the display
  ds_mx::GeoPoint current_pos(state.lon, state.lat);
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  const std::string LINESTRING  = "LINESTRING";

  // Got through the list of elements
  for (size_t idx=0; idx<result.elements.size(); idx++) {
    if (idx == 0) {
      EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[idx].role);
    } else {
      EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, result.elements[idx].role);
    }
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
    EXPECT_EQ(underTest->getUuid(), uuid_res);
    // just make sure everything is a linestring
    EXPECT_EQ(0, result.elements[idx].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));
    EXPECT_EQ(ds_trackline::wktLineLL(expected_waypoints[idx].x, expected_waypoints[idx].y,
                                      expected_waypoints[idx+1].x, expected_waypoints[idx+1].y),
              result.elements[idx].wellknowntext);
  }
}

// This test shifts the entire pattern 0.1 degrees east via the same mechanism the NavG GUI plugin does
TEST_F(Tracksfile3dTest, changeDisplayTest) {
  SetUpParameters();
  boost::uuids::uuid uuid_res;

  underTest->getParameters().get<ds_mx::GeoPointParam>("center_pt").setFromString("POINT(1.1 2.0)");
  state.lon += 0.1; // shift start position 0.1 degrees east to match change in center point & keep the first line
                    // matching the expected_waypoints

  // Actually get the display
  ds_mx::GeoPoint current_pos(state.lon, state.lat);
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  const std::string LINESTRING  = "LINESTRING";

  // Got through the list of elements
  for (size_t idx=0; idx<result.elements.size(); idx++) {
    if (idx == 0) {
      EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, result.elements[idx].role);
    } else {
      EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, result.elements[idx].role);
    }
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
    EXPECT_EQ(underTest->getUuid(), uuid_res);
    // just make sure everything is a linestring
    EXPECT_EQ(0, result.elements[idx].wellknowntext.compare(0, LINESTRING.length(), LINESTRING));
    EXPECT_EQ(ds_trackline::wktLineLL(expected_waypoints[idx].x+0.1, expected_waypoints[idx].y,
                                      expected_waypoints[idx+1].x+0.1, expected_waypoints[idx+1].y),
              result.elements[idx].wellknowntext);
  }
}

TEST_F(Tracksfile3dTest, ValidateMissingParamTest) {
    subtask_trackline->validateReturn = true;

    EXPECT_FALSE(underTest->validate());
}

TEST_F(Tracksfile3dTest, ValidateSuccessTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = true;
    EXPECT_TRUE(underTest->validate());
}

TEST_F(Tracksfile3dTest, ValidateFailTest) {
    SetUpParameters();
    subtask_trackline->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(Tracksfile3dTest, PatternGenerationTest) {
    SetUpParameters();
    const double TOL=1.0e-9;
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    for (size_t i=0; i<expected_waypoints.size()-1; i++) {
        // tracklines should be from i to i+1
        EXPECT_EQ(i+1, subtask_trackline->onStartCalls);
        EXPECT_EQ(i, subtask_trackline->ticks);
        EXPECT_EQ(i, subtask_trackline->onStopCalls);

        EXPECT_NEAR(expected_waypoints[i].x, trackline_start->get().x, TOL);
        EXPECT_NEAR(expected_waypoints[i].y, trackline_start->get().y, TOL);
        EXPECT_NEAR(expected_depths[i], depth_start->get(), TOL);
        EXPECT_NEAR(expected_waypoints[i+1].x, trackline_end->get().x, TOL);
        EXPECT_NEAR(expected_waypoints[i+1].y, trackline_end->get().y, TOL);
        EXPECT_NEAR(expected_depths[i+1], depth_end->get(), TOL);

      if (i == expected_waypoints.size()-2) {
            EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
        } else {
            ASSERT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
        }
    }
}

TEST_F(Tracksfile3dTest, TimeoutTest) {
    SetUpParameters();
    subtask_trackline->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    // set a overall-task-max timeout
    ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
    max_timeout.set(5.0);

    // start the task.  This *should* set the timeout
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_trackline->onStartCalls);
    EXPECT_EQ(0, subtask_trackline->ticks);
    EXPECT_EQ(0, subtask_trackline->onStopCalls);

    EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

    // advance time 5 seconds, and amke sure timeout actually fires
    state.header.stamp += ros::Duration(5.001);
    EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

} // namespace test
} // namespace ds_mx_stdtask