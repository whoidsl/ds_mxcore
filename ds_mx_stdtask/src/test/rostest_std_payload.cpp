/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/8/19.
//

#include <gtest/gtest.h>

#include <ds_mxutils/ds_test_message_queue.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mx_stdtask/std_payload.h>

namespace ds_mx_stdtask {
namespace test {

class TestPayload : public ds_mx_stdtask::StdPayload {

  void init_ros(ros::NodeHandle &nh) override {
    cmd_pub = nh.advertise<ds_mx_msgs::StdPayloadCommand>("payload_cmd", 10, false);
  }

  bool validate() const override {
    // since this is always overridden by our child class there's no point in
    // testing it.
    return true;
  }
};

class StdPayloadRostest : public ::testing::Test {

 protected:
  static const char* JSON_STR;

  StdPayloadRostest() : cmd_msgs("payload_cmd") {
    node_handle.reset(new ros::NodeHandle());
  }

  void SetUp() override {
    // prepare our subtask
    subtask = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask", subtask);

    // setup our subtask's parameters
    example_param.reset(new ds_mx::GeoPointParam(subtask->getParametersPtr(), "center_pt", ds_mx::DYNAMIC));

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest.reset(new TestPayload());
    underTest->init(config, compiler);
    underTest->init_ros(*node_handle); // run the ROS setup, since this is the
                                       // rostest edition!

    // fill in our test state with some basic values
    state.header.stamp = ros::Time::now();
    state.lat = 1;
    state.lon = 2;
    state.down = 1000;
    state.heading = 0.5;
    state.roll = 0.01;
    state.pitch = 0.02;
  }

 protected:
  std::shared_ptr<ds_mx::MxMockTask> subtask;
  std::shared_ptr<ds_mx_stdtask::StdPayload> underTest;
  ds_nav_msgs::NavState state;
  ds_mx::TestMessageQueue<ds_mx_msgs::StdPayloadCommand> cmd_msgs;
  std::shared_ptr<ds_mx::GeoPointParam> example_param;

 private:
  std::unique_ptr<ros::NodeHandle> node_handle;
};

const char* StdPayloadRostest::JSON_STR = R"( {
  "type": "sentry_drop_weights",
  "subtask": "subtask",
  "payload_params": {
    "param1": "fooo",
    "param2": 23.0,
    "param3": false
  }
})";

TEST_F(StdPayloadRostest, TestStartStop) {
  // start the task
  underTest->onStart(state);

  // get the next message
  ds_mx_msgs::StdPayloadCommand cmd;
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_START, cmd.command);
  ASSERT_EQ(3, cmd.config.size());
  EXPECT_EQ("param1", cmd.config[0].key);
  EXPECT_EQ("fooo", cmd.config[0].value);
  EXPECT_EQ("param2", cmd.config[1].key);
  EXPECT_EQ("23", cmd.config[1].value);
  EXPECT_EQ("param3", cmd.config[2].key);
  EXPECT_EQ("false", cmd.config[2].value);

  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  // tick (done)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // call on-stop
  state.header.stamp += ros::Duration(10);
  underTest->onStop(state);

  // look for a stop message
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check stop message
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_STOP, cmd.command);
  ASSERT_EQ(0, cmd.config.size());

  // check to make sure the subtask was asked to do the right stuff
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  EXPECT_FALSE(cmd_msgs.waitForMessage(0.025)); // wait 25ms for a message
}

TEST_F(StdPayloadRostest, TestStartStopConfigOnly) {
  // let's try this again but with the config-only flag set
  underTest->getParameters().get<ds_mx::BoolParam>("config_only").setFromString("true");

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_mx_msgs::StdPayloadCommand cmd;
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_CONFIGONLY, cmd.command);
  ASSERT_EQ(3, cmd.config.size());
  EXPECT_EQ("param1", cmd.config[0].key);
  EXPECT_EQ("fooo", cmd.config[0].value);
  EXPECT_EQ("param2", cmd.config[1].key);
  EXPECT_EQ("23", cmd.config[1].value);
  EXPECT_EQ("param3", cmd.config[2].key);
  EXPECT_EQ("false", cmd.config[2].value);

  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  // tick (done)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // call on-stop
  state.header.stamp += ros::Duration(10);
  underTest->onStop(state);

  // there is no stop message...

  // check to make sure the subtask was asked to do the right stuff
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  EXPECT_FALSE(cmd_msgs.waitForMessage(0.025)); // wait 25ms for a message
}

TEST_F(StdPayloadRostest, EmptyParameters) {

  underTest->getParameters().get<ds_mx::KeyValueListParam>("payload_params").set(ds_mx::KeyValueList());
  EXPECT_FALSE(cmd_msgs.waitForMessage(0.025)); // wait 25ms for a message

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_mx_msgs::StdPayloadCommand cmd;
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_START, cmd.command);
  EXPECT_EQ(0, cmd.config.size());
}

TEST_F(StdPayloadRostest, TestChanged) {

  underTest->getParameters().get<ds_mx::KeyValueListParam>("payload_params").set(ds_mx::KeyValueList({
    std::make_pair("param1", "val1"),
    std::make_pair("param2", "23.4")
  }));
  EXPECT_FALSE(cmd_msgs.waitForMessage(0.025)); // wait 25ms for a message

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_mx_msgs::StdPayloadCommand cmd;
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_START, cmd.command);
  ASSERT_EQ(2, cmd.config.size());
  EXPECT_EQ("param1", cmd.config[0].key);
  EXPECT_EQ("val1", cmd.config[0].value);
  EXPECT_EQ("param2", cmd.config[1].key);
  EXPECT_EQ("23.4", cmd.config[1].value);

  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  underTest->getParameters().get<ds_mx::KeyValueListParam>("payload_params").set(ds_mx::KeyValueList(
      {
          std::make_pair("param1", "something else"),
          std::make_pair("param2", "18.2")
      }));

  state.header.stamp += ros::Duration(10);
  // parameter changes take effect on the next tick
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_CONFIGONLY, cmd.command);
  ASSERT_EQ(2, cmd.config.size());
  EXPECT_EQ("param1", cmd.config[0].key);
  EXPECT_EQ("something else", cmd.config[0].value);
  EXPECT_EQ("param2", cmd.config[1].key);
  EXPECT_EQ("18.2", cmd.config[1].value);

  // tick (done)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // call on-stop
  state.header.stamp += ros::Duration(10);
  underTest->onStop(state);

  // look for a stop message
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check stop message
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_STOP, cmd.command);
  ASSERT_EQ(0, cmd.config.size());

  // check to make sure the subtask was asked to do the right stuff
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

// we need to pass parameters on if validation is going to work even a little bit.  So test it!
TEST_F(StdPayloadRostest, HasParam) {
  EXPECT_TRUE(underTest->getParameters().has<ds_mx::GeoPointParam>("center_pt"));
  EXPECT_FALSE(underTest->getParameters().has<ds_mx::IntParam>("center_pt"));
}

} // namespace test
} // namespace ds_mx_stdtask

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_std_payload");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}