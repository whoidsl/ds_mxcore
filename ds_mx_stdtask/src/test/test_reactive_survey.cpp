/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/8/19.
//


#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/reactive_survey.h>


namespace ds_mx_stdtask {
namespace test {

class ReactiveSurveyTest : public ::testing::Test {

 protected:

  static const char* JSON_STR;

  void SetUp() override {

    // prepare our subtask
    subtask_main = std::make_shared<ds_mx::MxMockTask>();
    subtask_detail = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask_main", subtask_main);
    compiler->addSubtask("subtask_detail", subtask_detail);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<ds_mx_stdtask::ReactiveSurvey>();
    underTest->init(config, compiler);
  }

  // setup any parameters required of our subtasks
  void SetUpParameters() {
    detail_center_param.reset(new ds_mx::GeoPointParam(subtask_detail->getParametersPtr(),
        "center_pt", ds_mx::DYNAMIC));
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask_main;
  std::shared_ptr<ds_mx::MxMockTask> subtask_detail;
  std::shared_ptr<ds_mx::MxMockTask> subtask_trap;
  std::shared_ptr<ds_mx::GeoPointParam> detail_center_param;
  std::shared_ptr<ds_mx_stdtask::ReactiveSurvey> underTest;
  ds_nav_msgs::NavState state;
};

const char* ReactiveSurveyTest::JSON_STR = R"( {
  "type": "reactive",
  "detection_event": "EVENT",
  "main_survey": "subtask_main",
  "detail_survey": "subtask_detail",
})";

TEST_F(ReactiveSurveyTest, VerifyOnInit) {
    EXPECT_EQ(1, subtask_main->initCalls);
    EXPECT_EQ(1, subtask_detail->initCalls);
}

TEST_F(ReactiveSurveyTest, VerifyOnStart) {
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(0, subtask_main->ticks);
    EXPECT_EQ(0, subtask_detail->ticks);

    EXPECT_EQ(0, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);
}

TEST_F(ReactiveSurveyTest, VerifyTick) {
    subtask_main->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(1, subtask_main->ticks);
    EXPECT_EQ(0, subtask_detail->ticks);

    EXPECT_EQ(0, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);
}

TEST_F(ReactiveSurveyTest, VerifySubtaskSuceeeds) {
    subtask_main->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(1, subtask_main->ticks);
    EXPECT_EQ(0, subtask_detail->ticks);

    EXPECT_EQ(0, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);
}

TEST_F(ReactiveSurveyTest, VerifySubtaskFails) {
    subtask_main->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(1, subtask_main->ticks);
    EXPECT_EQ(0, subtask_detail->ticks);

    // does not call onStop; parent of this task will do that
    EXPECT_EQ(0, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);
}

TEST_F(ReactiveSurveyTest, VerifyOnStop) {
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(0, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);

    underTest->onStop(state);

    EXPECT_EQ(1, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);
}

TEST_F(ReactiveSurveyTest, displayTest) {
    boost::uuids::uuid uuid_react, uuid_res_main;

    // Setup the subtask's displays
    ds_mx_msgs::MissionElementDisplay displayElements;
    displayElements.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
    displayElements.label = "main label";
    displayElements.wellknowntext = "WKT_main";
    subtask_main->displayElement = displayElements;

    displayElements.label = "detail label";
    displayElements.wellknowntext = "WKT_detail";
    subtask_detail->displayElement = displayElements;

    // Actually get the display
    ds_mx_msgs::MissionDisplay result;
    underTest->getDisplay(state, result);

    ASSERT_EQ(1, result.elements.size());

    // Order should be:
    // 1. Element from primary task
    size_t idx = 0;
    EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
    std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res_main.begin());
    EXPECT_EQ(subtask_main->getUuid(), uuid_res_main);
    EXPECT_EQ("WKT_main", result.elements[idx].wellknowntext);
    EXPECT_EQ("main label", result.elements[idx].label);
}

TEST_F(ReactiveSurveyTest, ValidateMissingParamTest) {
    subtask_main->validateReturn = true;
    subtask_detail->validateReturn = true;

    EXPECT_FALSE(underTest->validate());
}

TEST_F(ReactiveSurveyTest, ValidateSuccessTest) {
    SetUpParameters();
    subtask_main->validateReturn = true;
    subtask_detail->validateReturn = true;

    EXPECT_TRUE(underTest->validate());
}

TEST_F(ReactiveSurveyTest, ValidateFailTest) {
    SetUpParameters();
    subtask_main->validateReturn = false;
    subtask_detail->validateReturn = true;
    EXPECT_FALSE(underTest->validate());

    subtask_main->validateReturn = true;
    subtask_detail->validateReturn = false;
    EXPECT_FALSE(underTest->validate());
}

TEST_F(ReactiveSurveyTest, EventDetectedTest) {
    SetUpParameters();

    // This is a big one.  First, get things set up, start our task,
    // and tick the main survey once for luck
    subtask_main->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    EXPECT_EQ(1, subtask_main->ticks);
    EXPECT_EQ(0, subtask_detail->ticks);

    EXPECT_EQ(0, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);

    // we'll run this next one 5 times, because why not?
    for (size_t i=0; i<3; i++) {
        // reset all our counters
        subtask_main->onStartCalls = 0;
        subtask_detail->onStartCalls = 0;
        subtask_main->ticks = 0;
        subtask_detail->ticks = 0;
        subtask_main->onStopCalls = 0;
        subtask_detail->onStopCalls = 0;

        // deliver an event that will trigger the detailed survey
        ds_mx_msgs::MxEvent event;
        event.eventid = "EVENT";
        underTest->handleEvent(event);

        // shouldn't tick anything
        EXPECT_EQ(0, subtask_main->onStartCalls);
        EXPECT_EQ(0, subtask_detail->onStartCalls);

        EXPECT_EQ(0, subtask_main->ticks);
        EXPECT_EQ(0, subtask_detail->ticks);

        EXPECT_EQ(0, subtask_main->onStopCalls);
        EXPECT_EQ(0, subtask_detail->onStopCalls);

        // let's run a tick.  This should stop everything and cause us to switch
        subtask_detail->tickReturns.clear();
        subtask_detail->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
        subtask_detail->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

        // This one should tick the detail survey and trigger a stop, resuming the
        // main survey.  Ticks the main survey, NOT the detail survey
        EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
        EXPECT_EQ(0, subtask_main->onStartCalls);
        EXPECT_EQ(1, subtask_detail->onStartCalls);

        EXPECT_EQ(1, subtask_main->ticks);
        EXPECT_EQ(0, subtask_detail->ticks);

        EXPECT_EQ(1, subtask_main->onStopCalls);
        EXPECT_EQ(0, subtask_detail->onStopCalls);

        // Tick the detail survey once.  Nothing else should change
        EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

        EXPECT_EQ(0, subtask_main->onStartCalls);
        EXPECT_EQ(1, subtask_detail->onStartCalls);

        EXPECT_EQ(1, subtask_main->ticks);
        EXPECT_EQ(1, subtask_detail->ticks);

        EXPECT_EQ(1, subtask_main->onStopCalls);
        EXPECT_EQ(0, subtask_detail->onStopCalls);

        // The detail survey ticks and returns "SUCCESS".  This triggers
        // a switch to the main survey, but doesn't tick it
        EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

        EXPECT_EQ(1, subtask_main->onStartCalls);
        EXPECT_EQ(1, subtask_detail->onStartCalls);

        EXPECT_EQ(1, subtask_main->ticks);
        EXPECT_EQ(2, subtask_detail->ticks);

        EXPECT_EQ(1, subtask_main->onStopCalls);
        EXPECT_EQ(1, subtask_detail->onStopCalls);
    }

    // Finally, finish it off with the final survey running once
    subtask_main->tickReturns.clear();
    subtask_main->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

    EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
    EXPECT_EQ(2, subtask_main->ticks);
    EXPECT_EQ(2, subtask_detail->ticks);
}

TEST_F(ReactiveSurveyTest, EventSetsDetailCenterTest) {
    SetUpParameters();
    subtask_main->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

    // set non-zero state
    state.lon = 2.0;
    state.lat = 3.0;

    // start the subtask
    underTest->onStart(state);
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(0, subtask_detail->onStartCalls);

    // deliver a trigger event
    ds_mx_msgs::MxEvent event;
    event.eventid = "EVENT";
    underTest->handleEvent(event);

    // start trigger a switch to the detail survey
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask_main->onStartCalls);
    EXPECT_EQ(1, subtask_detail->onStartCalls);

    EXPECT_EQ(1, subtask_main->ticks);
    EXPECT_EQ(0, subtask_detail->ticks);

    EXPECT_EQ(1, subtask_main->onStopCalls);
    EXPECT_EQ(0, subtask_detail->onStopCalls);

    // Verify the detail survey center parameter got set correctly
    EXPECT_EQ(2.0, detail_center_param->get().x);
    EXPECT_EQ(3.0, detail_center_param->get().y);
}

TEST_F(ReactiveSurveyTest, TimeoutTest) {
  subtask_main->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  // set a overall-task-max timeout
  ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
  max_timeout.set(5.0);

  // start the task.  This *should* set the timeout
  underTest->onStart(state);
  EXPECT_EQ(1, subtask_main->onStartCalls);
  EXPECT_EQ(0, subtask_main->ticks);
  EXPECT_EQ(0, subtask_main->onStopCalls);

  EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

  // advance time 5 seconds, and amke sure timeout actually fires
  state.header.stamp += ros::Duration(5.001);
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

} // namespace test
} // namespace ds_mx_stdtask