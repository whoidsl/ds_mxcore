/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/16/19.
//


#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <gtest/gtest.h>

#include <ds_mx_stdtask/ascent.h>

namespace ds_mx_stdtask {
namespace test {

class AscentTest : public ::testing::Test {

 protected:

  static const char* JSON_STR;

  void SetUp() override {

    // prepare our subtask
    subtask = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask", subtask);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<ds_mx_stdtask::Ascent>();
    underTest->init(config, compiler);
  }

  void TearDown() override {

  }

  std::shared_ptr<ds_mx::MxMockTask> subtask;
  std::shared_ptr<ds_mx_stdtask::Ascent> underTest;
  ds_nav_msgs::NavState state;
};

const char* AscentTest::JSON_STR = R"( {
  "type": "ascent",
  "depth_min": 100,
  "depth_hits_hit_threshold": 3,
  "ascent_rate_min" : 0.1,
  "ascent_rate_hits_hit_threshold": 5,
  "subtask_can_finish": true,
  "primary": "subtask"
})";

TEST_F(AscentTest, VerifyOnStart) {
  EXPECT_EQ(1, subtask->initCalls);
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
}

TEST_F(AscentTest, VerifyTick) {
  EXPECT_EQ(1, subtask->initCalls);
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
}

TEST_F(AscentTest, VerifyOnStop) {
  EXPECT_EQ(1, subtask->initCalls);
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  underTest->onStop(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(AscentTest, SubtaskSucceeds) {
  subtask->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
  subtask->tickReturns.push_back(ds_mx::TaskReturnCode::SUCCESS);

  EXPECT_EQ(1, subtask->initCalls);
  underTest->onStart(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
}

TEST_F(AscentTest, SubtaskFails) {
  subtask->tickReturns.push_back(ds_mx::TaskReturnCode::RUNNING);
  subtask->tickReturns.push_back(ds_mx::TaskReturnCode::FAILED);

  EXPECT_EQ(1, subtask->initCalls);
  underTest->onStart(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
}

TEST_F(AscentTest, displayTest) {
  boost::uuids::uuid uuid_res;

  // Setup the subtask's displays
  ds_mx_msgs::MissionElementDisplay displayElements;
  displayElements.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
  displayElements.label = "task label";
  displayElements.wellknowntext = "WKT_subtask";
  subtask->displayElement = displayElements;

  // Actually get the display
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  // The result should be simply the subtask's stuff
  size_t idx=0;
  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[idx].role);
  std::copy(result.elements[idx].task_uuid.begin(), result.elements[idx].task_uuid.end(), uuid_res.begin());
  EXPECT_EQ(subtask->getUuid(), uuid_res);
  EXPECT_EQ("WKT_subtask", result.elements[idx].wellknowntext);
  EXPECT_EQ("task label", result.elements[idx].label);
}

TEST_F(AscentTest, ValidateSuccessTest) {
  subtask->validateReturn = true;
  EXPECT_TRUE(underTest->validate());
}

TEST_F(AscentTest, ValidateFailTest) {
  subtask->validateReturn = false;
  EXPECT_FALSE(underTest->validate());
}

TEST_F(AscentTest, ValidateBadParam) {
  subtask->validateReturn = true;

  // set this to an invalid value
  underTest->getParameters().get<ds_mx::DoubleParam>("depth_min").set(-1);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(AscentTest, TimeoutTest) {
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  // set a overall-task-max timeout
  ds_mx::DoubleParam max_timeout = underTest->getParameters().get<ds_mx::DoubleParam>("max_timeout");
  max_timeout.set(5.0);

  // start the task.  This *should* set the timeout
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  EXPECT_EQ(state.header.stamp + ros::Duration(5.0), underTest->timeoutTime());

  // advance time 5 seconds, and amke sure timeout actually fires
  state.header.stamp += ros::Duration(5.001);
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

TEST_F(AscentTest, DepthReached) {
  double depth = 101;
  double dd = -1.0;
  double dt = 1.0;

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  // start the task
  state.down = depth;
  state.heave_w = dd/dt;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);


  // we'll advance at 1m/s, well above the minimum descent rate
  for (size_t i=0; i<4; i++) {
    state.header.stamp += ros::Duration(dt);
    state.down += dd;
    state.heave_w = dd/dt;

    if (i < 3) {
      EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    } else {
      EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
    }
  }
}

TEST_F(AscentTest, PoorAscentRate) {
  double depth = 1000;
  double dd = -0.09;
  double dt = 1.0;

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;

  // start the task
  state.down = depth;
  state.heave_w = dd/dt;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  EXPECT_EQ(0, underTest->getAscentRateHits());


  // we'll advance at 1m/s, well above the minimum descent rate
  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(3, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(4, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += 10.0*dd;
  state.heave_w = 10.0*dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(3, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(4, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
}

TEST_F(AscentTest, DisableRateCheck) {
  double depth = 1000;
  double dd = -0.09;
  double dt = 1.0;

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  underTest->getParameters().get<ds_mx::BoolParam>("disable_rate_check").setFromString("true");

  // start the task
  state.down = depth;
  state.heave_w = dd/dt;
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  EXPECT_EQ(0, underTest->getAscentRateHits());


  // we'll advance at 1m/s, well above the minimum descent rate
  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(0, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(0, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(0, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(0, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += 10.0*dd;
  state.heave_w = 10.0*dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(0, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(0, underTest->getAscentRateHits());

  state.header.stamp += ros::Duration(dt);
  state.down += dd;
  state.heave_w = dd/dt;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
}

} // namespace test
} // namespace ds_mx_stdtask