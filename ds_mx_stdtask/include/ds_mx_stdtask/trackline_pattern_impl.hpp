/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/10/19.
//


#include <limits>

#include <ds_mx_stdtask/trackline_pattern.h>
#include <ds_mxcore/ds_mxeventlog.h>

namespace ds_mx_stdtask {

template<typename T>
TracklinePattern<T>::~TracklinePattern() = default;

template<typename T>
TracklinePattern<T>::TracklinePattern() :
    backup_dist(params, "backup_dist", 10.0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    current_trackline(0, 0, 0, 0),
    local_shift_east(params, "local_shift_east", 0.0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    local_shift_north(params, "local_shift_north", 0.0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    global_shift_east(params, "global_shift_east", 0.0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    global_shift_north(params, "global_shift_north", 0.0, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL)
{
  // do nothing
}

template<typename T>
void TracklinePattern<T>::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxComplexTask::init(config, compiler);

  tracklineTemplate = loadSubtask(compiler, "trackline", config["trackline"]);

  updatePattern();
  resetState();
}

template<typename T>
void TracklinePattern<T>::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxComplexTask::onStart(state);

  // Start by drawing a trackline to the start of the survey pattern
  ROS_INFO_STREAM("Drawing start trackline from " << state.lon << " " << state.lat
                                                  << " to " << onstart_pt.x << " " << onstart_pt.y);
  running_line_to_start = true;
  updateTemplate(ds_trackline::Trackline(state.lon, state.lat, onstart_pt.x, onstart_pt.y));
  startSubtask(state, tracklineTemplate);
}

template<typename T>
void TracklinePattern<T>::onStop(const ds_nav_msgs::NavState &state) {
  ds_mx::MxComplexTask::onStop(state);

  // only compute a new starting position if we're on a real trackline
  if (!running_line_to_start && trackline_idx < pattern.numLines()) {
    auto track_frame = current_trackline.lonlat_to_trackframe(state.lon, state.lat);

    // back up the specified amount
    track_frame(0) -= backup_dist.get();
    track_frame(1) = 0; // pick a point directly on the trackline

    // convert to latlon
    auto new_starting_point = current_trackline.trackframe_to_lonlat(track_frame);
    onstart_pt.x = new_starting_point(0);
    onstart_pt.y = new_starting_point(1);
    ROS_INFO_STREAM("Setting resume point to " << onstart_pt.x << " " << onstart_pt.y);
  }
}

template<typename T>
void TracklinePattern<T>::parameterChanged(const ds_nav_msgs::NavState &state) {
  MX_LOG_EVENT_STREAM(this, MX_LOG_PARAM_CHANGED, "Regenerating trackline pattern");

  // if we're transiting to the start of something, or a resume position, or whatever,
  // just give up and go directly to the next trackline index.
  ds_trackline::Trackline::VectorEN track_frame;
  if (!isRunning() && trackline_idx == 0) {
    updatePattern();
    // we haven't started yet, move onstart to the new start point
    auto start = pattern[0].getStartLL();
    onstart_pt.x = start(0);
    onstart_pt.y = start(1);
    ROS_INFO_STREAM("Handling parameter change by going to first point of new pattern");
    return;

  } else if (running_line_to_start) {

    // if we're transiting to a resume point or start of trackline, take the current resume point
    // and move it with the survey RATHER than where we are

    // note that we have to use the position of the resume point in the first ACTUAL trackline;
    // current_trackline is the line to the resume point, so by definition it always ends at the
    // resume point!
    track_frame = pattern[trackline_idx].lonlat_to_trackframe(onstart_pt.x, onstart_pt.y);
    track_frame(1) = 0.0; // pick a point directly on the line
    ROS_INFO_STREAM("Currently running line to start, picking location based on location of old resume point");

  } else if (trackline_idx < pattern.numLines()) {
    // ok, so there's a conundrum for how to handle this.  Let's do the following.  We'll
    // treat the mission as moving, and we want to preserve our current along-track
    // position.  To do that, we'll draw a transit trackline from our CURRENT position
    // to the NEW
    track_frame = current_trackline.lonlat_to_trackframe(state.lon, state.lat);
    track_frame(1) = 0.0; // pick a point directly on the line
    if (track_frame(0) < -pattern[trackline_idx].getLength()) {
      // clip to the length of the line-- if we're before the start when this
      // happens, just go there
      ROS_INFO_STREAM("Clipping trackline position to start of trackline");
      track_frame(0) = -pattern[trackline_idx].getLength();
    }
    ROS_INFO_STREAM("Running trackline " <<trackline_idx <<", picking position based on distance-to-go.");
  }
  ROS_INFO_STREAM("Handling shift by going to line coords " <<track_frame(0) <<" along / " <<track_frame(1) <<" across");

  stopActiveSubtask(state);
  updatePattern();

  auto new_starting_point = pattern[trackline_idx].trackframe_to_lonlat(track_frame);
  onstart_pt.x = new_starting_point(0);
  onstart_pt.y = new_starting_point(1);
  ROS_INFO_STREAM("Handling shift by drawing line to " << onstart_pt.x << " " << onstart_pt.y);

  running_line_to_start = true;
  updateTemplate(ds_trackline::Trackline(state.lon, state.lat, onstart_pt.x, onstart_pt.y));
  startSubtask(state, tracklineTemplate);
}

template<typename T>
ds_mx::TaskReturnCode TracklinePattern<T>::subtaskDone(const ds_nav_msgs::NavState &state,
                                                       ds_mx::TaskReturnCode subtaskCode) {

  // increment the trackline ID
  if (running_line_to_start) {
    running_line_to_start = false;
  } else {
    trackline_idx++;
  }

  // check for done
  if (trackline_idx >= pattern.numLines()) {
    // we're out of tracklines! Done!
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_SUCCEED, "Finished last trackline");
    return ds_mx::TaskReturnCode::SUCCESS;
  }

  // stop the current trackline setup the next one, and start it
  stopActiveSubtask(state);
  updateTemplate(pattern.operator[](trackline_idx));
  startSubtask(state, tracklineTemplate);

  // we JUST started a new trackline, so keep running
  return ds_mx::TaskReturnCode::RUNNING;
}

template<typename T>
bool TracklinePattern<T>::validate() const {
  const auto &templateParams = tracklineTemplate->getParameters();

  bool rc = true;
  if (!(templateParams.has<ds_mx::GeoPointParam>("start_pt")
      && templateParams.has<ds_mx::GeoPointParam>("end_pt"))) {
    ROS_ERROR_STREAM("TracklinePattern task \"" << name << "\" of type \"" << type
                                                << "\" has a trackline template that does not support "
                                                << "either start_pt or end_pt"
                                                << " Template has type: " << tracklineTemplate->getType());

    rc = false;
  }
  return validateSubtasks() && rc;
}

template<typename T>
void TracklinePattern<T>::resetState() {
  trackline_idx = 0;
  auto onstart_ll = pattern.lines().front().getStartLL();
  onstart_pt.x = onstart_ll(0);
  onstart_pt.y = onstart_ll(1);
}

template<typename T>
const T &TracklinePattern<T>::getPattern() const {
  return pattern;
}

template<typename T>
void TracklinePattern<T>::updateTemplate(const ds_trackline::Trackline &line) {
  current_trackline = line;

  auto start_pt = line.getStartLL();
  auto end_pt = line.getEndLL();

  ROS_INFO_STREAM("TL: #" << trackline_idx << " ("
                          << start_pt(0) << "," << start_pt(1) << ") -> ("
                          << end_pt(0) << "," << end_pt(1) << ")");

  // update trackline parameters
  auto &templateParams = tracklineTemplate->getParameters();

  templateParams.get<ds_mx::GeoPointParam>("start_pt").set(ds_mx::GeoPoint(start_pt(0), start_pt(1)));
  templateParams.get<ds_mx::GeoPointParam>("end_pt").set(ds_mx::GeoPoint(end_pt(0), end_pt(1)));

  tracklineTemplate->resetTimeout(ros::Time::now());
}

template<typename T>
void TracklinePattern<T>::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  // This is a BIG one

  // first things first-- regenerate the pattern!  This will fix the issue
  // where parameter changes often don't show up in the GUI.
  updatePattern();

  // first, we'll need our trackline speed
  double speed = std::numeric_limits<double>::quiet_NaN();
  auto &templateParams = tracklineTemplate->getParameters();
  if (templateParams.has<ds_mx::DoubleParam>("speed")) {
    speed = templateParams.get<ds_mx::DoubleParam>("speed").get();
  } else {
    ROS_WARN_STREAM("Template parameter for survey " << name << " does not have speed!  Assuming 0...");
  }

  // draw a connecting trackline from the previous state estimate to the survey pattern start.
  const auto &pattern_start = pattern.getStartLL();
  const auto &pattern_end = pattern.getEndLL();

  ds_trackline::Trackline connectingLine(state.lon, state.lat, pattern_start(0), pattern_start(1));

  ds_mx_msgs::MissionElementDisplay conn;
  conn.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING;
  conn.wellknowntext = connectingLine.getWktLL();
  std::copy(uuid.begin(), uuid.end(), conn.task_uuid.begin());
  if (!std::isnan(speed)) {
    state.header.stamp += ros::Duration(connectingLine.getLength() / speed);
  }
  display.elements.push_back(conn);

  // iterate through all our tracklines now and add them too
  const auto &tracklines = pattern.lines();
  for (const auto &line : tracklines) {
    ds_mx_msgs::MissionElementDisplay l;
    l.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE;
    l.wellknowntext = line.getWktLL();
    if (!std::isnan(speed)) {
      state.header.stamp += ros::Duration(line.getLength() / speed);
    }
    std::copy(uuid.begin(), uuid.end(), l.task_uuid.begin());
    display.elements.push_back(l);
  }

  state.lon = pattern_end(0);
  state.lat = pattern_end(1);
}

template<typename T>
const ds_mx::GeoPoint &TracklinePattern<T>::getOnstartPt() const {
  return onstart_pt;
}

template<typename T>
double TracklinePattern<T>::getShiftEast() const {
  return local_shift_east.get() + global_shift_east.get();
}

template<typename T>
double TracklinePattern<T>::getShiftNorth() const {
  return local_shift_north.get() + global_shift_north.get();
}

template <typename T>
void TracklinePattern<T>::applyBaseParams() {
  //ROS_INFO_STREAM("Applying a shift of E: " <<getShiftEast() <<" x N:" <<getShiftNorth());
  pattern.setShift(getShiftEast(), getShiftNorth());
}

} // namespace ds_mx_stdtask

// DO NOT EXPORT! This class is useless by itself
// PLUGINLIB_EXPORT_CLASS(ds_mx_stdtask::TracklinePattern, ds_mx::MxTask)