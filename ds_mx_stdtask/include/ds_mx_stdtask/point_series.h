/**
namespace ds_mx_stdtask {
 in
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by tmjoyce 10/20/22
//

#ifndef DS_MX_STDTASK_POINTSERIES_H
#define DS_MX_STDTASK_POINTSERIES_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mxcore/ds_mxcompiler.h>
#include "trackline_pattern.h"
#include <ds_libtrackline/PatternSeriesLine.h>

namespace ds_mx_stdtask {

class PointSeries : public TracklinePattern<ds_trackline::PatternSeriesLine> {
 public:
  PointSeries();
  virtual ~PointSeries();

  // usual stuff is handled by the TracklinePattern base class

 protected:

  ds_mx::GeoPointListParam gpoint_list;

  // connect to underlying pattern
  void updatePattern() override;

};

} // namespace ds_mx_stdtask

#endif // DS_MX_STDTASK_POINTSERIES_H
