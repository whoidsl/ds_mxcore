/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/29/20.
//

#ifndef DS_MX_STDTASK_GOTO_H
#define DS_MX_STDTASK_GOTO_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_libtrackline/Trackline.h>

namespace ds_mx_stdtask {

class Goto : public ds_mx::MxComplexTask {

 public:
  explicit Goto();
  virtual ~Goto();
  void init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) override;

  void onStart(const ds_nav_msgs::NavState& state) override;
  bool validate() const override;

  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) override;

 protected:
  ds_mx::GeoPointParam center_pt;

  ds_mx::DoubleParam global_shift_east; /// <! The global shift east.  Typically linked to shared_param
  ds_mx::DoubleParam global_shift_north; /// <! The global shift north.  Typically linked to shared_param

  ds_mx::DoubleParam local_shift_east; /// <! The local shift east.  Left as a literal.
  ds_mx::DoubleParam local_shift_north;/// <! The local shift north.  Left as a literal.

  /// The currently-executing trackline
  ds_trackline::Trackline current_trackline;
  std::shared_ptr<ds_mx::MxTask> tracklineTemplate;

  /// Update the trackline template
  void updateTemplate(const ds_trackline::Trackline& trackline);
  ds_trackline::Trackline makeTrackline(const ds_nav_msgs::NavState& state) const;

  ds_mx::TaskReturnCode subtaskDone(const ds_nav_msgs::NavState &state,
                                    ds_mx::TaskReturnCode subtaskCode) override;
  void parameterChanged(const ds_nav_msgs::NavState &state) override;

  double getShiftNorth() const;
  double getShiftEast() const;
};

} // namespace ds_mx_stdtask

#endif // DS_MX_STDTASK_GOTO_H
