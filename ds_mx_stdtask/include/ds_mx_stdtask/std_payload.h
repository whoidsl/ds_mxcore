/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/8/19.
//

#ifndef DS_MX_STDTASK_STD_PAYLOAD_H
#define DS_MX_STDTASK_STD_PAYLOAD_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mx_msgs/StdPayloadCommand.h>

namespace ds_mx_stdtask {

/// \brief The Standard Payload uses the MxPayloadCmd message to provide
/// the typically-expected behavior for most payloads.  This behavior is as
/// follows:
///
/// 1). onStart: Send the START command along with the current set of
///              sensor parameter key/value pairs
/// 2). when a parameter changes: Send the PARAM command along with the current set of
///              key/value pairs
/// 3). onStop: Send the STOP commmand
///
/// Individual users are expected to override this Task with their own name and
/// init_ros function to setup the correct output topic.
class StdPayload : public ds_mx::MxPayloadTask {
 public:
  explicit StdPayload();
  virtual ~StdPayload();

  /// \brief The StdPayload init is the same as the standard payload one,
  /// but it also links the subtask's parameters to ours.
  void init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) override;

  /// \brief The initialization function is pretty much the one thing that
  /// absolutely must be overriden by any implementation
  void init_ros(ros::NodeHandle &nh) override = 0;

  void onStart(const ds_nav_msgs::NavState& state) override;
  void onStop(const ds_nav_msgs::NavState& state) override;

  bool validate() const override = 0;

 protected:
  /// \brief Set of sensor parameters
  ds_mx::KeyValueListParam payload_params;

  /// \brief Only configure stuff, don't do any of the start/stop semantics
  ds_mx::BoolParam config_only;

  /// \brief Publisher for standard commands; MUST be initialized
  /// in the init_ros call overridden by each subclass.
  ros::Publisher cmd_pub;

  void parameterChanged(const ds_nav_msgs::NavState &state) override;

  static void fillInParams(ds_mx_msgs::StdPayloadCommand& msg, const ds_mx::KeyValueListParam& p);

};

}
#endif //DS_MX_STDTASK_STD_PAYLOAD_H
