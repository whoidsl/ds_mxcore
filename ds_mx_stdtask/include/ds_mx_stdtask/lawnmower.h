/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/28/19.
//

#ifndef DS_MX_STDTASK_LAWNMOWER_H
#define DS_MX_STDTASK_LAWNMOWER_H

#include "trackline_pattern.h"
#include <ds_libtrackline/PatternLawnmow.h>


namespace ds_mx_stdtask {

class Lawnmower : public TracklinePattern<ds_trackline::PatternLawnmow> {
 public:
  Lawnmower();
  virtual ~Lawnmower();

  // all the usual stuff is handled by the TracklinePattern parent class

 protected:
  // we still have to define our own parameters
  ds_mx::GeoPointParam center;
  ds_mx::DoubleParam survey_heading;
  ds_mx::DoubleParam width;
  ds_mx::DoubleParam length;
  ds_mx::DoubleParam spacing;
  ds_mx::DoubleParam cross_offset;
  ds_mx::BoolParam cross_skip;
  ds_mx::BoolParam flip_width;
  ds_mx::BoolParam flip_length;

  // and we have to figure out how to use those parameters to rebuild
  // the line plan
  void updatePattern() override;
};

}

#endif //DS_MX_STDTASK_LAWNMOWER_H