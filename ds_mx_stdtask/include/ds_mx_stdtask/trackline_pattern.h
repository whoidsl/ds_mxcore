/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/10/19.
//

#ifndef DS_MX_STDTASK_TRACKLINE_PATTERN_H
#define DS_MX_STDTASK_TRACKLINE_PATTERN_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mxcore/ds_mxcompiler.h>
#include <ds_libtrackline/AbstractPattern.h>

namespace ds_mx_stdtask {

/// A very common Sentry task is to generate a set of tracklines based on some parameters
/// and then execute them.  This simplifies that by collecting all the MX-related start/stop
/// stuff in one place.  Users need only provide a trackline generation class and some
/// wiring to make the parameters work.
///
/// Any class that implements ds_trackline::AbstractPattern will work, although its not actually required to
/// inherit from that class, because templates.
///
/// \tparam T
template<typename T>
class TracklinePattern : public ds_mx::MxComplexTask {

 public:
  TracklinePattern();
  virtual ~TracklinePattern();

  void init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) override;

  void onStart(const ds_nav_msgs::NavState &state) override;
  void onStop(const ds_nav_msgs::NavState &state) override;
  bool validate() const override;

  void resetState() override;
  void getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) override;

  // really only used for testing
  const ds_mx::GeoPoint &getOnstartPt() const;

  const T& getPattern() const;

 protected:
  /// The trackline pattern to follow.  Must be specified by base class.
  T pattern;

  /// The distance to back up along the trackline when resuming
  ds_mx::DoubleParam backup_dist;

  /// these are the longitude/latitude to go to when starting
  /// this task.  They are updated in the onStop callback so we can
  /// resume correctly.
  ds_mx::GeoPoint onstart_pt;

  /// The currently-executing trackline
  ds_trackline::Trackline current_trackline;

  ds_mx::DoubleParam global_shift_east; /// <! The global shift east.  Typically linked to shared_param
  ds_mx::DoubleParam global_shift_north; /// <! The global shift north.  Typically linked to shared_param

  ds_mx::DoubleParam local_shift_east; /// <! The local shift east.  Left as a literal.
  ds_mx::DoubleParam local_shift_north;/// <! The local shift north.  Left as a literal.

  /// This is a trackline (probably primitive) that is used as a template to run
  /// the however-many tracklines in this file
  std::shared_ptr<ds_mx::MxTask> tracklineTemplate;

  bool running_line_to_start;
  size_t trackline_idx;

  /// updates the pattern to use new parameters
  virtual void updatePattern() = 0;

  void applyBaseParams();

  /// Update the trackline template
  virtual void updateTemplate(const ds_trackline::Trackline &line);

  ds_mx::TaskReturnCode subtaskDone(const ds_nav_msgs::NavState &state,
                                    ds_mx::TaskReturnCode subtaskCode) override;
  void parameterChanged(const ds_nav_msgs::NavState &state) override;

  double getShiftNorth() const;
  double getShiftEast() const;
};

} // namespace ds_mx_stdtask

// include our implementation because this is a templated class
#include "trackline_pattern_impl.hpp"

#endif //DS_MX_STDTASK_TRACKLINE_PATTERN_H
