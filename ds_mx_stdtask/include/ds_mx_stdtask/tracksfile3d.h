/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//

#ifndef DS_MX_STDTASK_TRACKSFILE3D_H
#define DS_MX_STDTASK_TRACKSFILE3D_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_mxcore/ds_mxcompiler.h>
#include "trackline_pattern.h"
#include <ds_libtrackline/PatternTracksfile.h>

namespace ds_mx_stdtask {

/// \brief This is a file for specifying a set of 3D waypoints.  However, it was kinda
/// thrown together to test a very specfic thing, so it wasn't properly validated.  In particular,
/// the halt/resume features may not make a whole lot (or ANY) sense with regard to depth.
/// Take it, leave it, or fix it.
class Tracksfile3d : public TracklinePattern<ds_trackline::PatternTracksfile> {
 public:
  Tracksfile3d();
  virtual ~Tracksfile3d();

  // usual stuff is handled by the TracklinePattern base class
  bool validate() const override; // except for validate

 protected:
  // define our usual paramters
  ds_mx::GeoPointParam center_pt;
  ds_mx::StringParam point_list;

  // connect to underlying pattern
  void updatePattern() override;

  // override the trackline update code to add depth
  void updateTemplate(const ds_trackline::Trackline &line) override;

  // we'll need to maintain some of our own state
  std::vector<double> depth_waypoints;
};

} // namespace ds_mx_stdtask

#endif // DS_MX_STDTASK_TRACKSFILE3D_H
