# Coordinate Systems for LibTrackline

Libtrackline uses a few coordinates systems, and they all need documentation.

# Coordinates for Tracklines

Tracklines use three different coordinate systems.  The first is longitude / latitude,
expressed in degrees for config files but radians internally. The second is a projected
coordinate system.  We assume all projected coordinate systems are local projections around
a specific origin; more on this later.  The projected coordinate system is aligned with North
at its origin, so its expressed in terms of "Easting" and "Northing" (meters).  The final
coordinate system are "trackline coordinates."  Trackline coordinates replace the old
"oline" and "eline" functions that return distance off the trackline and the distance to the end
of the line.

## Trackline Coordinate Frame

Trackline coordinates express a position in terms of several values that are necessary when
controlling on the trackline.  Basically, this is just a fancy way of saying "we want easy access
to distance-from-line and distance-from-end-of-line".  By using some linear algebra trickery
instead of the old trigonometric math, however, we can get the answer without using trigonometry.
This avoids some error-prone and inefficient calls to trig functions.

The key insight is this: we want distance ALONG the trackline, and distance ACROSS the trackline,
so given a point we can get that by subtracting the endpoint and rotating to make one axis
parallel to the direction of the line.

It's very easy to get a unit vector for the direction of the line.  If we rotate that unit
vector 90 degrees, we get a vector that's perpendicular to the trackline.  Given an easting/northing
point in the projected coordinate system, distance-to-the-end of the trackline is just the part of
the vector from the point in question to the end of the trackline that's parallel to the first
unit vector.  The distance off the trackline is the other part of that vector, the part perpendicular
to the trackline.  In mathematical terms, the unit vector along the trackline and the
vector perpendicular to it form the orthonormal basis for the trackline coordinate frame.  All
we have to do to convert easting/northing to alongtrack/acrosstrack is to subtract the end-of-line
coordinate from the point being converted and rotate so one axis is parallel to the trackline.

There's a lovely proof that we can do this for any trackline direction involving rotation
matrices and so on, but to do the math, all you have to do is the following:

 * First, get a unit vector for the direction of the trackline.  (end - start).normalized() works.
 * Next, build a basis matrix-- this is just the along-track unit vector and a vector rotated
   90 deg to get the across-track component.  The first column is just the unit vector, the second
   column is the unit vector flipped with the sign of the second component inverted, or:
```
unitvec = [compute as before.  2-element column-vector, let's say 0-indexed]
basis = [unitvec(0),  unitvec(1);...
         unitvec(1), -unitvec(0)];
```
 * That basis matrix IS the rotation matrix to put a point into trackline coordinates.  Or,
   to convert "point" to alongtrack / acrosstrack coordinates:
```
[alongtrack; acrosstrack] = basis * (point - end_point)
```

The first element in the resulting column vector is the distance along track.  Positive
coordinates occur AFTER passing the endpoint, negative numbers occur BEFORE passing the endpoint.
The second element is the distance acrosstrack.  Positive is to the right of the line, negative
is to the left.  This is consistent with an NED coordinate frame.

