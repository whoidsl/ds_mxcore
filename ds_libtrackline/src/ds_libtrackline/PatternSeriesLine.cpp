/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 10/24/22.
//

#include "ds_libtrackline/PatternSeriesLine.h"

namespace ds_trackline {


PatternSeriesLine::PatternSeriesLine() : PatternSeriesLine(WaypointListT{WaypointT(0,0)}) {
}



PatternSeriesLine::PatternSeriesLine(const WaypointListT& waypoints)
    : waypoints_(waypoints) {
  generateLines();
}

/// Get the longitude of the the line's starting point, in degrees
/// \return The line's starting longitude in degrees
double PatternSeriesLine::getStartLongitude() const {
  return waypoints_.front().x();
}

/// Get the latitude of the the line's starting point, in degrees
/// \return The line's starting latitude in degrees
double PatternSeriesLine::getStartLatitude() const {
  return waypoints_.front().y();
}

/// Get the longitude of the the line's end point, in degrees
/// The end point is used to set the line's projection
/// \return The line's end longitude in degrees
double PatternSeriesLine::getEndLongitude() const {
  return waypoints_.back().x();
}

/// Get the latitude of the the line's end point, in degrees
/// The end point is used to set the line's projection
/// \return The line's end latitude in degrees
double PatternSeriesLine::getEndLatitude() const {
  return waypoints_.back().y();
}

void PatternSeriesLine::setWaypoints(const WaypointListT& w) {
  waypoints_ = w;
  generateLines();
}

void PatternSeriesLine::generateLines() {
  // if required, update the projection
  Projection::VectorLL origin = proj.getOrigin();
  auto end_longitude = getEndLongitude();
  auto end_latitude = getEndLatitude();

  if (origin(0) != end_longitude || origin(1) != end_latitude) {
    proj = Projection(end_longitude, end_latitude);
  }
 
  Eigen::Vector2d shift(shift_east, shift_north);
  tracklines.clear();

  if (waypoints_.size() == 1) {
    // 0 length TL. Shouldn't really happen. but it could...
    Eigen::Vector2d start_pt =
        proj.lonlat2projected(Projection::VectorLL(waypoints_[0]));
    Eigen::Vector2d end_pt = start_pt;
    Eigen::Vector2d shift(shift_east, shift_north);

    start_pt += shift;
    end_pt += shift;

    tracklines.push_back(Trackline(start_pt, end_pt, proj));
  } else {
    for (size_t i = 1; i < waypoints_.size(); i++) {
      // Eigen::Vector2d start_pt = waypoints_[i-1] + shift;
      // Eigen::Vector2d end_pt = waypoints_[i] + shift;

      Eigen::Vector2d start_pt =
          proj.lonlat2projected(Projection::VectorLL(waypoints_[i - 1]));
      Eigen::Vector2d end_pt =
          proj.lonlat2projected(Projection::VectorLL(waypoints_[i]));
      Eigen::Vector2d shift(shift_east, shift_north);

      start_pt += shift;
      end_pt += shift;

      tracklines.push_back(Trackline(start_pt, end_pt, proj));
    }
  }
}

} // namespace ds_trackline