/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#include "ds_libtrackline/PatternNavLegs.h"
#include "ds_libtrackline/TransformUtils.h"

namespace ds_trackline {

PatternNavLegs::PatternNavLegs() : PatternNavLegs(0,0,0,0,false) {};

PatternNavLegs::PatternNavLegs(double _center_lon, double _center_lat,
                               double _heading, double _length, bool _right_first) :
                               center_longitude(_center_lon),
                               center_latitude(_center_lat),
                               heading(_heading),
                               line_length(_length),
                               right_first(_right_first) {
  generateLines();
}

/// Get the longitude of the center of this pattern, in degrees
/// \return Longitude of pattern center, in degrees
double PatternNavLegs::getCenterLongitude() const {
  return center_longitude;
}

/// Set the longitude of this pattern, in degrees
/// \param center_longitude The longitude of the center of this pattern, in degrees
void PatternNavLegs::setCenterLongitude(double _lon) {
  PatternNavLegs::center_longitude = _lon;
  generateLines();
}

/// Get the latitude of the center of this pattern, in degrees
/// \return Latitude of pattern center, in degrees
double PatternNavLegs::getCenterLatitude() const {
  return center_latitude;
}

/// Set the latitude of this pattern, in degrees
/// \param center_latitude The latitude of the center of this pattern, in degrees
void PatternNavLegs::setCenterLatitude(double _lat) {
  PatternNavLegs::center_latitude = _lat;
  generateLines();
}

/// Set the center position of this pattern
/// \param center_longitude The longitude of the center of this pattern, in degrees
/// \param center_latitude The latitude of the center of this pattern, in degrees
void PatternNavLegs::setCenterPosition(double _lon, double _lat) {
  PatternNavLegs::center_longitude = _lon;
  PatternNavLegs::center_latitude = _lat;
  generateLines();
}

double PatternNavLegs::getHeading() const {
  return heading;
}
void PatternNavLegs::setHeading(double _heading) {
  PatternNavLegs::heading = _heading;
  generateLines();
}

double PatternNavLegs::getLineLength() const {
  return line_length;
}
void PatternNavLegs::setLineLength(double _len) {
  PatternNavLegs::line_length = _len;
  generateLines();
}

bool PatternNavLegs::getRightFirst() const {
  return right_first;
}
void PatternNavLegs::setRightFirst(bool _right_first) {
  PatternNavLegs::right_first = _right_first;
}

void PatternNavLegs::generateLines() {

  // if required, update the projection
  Projection::VectorLL origin = proj.getOrigin();
  if (origin(0) != center_longitude || origin(1) != center_latitude) {
    proj = Projection(center_longitude, center_latitude);
  }

  // Using STL containers with Eigen sucks, because alignment.  See:
  // https://eigen.tuxfamily.org/dox/group__TopicStlContainers.html
  std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d> > line_pts;

  // we'll start by constructing points as fractions of a line length;
  // so 1.0 == line_length
  line_pts.push_back(Eigen::Vector2d(-1.5,0));
  line_pts.push_back(Eigen::Vector2d(-0.5,0));
  line_pts.push_back(Eigen::Vector2d(-0.5,1));
  line_pts.push_back(Eigen::Vector2d( 0.5,1));
  line_pts.push_back(Eigen::Vector2d( 0.5,0));
  line_pts.push_back(Eigen::Vector2d( 1.5,0));

  // Prepare the transform to rotate the north-running pattern
  // to the requested orientation
  Eigen::Matrix2d tform = makeAlongtrackTransform(heading, false, !right_first);

  // pre-compute our shift
  Eigen::Vector2d shift(shift_east, shift_north);

  // actually compute the tracklines
  tracklines.clear();
  for (size_t i=1; i<line_pts.size(); i++) {
    Eigen::Vector2d start_pt = line_length * tform * line_pts[i-1] + shift;
    Eigen::Vector2d end_pt = line_length * tform * line_pts[i] + shift;

    tracklines.push_back(Trackline(start_pt, end_pt, proj));
  }
}

} // namespace ds_trackline