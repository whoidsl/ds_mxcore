/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "ds_libtrackline/TransformUtils.h"

#define _USE_MATH_DEFINES
#include <cmath>

namespace ds_trackline {

/// Make a transform for a track pattern.
/// This allows patterns to be constructed in a standard
/// coordinate system and then rotated / flipped
/// to meet the user's needs.
///
/// For example, the lawnmower pattern builds all surveys as if the
/// nominal trackline heading is 0 deg, then rotates to make the
/// trackline heading what the user requested.
///
/// \param heading The nominal heading of the along-track direction, in degrees
/// \param flip_along Flip in the along-track direction by applying
/// a reflection about the across-track axis
/// \param flip_across Flip in the across-track direction by applying
/// a reflection about the along-track axis
/// \return The transform matrix from along/across track to east/north
Eigen::Matrix2d makeAlongtrackTransform(double heading,
                                        bool flip_along,
                                        bool flip_across) {

  Eigen::Matrix2d basis;
  // This syntax is ROW major, so:
  // [ sin(hdg)  cos(hdg) ]
  // [ cos(hdg) -sin(hdg) ]
  //
  // All vectors will then be:
  // [ easting northing ]^T
  // or
  // [ along across ]^T
  // to match trackline
  basis << sin(heading * M_PI / 180.0), cos(heading * M_PI / 180.0),
      cos(heading * M_PI / 180.0), -sin(heading * M_PI / 180.0);

  Eigen::Matrix2d flips = Eigen::Matrix2d::Identity();

  // we implement the flips by adding them to the transform
  if (flip_along) {
    flips(0,0) = -1.0;
  }

  if (flip_across) {
    flips(1,1) = -1.0;
  }

  return basis*flips;

}
} // namespace ds_trackline
