/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/10/19.
//

#include "ds_libtrackline/AbstractPattern.h"

namespace ds_trackline {

/// Default constructor forms default projection about 0,0.  You'll want to override this.
AbstractPattern::AbstractPattern() : AbstractPattern(0,0) {}

/// Constructor to specify origin of the pattern
AbstractPattern::AbstractPattern(double lon_deg, double lat_deg) :
    proj(lon_deg, lat_deg),
    shift_east(0),
    shift_north(0) {}

/// Default destructor made pure-virtual to force subclassing
AbstractPattern::~AbstractPattern() {}

/// Accessor for the underlying set of tracklines
/// \return The underlying array of tracklines to run in the order given
const std::vector<Trackline> &AbstractPattern::lines() const {
  return tracklines;
}

/// Accessor for the number of trackline objects in this survey
/// \return The number of trackline objects in this survey
size_t AbstractPattern::numLines() const {
  return tracklines.size();
}

/// Accessor for a specific trackline, by index.  Overruns are handled by
/// the underlying std::vector
/// \param i The index of the trackline to get, from zero
/// \return A const reference to the requested trackline
const Trackline &AbstractPattern::operator[](size_t i) const {
  return tracklines[i];
}

/// Accessor for a specific trackline, by index.  Overruns are handled by
/// the underlying std::vector
/// \param i The index of the trackline to get, from zero
/// \return A const reference to the requested trackline
Trackline &AbstractPattern::operator[](size_t i) {
  return tracklines[i];
}

/// Accessor for this survey's internal projection
/// \return The projection used by all tracklines in this survey
const Projection &AbstractPattern::getProjection() const {
  return proj;
}

/// Get the start position for this survey as a lat/lon, including crossing line
Trackline::VectorLL AbstractPattern::getStartLL() const {
  return tracklines.front().getStartLL();
}

/// Get the end position for this survey as a lat/lon, including crossing line
Trackline::VectorLL AbstractPattern::getEndLL() const {
  return tracklines.back().getEndLL();
}

/// Return the total length of a trackline distances, in meters
/// \return Sum total of all trackline lengths
double AbstractPattern::getTotalLength() const {
  double sum;
  for (const auto &line : tracklines) {
    sum += line.getLength();
  }
  return sum;
}


/// Get the current amount this block is shifted east
/// \return The amount this block is shifted east (positive) or west (negative) in meters
double AbstractPattern::getShiftEast() const {
  return shift_east;
}

/// Set the current amount this block is shifted east
/// \param shift_east The amount to shift this block east (+) or west (-), in meters
void AbstractPattern::setShiftEast(double shift_east) {
  AbstractPattern::shift_east = shift_east;
  generateLines();
}

/// Get the current amount this block is shifted north
/// \return The amount this block is shifted north (positive) or south (negative) in meters
double AbstractPattern::getShiftNorth() const {
  return shift_north;
}

/// Set the current amount this block is shifted north
/// \param shift_north The amount to shift this block north (+) or south (-), in meters
void AbstractPattern::setShiftNorth(double shift_north) {
  AbstractPattern::shift_north = shift_north;
  generateLines();
}

/// Set the current shift for this block, in meters
/// \param shift_east The amount to shift this block east (+) or west (-), in meters
/// \param shift_north The amount to shift this block north (+) or south (-), in meters
void AbstractPattern::setShift(double shift_east, double shift_north) {
  AbstractPattern::shift_east = shift_east;
  AbstractPattern::shift_north = shift_north;
  generateLines();
}

} // namespace tarckline