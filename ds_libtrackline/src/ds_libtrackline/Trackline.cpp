/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/20/18.
//

#include "ds_libtrackline/Trackline.h"
#include "ds_libtrackline/WktUtil.h"

namespace ds_trackline {

/// Create a trackline from start and end lat/lon positions
///
/// This will auto-set the origin for this line to the end
///
/// \param start_lon Longitude for the start point, in degrees
/// \param start_lat Latitude for the start point, in degrees
/// \param end_lon Longitude for the end point, in degrees
/// \param end_lat Latitude for the start point, in degrees
Trackline::Trackline(double start_lon, double start_lat, double end_lon, double end_lat)
    : proj(end_lon, end_lat) {

  VectorEN line_start = proj.lonlat2projected(VectorLL(start_lon, start_lat));
  VectorEN line_end = proj.lonlat2projected(VectorLL(end_lon, end_lat));

  // Unless given otherwise, always use the END point as
  // the trackline origin
  setLineEN(line_start, line_end);
}

/// Create a trackline from start and end east/north positions in some
/// previously-specified coordinate frame
///
/// \param start_east Start point east coordinates, in meters
/// \param start_north Start point north coordinates, in meters
/// \param end_east End point east coordinates, in meters
/// \param end_north End point north coordinates, in meters
/// \param _proj The projection to use for these easting/northing coordinates
Trackline::Trackline(double start_east,
                     double start_north,
                     double end_east,
                     double end_north,
                     const Projection& _proj) : proj(_proj) {
  setLineEN(VectorEN(start_east, start_north),
      VectorEN(end_east, end_north));
}

/// Create a trackline from lat/lon start and end points
/// The end point will automatically be used as the origin
/// \param start_pt Start longitude/latitude, in degrees
/// \param end_pt End longitude/latitude, in degrees
Trackline::Trackline(const VectorLL &start_pt, const VectorLL &end_pt) : proj(end_pt) {
  setLineEN(proj.lonlat2projected(start_pt), proj.lonlat2projected(end_pt));
}

/// Create a trackline from start and end points in easting/northing meters
/// using some previously-defined coordinate frame
/// \param start_pt Trackline start point, in east/north meters
/// \param end_pt Trackline start point, in east/north meters
/// \param origin Origin of the coordinate system used to parameterize these tracklines
Trackline::Trackline(const VectorEN &start_pt, const VectorEN &end_pt, const Projection& _proj)
 : proj(_proj) {
  setLineEN(start_pt, end_pt);
}

/// Convert a lat/lon position into this trackline's track coordinate frame
/// \param lon Longitude to convert, in degrees
/// \param lat Latitude to convert, in degrees
/// \return Returns the position of the given lon/lat in this
/// trackline's rectangular coordinate system.  First element is "along track" (+ means PAST,
/// - means TO GO), second element is "across track" (+ means right, - means left)
Trackline::VectorEN Trackline::lonlat_to_trackframe(double lon, double lat) const {
  return en_to_trackframe(proj.lonlat2projected(VectorLL(lon, lat)));
}

/// Convert a lat/lon position into this trackline's track coordinate frame
/// \param lonlat The longitude/latitude to convert, in degrees
/// \return Returns the position of the given lon/lat in this
/// trackline's rectangular coordinate system.  First element is "along track" (+ means PAST,
/// - means TO GO), second element is "across track" (+ means right, - means left)
Trackline::VectorEN Trackline::lonlat_to_trackframe(const VectorLL &lonlat) const {
  return en_to_trackframe(proj.lonlat2projected(lonlat));
}

/// Convert an east/north position that has the same origin as this trackline into this
/// track's coordinate frame
/// \param easting Easting, meters.  Must use same projection as this trackline.
/// \param northing Northing, meters.  Must use same projection as this trackline.
/// \return Returns the given projected position rotated into this track's coordinate frame.
/// First element is "along track" (+ means PAST,
/// - means TO GO), second element is "across track" (+ means right, - means left)
Trackline::VectorEN Trackline::en_to_trackframe(double easting, double northing) const {
  return en_to_trackframe(VectorEN(easting, northing));
}

/// Convert an east/north position that has the same origin as this trackline into this
/// track's coordinate frame
/// \param en Northing and Easting, meters.  Must use same projection as this trackline.
/// \return Returns the given projected position rotated into this track's coordinate frame.
/// First element is "along track" (+ means PAST,
/// - means TO GO), second element is "across track" (+ means right, - means left)
Trackline::VectorEN Trackline::en_to_trackframe(const VectorEN &en) const {
  return basis * (en - end_pt);
}

/// Convert a trackframe position to latitude/longitude
/// \param along
/// \param across
Trackline::VectorLL Trackline::trackframe_to_lonlat(double along, double across) const {
  return proj.projected2lonlat(trackframe_to_en(VectorEN(along, across)));
}

Trackline::VectorLL Trackline::trackframe_to_lonlat(const VectorEN &along_across) const {
  return proj.projected2lonlat(trackframe_to_en(along_across));
}

/// Convert a trackframe position to lat/lon
Trackline::VectorEN Trackline::trackframe_to_en(double along, double across) const {
  return trackframe_to_en(VectorEN(along, across));
}

Trackline::VectorEN Trackline::trackframe_to_en(const VectorEN &along_across) const {
  return basis.transpose()*along_across + end_pt;
}

/// Get the origin of this trackline's projection
/// \return The projection object used by this trackline
const Projection& Trackline::getProjection() const {
  return proj;
}

/// Get this trackline's starting point
/// \return The start position of this trackline, in degree longitude/latitude
Trackline::VectorLL Trackline::getStartLL() const {
  return proj.projected2lonlat(start_pt);
}

/// Get this trackline's end point
/// \return The end position of this trackline, in degree longitude/latitude
Trackline::VectorLL Trackline::getEndLL() const {
  return proj.projected2lonlat(end_pt);
}

/// Get this trackline's starting point in the line's projected coordinate frame
/// \return The start position of this trackline, in easting/northing meters
const Trackline::VectorEN& Trackline::getStartEN() const {
  return start_pt;
}

/// Get this trackline's end point in the line's projected coordinate frame
/// \return The end position of this trackline, in easting/northing meters
const Trackline::VectorEN& Trackline::getEndEN() const {
 return end_pt;
}

/// Get this trackline's course in radians
/// \return The nominal course of this trackline, in radians
double Trackline::getCourseRad() const {
  return course;
}

/// Get this trackline's course in degrees
/// \return The nominal course of this trackline, in degrees
double Trackline::getCourseDeg() const {
  return course * 180.0 / M_PI;
}

/// Get the length of this trackline, in meters
/// \return The nominal length of this trackline, in meters
double Trackline::getLength() const {
  VectorEN displacement = end_pt - start_pt;
  return displacement.norm();
}

/// Setup this trackline based on it's start/end positions.
/// Used internally during setup.  Not intended for reuse.
/// \param line_start Trackline start position in easting/northing, meters
/// \param line_end Trackline end position in easting/northing, meters
void Trackline::setLineEN(const VectorEN& line_start, const VectorEN& line_end) {

  // first, bookkeeping
  start_pt = line_start;
  end_pt = line_end;

  // next, we need to get our along-the-line unitvector
  VectorEN unitvec = end_pt - start_pt;
  unitvec.normalize();

  // Finally, build our basis matrix
  // For a unit vector [u(1); u(2)], the final matrix we want is:
  //
  // [ u(0)  u(1) ]
  // [ u(1) -u(0) ]
  //
  // The comma initializer syntax is ROW-MAJOR
  basis << unitvec(0), unitvec(1), unitvec(1), -(unitvec(0));

  // pre-cache the nominal course for this trackline
  course = atan2(unitvec(0), unitvec(1));
  if (course < 0) {
    course += (2.0*M_PI);
  }
}

std::string Trackline::getWktLL() const {
  VectorLL start = getStartLL();
  VectorLL end = getEndLL();
  return wktLineLL(start(0), start(1), end(0), end(1));
}


}