/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#include "ds_libtrackline/PatternLine.h"

namespace ds_trackline {

PatternLine::PatternLine() : PatternLine(0,0,0,0) {}

PatternLine::PatternLine(double _start_lon, double _start_lat, double _end_lon, double _end_lat) :
    start_longitude(_start_lon),
    start_latitude(_start_lat),
    end_longitude(_end_lon),
    end_latitude(_end_lat)
{
  generateLines();
}

/// Get the longitude of the the line's starting point, in degrees
/// \return The line's starting longitude in degrees
double PatternLine::getStartLongitude() const {
  return start_longitude;
}

/// Get the latitude of the the line's starting point, in degrees
/// \return The line's starting latitude in degrees
double PatternLine::getStartLatitude() const {
  return start_latitude;
}

/// Set the longitude of the starting point for this line
/// \param lon The starting longitude, in degrees
void PatternLine::setStartLongitude(double lon) {
  PatternLine::start_longitude = lon;
  generateLines();
}

/// Set the latitude of the starting point for this line
/// \param lon The starting latitude, in degrees
void PatternLine::setStartLatitude(double lat) {
  PatternLine::start_latitude = lat;
  generateLines();
}

/// Set the starting point for this line
/// \param lon Starting longitude for this line, in degrees
/// \param lat Starting latitude for this line, in degrees
void PatternLine::setStartPoint(double lon, double lat) {
  PatternLine::start_longitude = lon;
  PatternLine::start_latitude = lat;
  generateLines();
}

/// Get the longitude of the the line's end point, in degrees
/// The end point is used to set the line's projection
/// \return The line's end longitude in degrees
double PatternLine::getEndLongitude() const {
  return end_longitude;
}

/// Get the latitude of the the line's end point, in degrees
/// The end point is used to set the line's projection
/// \return The line's end latitude in degrees
double PatternLine::getEndLatitude() const {
  return end_latitude;
}

/// Set the longitude of the end point for this line
/// \param lon The end point longitude, in degrees
void PatternLine::setEndLongitude(double lon) {
  PatternLine::end_longitude = lon;
  generateLines();
}

/// Set the latitude of the end point for this line
/// \param lat The end point latitude, in degrees
void PatternLine::setEndLatitude(double lat) {
  PatternLine::end_latitude = lat;
  generateLines();
}

/// Set the position of this line's end point
/// \param lon The end point longitude, in degrees
/// \param lat The end point latitude, in degrees
void PatternLine::setEndPoint(double lon, double lat) {
  PatternLine::end_longitude = lon;
  PatternLine::end_latitude = lat;
  generateLines();
}

void PatternLine::generateLines() {
  // if required, update the projection
  Projection::VectorLL origin = proj.getOrigin();
  if (origin(0) != end_longitude || origin(1) != end_latitude) {
    proj = Projection(end_longitude, end_latitude);
  }

  Eigen::Vector2d start_pt = proj.lonlat2projected(Projection::VectorLL(start_longitude, start_latitude));
  Eigen::Vector2d end_pt = proj.lonlat2projected(Projection::VectorLL(end_longitude, end_latitude));
  Eigen::Vector2d shift(shift_east, shift_north);

  start_pt += shift;
  end_pt += shift;

  tracklines.clear();
  tracklines.push_back(ds_trackline::Trackline(start_pt, end_pt, proj));
}

}