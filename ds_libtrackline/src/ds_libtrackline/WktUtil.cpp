/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_libtrackline/WktUtil.h"

#include <sstream>
#include <iomanip>
#include <vector>

namespace ds_trackline {

std::string wktPointLL(double lon, double lat) {
  std::stringstream ret;

  // ensure we always print enough digits
  ret.setf(std::ios::fixed, std::ios::floatfield);
  ret.precision(9); // mm precision; way more than we need

  ret <<"POINT (" <<lon <<" " <<lat <<")";

  return ret.str();
}

std::string wktLineLL(double lon_start, double lat_start, double lon_end, double lat_end) {
  std::stringstream ret;

  // ensure we always print enough digits
  ret.setf(std::ios::fixed, std::ios::floatfield);
  ret.precision(9); // mm precision; way more than we need

  ret << "LINESTRING (" << lon_start << " " << lat_start << ", "
      << lon_end << " " << lat_end << ")";

  return ret.str();
}

std::string wktLineStringLL(std::vector<std::pair<double,double>> points) {
  std::stringstream ret;

  // ensure we always print enough digits
  ret.setf(std::ios::fixed, std::ios::floatfield);
  ret.precision(9); // mm precision; way more than we need
  
  ret << "LINESTRING (";

  for(auto it = points.begin(); it != points.end(); ++it){ 
    ret << it->first << " " << it->second;
    if (it != points.end() - 1) {
      ret << ",";
    } 
  }
  ret << ")";

  return ret.str();
}

}