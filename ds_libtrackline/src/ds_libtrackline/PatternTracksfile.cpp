/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//

#include "ds_libtrackline/PatternTracksfile.h"
#include "ds_libtrackline/TransformUtils.h"

namespace ds_trackline {

PatternTracksfile::PatternTracksfile() : PatternTracksfile(0, 0, {}) {}
PatternTracksfile:: PatternTracksfile(double center_lon, double center_lat, const WaypointListT& waypoints) :
    center_longitude(center_lon), center_latitude(center_lat), waypoints_(waypoints) {
  generateLines();
}

double PatternTracksfile::getCenterLongitude() const {
  return center_longitude;
}

void PatternTracksfile::setCenterLongitude(double _lon) {
  center_longitude = _lon;
  generateLines();
}

double PatternTracksfile::getCenterLatitude() const {
  return center_latitude;
}

void PatternTracksfile::setCenterLatitude(double _lat) {
  center_latitude = _lat;
  generateLines();
}

void PatternTracksfile::setCenterPosition(double _lon, double _lat) {
  center_longitude = _lon;
  center_latitude = _lat;
  generateLines();
}

const PatternTracksfile::WaypointListT& PatternTracksfile::waypoints() const {
  return waypoints_;
}

void PatternTracksfile::setWaypoints(const WaypointListT& w) {
  waypoints_ = w;
  generateLines();
}

void PatternTracksfile::generateLines() {
  // if required, update the projection
  Projection::VectorLL origin = proj.getOrigin();
  if (origin(0) != center_longitude || origin(1) != center_latitude) {
    proj = Projection(center_longitude, center_latitude);
  }

  Eigen::Vector2d shift(shift_east, shift_north);

  // OPTION: It'd be pretty trivial to add a rotation here

  // actually compute our tracklines
  tracklines.clear();
  for (size_t i=1; i<waypoints_.size(); i++) {
    Eigen::Vector2d start_pt = waypoints_[i-1] + shift;
    Eigen::Vector2d end_pt = waypoints_[i] + shift;

    tracklines.push_back(Trackline(start_pt, end_pt, proj));
  }
}

} // namespace ds_libtrackline
