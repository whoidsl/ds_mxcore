/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/27/18.
//

#include "ds_libtrackline/PatternLawnmow.h"

#include "ds_libtrackline/TransformUtils.h"

#define _USE_MATH_DEFINES
#include <cmath>

namespace ds_trackline {

PatternLawnmow::PatternLawnmow() : PatternLawnmow(0,0,0,0,0,0,0,false,false,false) {}

/// Create a Lawnmower patter set of tracklines
/// \param _center_lon Longitude of the center of this survey block, in degrees
/// \param _center_lat Latitude of the center of this survey block, in degrees
/// \param _survey_heading Heading of this survey block, in degrees
/// \param _width_m Width of this survey block, in meters
/// \param _length_m Length of this survey block, in meters
/// \param _spacing_m Spacing between tracklines in this survey block, in meters
/// \param _cross_offset Distance between the center of this survey block and the
///                      crossing line, in meters
/// \param _cross_flip Boolean to decide whether or not to flip the crossing line
/// \param _cross_skip Boolean to decide whether to skip generating the crossing line
PatternLawnmow::PatternLawnmow(double _center_lon,
                               double _center_lat,
                               double _survey_heading,
                               double _width_m,
                               double _length_m,
                               double _spacing_m,
                               double _cross_offset,
                               bool _cross_skip,
                               bool _flip_width,
                               bool _flip_length)
                               : AbstractPattern(_center_lon, _center_lat), // use survey center as the origin
    center_longitude(_center_lon),
    center_latitude(_center_lat),
    survey_heading(_survey_heading),
    width(_width_m),
    length(_length_m),
    spacing(_spacing_m),
    cross_offset(_cross_offset),
    cross_padding(_spacing_m/2.0),
    cross_skip(_cross_skip),
    flip_width(_flip_width),
    flip_length(_flip_length) {
  // We've already initialized ALL our parameters! Simply generate a new set of tracklines!
  generateLines();
}

/// Get the longitude of the center of this block, in degrees
/// \return Longitude of block center, in degrees
double PatternLawnmow::getCenterLongitude() const {
  return center_longitude;
}

/// Set the longitude of this block, in degrees
/// \param center_longitude The longitude of the center of this survey block, in degrees
void PatternLawnmow::setCenterLongitude(double center_longitude) {
  PatternLawnmow::center_longitude = center_longitude;
  generateLines();
}

/// Get the latitude of the center of this block, in degrees
/// \return Latitude of block center, in degrees
double PatternLawnmow::getCenterLatitude() const {
  return center_latitude;
}

/// Set the latitude of this block, in degrees
/// \param center_latitude The latitude of the center of this survey block, in degrees
void PatternLawnmow::setCenterLatitude(double center_latitude) {
  PatternLawnmow::center_latitude = center_latitude;
  generateLines();
}


/// Get the heading of this survey block, in degrees (0 North, 90 East)
/// \return The heading of this survey block
double PatternLawnmow::getSurveyHeading() const {
  return survey_heading;
}

/// Set the orientation of this survey block, in degrees (0 is North, 90 is East)
/// \param survey_direction The heading of the first trackline in the survey
void PatternLawnmow::setSurveyHeading(double survey_heading) {
  PatternLawnmow::survey_heading = survey_heading;
  generateLines();
}

/// Get the current survey block width, in meters
/// \return The current width of this survey block, in meters
double PatternLawnmow::getWidth() const {
  return width;
}

/// Set the width of the current survey block, in meters
/// \param width The width of this block, in meters
void PatternLawnmow::setWidth(double width) {
  PatternLawnmow::width = width;
  generateLines();
}

/// Get the length of the survey block, in meters
/// \return The length of this survey block, in meters
double PatternLawnmow::getLength() const {
  return length;
}

/// Set the length of the current survey block, in meters
/// \param length The new length of the survey block, in meters
void PatternLawnmow::setLength(double length) {
  PatternLawnmow::length = length;
  generateLines();
}

/// Get the spacing between tracklines in this survey block, in meters
/// \return The spacing between tracklines in this survey block, in meters
double PatternLawnmow::getSpacing() const {
  return spacing;
}

/// Set the spacing between tracklines in this survey block, in meters
/// \param spacing The distance between adjacent survey lines, in meters
void PatternLawnmow::setSpacing(double spacing) {
  PatternLawnmow::spacing = spacing;
  generateLines();
}

/// Get the offset of the crossing line from the center of the survey block, in meters
/// \return The distance between the center of the survey and the crossing line
double PatternLawnmow::getCrossOffset() const {
  return cross_offset;
}

/// Set the distance between the center of the survey and the crossing line, in meters
/// \param cross_offset The new crossing line offset, in meters
void PatternLawnmow::setCrossOffset(double cross_offset) {
  PatternLawnmow::cross_offset = cross_offset;
  generateLines();
}

/// Get the extra padding on the crossing line, in meters
/// \return The number of meters of padding on each end of the crossing line
double PatternLawnmow::getCrossPadding() const {
  return cross_padding;
}

/// Set the padding on the crossing line.  Defaults to half the line spacing
/// \param cross_padding The new cross padding value to use
void PatternLawnmow::setCrossPadding(double cross_padding) {
  PatternLawnmow::cross_padding = cross_padding;
  generateLines();
}

/// Check whether this survey block is skipping the crossing line
/// \return False if the crossing line is generated as per usual, false if it is not generated
bool PatternLawnmow::isCrossSkipped() const {
  return cross_skip;
}

/// Set whether this survey block is skipping the crossing line
/// \param cross_skip False to generate a crossing line as usual, true to skip the crossing line
void PatternLawnmow::setCrossSkipped(bool cross_skip) {
  PatternLawnmow::cross_skip = cross_skip;
  generateLines();
}

/// Check if this survey block as been width-flipped; that is, flipped across
/// the axis parallel to the survey lines
/// \return True if the survey has been flipped
bool PatternLawnmow::isWidthFlipped() const {
  return flip_width;
}

/// Set whether or not this survey should be flipped across an axis
/// parallel to the survey lines
/// \param flip_width True if the survey should be flipped across a
/// line parallel to the primary tracklines
void PatternLawnmow::setWidthFlipped(bool flip_width) {
  PatternLawnmow::flip_width = flip_width;
  generateLines();
}

/// Check if this survey has been length-flipped; that is, flipped over a line
/// perpendicular to the survey lines
/// \return True if the survey has been flipped
bool PatternLawnmow::isLengthFlipped() const {
  return flip_length;
}

/// Set whether this survey should be flipped over an axis
/// perpendicular to the primary tracklines
/// \param flip_length True if the survey should be flipped
void PatternLawnmow::setLengthFlipped(bool flip_length) {
  PatternLawnmow::flip_length = flip_length;
  generateLines();
}

/// Generate a new set of tracklines based on the various other parameters in this class.
void PatternLawnmow::generateLines() {
  // If required, update the projection
  Projection::VectorLL origin = proj.getOrigin();
  if (origin(0) != center_longitude || origin(1) != center_latitude) {
    proj = Projection(center_longitude, center_latitude);
  }

  // Using STL containers with Eigen sucks, because alignment.  See:
  // https://eigen.tuxfamily.org/dox/group__TopicStlContainers.html
  std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d> > line_pts;

  // (probably) add a crossing line at the START of this survey
  if (!cross_skip) {
    line_pts.push_back(Eigen::Vector2d(cross_offset, cross_padding + width/2.0));
    line_pts.push_back(Eigen::Vector2d(cross_offset, -cross_padding - width/2.0));
  }

  // The line between the end of the crossing line and the start of the first
  // trackline is implicitly generated, as the final step guarantees a continous
  // set of tracklines

  // generate the endpoints of the actual lines
  int num_lines = floor(width / spacing) + 1;
  double direction = 1.0;

  for (int i=0; i<num_lines; i++) {
    double across = spacing * ( i - static_cast<double>(num_lines-1)/2.0);
    double along = direction*length/2;
    line_pts.push_back(Eigen::Vector2d(-along, across));
    line_pts.push_back(Eigen::Vector2d(along,  across));
    direction *= -1.0; // lines ALTERNATE, don't they??
  }

  // Prepare the transform we'll use to rotate our currently-North-running
  // survey to the requested orientation.
  Eigen::Matrix2d tform = makeAlongtrackTransform(survey_heading, flip_length, flip_width);

  // pre-compute our shift
  Eigen::Vector2d shift(shift_east, shift_north);

  // Create the actual tracklines
  tracklines.clear();
  for (size_t i=1; i<line_pts.size(); i++) {
    Eigen::Vector2d start_pt = tform * line_pts[i-1] + shift;
    Eigen::Vector2d end_pt = tform * line_pts[i] + shift;
    tracklines.push_back(Trackline(start_pt, end_pt, proj));
  }
}

}
