/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/26/18.
//

#include "ds_libtrackline/Projection.h"

#include <GeographicLib/Constants.hpp>
#include <math.h>
#include <stdexcept>

namespace ds_trackline {

/// Define a projection for a given origin longitude / latitude
///
/// If origin latitude is between 80 deg S and 84 deg North (inclusive),
/// a WGS84-Transverse Mercator projection centered at the origin is used.
/// It's not *quite* UTM because it has a unity scale factor, but otherwise
/// prety close.
///
/// If the origin laittude is outside those bounds, a
/// Universal Polar Stereographic (UPS) projection is used with false
/// easting/northing to put the origin at 0/0.  This is necessary
/// because otherwise change survey position could change its
/// orientation on the sphere, which seems wrong.
///
/// \param origin_lon Origin longitude, in degrees
/// \param origin_lat Origin latitude, in degrees
Projection::Projection(double origin_lon, double origin_lat)
          : Projection(Projection::VectorLL(origin_lon, origin_lat)){
  // Yay delegation! Nothing to do here...
}

/// Define a projection for a given origin longitude / latitude
///
/// If origin latitude is between 80 deg S and 84 deg North (inclusive),
/// a WGS84-Transverse Mercator projection centered at the origin is used.
/// It's not *quite* UTM because it has a unity scale factor, but otherwise
/// prety close.
///
/// If the origin laittude is outside those bounds, a
/// Universal Polar Stereographic (UPS) projection is used with false
/// easting/northing to put the origin at 0/0.  This is necessary
/// because otherwise change survey position could change its
/// orientation on the sphere, which seems wrong.
///
/// \param origin_lonlat Origin longitude/latitude vector, in degrees
Projection::Projection(const VectorLL& origin_lonlat) {

  _origin = origin_lonlat;
  _northp = origin_lonlat(1) > 0; // needed for the polar projection

  // we use the crossover points for Universal Polar Stereographic
  // because why reinvent the wheel
  if (origin_lonlat(1) > 84.0 || origin_lonlat(1) < -80.0) {
    double northdir; // need to pick a North for the projection

    _pstereo = GeographicLib::PolarStereographic(GeographicLib::Constants::WGS84_a(),
        GeographicLib::Constants::WGS84_f(), GeographicLib::Constants::UPS_k0());


    // we also need to track our origin
    _pstereo.get().Forward(_northp, _origin(1), _origin(0), _false_easting, _false_northing);

  } else {
    // we use a WGS84 UTM-style transverse mercator grid, but center wherever
    _tmerc = GeographicLib::TransverseMercator(GeographicLib::Constants::WGS84_a(),
                                               GeographicLib::Constants::WGS84_f(), 1.0);
    // we need to track our origin to make the origin 0,0
    _tmerc.get().Forward(_origin(0), _origin(1), _origin(0), _false_easting, _false_northing);
  }

}

/// Convert projected easting/northing points (in meters) to longitude/latitude (in degrees)
/// \param en Vector of coordinates, Easting/Northing
/// \return Vector of longitude / latitude (in degrees)
Projection::VectorLL Projection::projected2lonlat(const VectorEN& en) const {

  double lon = 0, lat = 0;
  if (_tmerc) {
    _tmerc.get().Reverse(_origin(0), en(0)+_false_easting, en(1)+_false_northing, lat, lon);
  } else if (_pstereo) {
    _pstereo.get().Reverse(_northp, en(0)+_false_easting, en(1)+_false_northing, lat, lon);
  } else {
    throw std::runtime_error("No projection available (HOW is this possible??");
  }
  return VectorLL(lon, lat);
}

/// Project a given longitude/latitude to projected coordinates
/// \param ll Longitude/latitude, in degrees
/// \return Returns projected coordinates, Easting/Northing, in meters.
Projection::VectorEN Projection::lonlat2projected(const VectorLL& ll) const {
  double east=0, north=0;
  if (_tmerc) {
    _tmerc.get().Forward(_origin(0), ll(1), ll(0), east, north);
  } else if (_pstereo) {
    _pstereo.get().Forward(_northp, ll(1), ll(0), east, north);
  } else {
    throw std::runtime_error("No projection available (HOW is this possible??");
  }

  // Don't forget to apply false easting/northing
  east -= _false_easting;
  north -= _false_northing;

  return VectorEN(east, north);
}

/// Return the origin as a vector
/// \return Longitude / Latitude, in degrees
const Projection::VectorLL& Projection::getOrigin() const {
  return _origin;
}

}