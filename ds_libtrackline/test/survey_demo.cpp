/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/27/18.
//

#include "ds_libtrackline/PatternLawnmow.h"

#include <iostream>
#include <iomanip>
#include <string>

int main(int argc, char* argv[]) {

  double lon, lat;
  double heading;
  double width;
  double length;
  double spacing;

  if (argc < 3) {
    std::cerr << "Need starting longitude / latitude (in decimal degrees)" << std::endl;
    return -1;
  } else {
    lon = atof(argv[1]);
    lat = atof(argv[2]);
  }

  if (argc < 4) {
    std::cerr <<"Assuming heading of 0" <<std::endl;
    heading = 0;
  } else {
    heading = atof(argv[3]);
  }

  if (argc < 5) {
    std::cerr <<"Assuming width of 1000m" <<std::endl;
    width = 1000;
  } else {
    width = atof(argv[4]);
  }

  if (argc < 6) {
    std::cerr <<"Assuming length of 2000m" <<std::endl;
    length = 2000;
  } else {
    length = atof(argv[5]);
  }

  if (argc < 7) {
    std::cerr << "Assuming a spacing of 100m" << std::endl;
    spacing = 100;
  } else {
    spacing = atof(argv[6]);
  }

  ds_trackline::PatternLawnmow survey(lon, lat, heading,
      width, length, spacing,
      0, false, false, false);

  for (size_t i=0; i<survey.numLines(); i++) {
    ds_trackline::Trackline::VectorLL start_pt = survey[i].getStartLL();
    ds_trackline::Trackline::VectorLL end_pt = survey[i].getEndLL();
    std::cout <<std::fixed <<std::setprecision(9) <<start_pt(0) <<", ";
    std::cout <<std::fixed <<std::setprecision(9) <<start_pt(1) <<", ";
    std::cout <<std::fixed <<std::setprecision(9) <<end_pt(0) <<", ";
    std::cout <<std::fixed <<std::setprecision(9) <<end_pt(1) <<"\n";
  }
  std::cout <<std::endl;


  return 0;
}

