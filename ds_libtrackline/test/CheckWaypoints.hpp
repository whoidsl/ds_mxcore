/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#include "ds_libtrackline/Trackline.h"

typedef std::vector<std::pair<double, double> > PointList;

inline double abs_difference_longitude(double lon1, double lon2) {
  double delta = fabs(lon1 - lon2);
  if (delta > 180.0) {
    delta -= 360.0;
  } else if (delta < -180.0) {
    delta += 360.0;
  }

  return delta;
}

template<typename T>
void CheckWaypoints(const T& pattern, const PointList& waypoints, const double tolerance) {

  ASSERT_EQ(waypoints.size()-1, pattern.numLines());

  for (size_t i=0; i<pattern.numLines(); i++) {
    ds_trackline::Trackline::VectorLL start_pt = pattern[i].getStartLL();
    ds_trackline::Trackline::VectorLL end_pt = pattern[i].getEndLL();

    // wrap-around.  Yay.
    EXPECT_LT(abs_difference_longitude(waypoints[i].first, start_pt(0)), tolerance);
    EXPECT_NEAR(waypoints[i].second, start_pt(1), tolerance);

    EXPECT_LT(abs_difference_longitude(waypoints[i+1].first, end_pt(0)), tolerance);
    EXPECT_NEAR(waypoints[i+1].second, end_pt(1), tolerance);
  }

}

/// This function is used to generate output that can go into the matlab
/// scripts for manual review.
///
/// To easily run, use:
/// catkin_make run_tests 2>&1 | grep WAYPOINT_TAG | awk -F $'\t' '{print $2}'
///
/// Then copy/paste into a matlab file and run the analysis scripts.
///
/// \tparam T
/// \param pattern
/// \param varname
template <typename T>
void PrintWaypoints(const T& pattern, const std::string varname) {

  std::cout <<"WAYPOINT_TAG\t" <<varname <<" = [...\n";
  for (size_t i=0; i<pattern.numLines(); i++) {
    ds_trackline::Trackline::VectorLL start_pt = pattern[i].getStartLL();
    ds_trackline::Trackline::VectorLL end_pt = pattern[i].getEndLL();

    std::cout <<"WAYPOINT_TAG\t"<<i <<" ";
    std::cout <<std::setprecision(13) <<start_pt(0) <<" ";
    std::cout <<std::setprecision(13) <<start_pt(1) <<" ";
    std::cout <<std::setprecision(13) <<end_pt(0) <<" ";
    std::cout <<std::setprecision(13) <<end_pt(1) <<";...\n";
  }
  std::cout <<"WAYPOINT_TAG\t];\n";
}