/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include <gtest/gtest.h>
#include "ds_libtrackline/TransformUtils.h"

TEST(MakeAlongtrackTransform, identity) {
  Eigen::Matrix2d res = ds_trackline::makeAlongtrackTransform(0, false, false);

  EXPECT_NEAR(0, res(0,0), 1.0e-16);
  EXPECT_NEAR(1, res(0,1), 1.0e-16);
  EXPECT_NEAR(1, res(1,0), 1.0e-16);
  EXPECT_NEAR(0, res(1,1), 1.0e-16);
}

TEST(MakeAlongtrackTransform, ninety) {
  Eigen::Matrix2d res = ds_trackline::makeAlongtrackTransform(90, false, false);

  EXPECT_NEAR(1, res(0,0), 1.0e-16);
  EXPECT_NEAR(0, res(0,1), 1.0e-16);
  EXPECT_NEAR(0, res(1,0), 1.0e-16);
  EXPECT_NEAR(-1, res(1,1), 1.0e-16);
}

TEST(MakeAlongtrackTransform, oneEighty) {
  Eigen::Matrix2d res = ds_trackline::makeAlongtrackTransform(180, false, false);

  EXPECT_NEAR(0, res(0,0), 1.0e-15);
  EXPECT_NEAR(-1, res(0,1), 1.0e-15);
  EXPECT_NEAR(-1, res(1,0), 1.0e-15);
  EXPECT_NEAR(0, res(1,1), 1.0e-15);
}

TEST(MakeAlongtrackTransform, minusNinety) {
  Eigen::Matrix2d res = ds_trackline::makeAlongtrackTransform(-90, false, false);

  EXPECT_NEAR(-1, res(0,0), 1.0e-16);
  EXPECT_NEAR(0, res(0,1), 1.0e-16);
  EXPECT_NEAR(0, res(1,0), 1.0e-16);
  EXPECT_NEAR(1, res(1,1), 1.0e-16);
}

TEST(MakeAlongtrackTransform, flipAlong) {
  Eigen::Matrix2d res = ds_trackline::makeAlongtrackTransform(0, true, false);

  EXPECT_NEAR(0, res(0,0), 1.0e-16);
  EXPECT_NEAR(1, res(0,1), 1.0e-16);
  EXPECT_NEAR(-1, res(1,0), 1.0e-16);
  EXPECT_NEAR(0, res(1,1), 1.0e-16);
}

TEST(MakeAlongtrackTransform, flipAcross) {
  Eigen::Matrix2d res = ds_trackline::makeAlongtrackTransform(0, false, true);

  EXPECT_NEAR(0, res(0,0), 1.0e-16);
  EXPECT_NEAR(-1, res(0,1), 1.0e-16);
  EXPECT_NEAR(1, res(1,0), 1.0e-16);
  EXPECT_NEAR(0, res(1,1), 1.0e-16);
}

