/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/26/18.
//

#include "ds_libtrackline/Projection.h"
#include <math.h>
#include <gtest/gtest.h>

using namespace ds_trackline;

double normalize_deg(double in) {
  if (in >= 180.0) {
    return in - 360.0;
  } else if (in < -180.0){
    return in + 360.0;
  }
  return in;
}

#define EXPECT_DEG_NEAR(x,y,d) \
 EXPECT_NEAR( normalize_deg(x), normalize_deg(y), (d))

// We're going to test a bunch of changes at small and large bits
class TestAtPoint {
 public:
  TestAtPoint(double origin_lon_deg, double origin_lat_deg) :
    underTest(origin_lon_deg, origin_lat_deg) {
    origin = underTest.getOrigin();

    EXPECT_DOUBLE_EQ(origin_lon_deg, origin(0));
    EXPECT_DOUBLE_EQ(origin_lat_deg, origin(1));
  }

  // meters per degree latitude
  double mdeglat() {
    double lat = origin(1) * M_PI/180.0;
    // from wikipedia: https://en.wikipedia.org/wiki/Geographic_coordinate_system
    // Assumes WGS84, but so does our implementation
    return 111132.92 - 559.82*cos( 2 * lat ) + 1.175*cos( 4 * lat ) - 0.0023 * cos( 6 * lat );
  }

  // meters per degree longitude
  double mdeglon() {
    double lat = origin(1) * M_PI/180.0;
    // Yes, this really takes LATITUDE
    return (111412.84*cos( lat ) - 93.5 * cos( 3 * lat ) + 0.118 * cos( 5 * lat ));

  }

  void test() {
    double TOL;
    double LL_TOL=0.1;
    // This is a gross way to set the tolerance, but it has to do with the
    // DIRECTIONS being wrong in our chosen coordinate systems-- basically, this is
    // representative of the error in ALVIN XY, which is what we're using
    // to generate the test cases
    if (abs(origin(1)) > 75) {
      TOL = 1.0;
    } else {
      TOL = 1.5e-3;
    }

    //std::cout <<"LAT: " <<origin(1) <<", 100m Lat: " <<100.0/mdeglat()
    // <<" 100m lon: " <<100.0/mdeglon() <<std::endl;

    // don't move at all
    Projection::VectorEN projected_s = underTest.lonlat2projected(
        Projection::VectorLL(origin(0), origin(1)));
    EXPECT_NEAR(  0.0, projected_s(0), TOL);
    EXPECT_NEAR(  0.0, projected_s(1), TOL);

    Projection::VectorLL geo_s = underTest.projected2lonlat(Projection::VectorEN(0,0));
    EXPECT_DEG_NEAR( origin(0), geo_s(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1), geo_s(1), LL_TOL);

    // move east
    Projection::VectorEN projected_pe = underTest.lonlat2projected(
        Projection::VectorLL(origin(0) + 100.0 / mdeglon(), origin(1)));
    EXPECT_NEAR(100.0, projected_pe(0), TOL);
    EXPECT_NEAR(  0.0, projected_pe(1), TOL);

    Projection::VectorEN projected_me = underTest.lonlat2projected(
        Projection::VectorLL(origin(0) - 100.0 / mdeglon(), origin(1)));
    EXPECT_NEAR(-100.0, projected_me(0), TOL);
    EXPECT_NEAR(   0.0, projected_me(1), TOL);

    Projection::VectorLL geo_pe = underTest.projected2lonlat(
        Projection::VectorEN(100.0,0.0));
    EXPECT_DEG_NEAR( origin(0) + 100.0 / mdeglon(), geo_pe(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1), geo_pe(1), LL_TOL);

    Projection::VectorLL geo_me = underTest.projected2lonlat(
        Projection::VectorEN(-100.0,0.0));
    EXPECT_DEG_NEAR( origin(0) - 100.0 / mdeglon(), geo_me(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1), geo_me(1), LL_TOL);

    // move north
    Projection::VectorEN projected_pn = underTest.lonlat2projected(
        Projection::VectorLL(origin(0), origin(1) + 100.0 / mdeglat()));
    EXPECT_NEAR(  0.0, projected_pn(0), TOL);
    EXPECT_NEAR(100.0, projected_pn(1), TOL);

    Projection::VectorEN projected_mn = underTest.lonlat2projected(
        Projection::VectorLL(origin(0), origin(1) - 100.0 / mdeglat()));
    EXPECT_NEAR(   0.0, projected_mn(0), TOL);
    EXPECT_NEAR(-100.0, projected_mn(1), TOL);

    Projection::VectorLL geo_pn = underTest.projected2lonlat(
        Projection::VectorEN(0.0,100.0));
    EXPECT_DEG_NEAR( origin(0), geo_pn(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1) + 100.0 / mdeglat(), geo_pn(1), LL_TOL);

    Projection::VectorLL geo_mn = underTest.projected2lonlat(
        Projection::VectorEN(0.0,-100.0));
    EXPECT_DEG_NEAR( origin(0), geo_mn(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1) - 100.0 / mdeglat(), geo_mn(1), LL_TOL);

    // move both
    Projection::VectorEN projected_pp = underTest.lonlat2projected(
        Projection::VectorLL(origin(0) + 100.0 / mdeglon(), origin(1) + 100.0 / mdeglat()));
    EXPECT_NEAR( 100.0, projected_pp(0), TOL);
    EXPECT_NEAR( 100.0, projected_pp(1), TOL);

    Projection::VectorEN projected_mp = underTest.lonlat2projected(
        Projection::VectorLL(origin(0) - 100.0 / mdeglon(), origin(1) + 100.0 / mdeglat()));
    EXPECT_NEAR(-100.0, projected_mp(0), TOL);
    EXPECT_NEAR( 100.0, projected_mp(1), TOL);

    Projection::VectorEN projected_pm = underTest.lonlat2projected(
        Projection::VectorLL(origin(0) + 100.0 / mdeglon(), origin(1) - 100.0 / mdeglat()));
    EXPECT_NEAR( 100.0, projected_pm(0), TOL);
    EXPECT_NEAR(-100.0, projected_pm(1), TOL);

    Projection::VectorEN projected_mm = underTest.lonlat2projected(
        Projection::VectorLL(origin(0) - 100.0 / mdeglon(), origin(1) - 100.0 / mdeglat()));
    EXPECT_NEAR(-100.0, projected_mm(0), TOL);
    EXPECT_NEAR(-100.0, projected_mm(1), TOL);

    Projection::VectorLL geo_pp = underTest.projected2lonlat(
        Projection::VectorEN( 100.0, 100.0));
    EXPECT_DEG_NEAR( origin(0) + 100.0 / mdeglon(), geo_pp(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1) + 100.0 / mdeglat(), geo_pp(1), LL_TOL);

    Projection::VectorLL geo_mp = underTest.projected2lonlat(
        Projection::VectorEN(-100.0, 100.0));
    EXPECT_DEG_NEAR( origin(0) - 100.0 / mdeglon(), geo_mp(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1) + 100.0 / mdeglat(), geo_mp(1), LL_TOL);

    Projection::VectorLL geo_pm = underTest.projected2lonlat(
        Projection::VectorEN( 100.0,-100.0));
    EXPECT_DEG_NEAR( origin(0) + 100.0 / mdeglon(), geo_pm(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1) - 100.0 / mdeglat(), geo_pm(1), LL_TOL);

    Projection::VectorLL geo_mm = underTest.projected2lonlat(
        Projection::VectorEN(-100.0,-100.0));
    EXPECT_DEG_NEAR( origin(0) - 100.0 / mdeglon(), geo_mm(0), LL_TOL);
    EXPECT_DEG_NEAR( origin(1) - 100.0 / mdeglat(), geo_mm(1), LL_TOL);
  }

  const Projection::VectorLL& getOrigin() const {
    return origin;
  }

 private:
  Projection::VectorLL origin;
  Projection underTest;

};
// Equitorial test cases happen near the equator
TEST(TestProjection, equatorial) {
  TestAtPoint test(0, 0);
  test.test();
}

TEST(TestProjection, tropics) {
  TestAtPoint test(90, 10);
  test.test();
}

TEST(TestProjection, dateline) {
  TestAtPoint test(180, 10);
  test.test();
}

TEST(TestProjection, whoi) {
  TestAtPoint test(70.6, 41.5);
  test.test();
}

TEST(TestProjection, nearly_arctic) {
  TestAtPoint test(70.6, 83.9);
  test.test();
}

TEST(TestProjection, nearly_antarctic) {
  TestAtPoint test(70.6, -79.9);
  test.test();
}

TEST(TestProjection, slightly_arctic) {
  TestAtPoint test(0, 84.1);
  test.test();
}

TEST(TestProjection, slightly_antarctic) {
  TestAtPoint test(0, -80.1);
  test.test();
}

TEST(TestProjection, arctic) {
  TestAtPoint test(0, 87.0);
  test.test();
}

TEST(TestProjection, antarctic) {
  TestAtPoint test(0, -87.0);
  test.test();
}
