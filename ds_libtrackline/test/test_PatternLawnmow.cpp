/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#include <gtest/gtest.h>
#include "matlab/patternLawnmowerWaypoints.h"
using namespace PatternLawnmowerWaypoints;

#include "ds_libtrackline/PatternLawnmow.h"

// So the way these tests are generated is as follows:
// 1. Generate a bunch of lines
// 2. Check in matlab
// 3. Set computed waypoints

const double TOLERANCE = 1.0e-12;

TEST(PatternLawnmow, fullPattern0deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern0deg");
  CheckWaypoints(underTest, fullPattern0degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern45deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 45, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern45deg");
  CheckWaypoints(underTest, fullPattern45degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern90deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 90, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern90deg");
  CheckWaypoints(underTest, fullPattern90degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern135deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 135, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern135deg");
  CheckWaypoints(underTest, fullPattern135degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern180deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 180, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern180deg");
  CheckWaypoints(underTest, fullPattern180degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern225deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 225, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern225deg");
  CheckWaypoints(underTest, fullPattern225degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern270deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 270, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern270deg");
  CheckWaypoints(underTest, fullPattern270degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, fullPattern315deg) {
  ds_trackline::PatternLawnmow underTest(0, 0, 315, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "fullPattern315deg");
  CheckWaypoints(underTest, fullPattern315degWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, noCrossPattern) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 0, true, false, false);
  //PrintWaypoints(underTest, "noCrossPattern");
  CheckWaypoints(underTest, noCrossPatternWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, flipWidth) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 0, false, true, false);
  //PrintWaypoints(underTest, "flipWidth");
  CheckWaypoints(underTest, flipWidthWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, flipLength) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 0, false, false, true);
  //PrintWaypoints(underTest, "flipLength");
  CheckWaypoints(underTest, flipLengthWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, flipBoth) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 0, false, true, true);
  //PrintWaypoints(underTest, "flipBoth");
  CheckWaypoints(underTest, flipBothWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, longerLines) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 2000, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "longerLines");
  CheckWaypoints(underTest, longerLinesWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, internationalDateLine) {
  ds_trackline::PatternLawnmow underTest(-180, 0, 0, 100, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "internationalDateLine");
  // tolerance needs to be relaxed here for some reason;
  // probably because we're at the limits of the projection
  // still 10^-11, which isn't too bad
  CheckWaypoints(underTest, internationalDateLineWaypoints, TOLERANCE*1000.0);
}

TEST(PatternLawnmow, superNarrow) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 1, 200, 10, 0, false, false, false);
  //PrintWaypoints(underTest, "superNarrow");
  CheckWaypoints(underTest, superNarrowWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, offsetCrossline) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 25, false, false, false);
  //PrintWaypoints(underTest, "offsetCrossline");
  CheckWaypoints(underTest, offsetCrosslineWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, negativeOffsetCrossline) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, -25, false, false, false);
  //PrintWaypoints(underTest, "negativeOffsetCrossline");
  CheckWaypoints(underTest, negativeOffsetCrosslineWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, flippedWidthOffsetCrossLine) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 10, false, true, false);
  //PrintWaypoints(underTest, "flippedWidthOffsetCrossLine");
  CheckWaypoints(underTest, flippedWidthOffsetCrossLineWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, flippedLengthOffsetCrossLine) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 10, false, false, true);
  //PrintWaypoints(underTest, "flippedLengthOffsetCrossLine");
  CheckWaypoints(underTest, flippedLengthOffsetCrossLineWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, shifted) {
  ds_trackline::PatternLawnmow underTest(0, 0, 0, 100, 200, 10, 0, false, false, false);
  underTest.setShiftEast(10);
  underTest.setShiftNorth(20);

  //PrintWaypoints(underTest, "shifted");
  CheckWaypoints(underTest, shiftedWaypoints, TOLERANCE);
}

TEST(PatternLawnmow, originCorrect) {
  ds_trackline::PatternLawnmow underTest(70.6712, 41.5242, 0, 100, 200, 10, 0, false, false, false);
  ds_trackline::Projection::VectorLL origin = underTest.getProjection().getOrigin();

  EXPECT_NEAR(70.6712, origin(0), 0.0001);
  EXPECT_NEAR(41.5242,  origin(1), 0.0001);
}