/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//

#include <gtest/gtest.h>
#include "CheckWaypoints.hpp"

#include "ds_libtrackline/PatternTracksfile.h"

const double PATTERN_TRACKS_MDEGLON = 111320.700000;
const double PATTERN_TRACKS_MDEGLAT = 110567.238000;

TEST(PatternTracksfile, square) {
  ds_trackline::PatternTracksfile::WaypointListT waypts;
  waypts.push_back(Eigen::Vector2d(-10, 10));
  waypts.push_back(Eigen::Vector2d(-10, -10));
  waypts.push_back(Eigen::Vector2d(10, -10));
  waypts.push_back(Eigen::Vector2d(10, 10));
  waypts.push_back(Eigen::Vector2d(-10, 10));

  ds_trackline::PatternTracksfile underTest(0, 0, waypts);
  CheckWaypoints(underTest, {
      {-10 / PATTERN_TRACKS_MDEGLON, 10 / PATTERN_TRACKS_MDEGLAT},
      {-10 / PATTERN_TRACKS_MDEGLON, -10 / PATTERN_TRACKS_MDEGLAT},
      {10 / PATTERN_TRACKS_MDEGLON, -10 / PATTERN_TRACKS_MDEGLAT},
      {10 / PATTERN_TRACKS_MDEGLON, 10 / PATTERN_TRACKS_MDEGLAT},
      {-10 / PATTERN_TRACKS_MDEGLON, 10 / PATTERN_TRACKS_MDEGLAT}
  }, 1.0e-8);

}

TEST(PatternTracksfile, shifted) {
  ds_trackline::PatternTracksfile::WaypointListT waypts;
  waypts.push_back(Eigen::Vector2d(-10, 10));
  waypts.push_back(Eigen::Vector2d(-10, -10));
  waypts.push_back(Eigen::Vector2d(10, -10));
  waypts.push_back(Eigen::Vector2d(10, 10));
  waypts.push_back(Eigen::Vector2d(-10, 10));

  ds_trackline::PatternTracksfile underTest(0, 0, waypts);

  underTest.setShiftEast(10);
  underTest.setShiftNorth(20);

  // relatively poor tolerance is driven by MDEGLAT/MDEGLON approximation.
  // Still, 2.0e8 is on the order of millimeters
  CheckWaypoints(underTest, {
      {0 / PATTERN_TRACKS_MDEGLON, 30 / PATTERN_TRACKS_MDEGLAT},
      {0 / PATTERN_TRACKS_MDEGLON, 10 / PATTERN_TRACKS_MDEGLAT},
      {20 / PATTERN_TRACKS_MDEGLON, 10 / PATTERN_TRACKS_MDEGLAT},
      {20 / PATTERN_TRACKS_MDEGLON, 30 / PATTERN_TRACKS_MDEGLAT},
      {0 / PATTERN_TRACKS_MDEGLON, 30 / PATTERN_TRACKS_MDEGLAT}
  }, 2.0e-8);
}