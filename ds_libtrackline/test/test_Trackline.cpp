/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/27/18.
//

#include "ds_libtrackline/Trackline.h"
#include <math.h>
#include <memory>
#include <gtest/gtest.h>

using namespace ds_trackline;

class TracklineTest : public ::testing::Test {
 protected:
  void SetUp() override {
    // set up a single south-to-north
    underTest.reset(new Trackline(20, 1, 20, 1.01));
  }

  std::unique_ptr<Trackline> underTest;
  //static constexpr double TOL = 1.0e-4;
  static constexpr double TOL = 0.01;

  // length of 0.01 deg of longitude at 10 deg North
  // (magic value to make tests pass)
  static constexpr double DELTA_EAST_M = 1113.0265;

  // length of 0.01 deg of latitude at 10 deg North
  // (magic value to make tests pass)
  static constexpr double DELTA_NORTH_M = 1105.7445;
};

TEST_F(TracklineTest, checkInit) {
  Trackline::VectorLL startLL = underTest->getStartLL();
  Trackline::VectorLL endLL = underTest->getEndLL();

  EXPECT_DOUBLE_EQ(20, startLL(0));
  EXPECT_DOUBLE_EQ(1, startLL(1));

  EXPECT_DOUBLE_EQ(20, endLL(0));
  EXPECT_DOUBLE_EQ(1.01, endLL(1));

  Trackline::VectorEN startEN = underTest->getStartEN();
  EXPECT_DOUBLE_EQ(0, startEN(0));
  EXPECT_NEAR(-DELTA_NORTH_M, startEN(1), 0.10); // VERY big numbers,
                                                   // DELTA_NORTH_M is a bad approximation

  Trackline::VectorEN endEN = underTest->getEndEN();
  EXPECT_DOUBLE_EQ(0, endEN(0));
  EXPECT_DOUBLE_EQ(0, endEN(1));
}

TEST_F(TracklineTest, rightOfLine) {
  Trackline::VectorEN ret = underTest->lonlat_to_trackframe(20.01, 1.00);
  EXPECT_NEAR(-DELTA_NORTH_M, ret(0), TOL); // along track
  EXPECT_NEAR( DELTA_EAST_M , ret(1), TOL); // across track
}

TEST_F(TracklineTest, leftOfLine) {
  Trackline::VectorEN ret = underTest->lonlat_to_trackframe(19.99, 1.00);
  EXPECT_NEAR(-DELTA_NORTH_M, ret(0), TOL); // along track
  EXPECT_NEAR(-DELTA_EAST_M , ret(1), TOL); // across track
}

TEST_F(TracklineTest, beforeLine) {
  Trackline::VectorEN ret = underTest->lonlat_to_trackframe(20.00, 1.00);
  EXPECT_NEAR(-DELTA_NORTH_M, ret(0), TOL); // along track
  EXPECT_NEAR(0, ret(1), TOL); // across track
}

TEST_F(TracklineTest, afterLine) {
  Trackline::VectorEN ret = underTest->lonlat_to_trackframe(20.00, 1.011);
  EXPECT_NEAR(DELTA_NORTH_M/10.0, ret(0), TOL); // along track
  EXPECT_NEAR(0, ret(1), TOL); // across track
}

TEST(TracklinePoleTest, northPole) {
  double TOL=0.01;
  double DELTA_LON_M = 1.93773; // change for 0.1 deg longitude @89.99deg N
  double DELTA_LON_TOL = 0.001; // tolerance in METERS
  // this is a line "north" in the stereographic projection THROUGH
  // the pole
  Trackline underTest(0.0, 89.99, 180, 89.99);

  Trackline::VectorEN start = underTest.getStartEN();

  Trackline::VectorEN before = underTest.lonlat_to_trackframe(0.0, 89.99);
  EXPECT_NEAR( start(1), before(0), TOL); // along track
  EXPECT_NEAR(0, before(1), TOL); // across track

  Trackline::VectorEN after  = underTest.lonlat_to_trackframe(180.0, 89.97);
  EXPECT_NEAR(-start(1), after(0), TOL); // along track
  EXPECT_NEAR(0, after(1), TOL); // across track

  Trackline::VectorEN right  = underTest.lonlat_to_trackframe(0.1, 89.99);
  EXPECT_NEAR( start(1), right(0), TOL); // along track
  EXPECT_NEAR( DELTA_LON_M, right(1), DELTA_LON_TOL); // across track

  Trackline::VectorEN left   = underTest.lonlat_to_trackframe(-0.1, 89.99);
  EXPECT_NEAR( start(1), left(0), TOL); // along track
  EXPECT_NEAR(-DELTA_LON_M, left(1), DELTA_LON_TOL); // across track
}

#define DTOR (M_PI/180.0)
TEST(TracklineCourseTest, course) {
  Projection proj(0,0);

  Trackline north(0,0,0,1,proj);
  EXPECT_DOUBLE_EQ(DTOR*0.0, north.getCourseRad());
  EXPECT_DOUBLE_EQ(0.0, north.getCourseDeg());

  Trackline northeast(0,0,1,1,proj);
  EXPECT_DOUBLE_EQ(DTOR*45.0, northeast.getCourseRad());
  EXPECT_DOUBLE_EQ(45.0, northeast.getCourseDeg());

  Trackline east(0,0,1,0,proj);
  EXPECT_DOUBLE_EQ(DTOR*90.0, east.getCourseRad());
  EXPECT_DOUBLE_EQ(90.0, east.getCourseDeg());

  Trackline southeast(0,0,1,-1,proj);
  EXPECT_DOUBLE_EQ(DTOR*135.0, southeast.getCourseRad());
  EXPECT_DOUBLE_EQ(135.0, southeast.getCourseDeg());

  Trackline south(0,0,0,-1,proj);
  EXPECT_DOUBLE_EQ(DTOR*180.0, south.getCourseRad());
  EXPECT_DOUBLE_EQ(180.0, south.getCourseDeg());

  Trackline southwest(0,0,-1,-1,proj);
  EXPECT_DOUBLE_EQ(DTOR*225.0, southwest.getCourseRad());
  EXPECT_DOUBLE_EQ(225.0, southwest.getCourseDeg());

  Trackline west(0,0,-1,0,proj);
  EXPECT_DOUBLE_EQ(DTOR*270.0, west.getCourseRad());
  EXPECT_DOUBLE_EQ(270.0, west.getCourseDeg());

  Trackline northwest(0,0,-1,1,proj);
  EXPECT_DOUBLE_EQ(DTOR*315.0, northwest.getCourseRad());
  EXPECT_DOUBLE_EQ(315.0, northwest.getCourseDeg());
}