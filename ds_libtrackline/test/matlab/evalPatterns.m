% For this script to work:
% 1). Call clear
% 2). Create your set of pattern waypoints as
%     idx start_lon start_lat end_lon end_lat
% 3). Call this script

vars = whos;
close all;
for i=1:length(vars)
    if any(strcmp(vars(i).name, {'ans', 'i', 'vars'}))
        continue
    end
    evalTestPattern(vars(i).name);
end