/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#ifndef DS_LIBTRACKLINE_PATTERNLAWNMOWERWAYPOINTS_H
#define DS_LIBTRACKLINE_PATTERNLAWNMOWERWAYPOINTS_H

#include "../CheckWaypoints.hpp"

namespace PatternNavLegsWaypoints {

PointList atWhoiWaypoints = {
    {70.6712000000000, 41.5238623569900},
    {70.6712000000000, 41.5240874523300},
    {70.6709004740500, 41.5240874519400},
    {70.6709004730100, 41.5243125472700},
    {70.6712000000000, 41.5243125476600},
    {70.6712000000000, 41.5245376429900}
};

PointList fullPattern0degWaypoints = {
    {0.0000000000000, -0.0003391385539},
    {0.0000000000000, -0.0001130461846},
    {-0.0002245788210, -0.0001130461846},
    {-0.0002245788210, 0.0001130461846},
    {0.0000000000000, 0.0001130461846},
    {0.0000000000000, 0.0003391385539}
};

PointList fullPattern45degWaypoints = {
    {-0.0002382018109, -0.0002398071712},
    {-0.0000794006036, -0.0000799357237},
    {-0.0002382018109, 0.0000799357237},
    {-0.0000794006036, 0.0002398071712},
    {0.0000794006036, 0.0000799357237},
    {0.0002382018109, 0.0002398071712}
};

PointList fullPattern90degWaypoints = {
    {-0.0003368682315, -0.0000000000000},
    {-0.0001122894105, -0.0000000000000},
    {-0.0001122894105, 0.0002260923693},
    {0.0001122894105, 0.0002260923693},
    {0.0001122894105, 0.0000000000000},
    {0.0003368682315, 0.0000000000000}
};

PointList internationalDateLineWaypoints = {
    {-180.0000000000000, -0.0003391385539},
    {-180.0000000000000, -0.0001130461846},
    {179.9997754212000, -0.0001130461846},
    {179.9997754212000, 0.0001130461846},
    {-180.0000000000000, 0.0001130461846},
    {-180.0000000000000, 0.0003391385539}
};

PointList largerPatternWaypoints = {
    {0.0000000000000, -0.0013565542156},
    {0.0000000000000, -0.0004521847385},
    {-0.0008983152841, -0.0004521847385},
    {-0.0008983152841, 0.0004521847385},
    {0.0000000000000, 0.0004521847385},
    {0.0000000000000, 0.0013565542156}
};

PointList rightFirstWaypoints = {
    {0.0000000000000, -0.0003391385539},
    {0.0000000000000, -0.0001130461846},
    {0.0002245788210, -0.0001130461846},
    {0.0002245788210, 0.0001130461846},
    {0.0000000000000, 0.0001130461846},
    {0.0000000000000, 0.0003391385539}
};

PointList shiftedWaypoints = {
{0.0000898315284, -0.0001582646585},
{0.0000898315284, 0.0000678277108},
{0.0003144103494, 0.0000678277108},
{0.0003144103494, 0.0002939200800},
{0.0000898315284, 0.0002939200800},
{0.0000898315284, 0.0005200124493}
};

} //namespace PatternNavLegsWaypoints

#endif //DS_LIBTRACKLINE_PATTERNLAWNMOWERWAYPOINTS_H
