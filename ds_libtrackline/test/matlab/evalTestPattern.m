function evalTestPattern(varname)

% Get the actual data
pattern = evalin('base', varname);

fprintf('\n');
fprintf('PointList %sWaypoints = {\n', varname);
for i=1:size(pattern,1)
    fprintf('{%.13f, %.13f},\n', pattern(i,2), pattern(i,3));
end
fprintf('{%.13f, %.13f}\n', pattern(end,4), pattern(end,5));
fprintf('};\n\n');

if (strcmp(varname, 'internationalDateLine'))
    idx = pattern(:,2) < 0;
    pattern(idx,2) = pattern(idx,2) + 360;
    
    idx = pattern(:,4) < 0;
    pattern(idx,4) = pattern(idx,4) + 360;
end

% Find the "origin"
org_lon = mean(pattern(:,2));
org_lat = mean(pattern(:,3));

[start_x, start_y] = ll2xy(pattern(:,3), pattern(:,2), org_lat, org_lon);
[end_x, end_y] = ll2xy(pattern(:,5), pattern(:,4), org_lat, org_lon);
[zero_x, zero_y] = ll2xy(0, 0, org_lat, org_lon);

figure();

% Draw the lat/lon picture
subplot(1,2,1);
hold off;
for i=1:size(pattern,1)
    plot(pattern(i,[2,4]), pattern(i,[3,5]), '-b.', 'MarkerSize', 10);
    hold on;
end
plot(pattern(1,2), pattern(1,3), 'g.', 'MarkerSize', 10);
plot(pattern(end,4), pattern(end,5), 'r.', 'MarkerSize', 10);
plot(org_lon, org_lat, 'k.', 'MarkerSize', 10);
title(varname);
grid on;
xlabel('Longitude [deg]');
ylabel('Latitude [deg]');
axis equal;

% Draw the X/Y version
subplot(1,2,2);
hold off;
for i=1:size(pattern,1)
    plot([start_x(i),end_x(i)], [start_y(i),end_y(i)], '-b.', 'MarkerSize', 10);
    hold on;
end
plot(start_x(1), start_y(1), 'g.', 'MarkerSize', 10);
plot(end_x(end), end_y(end), 'r.', 'MarkerSize', 10);
if abs(org_lon) < 1e-3 && abs(org_lat) < 1e-3
    plot(zero_x, zero_y, 'k.', 'MarkerSize', 15);
end
title(varname);
grid on;
xlabel('Easting [m]');
ylabel('Northing [m]');
axis equal;