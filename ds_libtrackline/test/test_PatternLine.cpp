/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#include <gtest/gtest.h>
#include "CheckWaypoints.hpp"

#include "ds_libtrackline/PatternLine.h"

TEST(PatternLine, lineEast) {
  ds_trackline::PatternLine underTest(0, 0, 1, 0);
  CheckWaypoints(underTest, {{0,0},{1,0}}, 1.0e-14);
}

TEST(PatternLine, lineNorth) {
  ds_trackline::PatternLine underTest(0, 0, 0, 1);
  CheckWaypoints(underTest, {{0,0},{0,1}}, 1.0e-14);
}

TEST(PatternLine, lineThrough00) {
  ds_trackline::PatternLine underTest(-1, -1, 1, 1);
  CheckWaypoints(underTest, {{-1,-1},{1,1}}, 1.0e-14);
}

TEST(PatternLine, updateLine) {
  ds_trackline::PatternLine underTest(0, 0, 1, 0);
  underTest.setStartPoint(1,2);
  underTest.setEndPoint(3,4);

  CheckWaypoints(underTest, {{1,2},{3,4}}, 1.0e-14);
  // make sure the projection is rejiggered
  ds_trackline::Trackline::VectorLL origin = underTest.getProjection().getOrigin();
  EXPECT_NEAR(3, origin(0), 1.0e-13);
  EXPECT_NEAR(4, origin(1), 1.0e-13);
}

const double PATTERN_LINE_MDEGLON = 111320.700000;
const double PATTERN_LINE_MDEGLAT = 110567.238000;
TEST(PatternLine, shifted) {
  ds_trackline::PatternLine underTest(0, 0, 1, 0);
  underTest.setShiftEast(10);
  underTest.setShiftNorth(20);
  // tolerance is driven by accuracy of the MDEGLON/MDEGLAT calculations,
  // or the lack thereof.
  CheckWaypoints(underTest, {{0+10/PATTERN_LINE_MDEGLON,0+20/PATTERN_LINE_MDEGLAT},
                             {1+10/PATTERN_LINE_MDEGLON,0+20/PATTERN_LINE_MDEGLAT}
                             }, 5.0e-8);
}
