/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#include <gtest/gtest.h>
#include "matlab/patternNavLegsWaypoints.h"
using namespace PatternNavLegsWaypoints;

#include "ds_libtrackline/PatternNavLegs.h"

// So the way these tests are generated is as follows:
// 1. Generate a bunch of lines
// 2. Check in matlab
// 3. Set computed waypoints

const double TOLERANCE = 1.0e-11;

TEST(PatternNavLegs, fullPattern0deg) {
  ds_trackline::PatternNavLegs underTest(0, 0, 0, 25, false);
  //PrintWaypoints(underTest, "fullPattern0deg");
  CheckWaypoints(underTest, fullPattern0degWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, fullPattern45deg) {
  ds_trackline::PatternNavLegs underTest(0, 0, 45, 25, false);
  //PrintWaypoints(underTest, "fullPattern45deg");
  CheckWaypoints(underTest, fullPattern45degWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, fullPattern90deg) {
  ds_trackline::PatternNavLegs underTest(0, 0, 90, 25, false);
  //PrintWaypoints(underTest, "fullPattern90deg");
  CheckWaypoints(underTest, fullPattern90degWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, largerPattern) {
  ds_trackline::PatternNavLegs underTest(0, 0, 0, 100, false);
  //PrintWaypoints(underTest, "largerPattern");
  CheckWaypoints(underTest, largerPatternWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, atWhoi) {
  ds_trackline::PatternNavLegs underTest(70.6712, 41.5242, 0, 25, false);
  //PrintWaypoints(underTest, "atWhoi");
  CheckWaypoints(underTest, atWhoiWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, internationalDateLine) {
  ds_trackline::PatternNavLegs underTest(-180, 0, 0, 25, false);
  // PrintWaypoints(underTest, "internationalDateLine");
  // tolerance needs to be relaxed here for some reason;
  // probably because we're at the limits of the projection
  // still 10^-11, which isn't too bad
  CheckWaypoints(underTest, internationalDateLineWaypoints, TOLERANCE*100.0);
}

TEST(PatternNavLegs, rightFirst) {
  ds_trackline::PatternNavLegs underTest(0, 0, 0, 25, true);
  // PrintWaypoints(underTest, "rightFirst");
  CheckWaypoints(underTest, rightFirstWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, shifted) {
  ds_trackline::PatternNavLegs underTest(0, 0, 0, 25, true);
  underTest.setShiftEast(10);
  underTest.setShiftNorth(20);

  //PrintWaypoints(underTest, "shifted");
  CheckWaypoints(underTest, shiftedWaypoints, TOLERANCE);
}

TEST(PatternNavLegs, originCorrect) {
  ds_trackline::PatternNavLegs underTest(70.6712, 41.5242, 0, 25, false);
  ds_trackline::Projection::VectorLL origin = underTest.getProjection().getOrigin();

  EXPECT_NEAR(70.6712, origin(0), 0.0001);
  EXPECT_NEAR(41.5242,  origin(1), 0.0001);
}
