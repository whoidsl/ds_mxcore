/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/19/18.
//

#ifndef PROJECT_TRACKLINE_H
#define PROJECT_TRACKLINE_H

#include <Eigen/Dense>
#include "Projection.h"

namespace ds_trackline {

/// \brief A class to represent a single trackline
///
/// Tracklines are lat/lon aware
class Trackline {
 public:

  /// \brief A 2D vector holding Longitude / Latitude in degrees
  typedef Eigen::Vector2d VectorLL;

  /// \brief A 2D vector holding X/Y in meters (Easting / Northing)
  typedef Eigen::Vector2d VectorEN;


  // required when having Eigen elements as class elements. See:
  // https://eigen.tuxfamily.org/dox/group__TopicStructHavingEigenMembers.html
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Trackline(double start_lon, double start_lat, double end_lon, double end_lat);
  Trackline(double start_east, double start_north, double end_east, double end_north,
      const Projection& _proj);
  Trackline(const VectorLL& start_pt, const VectorLL& end_pt);
  Trackline(const VectorEN& start_pt, const VectorEN& end_pt, const Projection& _proj);

  VectorEN lonlat_to_trackframe(double lon, double lat) const;
  VectorEN lonlat_to_trackframe(const VectorLL& lonlat) const;
  VectorEN en_to_trackframe(double easting, double northing) const;
  VectorEN en_to_trackframe(const VectorEN& en) const;

  VectorLL trackframe_to_lonlat(double along, double across) const;
  VectorLL trackframe_to_lonlat(const VectorEN &along_across) const;
  VectorEN trackframe_to_en(double along, double across) const;
  VectorEN trackframe_to_en(const VectorEN &along_across) const;

  const Projection& getProjection() const;
  VectorLL getStartLL() const;
  VectorLL getEndLL() const;
  const VectorEN& getStartEN() const;
  const VectorEN& getEndEN() const;
  double getCourseRad() const;
  double getCourseDeg() const;
  double getLength() const;

  std::string getWktLL() const;

 protected:

  VectorEN start_pt;
  VectorEN end_pt;
  Projection proj;
  Eigen::Matrix2d basis;
  double course;

  void setLineEN(const VectorEN& line_start, const VectorEN& line_end);

};

}

#endif //PROJECT_TRACKLINE_H
