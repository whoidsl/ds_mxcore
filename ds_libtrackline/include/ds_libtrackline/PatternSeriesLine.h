/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by tmjoyce on 10/20/22.
//

#ifndef DS_LIBTRACKLINE_PATTERNSERIESLINE_H
#define DS_LIBTRACKLINE_PATTERNSERIESLINE_H

#include "AbstractPattern.h"

namespace ds_trackline {

/// This class is a thin wrapper around trackline that allows a multiple tracks pieced together in order
/// The lines generated here do not rely on a center position, but an absolute position for each point of trackline
/// to be used when following the AbstractPattern interface
class PatternSeriesLine : public AbstractPattern {
 public:
 EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  typedef Eigen::Vector2d WaypointT;
  typedef std::vector<WaypointT, Eigen::aligned_allocator<WaypointT>> WaypointListT;
  PatternSeriesLine();
  PatternSeriesLine(const WaypointListT& waypoints);

  void setWaypoints(const WaypointListT& w);

  double getStartLongitude() const;
  double getStartLatitude() const;

  double getEndLongitude() const;
  double getEndLatitude() const;

 protected:

  /// Regenerate lines from the given parameters
  void generateLines() override;
  /// List of waypoints.  LatLon coords
  WaypointListT waypoints_;
};

}

#endif //DS_LIBTRACKLINE_PATTERNSERIESLINE_H
