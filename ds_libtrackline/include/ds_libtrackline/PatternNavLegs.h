/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/11/19.
//

#ifndef DS_LIBTRACKLINE_NAVLEGS_H
#define DS_LIBTRACKLINE_NAVLEGS_H

#include "AbstractPattern.h"

namespace ds_trackline {

/// This class generates a classic dog-leg pattern that Sentry
/// routinely uses as a navigation fiduccial.  It consists of
/// five equal-length lines with 90 degree turns between them.
///
///
/// -----\  X  /-----
///      |     |
///      \-----/
///
/// X == Center of pattern
///
/// Parameters are the heading, length of each line segment, and whether to turn
/// right first or left first
class PatternNavLegs : public AbstractPattern {
 public:
  PatternNavLegs();
  PatternNavLegs(double _center_lon, double _center_lat,
      double _heading, double _length, bool rightFirst);

  double getCenterLongitude() const;
  void setCenterLongitude(double _lon);

  double getCenterLatitude() const;
  void setCenterLatitude(double _lat);

  void setCenterPosition(double _lon, double _lat);

  double getHeading() const;
  void setHeading(double _heading);

  double getLineLength() const;
  void setLineLength(double _len);

  bool getRightFirst() const;
  void setRightFirst(bool _right_first);

 protected:
  /// Longitude (in degrees) of the center of the pattern.  Used
  /// with latitude to define the projection
  double center_longitude;

  /// Latitude (in degrees) of the center of the pattern.  Used
  /// with longitude to define the projection
  double center_latitude;

  /// The principal direction of the pattern; the heading of the first, third,
  /// and fifth real tracklines
  double heading;

  /// The length of each trackline, in meters
  double line_length;

  /// whether to turn right first or left first when running the pattern
  bool right_first;

  /// Convenience function to actually generate the lines
  void generateLines() override;
};

}

#endif //DS_LIBTRACKLINE_NAVLEGS_H
