/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/21/18.
//

#ifndef PROJECT_PROJECTION_H
#define PROJECT_PROJECTION_H

#include <Eigen/Dense>
#include <boost/optional.hpp>
#include <GeographicLib/TransverseMercator.hpp>
#include <GeographicLib/PolarStereographic.hpp>

namespace ds_trackline {

/// \brief A class to handle projections.
///
/// Projections are initialized from an initial lat/lon, and very predictable
/// rules are used to build the projection string.
class Projection {

 public:
  typedef Eigen::Vector2d VectorLL;
  typedef Eigen::Vector2d VectorEN;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Projection(double origin_lon, double origin_lat);
  Projection(const VectorLL &origin_lonlat);

  VectorEN lonlat2projected(const VectorLL &ll) const;
  VectorLL projected2lonlat(const VectorEN &en) const;

  const VectorLL &getOrigin() const;

 private:
  // we'll use one of these projections, but not BOTH
  boost::optional<GeographicLib::TransverseMercator> _tmerc;
  boost::optional<GeographicLib::PolarStereographic> _pstereo;

  VectorLL _origin;
  // we have to track false northing to make the origin 0,0
  double _false_northing;
  double _false_easting;

  bool _northp;
};

}

#endif //PROJECT_PROJECTION_H
