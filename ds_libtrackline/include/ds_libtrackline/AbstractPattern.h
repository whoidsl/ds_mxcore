/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/10/19.
//

#ifndef DS_MX_ABSTRACTPATTERN_H
#define DS_MX_ABSTRACTPATTERN_H

#include <vector>
#include "Trackline.h"

namespace ds_trackline {

/// \brief Interface class for a generic trackline pattern
class AbstractPattern {
 public:

  AbstractPattern();
  AbstractPattern(double lon_deg, double lat_deg);
  virtual ~AbstractPattern() = 0;

  // accessors to the underlying tracklines
  const std::vector<Trackline>& lines() const;
  size_t numLines() const;

  const Trackline& operator[](size_t i) const;
  Trackline& operator[](size_t i);

  const Projection& getProjection() const;

  Trackline::VectorLL getStartLL() const;
  Trackline::VectorLL getEndLL() const;

  double getTotalLength() const;

  double getShiftEast() const;
  void setShiftEast(double shift_east);

  double getShiftNorth() const;
  void setShiftNorth(double shift_north);

  void setShift(double shift_east, double shift_north);

 protected:
  /// The tracklines that comprise this survey
  std::vector<Trackline> tracklines;

  /// The projection from lon-lat to a rectangular coordinate system
  /// used by all tracklines in this survey
  Projection proj; // common projection for all tracklines

  // Any pattern should be shiftable

  /// A post-generation shift in meters, east.  Used to shift without reprojection
  double shift_east;

  /// A post-generation shift in meters, north.  Used to shift without reprojection
  double shift_north;

  virtual void generateLines() = 0;
};

}

#endif //DS_MX_ABSTRACTPATTERN_H
