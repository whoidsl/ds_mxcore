/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/27/20.
//

#ifndef DS_LIBTRACKLINE_PATTERNTRACKSFILE_H
#define DS_LIBTRACKLINE_PATTERNTRACKSFILE_H

#include "AbstractPattern.h"

namespace ds_trackline {

/// This class builds a trackline pattern from a list of 3D endpoints
class PatternTracksfile : public AbstractPattern {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  typedef Eigen::Vector2d WaypointT;
  typedef std::vector<WaypointT, Eigen::aligned_allocator<WaypointT>> WaypointListT;

  PatternTracksfile();
  PatternTracksfile(double center_lon, double center_lat, const WaypointListT& waypoints);

  double getCenterLongitude() const;
  void setCenterLongitude(double _lon);

  double getCenterLatitude() const;
  void setCenterLatitude(double _lat);

  void setCenterPosition(double _lon, double _lat);

  const WaypointListT& waypoints() const;
  void setWaypoints(const WaypointListT& w);

 protected:
  /// Longitude (in degrees) of (0,0) in the tracks file.  Used to define the projection.
  double center_longitude;

  /// Latitude (in degrees) of (0,0) in the tracks file.  Used to define the projection.
  double center_latitude;

  /// List of waypoints.  Easting / Northing, in meters.
  WaypointListT waypoints_;

  /// Regenerate lines from the given parameters
  void generateLines() override;
};

}

#endif // DS_LIBTRACKLINE_PATTERNTRACKSFILE_H
