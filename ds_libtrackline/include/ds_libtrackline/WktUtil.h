/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/21/19.
//

#ifndef PROJECT_WKTUTIL_H
#define PROJECT_WKTUTIL_H

#include <string>
#include <vector>

namespace ds_trackline {

// various utilities for convienently creating Well-Known Text Strings

/// Draw a single point from lat/lon as well-known text
/// \param lon
/// \param lat
/// \return
extern std::string wktPointLL(double lon, double lat);

/// Convert a pair of lat/lon points to a single string
/// \param lon_start Longitude of starting point, decimal degrees
/// \param lat_start Latitude of starting point, decimal degrees
/// \param lon_end Longitude of ending point, decimal degrees
/// \param lat_end Latitude of ending point, decimal degrees
/// \return
extern std::string wktLineLL(double lon_start, double lat_start,
    double lon_end, double lat_end);

// Convert a vector of n amount points to a string
extern std::string
wktLineStringLL(std::vector<std::pair<double, double>> points);
}

#endif //PROJECT_WKTUTIL_H
