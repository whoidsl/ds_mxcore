/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/19/18.
//

#ifndef PROJECT_GRID_H
#define PROJECT_GRID_H

#include "Trackline.h"
#include "AbstractPattern.h"

#include <vector>

namespace ds_trackline {

class PatternLawnmow : public AbstractPattern {
 public:
  PatternLawnmow();
  PatternLawnmow(double _center_lon, double _center_lat,
                 double _survey_heading,
                 double _width_m, double _length_m,
                 double _spacing_m,
                 double _cross_offset,
                 bool _cross_skip,
                 bool _flip_width, bool flip_length);

  // where the survey is
  double getCenterLongitude() const;
  void setCenterLongitude(double center_longitude);

  double getCenterLatitude() const;
  void setCenterLatitude(double center_latitude);

  double getSurveyHeading() const;
  void setSurveyHeading(double survey_heading);

  // what its dimensions are
  double getWidth() const;
  void setWidth(double width);

  double getLength() const;
  void setLength(double length);

  double getSpacing() const;
  void setSpacing(double spacing);

  // all things crossing line
  double getCrossOffset() const;
  void setCrossOffset(double cross_offset);

  double getCrossPadding() const;
  void setCrossPadding(double cross_padding);

  bool isCrossSkipped() const;
  void setCrossSkipped(bool cross_skip);

  // flips!
  bool isWidthFlipped() const;
  void setWidthFlipped(bool flip_width);

  bool isLengthFlipped() const;
  void setLengthFlipped(bool flip_length);

 protected:
  /// Longitude (in degrees) of the center of the survey.  Used
  /// with latitude to define the projection
  double center_longitude;

  /// Latitude (in degrees) of the center of the survey.  Used
  /// with longitude to define the projection
  double center_latitude;

  /// The principal direction of the survey; the heading of the first real trackline
  /// in degrees
  double survey_heading;

  /// The survey width, in meters; max distance between the outermost tracklines
  double width;

  /// The length of the survey; the length in meters of the primary tracklines
  double length;

  /// distance between meters, in degrees
  double spacing;

  /// distance from center of the survey for the crossing line;
  /// positive is AWAY from the start of the first real trackline,
  /// negative is TOWARDS; in meters
  double cross_offset;

  /// The extra padding for the crossing line outside the normal
  /// tracklines; defaults to spacing/2
  double cross_padding;

  /// Flag for whether or not to skip the crossing line
  bool cross_skip;

  /// flip the survey left/right-- flip over the tracklines
  bool flip_width;

  /// flip the survey top/bottom-- flip over the crossing line
  bool flip_length;


  void generateLines() override;

};

} // namespace ds_trackline


#endif //PROJECT_GRID_H
