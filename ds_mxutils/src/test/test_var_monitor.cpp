/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/24/19.
//


#include <ros/time.h>
#include <gtest/gtest.h>

#include <ds_mxutils/var_monitor.h>

void runVarMonitorTest(const std::string& name, bool min, bool max, bool timeout) {

  ros::NodeHandle nh;

  ds_mxutils::VarMonitor underTest(name, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL);

  underTest.loadParameters(nh, "var_monitor_test");

  ros::Time msg_t(100, 0);
  EXPECT_TRUE(underTest.checkTimeoutOk(msg_t));

  // lower limit
  if (min) {
    EXPECT_FALSE(underTest.checkValue(0.99, msg_t));
  } else {
    EXPECT_TRUE(underTest.checkValue(0.99, msg_t));
  }
  EXPECT_TRUE(underTest.checkValue(1.00, msg_t));

  // upper limit
  EXPECT_TRUE(underTest.checkValue(10.00, msg_t));
  if (max) {
    EXPECT_FALSE(underTest.checkValue(10.01, msg_t));
  } else {
    EXPECT_TRUE(underTest.checkValue(10.01, msg_t));
  }

  // timeout
  msg_t += ros::Duration(20.000);
  EXPECT_TRUE(underTest.checkTimeoutOk(msg_t));
  msg_t += ros::Duration(0.001);
  if (timeout) {
    EXPECT_FALSE(underTest.checkTimeoutOk(msg_t));
  } else {
    EXPECT_TRUE(underTest.checkTimeoutOk(msg_t));
  }

}

TEST(TestVarMonitor, param_min) {
  runVarMonitorTest("param_min", true, false, false);
}
TEST(TestVarMonitor, param_max) {
  runVarMonitorTest("param_max", false, true, false);
}
TEST(TestVarMonitor, param_timeout) {
  runVarMonitorTest("param_timeout", false, false, true);
}
TEST(TestVarMonitor, param_minmax) {
  runVarMonitorTest("param_minmax", true, true, false);
}
TEST(TestVarMonitor, param_mintimeout) {
  runVarMonitorTest("param_mintimeout", true, false, true);
}
TEST(TestVarMonitor, param_maxtimeout) {
  runVarMonitorTest("param_maxtimeout", false, true, true);
}
TEST(TestVarMonitor, param_minmaxtimeout) {
  runVarMonitorTest("param_minmaxtimeout", true, true, true);
}

namespace VarMonitorTest {

struct ParamEntry {
  std::string name;
  bool hasMin;
  bool hasMax;
  bool hasTimeout;

  ParamEntry(std::string _n, bool min, bool max, bool timeout)
      : name(_n), hasMin(min), hasMax(max), hasTimeout(timeout) {}
};

std::vector<ParamEntry> param_entries = {
    ParamEntry("param_min", true, false, false),
    ParamEntry("param_max", false, true, false),
    ParamEntry("param_timeout", false, false, true),
    ParamEntry("param_minmax", true, true, false),
    ParamEntry("param_mintimeout", true, false, true),
    ParamEntry("param_maxtimeout", false, true, true),
    ParamEntry("param_minmaxtimeout", true, true, true),
};

}

TEST(TestVarMonitor, param_loading_min) {

  ros::NodeHandle nh;

  for (const VarMonitorTest::ParamEntry& entry : VarMonitorTest::param_entries) {

    ds_mxutils::VarMonitor required(entry.name, ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL);
    if (entry.hasMin) {
      required.loadParameters(nh, "var_monitor_test");
      ASSERT_TRUE(static_cast<bool>(required.getMin()));
      EXPECT_EQ(1.0, *(required.getMin()));
    } else {
      EXPECT_THROW(required.loadParameters(nh, "var_monitor_test"), std::runtime_error);
    }

    ds_mxutils::VarMonitor optional(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL);
    optional.loadParameters(nh, "var_monitor_test");
    if (entry.hasMin) {
      ASSERT_TRUE(static_cast<bool>(optional.getMin()));
      EXPECT_EQ(1.0, *(optional.getMin()));
    } else {
      EXPECT_FALSE(static_cast<bool>(optional.getMin()));
    }

    ds_mxutils::VarMonitor forbidden(entry.name, ds_mxutils::FORBIDDEN, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL);
    if (entry.hasMin) {
      EXPECT_THROW(forbidden.loadParameters(nh, "var_monitor_test"), std::runtime_error);
    } else {
      forbidden.loadParameters(nh, "var_monitor_test");
      EXPECT_FALSE(static_cast<bool>(forbidden.getMin()));
    }
  }
}

TEST(TestVarMonitor, param_loading_max) {

  ros::NodeHandle nh;

  for (const VarMonitorTest::ParamEntry& entry : VarMonitorTest::param_entries) {

    ds_mxutils::VarMonitor required(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL);
    if (entry.hasMax) {
      required.loadParameters(nh, "var_monitor_test");
      ASSERT_TRUE(static_cast<bool>(required.getMax()));
      EXPECT_EQ(10.0, *(required.getMax()));
    } else {
      EXPECT_THROW(required.loadParameters(nh, "var_monitor_test"), std::runtime_error);
    }

    ds_mxutils::VarMonitor optional(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL);
    optional.loadParameters(nh, "var_monitor_test");
    if (entry.hasMax) {
      ASSERT_TRUE(static_cast<bool>(optional.getMax()));
      EXPECT_EQ(10.0, *(optional.getMax()));
    } else {
      EXPECT_FALSE(static_cast<bool>(optional.getMax()));
    }

    ds_mxutils::VarMonitor forbidden(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::FORBIDDEN, ds_mxutils::OPTIONAL);
    if (entry.hasMax) {
      EXPECT_THROW(forbidden.loadParameters(nh, "var_monitor_test"), std::runtime_error);
    } else {
      forbidden.loadParameters(nh, "var_monitor_test");
      EXPECT_FALSE(static_cast<bool>(forbidden.getMax()));
    }
  }
}

TEST(TestVarMonitor, param_loading_timeout) {

  ros::NodeHandle nh;

  for (const VarMonitorTest::ParamEntry& entry : VarMonitorTest::param_entries) {

    ds_mxutils::VarMonitor required(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED);
    if (entry.hasTimeout) {
      required.loadParameters(nh, "var_monitor_test");
      ASSERT_TRUE(static_cast<bool>(required.getTimeout()));
      EXPECT_EQ(20.0, (*(required.getTimeout())).toSec() );
    } else {
      EXPECT_THROW(required.loadParameters(nh, "var_monitor_test"), std::runtime_error);
    }

    ds_mxutils::VarMonitor optional(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL);
    optional.loadParameters(nh, "var_monitor_test");
    if (entry.hasTimeout) {
      ASSERT_TRUE(static_cast<bool>(optional.getTimeout()));
      EXPECT_EQ(20.0, (*(optional.getTimeout())).toSec() );
    } else {
      EXPECT_FALSE(static_cast<bool>(optional.getTimeout()));
    }

    ds_mxutils::VarMonitor forbidden(entry.name, ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::FORBIDDEN);
    if (entry.hasTimeout) {
      EXPECT_THROW(forbidden.loadParameters(nh, "var_monitor_test"), std::runtime_error);
    } else {
      forbidden.loadParameters(nh, "var_monitor_test");
      EXPECT_FALSE(static_cast<bool>(forbidden.getTimeout()));
    }
  }
}

TEST(TestVarMonitor, no_timeout_unless_received) {
  ros::NodeHandle nh;

  ds_mxutils::VarMonitor underTest("param_timeout", ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED);
  underTest.loadParameters(nh, "var_monitor_test");

  // no timeouts should be generated as we keep increasing the time
  ros::Time stamp(1000,0);
  for(size_t i=0; i<100; i++) {
    EXPECT_TRUE(underTest.checkTimeoutOk(stamp));
    stamp += ros::Duration(10);
  }

  // ... until we check the first value
  underTest.checkValue(0.0, stamp);
  stamp += ros::Duration(19.999);
  EXPECT_TRUE(underTest.checkTimeoutOk(stamp));

  stamp += ros::Duration(0.001);
  EXPECT_TRUE(underTest.checkTimeoutOk(stamp));

  stamp += ros::Duration(0.001);
  EXPECT_FALSE(underTest.checkTimeoutOk(stamp));
}

TEST(TestVarMonitor, bad_value_to_read) {
  ros::NodeHandle nh;

  ds_mxutils::VarMonitor underTest("param_bad", ds_mxutils::OPTIONAL, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED);
  EXPECT_THROW(underTest.loadParameters(nh, "var_monitor_test"), std::runtime_error);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "test_var_monitor");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}