/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/24/19.
//

#include "ds_mxutils/var_monitor.h"

#include <stdexcept>

namespace ds_mxutils {


VarMonitor::VarMonitor(const std::string& _name, VarMonitorFlags _min_flags, VarMonitorFlags _max_flags, VarMonitorFlags _timeout_flags) :
name(_name), min_flags(_min_flags), max_flags(_max_flags), timeout_flags(_timeout_flags)
{
  // do nothing
}

bool VarMonitor::checkValue(double val, const ros::Time& now) {
  bool ret = true;

  if (minimum && (val < *minimum)) {
    ret = false;
  }

  if (maximum && (val > *maximum)) {
    ret = false;
  }

  // update the timeout
  if (timeout) {
    timeout_time = now + *timeout;
  }

  return ret;
}

bool VarMonitor::checkTimeoutOk(const ros::Time& now) const {
  if (timeout_time) {
    return now <= *timeout_time;
  }
  return true;
}

const boost::optional<ros::Time>& VarMonitor::getTimeoutTime() const {
  return timeout_time;
}
const boost::optional<double>& VarMonitor::getMin() const {
  return minimum;
}
const boost::optional<double>& VarMonitor::getMax() const {
  return maximum;
}

const boost::optional<ros::Duration>& VarMonitor::getTimeout() const {
  return timeout;
}

void VarMonitor::loadParameters(ros::NodeHandle& nh, const std::string& ns) {
  std::string min_key = ns + "/" + name + "/min";
  std::string max_key = ns + "/" + name + "/max";
  std::string timeout_key = ns + "/" + name + "/timeout";

  minimum = loadParam(nh, min_key, min_flags);
  maximum = loadParam(nh, max_key, max_flags);

  auto timeout_sec = loadParam(nh, timeout_key, timeout_flags);
  if (timeout_sec) {
    timeout = ros::Duration(*timeout_sec);
  }

  if (!minimum && !maximum && !timeout) {
    ROS_WARN_STREAM("Variable monitor " <<ns <<"/" <<name <<" defines no constraints!");
  }
}

boost::optional<double> VarMonitor::loadParam(ros::NodeHandle& nh, const std::string& key, const VarMonitorFlags& flags) {
  boost::optional<double> ret;

  if (nh.hasParam(key)) {
    if (flags == FORBIDDEN) {
      // using ROS break isn't catchable in a unit test.  So that's useless...
      ROS_FATAL_STREAM("Parameter server has a key for " <<key <<" but it is marked as forbidden!");
      throw std::runtime_error("Parameter has key " + key + " but its marked as forbiddedn");
    }
    double tmp = std::numeric_limits<double>::quiet_NaN();
    nh.getParam(key, tmp);
    if (std::isnan(tmp)) {
      ROS_FATAL_STREAM("Unable to load parameter " + key);
      throw std::runtime_error("Unable to load parameter " + key);
    } else {
      ret = tmp;
    }
  } else if (flags == REQUIRED) {
    ROS_FATAL_STREAM("Parameter server has no value for required minimum value " <<key);
    throw std::runtime_error("Parameter server has no value for required minimum value " + key);
  }

  return ret;
}

} // namespace ds_mxutils
