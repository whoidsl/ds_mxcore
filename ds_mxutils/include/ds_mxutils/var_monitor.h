/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/24/19.
//

#ifndef DS_MXUTILS_VAR_MONITOR_H
#define DS_MXUTILS_VAR_MONITOR_H

#include <string>
#include <boost/optional.hpp>
#include <ros/ros.h>

namespace ds_mxutils {

enum VarMonitorFlags {
  REQUIRED=0,
  OPTIONAL=1,
  FORBIDDEN=2,
};
/// \brief The VarMonitor class is a conveinence class that monitors a single scalar value
/// periodically in response to an incoming message.  It is mostly useful when writing vehicle
/// status monitors.  Functionality is included for requiring minimal, maximal, and timeout values.
class VarMonitor {
 public:
  /// \brief Create the var monitor
  /// \param name The name in the parameter server to use when looking for parameters
  explicit VarMonitor(const std::string& name,
      VarMonitorFlags min_flags,
      VarMonitorFlags max_flags,
      VarMonitorFlags timeout_flags);

  /// \brief Check a value against the limits
  ///
  /// \param val The value to check against limits
  /// \param now The current time.  Used to update timeouts.
  /// \return True if the value is within the limits
  bool checkValue(double val, const ros::Time& now);

  /// \brief Check if a message has been received within the timeout period for this value
  ///
  /// \param now The current time.  Used to check the timeout
  /// \return True if a message has been received within the limit (or there is no timeout)
  bool checkTimeoutOk(const ros::Time& now) const;

  /// \brief Load min/max/timeout parameters from the parameter server
  ///
  /// If any parameters listed as required are not found, or forbidden parameters are found,
  /// a ROS_FATAL error message is printed and the node is halted with ROS_BREAK
  ///
  /// \param nh The ROS node handle to use
  /// \param ns The namespace to use.  Final parameters are read from <ns>/<name>/[min|max|timeout]
  void loadParameters(ros::NodeHandle& nh, const std::string& ns);

  const boost::optional<ros::Time>& getTimeoutTime() const;
  const boost::optional<double>& getMin() const;
  const boost::optional<double>& getMax() const;
  const boost::optional<ros::Duration>& getTimeout() const;

 protected:
  std::string name;
  VarMonitorFlags min_flags, max_flags, timeout_flags;
  boost::optional<ros::Duration> timeout;
  boost::optional<double> minimum;
  boost::optional<double> maximum;

  boost::optional<ros::Time> timeout_time;

  static boost::optional<double> loadParam(ros::NodeHandle& nh,
      const std::string& key, const VarMonitorFlags& flags);
};

} // namespace ds_mxutils

#endif //SRC_VAR_MONITOR_H
