/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/19/19.
//

#ifndef DS_MX_MESSAGE_QUEUE_H
#define DS_MX_MESSAGE_QUEUE_H

#include <mutex>
#include <condition_variable>
#include <memory>
#include <deque>

#include <ros/ros.h>

namespace ds_mx {

/// This class provides a receive queue of messages for use when writing rostests.  It's
/// templated to allow different message types.
///
/// Although simple to use, there is one major caveat: something, somewhere, must be running
/// ROS's tick function.  With a rostest node that runs a google test-- the typical usecase
/// here-- the easiest way to get this is to add a ros::AsyncSpinner to the main function
/// before calling RUN_ALL_TESTS().  For example:
///
///  ros::AsyncSpinner spinner(2); // use two threads
///  spinner.start(); // returns immediately
///
///  return RUN_ALL_TESTS();
///
/// \tparam T The message type to subscribe to.
template <typename T>
class TestMessageQueue {
 public:
  TestMessageQueue(const std::string topicname) {
    recv_sub = node_handle.subscribe(topicname, 10, &TestMessageQueue<T>::handle_cmd, this);
    recv_timeout = node_handle.createWallTimer(ros::WallDuration(1.0), &TestMessageQueue<T>::handle_timeout, this, true);
  }

  ~TestMessageQueue() {
    recv_sub.shutdown();
  }

  void SetUp() {
    std::lock_guard<std::mutex> lock(recv_mutex);
    recv_messages.clear();
  }

  /// \brief Wait until a message is in the queue.  Does not distinguish between
  /// new and old messages.
  bool waitForMessage(double timeout=0.0) {
    std::unique_lock<std::mutex> lock(recv_mutex);
    return _waitForMessage_inner(timeout, lock);
  }

  std::pair<bool, T> nextMessage(double timeout=0.0) {
    std::unique_lock<std::mutex> lock(recv_mutex);

    if (_waitForMessage_inner(timeout, lock)) {
      T ret = recv_messages.front();
      recv_messages.pop_front();

      return std::make_pair(true, ret);
    } else {
      return std::make_pair(false, T());
    }
  }

  size_t numMessages() {
    std::lock_guard<std::mutex> lock(recv_mutex);
    return recv_messages.size();
  }

 protected:
  void handle_cmd(const T& msg) {
    {
      std::lock_guard<std::mutex> lock(recv_mutex);
      recv_messages.push_back(msg);
    }
    recv_cond.notify_one();
  }

  void handle_timeout(const ros::WallTimerEvent& tim) {
    {
      std::lock_guard<std::mutex> lock(recv_mutex);
      recv_waiting = false;
    }
    recv_cond.notify_all();
  }

  /// The guts of waitForMessage are used by both usedForMessage AND nextMessage, but
  /// each function can only create a single std::unique_lock.  The standard solution of
  /// using a recursive_mutex won't work with condition variable because the condition
  /// will only unlock the recursive lock once-- which may not fully unlock a
  /// recursive lock.  This is why the standard condition variable doesn't include
  /// a specialization for the resurive_mutex type.  See how wonderful C++11 can be?
  /// \param timeout The timeout to wait for.  0 for no timeout.
  /// \param lock The lock to pass down to the condition variable.
  /// \return
  bool _waitForMessage_inner(double timeout, std::unique_lock<std::mutex>& lock) {
    recv_waiting = true;
    if (timeout > 0) {
      recv_timeout.setPeriod(ros::WallDuration(timeout), true);
      recv_timeout.start();
    }
    while (recv_messages.size() < 1 && recv_waiting) {
      recv_cond.wait(lock);
    }

    return recv_messages.size() > 0;
  }

 private:
  std::deque<T> recv_messages;
  std::mutex recv_mutex;
  std::condition_variable recv_cond;

  ros::NodeHandle node_handle;
  ros::Subscriber recv_sub;
  ros::WallTimer recv_timeout;
  bool recv_waiting;

};

/// \brief Expect a message from this queue in timeout seconds.  If no message is
/// received, the test automatically fails.
#define DSMX_EXPECT_MESSAGE(message, queue, timeout) \
{\
  auto tmp = (queue).nextMessage(timeout);\
  if (tmp.first) { message = tmp.second; }\
  else { FAIL() <<"No message received"; }\
}

}

#endif //DS_MX_MESSAGE_QUEUE_H
