# Importing MX Events into Sealog

The MX event reporting system generates events from a number of different sources.  In the future, it might prove
handy to convert these to events that could be imported into Webb Pinner's Sealog, currently in use for Jason and Alvin.

The format Sealog expects a JSON object with an ISO 8061 timestamp-- the standard used by JSON/Mongo.  The "id" field
appears to be a MongoDB ID field and can be omitted when submitting events.

```json
{
    "event_value": "ASNAP",
    "ts": "2018-05-22T22:28:52.268Z",
    "event_free_text": "",
    "event_author": "bot",
    "event_options": [],
    "id": "5b0499a692faed46c5aa905d"
}
```

Events are generated and logged in ROS (see the MxEventLog message) and logged in a bag file.  They can, however,
be converted into JSON objects via a python script and either written out or inserted directly into Sealog.

As of 2 May 2019, Webb recommended the Jason "asnap" script as a good example for how to do this:
https://github.com/webbpinner/sealog-server/blob/master/misc/sealog_asnap.py

