/**
* Copyright 2022 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by tmjoyce on 10/18/22.
//

#ifndef DS_MXPARAM_GEOPOINTLIST_H
#define DS_MXPARAM_GEOPOINTLIST_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <json/json.h>
#include "ds_mxparam_geopoint.h"

namespace ds_mx {

/// \brief This parameter stores a list of LatLon Geopoints
struct GeoPointList {
  typedef std::pair<std::string, std::string> Pair_t;
  typedef std::map<std::string, std::string> Map_t;

  GeoPointList();
  GeoPointList(const GeoPoint& element);
  GeoPointList(const std::vector<GeoPoint>& elements);

  std::map<std::string, std::string> dict;
  std::vector<ds_mx::GeoPoint> points;

  //Can't bind this return to const currently
  //GeoPoint& operator[](int position) const;
  GeoPoint& operator[](int position);

  bool operator >= (const GeoPointList& other) const;
  bool operator <= (const GeoPointList& other) const;
  bool operator != (const GeoPointList& other) const;
  bool operator == (const GeoPointList& other) const;

  static GeoPointList FromString(const std::string&);
  friend std::ostream & operator << (std::ostream &out, const GeoPointList& p);

  Json::Value toJson() const;
  static GeoPointList FromJson(const Json::Value&);

};

std::ostream & operator << (std::ostream &out, const GeoPointList& p);

} // namespace ds_mx

#endif //DS_MXPARAM_GEOPOINTLIST_H
