/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 4/23/19.
//

#ifndef DS_MXPARAMETER_SHARED_H
#define DS_MXPARAMETER_SHARED_H

#include <memory>
#include <string>
#include <vector>
#include <json/json.h>
#include <boost/optional.hpp>
#include <ds_mx_msgs/MxSharedParams.h>

#include "ds_mxparameter.h"

#include "ds_mxparam_geopoint.h"
#include "ds_mxparam_keyvaluelist.h"

namespace ds_mx {

template <typename T>
class SharedParameterPrivateT;

template <typename T>
class SharedParameterT : public ParameterT<T> {
 public:
  typedef std::shared_ptr<SharedParameterT> Ptr;
  typedef SharedParameterPrivateT<T> PrivateType;

  explicit SharedParameterT(const std::shared_ptr<SharedParameterPrivateT<T> >& _impl);
  SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name,
      ParameterFlag isStatic=ParameterFlag::STATIC);
  SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, const T& defaultValue,
      ParameterFlag isStatic=ParameterFlag::STATIC);

  const boost::optional<T> & getMin() const;
  const boost::optional<T> & getMax() const;

  bool isValid(const T& test) const;
  bool isValidStr(const std::string& str) const override;

  void setFromString(const std::string&, Role role=RoleValue) override;
  bool canSetFromString(const std::string&, Role role=RoleValue) const override;
  std::string GuiString(Role role=RoleValue) const override;

  void loadJson(const Json::Value& config) override;
  Json::Value saveJson() const override;

 private:
  auto d_func() noexcept -> SharedParameterPrivateT<T>*;
  auto d_func() const noexcept -> SharedParameterPrivateT<T> const*;

};

typedef SharedParameterT<bool> SharedBoolParam;
typedef SharedParameterT<int> SharedIntParam;
typedef SharedParameterT<double> SharedDoubleParam;
typedef SharedParameterT<std::string> SharedStringParam;
typedef SharedParameterT<GeoPoint> SharedGeoPointParam;
typedef SharedParameterT<KeyValueList> SharedKeyValueListParam;
typedef SharedParameterT<GeoPointList> SharedGeoPointListParam;

/// \brief The shared parameter registry keeps track of all shared parameters
/// in the mission and provides a single point-of-entry for changing them
class SharedParameterRegistry {
 public:
  SharedParameterRegistry();

  void loadJson(const Json::Value& config);
  Json::Value saveJson();

  void loadRos(const ds_mx_msgs::MxSharedParams& rosmsg);
  ds_mx_msgs::MxSharedParams saveRos();


  const ParameterCollection& collection() const;
  ParameterCollection& collection();

  void loadOrLink(const ds_mx::Parameter::Ptr& param, const Json::Value& json);

 protected:
  std::shared_ptr<ParameterCollection> collection_;

};
}

#endif //DS_MXPARAMETER_SHARED_H
