/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 7/24/21.
//

#ifndef DS_MX_ACTION_PAYLOAD_H
#define DS_MX_ACTION_PAYLOAD_H

#include <ds_mxcore/ds_mxcore.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/action_definition.h>
#include <memory>

namespace ds_mx {
/// \brief Base class for payloads implemented as an action
///
/// There are two modes of operation.  If "finish_on_action_done" is set (default), then
/// the idea is to run the subtask as long as the action is ongoing and then stop it.
/// In this mode, return code is:
///
/// * SUCCESS if the actionlib call finishes successfully
/// * FAIL if the actionlib call finishes unsuccessfully
/// * FAIL if the subtask finishes before either
///
/// In the second mode, if "finish_on_action_done" is false, the idea is to run
/// an actionlib call at the start of the task and continue running the subtask to its own completion.
/// In this mode, return code is:
///
/// * SUCCESS if the actionlib call finishes successfully AND the subtask finishes successfully
/// * FAIL if the subtask finishes before the actionlib call
/// * FAIL if the subtask or actionlib call finish with failure
///
/// \tparam ActionSpec ActionLib type
/// \tparam Derived Derived class
template<typename ActionSpec, class Derived>
class ActionPayload : public ds_mx::MxPayloadTask {
 public:
  ACTION_DEFINITION(ActionSpec);

  ActionPayload(const std::string &action_server_name);

  virtual ~ActionPayload() = 0;

  void init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) override;

  void init_ros(ros::NodeHandle &nh) override;
  bool validate() const override = 0;

  void onStart(const ds_nav_msgs::NavState &state) override;
  void onStop(const ds_nav_msgs::NavState &state) override;

  std::string getActionServerName() const;
  std::string getResolvedServerName() const;
  /// Check to see if an action result was successful.  By default, just assume it is.
  /// This is a BAD assumption.  You should override this!
  virtual bool resultSuccessful(const actionlib::SimpleClientGoalState& state, const ResultConstPtr& result) { return true;}

  void _done_callback(const actionlib::SimpleClientGoalState &state, const ResultConstPtr &result);
  void _active_callback();

 protected:
  std::unique_ptr <actionlib::SimpleActionClient<ActionSpec>> action_client_;
  std::string action_server_name_;
  std::string resolved_action_server_name_;

  ds_mx::BoolParam finish_on_action_done_;
  ds_mx::TaskReturnCode next_return_code_;
  bool task_succeeded_;

  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState &state) override;
  ds_mx::TaskReturnCode subtaskDone(const ds_nav_msgs::NavState& state,
                                    TaskReturnCode subtaskCode) override;

  void parameterChanged(const ds_nav_msgs::NavState &state) override;

  void _send_goal(const ds_nav_msgs::NavState &state);
  int disconnected_ticks;

}; // ActionPayload


template <typename ActionSpec, class Derived>
ActionPayload<ActionSpec, Derived>::ActionPayload(const std::string& action_server_name)
: action_server_name_(action_server_name),
  resolved_action_server_name_("ROS_NOT_INITIALIZED"),
  finish_on_action_done_(params, "finish_on_action_done", true,
                         ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL) {
  next_return_code_ = ds_mx::TaskReturnCode::FAILED;
}

template <typename ActionSpec, class Derived>
ActionPayload<ActionSpec, Derived>::~ActionPayload() = default;

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPayloadTask::init(config, compiler);

  // link our child's parameters so that parameters like center point, etc,
  // can propagate down.
  params->linkCollection(subtask->getParametersPtr());
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::init_ros(ros::NodeHandle &nh) {
  ds_mx::MxPayloadTask::init_ros(nh);

  // initialize the action client
  resolved_action_server_name_ = ros::names::resolve(nh.getNamespace(), action_server_name_);

  ROS_INFO_STREAM("Initializing action_client to access "<<resolved_action_server_name_);
  action_client_.reset(new actionlib::SimpleActionClient<ActionSpec>(nh,
      action_server_name_, false));

  /*
  if (!action_client_->isServerConnected()) {
    ROS_WARN_STREAM("ActionClient for " <<resolved_action_server_name_ <<" not connected! Waiting... 10 seconds");
    action_client_->waitForServer(ros::Duration(10));
    if (!action_client_->isServerConnected()) {
      ROS_FATAL_STREAM("Unable to connect to ActionServer for: \"" <<resolved_action_server_name_ <<"\"");
      ROS_BREAK();
    }
  }
  */
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxPayloadTask::onStart(state);

  if (!action_client_) {
    ROS_ERROR_STREAM("No action client defined for server + " <<action_server_name_);
    next_return_code_ = ds_mx::TaskReturnCode::FAILED;
    return;
  }

  task_succeeded_ = false;
  disconnected_ticks = 0;
  _send_goal(state);
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::_send_goal(const ds_nav_msgs::NavState &state) {
  next_return_code_ = ds_mx::TaskReturnCode::RUNNING;

  // this is annoying because of static vs. dynamic binding.  But we can get around it by remembering that
  // the this pointer should be of the Derived type
  Derived* derived_this = dynamic_cast<Derived*>(this);
  if (derived_this == nullptr) {
    // of course, we still double-check this assumption!
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Task " <<name
                                                          <<" has derived parameter " << typeid(Derived).name()
                                                          <<" but cannot cast this!  This should never happen, and breaks action primitives.  This is a programming bug");
    next_return_code_ = ds_mx::TaskReturnCode::FAILED;
    return;
  }
  Goal goal = derived_this->toMsg();

  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "Action dispatched to " + resolved_action_server_name_);
  action_client_->sendGoal(goal, boost::bind(&ActionPayload<ActionSpec, Derived>::_done_callback, this, _1, _2),
                           boost::bind(&ActionPayload<ActionSpec, Derived>::_active_callback, this));
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::onStop(const ds_nav_msgs::NavState &state) {
  // PayloadTask does not specify an onStop
  ds_mx::MxComplexTask::onStop(state);

  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "OnStop: Cancel Action issued to " + resolved_action_server_name_);
  action_client_->cancelGoal();
}

template <typename ActionSpec, class Derived>
ds_mx::TaskReturnCode ActionPayload<ActionSpec, Derived>::onTick(const ds_nav_msgs::NavState &state) {
  if (action_client_->isServerConnected()) {
    disconnected_ticks = 0;
  } else {
    ROS_WARN_STREAM("MxActionPayload disconnected for " <<disconnected_ticks
    << " ticks @ " <<state.header.stamp);
    disconnected_ticks++;
    if (disconnected_ticks > 10) {
      MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_FAILED, "Connection to actionserver lost");
      return ds_mx::TaskReturnCode::FAILED;
    }
  }
  return next_return_code_;
}

template <typename ActionSpec, class Derived>
ds_mx::TaskReturnCode ActionPayload<ActionSpec, Derived>::subtaskDone(const ds_nav_msgs::NavState& state,
                                    TaskReturnCode subtaskCode) {

  if (finish_on_action_done_.get()) {
    // I suppose this could have happened, but it's extremely unlikely
    if (task_succeeded_) {
      return ds_mx::TaskReturnCode::SUCCESS;
    }
    return ds_mx::TaskReturnCode::FAILED;
  } else {
    // continue-on-action-success mode
    if (!task_succeeded_) {
      // actionlib call must succeed for task to be successful
      return ds_mx::TaskReturnCode::FAILED;
    }

    // if actionlib is good, then simply use the subtask code
    return subtaskCode;
  }
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::parameterChanged(const ds_nav_msgs::NavState& state) {
  // cancel the existing task, if its running
  if (isRunning()) {
    MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "ParamChanged: Cancel Action issued to " + resolved_action_server_name_);
    action_client_->cancelGoal();

    // update parameters & issue a new command
    _send_goal(state);
  }
}

template <typename ActionSpec, class Derived>
std::string ActionPayload<ActionSpec, Derived>::getActionServerName() const {
  return action_server_name_;
}

template <typename ActionSpec, class Derived>
std::string ActionPayload<ActionSpec, Derived>::getResolvedServerName() const {
  return resolved_action_server_name_;
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::_done_callback(const actionlib::SimpleClientGoalState& state, const ResultConstPtr& result) {
  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "Result from Action " + action_server_name_);
  if (result && resultSuccessful(state, result)) {
    task_succeeded_ = true;
    if (finish_on_action_done_.get()) {
      next_return_code_ = ds_mx::TaskReturnCode::SUCCESS;
    } else {
      next_return_code_ = ds_mx::TaskReturnCode::RUNNING;
    }
  } else {
    next_return_code_ = ds_mx::TaskReturnCode::FAILED;
  }
}

template <typename ActionSpec, class Derived>
void ActionPayload<ActionSpec, Derived>::_active_callback() {
  // don't think we have to do anything? Maybe we should just log?
  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "Action " + action_server_name_ + " now ACTIVE");
}

} // namespace ds_mx




#endif //DS_MX_ACTION_PAYLOAD_H
