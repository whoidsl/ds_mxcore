/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/30/20.
//

#ifndef DS_MX_STDPRIMITIVE_TEST_MOCK_ACTION_SERVER_H
#define DS_MX_STDPRIMITIVE_TEST_MOCK_ACTION_SERVER_H

#include <gtest/gtest.h>

#include <mutex>
#include <condition_variable>
#include <chrono>
#include <boost/optional.hpp>

#include <actionlib/server/simple_action_server.h>
#include <ds_mxcore/ds_mx_action_primitive.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <actionlib/action_definition.h>

namespace ds_mx {

template<typename ActionSpec, class Primitive>
class MockActionServer : public ::testing::Test {

  // actionlib provides this handy macro to define easy access to all the different subtypes for the ActionSpec
  // tempmlate parameter
  ACTION_DEFINITION(ActionSpec)

  virtual std::string getJSON() const = 0;

  void SetUp() override {
    node_handle.reset(new ros::NodeHandle());

    // initialize internal test stuff
    test_counter = 0;
    test_done = 0;
    test_fail = 0;
    test_return_failed = 0;
    callback_started = 0;
    callback_finished = 0;
    last_goal.reset();

    // initialize our task
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    Json::Value config;
    Json::Reader reader;
    reader.parse(getJSON(), config);

    // initialize test Task
    underTest = std::make_shared<Primitive>();
    underTest->init(config, compiler);

    // initialize the action server
    action_server.reset(new actionlib::SimpleActionServer<ActionSpec>(*node_handle, underTest->getActionServerName(),
                                                                      boost::bind(&MockActionServer<ActionSpec, Primitive>::action_server_callback, this, _1), false));
    action_server->start();

    // wire up the ROS connections for our Task
    underTest->init_ros(*node_handle);
  }

  void action_server_callback(const GoalConstPtr& goal) {
    Feedback feedback;
    Result result;

    std::unique_lock<std::mutex> lock(test_mutex);
    int server_counter = test_counter;

    last_goal = *goal;
    ROS_INFO_STREAM("Starting callback...");
    callback_started++;
    while (!test_done) {
      // wait until prompted to do something
      auto now = std::chrono::system_clock::now();
      test_cv.wait_until(lock, now + std::chrono::milliseconds(100), [this, server_counter]
          { return this->test_counter > server_counter || this->action_server->isPreemptRequested() || !ros::ok(); });

      // check if we've been asked to stop
      if (action_server->isPreemptRequested() || !ros::ok()) {
        result.success = false;
        action_server->setPreempted(result);
        test_fail++;
        callback_finished++;
        return;
      }

      // continue as per usual
      feedback.distance_to_go = test_counter;
      feedback.time_to_go = test_counter;
      server_counter = test_counter;
      action_server->publishFeedback(feedback);
    }

    result.success = !test_return_failed;
    action_server->setSucceeded(result);
    callback_finished++;
  }

 protected:
  std::unique_ptr<ros::NodeHandle> node_handle;
  std::unique_ptr<actionlib::SimpleActionServer<ActionSpec> > action_server;
  std::shared_ptr<Primitive> underTest;
  ds_nav_msgs::NavState state;
  boost::optional<Goal> last_goal;

  std::mutex test_mutex;
  std::condition_variable test_cv;
  int test_counter;
  int test_done;
  int test_fail;
  int test_return_failed;

  int callback_started;
  int callback_finished;
};

} // namespace ds_mx

#endif // DS_MX_STDPRIMITIVE_TEST_MOCK_ACTION_SERVER_H
