/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/19.
//

#ifndef DS_MX_MOCK_COMPILER_H
#define DS_MX_MOCK_COMPILER_H

#include "../ds_mxcompiler.h"

namespace ds_mx {

class MxMockException : public std::runtime_error {
 public:
  MxMockException(const std::string& what) : std::runtime_error(what) {}
};

class MxMockCompiler : public MxCompiler {
 protected:
  MxMockCompiler();

 public:
  static std::shared_ptr<MxMockCompiler> create();

  void addSubtask(const std::string &token, const std::shared_ptr<ds_mx::MxTask> &subtask);
  std::shared_ptr<MxTask> loadTask(const std::string &name, const Json::Value &ast) override;

 protected:
  std::map<std::string, std::shared_ptr<ds_mx::MxTask>> subtaskRegistry;
};


} // namespace ds_mx

#endif //DS_MX_MOCK_COMPILER_H
