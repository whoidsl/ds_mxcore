/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//

#ifndef PROJECT_DS_MXROOTTASK_H
#define PROJECT_DS_MXROOTTASK_H

#include "ds_mxtask.h"
#include "ds_mxcomplextask.h"

namespace ds_mx {


/// \brief A root task of the task tree; the starting point for any mission
class MxRootTask : public MxComplexTask {
 public:

  MxRootTask();
  ~MxRootTask() override;

  std::string toDot() override;

  /// \brief This is the core function that gets called to update state internally
  /// Root Tasks should not ever quit, so timestamp handling needs to be done
  /// differently.
  virtual TaskReturnCode tick(const ds_nav_msgs::NavState& state) = 0;

  /// Stop event handler for root task.
  /// The root task should never stop, so we throw an error message
  void onStop(const ds_nav_msgs::NavState& state) override;

  /// The Root Task includes launch positions as a required parameter.  This
  /// can be used to get an initial state for prediction / display stuff
  ds_nav_msgs::NavState getLaunchState() const;


 protected:
  ds_mx::GeoPointParam launch_pt;
};

} // namespace ds_mx

#endif //PROJECT_DS_MXROOTTASK_H
