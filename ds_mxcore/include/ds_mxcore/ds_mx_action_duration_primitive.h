/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 7/1/20.
//

#ifndef DS_MX_ACTION_DURATION_PRIMITIVE_H
#define DS_MX_ACTION_DURATION_PRIMITIVE_H

#include "ds_mx_action_primitive.h"

namespace ds_mx {

/// Implements timeouts & duration math for primitives that execute for a specified duration.  This is
/// a common way to implement primitives, so it makes sense to centralize that functionality in a common base class.
/// \tparam ActionSpec The action type used.  In addition to the ActionPrimitive requirements, this must include
/// a ros time field "finish_at"
/// \tparam Derived Type derived from this class
template <typename ActionSpec, class Derived>
class ActionDurationPrimitive : public ds_mx::ActionPrimitive<ActionSpec, Derived> {
 public:
  ActionDurationPrimitive(const std::string& action_server_name);
  virtual ~ActionDurationPrimitive() = 0;

  void onStart(const ds_nav_msgs::NavState &state) override;

  void resetTimeout(const ros::Time& now) override;
 protected:
  ds_mx::DoubleParam duration;
  ds_mx::DoubleParam timeout_padding;

  ros::Time start_time;
  // annoyingly, templates sometimes break the 2-pass compilation process unless you specify things like this
  using ds_mx::ActionPrimitive<ActionSpec, Derived>::params;
  using ds_mx::ActionPrimitive<ActionSpec, Derived>::name;
  using ds_mx::ActionPrimitive<ActionSpec, Derived>::timeout;
  //using ds_mx::ActionPrimitive<ActionSpec, Derived>::parameterChanged;

  ros::Time getEndTime() const;
  ros::Time getSimEnd(const ds_nav_msgs::NavState& state) const;
  bool durationValid() const; // do this seperately, so child classes must STILL implement validate

  // necessary to dynamically update timeouts
  void parameterChanged(const ds_nav_msgs::NavState& state) override;
};

template <typename ActionSpec, class Derived>
ActionDurationPrimitive<ActionSpec, Derived>::ActionDurationPrimitive(const std::string& action_server_name) :
    ds_mx::ActionPrimitive<ActionSpec, Derived>(action_server_name),
    duration(params, "duration", ds_mx::ParameterFlag::DYNAMIC),
    timeout_padding(params, "timeout_padding", 5.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL)
    { /* do nothing else */ }

template <typename ActionSpec, class Derived>
ActionDurationPrimitive<ActionSpec, Derived>::~ActionDurationPrimitive() = default;

template <typename ActionSpec, class Derived>
void ActionDurationPrimitive<ActionSpec, Derived>::onStart(const ds_nav_msgs::NavState &state) {
  start_time = state.header.stamp;
  ds_mx::ActionPrimitive<ActionSpec, Derived>::onStart(state);
}

template <typename ActionSpec, class Derived>
void ActionDurationPrimitive<ActionSpec, Derived>::resetTimeout(const ros::Time &now) {
  // set the MX timeout for 5 seconds after the
  ros::Time end = getEndTime();
  if (!end.isValid() || end.isZero()) {
    ROS_ERROR("UNABLE to set timeout for this task, forcing immediate timeout!");
    timeout = now;
    return;
  }
  timeout = end + ros::Duration(timeout_padding.get()); // Task timeout is always 5 seconds after action should timeout on its own
}

template <typename ActionSpec, class Derived>
ros::Time ActionDurationPrimitive<ActionSpec, Derived>::getEndTime() const {
  if (!start_time.isValid() || start_time.isZero()) {
    ROS_ERROR("Invalid start time! This probably means initial time was invalid");
    return ros::Time();
  }

  return start_time + ros::Duration(duration.get());
}
template <typename ActionSpec, class Derived>
ros::Time ActionDurationPrimitive<ActionSpec, Derived>::getSimEnd(const ds_nav_msgs::NavState& state) const {
  return state.header.stamp + ros::Duration(duration.get());
}

template <typename ActionSpec, class Derived>
bool ActionDurationPrimitive<ActionSpec, Derived>::durationValid() const {
  if (duration.get() <= 0) {
    ROS_ERROR_STREAM("Duration for task " <<name <<" is not defined");
    return false;
  }
  return true;
}

template <typename ActionSpec, class Derived>
void ActionDurationPrimitive<ActionSpec, Derived>::parameterChanged(const ds_nav_msgs::NavState &state) {
  ds_mx::ActionPrimitive<ActionSpec, Derived>::parameterChanged(state);
  resetTimeout(state.header.stamp); // also update our internal timeout
}

} // namespace ds_mx

#endif // DS_MX_ACTION_DURATION_PRIMITIVE_H
