/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/3/19.
//

#ifndef DS_MXPARAM_KEYVALUELIST_H
#define DS_MXPARAM_KEYVALUELIST_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <json/json.h>

namespace ds_mx {

/// \brief This parameter stores a list of key/value pairs, mostly for use
/// with payloads.
struct KeyValueList {
  typedef std::pair<std::string, std::string> Pair_t;
  typedef std::map<std::string, std::string> Map_t;

  KeyValueList();
  KeyValueList(const std::pair<std::string, std::string>& element);
  KeyValueList(const std::vector<std::pair<std::string, std::string>>& elements);

  std::map<std::string, std::string> dict;


  const std::string& operator[](std::string key) const;
  std::string& operator[](std::string key);
  // we have to implement the greater-than / less-than operators
  // because some template code requires it, but they really don't make
  // much sense.  We just pass the comparison on to the underlying map.
  // Which probably doesn't do what you want.
  bool operator >= (const KeyValueList& other) const;
  bool operator <= (const KeyValueList& other) const;
  bool operator != (const KeyValueList& other) const;
  bool operator == (const KeyValueList& other) const;

  static KeyValueList FromString(const std::string&);
  friend std::ostream & operator << (std::ostream &out, const KeyValueList& p);

  Json::Value toJson() const;
  static KeyValueList FromJson(const Json::Value&);

};

std::ostream & operator << (std::ostream &out, const KeyValueList& p);

} // namespace ds_mx

#endif //DS_MXPARAM_KEYVALUELIST_H
