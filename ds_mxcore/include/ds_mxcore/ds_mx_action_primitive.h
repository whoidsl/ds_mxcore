/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
//
// Created by ivaughn on 6/16/20.
//

#ifndef DS_MX_ACTION_PRIMITIVE_H
#define DS_MX_ACTION_PRIMITIVE_H

#include <ds_mxcore/ds_mxcore.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/action_definition.h>
#include <memory>

namespace ds_mx {

/// \brief Base class for primitives implemented as an action
template<typename ActionSpec, class Derived>
class ActionPrimitive : public ds_mx::MxPrimitiveTask {
 public:
  ACTION_DEFINITION(ActionSpec);

  ActionPrimitive(const std::string& action_server_name);
  virtual ~ActionPrimitive() = 0;

  void init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) override;
  void init_ros(ros::NodeHandle& nh) override;
  // bool validate() const override must be overridden by child class

  void onStart(const ds_nav_msgs::NavState &state) override;
  void onStop(const ds_nav_msgs::NavState &state) override;

  // getDisplay must be overridden

  double distanceToGo() const;
  double timeToGo() const;

  std::string getActionServerName() const;
  std::string getResolvedServerName() const;

  void _done_callback(const actionlib::SimpleClientGoalState& state, const ResultConstPtr& result);
  void _active_callback();
  void _feedback_callback(const FeedbackConstPtr& feedback);
  bool _cancel_event_callback(ds_mx_msgs::MxEvent event);

 protected:
  std::unique_ptr<actionlib::SimpleActionClient<ActionSpec> > action_client_;
  std::string action_server_name_;
  std::string resolved_action_server_name_;

  // event handler will cause event to FAIL if triggered
  ds_mx::EventHandler cancel;

  double distance_to_go_;
  double time_to_go_;
  ds_mx::TaskReturnCode next_return_code_;

  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState &state) override;
  void parameterChanged(const ds_nav_msgs::NavState& state) override;
  void _send_goal(const ds_nav_msgs::NavState &state);

  int disconnected_ticks;
};

template <typename ActionSpec, class Derived>
ActionPrimitive<ActionSpec, Derived>::ActionPrimitive(const std::string& action_server_name)
: action_server_name_(action_server_name),
  resolved_action_server_name_("ROS_NOT_INITIALIZED"),
  cancel(events, "cancel", "/cancel/primitive", &ActionPrimitive<ActionSpec, Derived>::_cancel_event_callback, this) {
  distance_to_go_ = std::numeric_limits<double>::quiet_NaN();
  time_to_go_ = std::numeric_limits<double>::quiet_NaN();
  next_return_code_ = ds_mx::TaskReturnCode::FAILED;
}

template <typename ActionSpec, class Derived>
ActionPrimitive<ActionSpec, Derived>::~ActionPrimitive() = default;

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);

  // don't really have to do anything else
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::init_ros(ros::NodeHandle &nh) {
  // initialize the action client
  resolved_action_server_name_ = ros::names::resolve(nh.getNamespace(), action_server_name_);

  ROS_INFO_STREAM("Initializing action_client to access "<<resolved_action_server_name_);
  action_client_.reset(new actionlib::SimpleActionClient<ActionSpec>(nh,
      action_server_name_, false));

  /*
  if (!action_client_->isServerConnected()) {
    ROS_WARN_STREAM("ActionClient for " <<resolved_action_server_name_ <<" not connected! Waiting... 10 seconds");
    action_client_->waitForServer(ros::Duration(10));
    if (!action_client_->isServerConnected()) {
      ROS_FATAL_STREAM("Unable to connect to ActionServer for: \"" <<resolved_action_server_name_ <<"\"");
      ROS_BREAK();
    }
  }
  */
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxPrimitiveTask::onStart(state);

  if (!action_client_) {
    ROS_ERROR_STREAM("No action client defined for server + " <<action_server_name_);
    next_return_code_ = ds_mx::TaskReturnCode::FAILED;
    return;
  }

  disconnected_ticks = 0;
  _send_goal(state);
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::_send_goal(const ds_nav_msgs::NavState &state) {
  distance_to_go_ = std::numeric_limits<double>::quiet_NaN();
  time_to_go_ = std::numeric_limits<double>::quiet_NaN();
  next_return_code_ = ds_mx::TaskReturnCode::RUNNING;

  // this is annoying because of static vs. dynamic binding.  But we can get around it by remembering that
  // the this pointer should be of the Derived type
  Derived* derived_this = dynamic_cast<Derived*>(this);
  if (derived_this == nullptr) {
    // of course, we still double-check this assumption!
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Task " <<name
                                                          <<" has derived parameter " << typeid(Derived).name()
                                                          <<" but cannot cast this!  This should never happen, and breaks action primitives.  This is a programming bug");
    next_return_code_ = ds_mx::TaskReturnCode::FAILED;
    return;
  }
  Goal goal = derived_this->toMsg();

  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "Action dispacted to " + resolved_action_server_name_);
  action_client_->sendGoal(goal, boost::bind(&ActionPrimitive<ActionSpec, Derived>::_done_callback, this, _1, _2),
                           boost::bind(&ActionPrimitive<ActionSpec, Derived>::_active_callback, this),
                           boost::bind(&ActionPrimitive<ActionSpec, Derived>::_feedback_callback, this, _1));
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::onStop(const ds_nav_msgs::NavState &state) {
  ds_mx::MxPrimitiveTask::onStop(state);

  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "OnStop: Cancel Action issued to " + resolved_action_server_name_);
  action_client_->cancelGoal();
}

template <typename ActionSpec, class Derived>
ds_mx::TaskReturnCode ActionPrimitive<ActionSpec, Derived>::onTick(const ds_nav_msgs::NavState &state) {
  if (action_client_->isServerConnected()) {
    disconnected_ticks = 0;
  } else {
    ROS_WARN_STREAM("MxActionPrimitive disconnected for " <<disconnected_ticks
    << " ticks @ " <<state.header.stamp);
    disconnected_ticks++;
    if (disconnected_ticks > 10) {
      MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_FAILED, "Connection to actionserver lost");
      return ds_mx::TaskReturnCode::FAILED;
    }
  }
  return next_return_code_;
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::parameterChanged(const ds_nav_msgs::NavState& state) {
  // cancel the existing task, if its running
  if (isRunning()) {
    MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "ParamChanged: Cancel Action issued to " + resolved_action_server_name_);
    action_client_->cancelGoal();

    // update parameters & issue a new command
    _send_goal(state);
  }
}

template <typename ActionSpec, class Derived>
double ActionPrimitive<ActionSpec, Derived>::distanceToGo() const {
  return distance_to_go_;
}

template <typename ActionSpec, class Derived>
double ActionPrimitive<ActionSpec, Derived>::timeToGo() const {
  return time_to_go_;
}

template <typename ActionSpec, class Derived>
std::string ActionPrimitive<ActionSpec, Derived>::getActionServerName() const {
  return action_server_name_;
}

template <typename ActionSpec, class Derived>
std::string ActionPrimitive<ActionSpec, Derived>::getResolvedServerName() const {
  return resolved_action_server_name_;
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::_done_callback(const actionlib::SimpleClientGoalState& state, const ResultConstPtr& result) {
  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "Result from Action " + action_server_name_);
  if (result && result->success) {
    next_return_code_ = ds_mx::TaskReturnCode::SUCCESS;
  } else {
    next_return_code_ = ds_mx::TaskReturnCode::FAILED;
  }
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::_active_callback() {
  // don't think we have to do anything? Maybe we should just log?
  MX_LOG_EVENT_TEXT(this, MX_LOG_TASK_COMMS, "Action " + action_server_name_ + " now ACTIVE");
}

template <typename ActionSpec, class Derived>
void ActionPrimitive<ActionSpec, Derived>::_feedback_callback(const FeedbackConstPtr& feedback) {
  if (feedback) {
    distance_to_go_ = feedback->distance_to_go;
    time_to_go_ = feedback->time_to_go;
  }
}

template <typename ActionSpec, class Derived>
bool ActionPrimitive<ActionSpec, Derived>::_cancel_event_callback(ds_mx_msgs::MxEvent event) {
  next_return_code_ = ds_mx::TaskReturnCode::FAILED;
  return false;
}

} // namespace mx_stdprimitives

#endif // DS_MX_ACTION_PRIMITIVE_H
