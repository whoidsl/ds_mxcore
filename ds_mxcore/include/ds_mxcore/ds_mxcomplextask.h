/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//

#ifndef PROJECT_DS_MXCOMPLEXTASK_H
#define PROJECT_DS_MXCOMPLEXTASK_H

#include "ds_mxtask.h"

namespace ds_mx {


/// \brief The base class for all non-primitive tasks
class MxComplexTask : public MxTask {
 public:

  virtual ~MxComplexTask();

  TaskReturnCode tick(const ds_nav_msgs::NavState& state) override;
  Json::Value toJson() override;
  void init(const Json::Value& config, MxCompilerPtr compiler) override;
  void init_ros(ros::NodeHandle& nh) override; // does nothing
  std::string toDot() override;

  const std::vector<std::shared_ptr<MxTask> >& getSubtasks() const;
  std::vector<std::shared_ptr<MxTask> >& getSubtasks();

  std::shared_ptr<MxTask> loadSubtask(ds_mx::MxCompilerPtr compiler,
                                      const std::string& name, const Json::Value& config);

  void onStart(const ds_nav_msgs::NavState& state) override = 0;
  void onStop(const ds_nav_msgs::NavState& state) override;

  void updateStatus(ds_mx_msgs::MxMissionStatus& status) const override;
  void handleEvent(const ds_mx_msgs::MxEvent& event) override;
  bool subtreeContains(const boost::uuids::uuid& query_uuid) const override;

 protected:
  /// \brief This is a complete list of all this task's subtasks.
  ///
  /// It is automatically managed by the tree parser.  It usually helps
  /// to keep your own pointers to subtasks to make them easy to
  /// keep track of.
  std::vector<std::shared_ptr<MxTask> > subtasks;

  /// \brief Pointer to the currently-active subtask
  std::shared_ptr<MxTask> active_subtask;


  /// \brief Callback triggered every time a subtask completes
  virtual TaskReturnCode subtaskDone(const ds_nav_msgs::NavState& state,
                                     TaskReturnCode subtaskCode) = 0;

  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState& state) override;

  /// \brief Start a subtask.
  /// \param newSubtask The new subtask to start.
  void startSubtask(const ds_nav_msgs::NavState& state,
                    const std::shared_ptr<MxTask>& newSubtask);

  /// \brief Switch from the currently-active subtask to a new one.  This is
  /// the recommended way to switch subtasks
  ///
  /// Basically a shortcut for:
  ///     stopActiveSubtask();
  ///     startSubtask(some_other_subtask);
  ///
  /// But it's less chalk and
  ///
  /// \param newSubtask
  void switchSubtasks(const ds_nav_msgs::NavState& state,
                      const std::shared_ptr<MxTask>& newSubtask);

  /// \brief Preempt a subtask
  /// Stop the active subtask regardless of whether it wants to be stopped.
  /// This function sets the active subtask to NULL, which MUST be dealt with
  /// before calling TICK again
  void stopActiveSubtask(const ds_nav_msgs::NavState& state);

  /// \brief Verifies that the active subtask is running;
  /// this is a REQUIRED precondition to run a tick, and should be checked at the start
  /// of every tick.
  /// \throws std::domain_error if there is no currently-running active subtask
  void assertActiveSubtask() const;

  /// \brief Check if all subtasks are valid
  /// This function validates all subtasks and returns true if ALL of them are valid.
  /// \return True  if all subtasks are valid
  bool validateSubtasks() const;
};


} // namespace ds_mx

#endif //PROJECT_DS_MXCOMPLEXTASK_H
