/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/15/19.
//

#ifndef DS_MXHITCOUNTER_H
#define DS_MXHITCOUNTER_H

#include "ds_mxparameter.h"

namespace ds_mx {

/// \brief The HitCounter class implements an increment/decrement + decay
/// hit counter for outlier detection.
///
/// Suppose you want to wait until a depth is below a certain value.  To keep outliers
/// from causing unexpected breakage, we use the following algorithm:
///
/// * If the reported depth is past the threshold, increment the hit counter by GOOD_HITS
///
/// * If the reported depth is NOT past the threshold,
///   increment the hit counter by BAD_HITS (may be negative)
///
/// * In all cases, increment the hit counter by DECAY while ensuring counter >= 0 (likely negative)
///
/// Note that the hit counter ONLY tracks the effects of good hits vs. bad hits;
/// the calling code must run its own comparisons.
///
/// The counter is considered triggered if hits >= hit_threshold.
///
/// Users can check if the hit counter is triggered with the triggered method.
/// Note that the hit counter manages all of these threshold via a standardized set of
/// MX Parameters.  Many of these parameters are optional and have sensible defaults:
///
/// hit_threshold: 5
/// good_increment: +1   (increase for each good hit, clamping at zero)
/// bad_increment: -1    (decrease for each bad hit, clamping at zero)
/// decay_increment: 0   (disable decay, rely on hits; works well if you don't want to drop on no data
/// initial: 0
///
/// Users are strongly encouraged to call "reset" in the OnStart method of each of their Tasks.
///
/// Parameters are created with the "name" prepended; so for a hit counter named "foo", the
/// parameters will be:
/// foo_hit_threshold
class HitCounter {
 public:
  HitCounter(std::shared_ptr<ParameterCollection>& col, const std::string& name);
  HitCounter(std::shared_ptr<ParameterCollection>& col, const std::string& name, int threshold);
  HitCounter(std::shared_ptr<ParameterCollection>& col, const std::string& name,
      int threshold, int good_increment, int bad_increment, int decay_increment, int initial=0);

  // change the state of the hit counter
  void reset();
  void goodHit();
  void badHit();
  void doDecay();

  // check if hits has past the threshold
  bool triggered() const;

  // overload so we can do if (counter)
  operator bool() const;

  const std::string& getName() const;

  // Really only for testing / logging
  int getHits() const;
  int getHitThreshold() const;
  int getGoodIncrement() const;
  int getBadIncrement() const;
  int getDecayIncrement() const;
  int getInitialCount() const;

 protected:
  int hits;
  std::string name;

  ds_mx::IntParam hit_threshold;
  ds_mx::IntParam good_increment;
  ds_mx::IntParam bad_increment;
  ds_mx::IntParam decay_increment;
  ds_mx::IntParam initial_count;
};

}

#endif //DS_MXHITCOUNTER_H
