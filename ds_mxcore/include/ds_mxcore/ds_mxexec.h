/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/8/19.
//

#ifndef PROJECT_DS_MXEXEC_H
#define PROJECT_DS_MXEXEC_H

#include <string>

#include <ds_base/ds_process.h>
#include <ds_mxcore/ds_mxparameter_shared.h>

#include <ds_nav_msgs/NavState.h>
#include <ds_nav_msgs/AggregatedState.h>
#include <ds_mx_msgs/MxEvent.h>
#include <ds_mx_msgs/MxSharedParams.h>
#include <ds_mx_msgs/MxUpdateSharedParams.h>

#include "ds_mxcore.h"

namespace ds_mx {

class MxExecProcess : public ds_base::DsProcess {
 public:
  explicit MxExecProcess();
  MxExecProcess(int argc, char* argv[], const std::string &name);
  ~MxExecProcess() override;

  DS_DISABLE_COPY(MxExecProcess);

  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupServices() override;
  void setupTimers() override;
  void setup() override;

  virtual void setupMission();

  void tick(const ds_nav_msgs::NavState& state);
  void publishState(const std_msgs::Header& hdr) const;
  void publishSharedParams(const std_msgs::Header& hdr) const;

 protected:
  // NOTE: Order here is CRITICAL!  The root_task pointer
  // MUST be destroyed before compiler, or all sorts of crazy
  // library-unloading-segfault nonsense can happen.
  // The order of destruction is guaranteed by the compiler, so
  // this SHOULD be fine.
  std::shared_ptr<MxCompiler> compiler;
  std::shared_ptr<MxRootTask> root_task;
  std::shared_ptr<SharedParameterRegistry> shared_params;
  bool started;

  std::string mission_filename;

  // subscriber: incoming events
  ros::Publisher status_pub;
  ros::Publisher shared_param_pub;
  ros::Publisher eventlog_pub;
  ros::Publisher mission_pub;
  ros::ServiceServer shared_param_update_client;
  ros::Subscriber nav_state_sub;
  ros::Subscriber mx_event_sub;
  ros::Timer status_timer;

  void navstate_callback(const ds_nav_msgs::NavState& state);
  void mx_event_callback(const ds_mx_msgs::MxEvent& event);
  bool shared_param_update_callback(ds_mx_msgs::MxUpdateSharedParamsRequest &req,
                                    ds_mx_msgs::MxUpdateSharedParamsResponse &res);
  void status_timer_callback(const ros::TimerEvent& evt);
};

}

#endif //PROJECT_DS_MXEXEC_H
