/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/19/19.
//

#ifndef DS_MX_PARAMHELPER_H
#define DS_MX_PARAMHELPER_H

#include <json/json.h>
#include <sstream>
#include <string>

#include "ds_mxcore/ds_mxparam_geopoint.h"
#include "ds_mxcore/ds_mxparam_keyvaluelist.h"
#include "ds_mxcore/ds_mxparam_geopointlist.h"

namespace ds_mx {
namespace param_helpers {

// ////////////////////////////////////////////////////////////////////////////
// JSON to Value & back
// ////////////////////////////////////////////////////////////////////////////

template<typename T>
T jsonToValue(const Json::Value &val);

template<typename T>
Json::Value valueToJson(T value);

// ////////////////////////////////////////////////////////////////////////////
// Value To String & Back
// ////////////////////////////////////////////////////////////////////////////

/// Convert a value to a string
/// \tparam T The type to convert from
/// \param value
/// \return
template<typename T>
std::string valueToString(const T& value);

/// Convert a string value to JSON while preserving type
/// \tparam T
/// \param str
/// \return
template<typename T>
T stringToValue(const std::string& str);

/// Check to see if converting a given string to the specified type will succeed
/// \tparam T
/// \param str
/// \return
template<typename T>
bool canConvertStringToValue(const std::string& str);

}
}

#endif //DS_MX_PARAMHELPER_H
