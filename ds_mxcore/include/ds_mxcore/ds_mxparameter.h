/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/4/19.
//

#ifndef DS_MXPARAMETER_H
#define DS_MXPARAMETER_H

#include <memory>
#include <string>
#include <vector>
#include <json/json.h>

#include "ds_mxparam_geopoint.h"
#include "ds_mxparam_keyvaluelist.h"
#include "ds_mxparam_geopointlist.h"

namespace ds_mx {


// forward declaration
class ParameterPrivate;

enum ParameterFlag {
  STATIC=0x00,
  DYNAMIC=0x01,
  OPTIONAL=0x02,
};

enum Role {
  RoleValue=0,
  RoleMin=1,
  RoleMax=2
};

/// Operator to let us combine multiple flags sanely
inline ParameterFlag operator|(ParameterFlag lhs, ParameterFlag rhs) {
  return static_cast<ParameterFlag>(static_cast<int>(lhs) | static_cast<int>(rhs));
}

class Parameter {
 public:
  typedef std::shared_ptr<Parameter> Ptr;

  virtual ~Parameter() = default;
  const std::string& getName() const;
  bool isStatic() const;
  bool isDynamic() const;
  bool isOptional() const;
  virtual bool isValidStr(const std::string& str) const=0;
  bool hasChanged() const;
  void resetChanged();

  virtual std::string ValueString() const = 0;
  virtual void setFromString(const std::string&, Role role=RoleValue) = 0;
  virtual bool canSetFromString(const std::string&, Role role=RoleValue) const = 0;
  virtual std::string GuiString(Role role=RoleValue) const = 0;
  virtual std::string TypeName() const = 0;

  const std::shared_ptr<ParameterPrivate>& getPrivate() const;

  virtual void loadJson(const Json::Value& config) = 0;
  virtual Json::Value saveJson() const = 0;

  virtual void linkParameter(const Parameter::Ptr& other)=0;
  virtual void unlinkParameter()=0;

 protected:
  std::shared_ptr<ParameterPrivate> d_ptr_;

  // Constructor is protected, although the class is already pure-virtual
  explicit Parameter(const std::shared_ptr<ParameterPrivate>& impl);

 private:
  auto d_func() noexcept -> ParameterPrivate*;
  auto d_func() const noexcept -> ParameterPrivate const*;

};

// forward declarations
template<typename T>
class ParameterPrivateT;
class ParameterCollection;

// We're going to use my favorite trick for this: Explicit Template Instantiation!
// You get bool, int, double, string and nothing else!
template<typename T>
class ParameterT : public Parameter {

 public:
  typedef std::shared_ptr<ParameterT> Ptr;
  typedef T ValueType;
  typedef ParameterPrivateT<T> PrivateType;

  explicit ParameterT(const std::shared_ptr<ParameterPrivateT<T> > &_impl);
  ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name,
      ParameterFlag isStatic=ParameterFlag::STATIC);
  ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, const T& defaultValue,
      ParameterFlag isStatic=ParameterFlag::STATIC);

  // GUI interface; DO NOT USE within Tasks, as this overrides most sanity checking.
  std::string ValueString() const override;
  void setFromString(const std::string&, Role role=RoleValue) override;
  bool canSetFromString(const std::string&, Role role=RoleValue) const override;
  std::string GuiString(Role role=RoleValue) const override;
  bool isValidStr(const std::string&) const override;
  std::string TypeName() const override;
  void loadJson(const Json::Value& config) override;
  Json::Value saveJson() const override;

  // Link this parameter to another one for shared parameters, macros, etc
  void linkParameter(const Parameter::Ptr& other) override;
  void linkParameter(const ParameterT<T>& other);
  void unlinkParameter() override;

  // Getter and setter for use WITHIN Tasks
  const T& get() const;
  void set(const T& v);

 private:
  auto d_func() noexcept -> ParameterPrivateT<T>*;
  auto d_func() const noexcept -> ParameterPrivateT<T> const*;
};

// provide some standard typedefs; these are the only
// parameters you should use
typedef ParameterT<bool> BoolParam;
typedef ParameterT<int> IntParam;
typedef ParameterT<double> DoubleParam;
typedef ParameterT<std::string> StringParam;
typedef ParameterT<GeoPoint> GeoPointParam;
typedef ParameterT<KeyValueList> KeyValueListParam;
typedef ParameterT<GeoPointList> GeoPointListParam;

// we hide the template implementation elsewhere to prevent users from declaring
// their own parameter types; we want to ONLY support these.  For this to work correctly, however,
// we need to declare that the code for these is hiding elsewhere (in ds_mxparameter.cpp, for those who care)

class ParameterCollection {
 public:
  virtual ~ParameterCollection();
  void addParameter(const Parameter& p);

  /// \brief Link another parameter collection to this one.
  /// has(), get(), and other methods will look for parameters in the other collection, but
  /// ONLY after checking this collection.  Only one collection can be linked at a time.
  void linkCollection(const std::shared_ptr<ParameterCollection>& other);

  std::shared_ptr<Parameter> getGeneric(const std::string& name) const;
  std::shared_ptr<Parameter> getGeneric(size_t i) const;
  bool hasGeneric(const std::string& name) const;
  bool hasGeneric(size_t i) const;
  std::shared_ptr<Parameter> operator[](size_t i);
  std::shared_ptr<Parameter> operator[](size_t i) const;
  size_t size() const;

  template<typename T>
  T get(const std::string& name) const;

  template<typename T>
  T get(size_t i) const;

  template<typename T>
  bool has(const std::string& name) const;

  template<typename T>
  bool hasStatic(const std::string& name) const;

  template<typename T>
  bool hasDynamic(const std::string& name) const;

  template<typename T>
  bool has(size_t i) const;

  template<typename T>
  bool hasStatic(size_t i) const;

  template<typename T>
  bool hasDynamic(size_t i) const;

  /// \brief Reset the changed flag on every parameter in this collection
  void resetChanged();

  /// \brief Check to see if any of this collection's parameters has changed
  bool anyChanged() const;

  /// \brief Iterator for Parameter collections
  ///
  /// This iterator class is defined to let us use C++'s very pretty
  /// foreach syntax.  So, for example, the following will work:
  ///
  /// for(ds_mx::Parameter::Ptr param : collection) {
  ///   // do stuff
  /// }
  ///
  /// Note, however, that you CAN'T use ds_mx::Parameter::Ptr&-- that's because
  /// the * operator returns a temporary value, so a reference to it makes no sense.
  class iterator {
   public:
    std::shared_ptr<Parameter> operator*();
    void operator++();
    bool operator!=(const ParameterCollection::iterator& rhs);
    bool operator==(const ParameterCollection::iterator& rhs);
   protected:
    std::vector<std::shared_ptr<ParameterPrivate> >::iterator iter;

   public:
    iterator(std::vector< std::shared_ptr<ParameterPrivate> >::iterator i);

  };

  iterator begin();
  iterator end();

 protected:
  /// We keep pointers to the private guts rather than the view that's exposed publically.
  std::vector< std::shared_ptr<ParameterPrivate> > _param_ptrs_;
  std::shared_ptr<ParameterCollection> _linked_collection;

  std::shared_ptr<ParameterPrivate> _get_inner(const std::string& name) const;
  std::shared_ptr<ParameterPrivate> _get_inner(size_t i) const;

};

}

#endif // DS_MXPARAMETER_H
