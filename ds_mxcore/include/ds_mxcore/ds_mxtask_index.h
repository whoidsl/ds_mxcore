/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//

#ifndef PROJECT_DS_MXTASK_INDEX_H
#define PROJECT_DS_MXTASK_INDEX_H

#include "ds_mxtask.h"
#include "ds_mxroottask.h"
#include <memory>
#include <map>
#include <boost/uuid/uuid.hpp>

namespace ds_mx {

class TaskIndex {
 public:
  TaskIndex();
  TaskIndex(const std::shared_ptr<ds_mx::MxRootTask>&);

  std::shared_ptr<ds_mx::MxTask> operator[](const boost::uuids::uuid& id);
  const std::shared_ptr<ds_mx::MxRootTask>& getRoot() const;
  void setRoot(const std::shared_ptr<ds_mx::MxRootTask>& _root);
  bool has(const boost::uuids::uuid& id);
  size_t numTasks() const;

  void reindex();

 private:
  std::shared_ptr<ds_mx::MxRootTask> root;
  typedef std::map<boost::uuids::uuid, std::shared_ptr<ds_mx::MxTask> > TaskIndex_t;
  TaskIndex_t index;

  void indexSubtree(std::shared_ptr<ds_mx::MxTask> task);
};

}


#endif //PROJECT_DS_MXTASK_INDEX_H
