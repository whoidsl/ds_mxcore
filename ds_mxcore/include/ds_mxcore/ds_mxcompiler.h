/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/3/19.
//

#ifndef PROJECT_DS_MXCOMPILER_H
#define PROJECT_DS_MXCOMPILER_H

#include <memory>

// System dependnecies
#include <boost/filesystem.hpp>
#include <json/json.h>

// ROS dependencies
#include <pluginlib/class_loader.h>

// MX dependencies
#include <ds_mxcore/ds_mxexceptions.h>
#include <ds_mxcore/ds_mxtask.h>
#include <ds_mxcore/ds_mxroottask.h>
#include <ds_mxcore/ds_mxparameter_shared.h>

namespace ds_mx {

/// \brief Parse a file
Json::Value parseFile(const boost::filesystem::path& filename);

/// \brief Compile a source JSON file into a task tree
///
/// The compiler converts the abstract syntax tree for a
class MxCompiler : public std::enable_shared_from_this<MxCompiler> {

 protected:
  // this class uses shared_from_this, so it can ONLY be used as a shared ptr
  MxCompiler();

 public:
  // use this instead of constructor
  static std::shared_ptr<MxCompiler> create();

  std::shared_ptr<MxRootTask> compile(const Json::Value& ast);
  void loadSharedParams(const Json::Value& ast);
  void loadMacro(const Json::Value& ast);
  std::shared_ptr<MxRootTask> loadMission(const Json::Value& ast);
  virtual std::shared_ptr<MxTask> loadTask(const std::string& name, const Json::Value& ast);

  /// save a mission to a JSON object
  Json::Value save(const std::shared_ptr<MxRootTask>& root) const;

  pluginlib::ClassLoader<ds_mx::MxTask>& getTaskLoader();
  const pluginlib::ClassLoader<ds_mx::MxTask>& getTaskLoader() const;

  const std::shared_ptr<ds_mx::SharedParameterRegistry>& getSharedParams() const;

 protected:
  // Access to factory for Tasks
  pluginlib::ClassLoader<ds_mx::MxTask> mxtask_loader;

  /// \brief Storage for shared parameters
  std::shared_ptr<ds_mx::SharedParameterRegistry> shared_params;

  // TODO: We'll need some storage here for macros and things
};

typedef std::shared_ptr<MxCompiler> MxCompilerPtr;
}

#endif //PROJECT_DS_MXCOMPILER_H
