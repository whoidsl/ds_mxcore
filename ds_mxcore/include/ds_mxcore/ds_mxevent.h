/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/26/18
//

#ifndef DS_MXEVENT_H
#define DS_MXEVENT_H

#include <string>
#include <memory>
#include <json/value.h>

#include <ds_mx_msgs/MxEvent.h>
#include <functional>

namespace ds_mx {

extern bool eventStringValid(const std::string& eventstr);
extern bool eventMatcherStringValid(const std::string& eventstr);

// forward declarations
class EventHandlerPrivate;
class EventHandlerCollection;

/// \brief The base class for all MX events
class EventHandler {
 public:
  typedef std::shared_ptr<EventHandler> Ptr;
 public:

  explicit EventHandler(const std::shared_ptr<EventHandlerPrivate>& _impl);

  /// Constructor provided mostly for testing.  You can pass a function pointer here
  /// directly, and it will get converted to a std::function.  For example, the test
  /// code does the following:
  ///
  /// bool empty_callback(ds_mx_msgs::MxEvent evt) {/* do nothing */}
  /// TEST(ds_mxevent, creation) {
  ///  ds_mx::EventHandlerCollection collection;
  ///  ds_mx::EventHandler event(collection, "test_event", "/event_default", empty_callback);
  ///  // rest of test
  ///  }
  ///
  /// \param col Collection to use
  /// \param name Name of this event
  /// \param default_event Default event matching pattern
  /// \param _callback Callback function.  See above.
  EventHandler(EventHandlerCollection& col, const std::string& name,
      const std::string& default_event, std::function<bool(ds_mx_msgs::MxEvent)> _callback);


  /// This is the constructor you probably want to use.  It's setup to take
  /// a pointer to a class method.  During normal use, EventHandlers exist
  /// exclusively inside a Task and should only trigger the Task's own methods.
  /// Here's an example using a stand-in Task object.  In the header, you want:
  ///
  /// class YourTask : public ds_mx::MxComplexTask {
  ///  public:
  ///   YourTask();
  ///
  ///   bool event_callback(ds_mx_msgs::MxEvent evt);
  ///
  ///   // probably other stuff here
  ///
  ///  protected:
  ///   ds_mx::EventHandler my_event;
  ///   // probably other stuff here
  /// };
  ///   void callback(ds_mx_msgs::MxEvent evt) {
  ///     event_count++;
  ///   }
  ///
  ///   // most of class omitted
  ///
  /// };
  ///
  /// Now in the .cpp file:
  ///
  /// YourTask::YourTask() : my_event(events, "event_name",
  ///                                 "/default_event", &YourTask::callback, this) {}
  ///
  /// bool YourTask::callback(ds_mx_msgs::MxEvent evt) {
  ///   // callback code goes here
  ///   return true; // pass event on down the Active Task Path
  /// }
  ///
  /// \tparam T The type of the object the passed callback is bound to
  /// (probably a Task), but the compiler MUST guess this-- there's no way to
  /// specify the template type in a constructor!
  /// \param col Collection to use
  /// \param name Name of this event
  /// \param default_event Default event matching pattern
  /// \param _callback Callback function.  See above.
  /// \param obj The "this" object for the callback function
  template<typename T>
  EventHandler(EventHandlerCollection& col, const std::string& name,
               const std::string& default_event,
               bool(T::*_callback)(ds_mx_msgs::MxEvent), T* obj)
               : EventHandler(col, name, default_event,
                   std::bind(_callback, obj, std::placeholders::_1)){}


  virtual ~EventHandler() = default;

  const std::string& getName() const;
  std::vector<std::string> getMatcherStrings() const;
  void addMatcherString(const std::string& eventstr);
  bool hasMatcherString(const std::string& eventstr) const;
  void removeMatcherString(const std::string& eventstr);
  void removeMatcherString(size_t i);

  virtual void loadJson(const Json::Value& config);
  virtual Json::Value saveJson() const;

  bool eventMatches(const std::string& eventstr) const;
  bool handleEvent(const ds_mx_msgs::MxEvent& event);

  const std::shared_ptr<EventHandlerPrivate>& getPrivate() const;

 protected:
  std::shared_ptr<EventHandlerPrivate> d_ptr_;

 private:
  auto d_func() noexcept -> EventHandlerPrivate*;
  auto d_func() const noexcept -> EventHandlerPrivate const*;

};

class EventHandlerCollection {

 public:
  virtual ~EventHandlerCollection() = default;
  bool has(const std::string& name);
  const std::shared_ptr<EventHandler> operator[](const std::string& str) const;
  const std::shared_ptr<EventHandler> operator[](size_t i) const;
  size_t size() const;
  void clear();

  bool handleEvent(const ds_mx_msgs::MxEvent& event);
  void addHandler(const EventHandler& handler);

  void loadJson(const Json::Value& config);
  void saveJson(Json::Value& config) const;

  class iterator {
   public:
    std::shared_ptr<EventHandler> operator*();
    void operator++();
    bool operator!=(const EventHandlerCollection::iterator& rhs);
    bool operator==(const EventHandlerCollection::iterator& rhs);
   protected:
    std::vector<std::shared_ptr<EventHandlerPrivate> >::iterator iter;
   public:
    iterator(std::vector<std::shared_ptr<EventHandlerPrivate> >::iterator i);
  };

  iterator begin();
  iterator end();

 protected:
  std::vector<std::shared_ptr<EventHandlerPrivate> > _event_handlers;
};

};

#endif // DS_MXEVENT_H
