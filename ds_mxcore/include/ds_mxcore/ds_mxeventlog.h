/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/8/19.
//

#ifndef DS_MXEVENTLOG_H
#define DS_MXEVENTLOG_H

#include <string>
#include <sstream>

#include <ros/node_handle.h>

#include "ds_mxtask.h"

namespace ds_mx {

void eventlog_init(ros::NodeHandle& node_handle, const std::string& topicname);
void eventlog_disable();
void eventlog_destroy();

void eventlog_log(ds_mx::MxTask* task, const std::string& type, const std::string& text);
}

const static ds_mx::MxTask* MX_LOG_NOEVENT = nullptr;
const static std::string MX_LOG_MISSION_LOADED = "mission_loaded";
const static std::string MX_LOG_MISSION_START = "mission_start";
const static std::string MX_LOG_TASK_INIT = "task_init";
const static std::string MX_LOG_TASK_START = "task_start";
const static std::string MX_LOG_TASK_SUCCEED = "task_succeed";
const static std::string MX_LOG_TASK_FAILED = "task_failed";
const static std::string MX_LOG_TASK_STOP = "task_stop";
const static std::string MX_LOG_TASK_ERROR = "task_error";
const static std::string MX_LOG_TASK_COMMS = "task_comms";
const static std::string MX_LOG_TASK_UPDATE = "task_update";
const static std::string MX_LOG_DETECTION = "detection";
const static std::string MX_LOG_ABORT = "mission_abort";
const static std::string MX_LOG_EVENT_RECEIVED = "event_received";
const static std::string MX_LOG_SHARED_PARAM_INIT = "shared_param_update_started";
const static std::string MX_LOG_SHARED_PARAM_SUCCEED = "shared_param_update_succeeded";
const static std::string MX_LOG_SHARED_PARAM_FAILED = "shared_param_update_failed";
const static std::string MX_LOG_PARAM_CHANGED = "param_change_detected";
const static std::string MX_LOG_WEIGHTDROP = "weightdrop";

#define MX_LOG_EVENT(task, type) \
  ::ds_mx::eventlog_log( (task), (type), "");

#define MX_LOG_EVENT_TEXT(task, type, text) \
  ::ds_mx::eventlog_log( (task), (type), (text));

#define MX_LOG_EVENT_STREAM(task, type, args) \
{ \
  std::stringstream ss; \
  ss << args; \
  ::ds_mx::eventlog_log( (task), (type), ss.str());\
}

#endif //DS_MXEVENTLOG_H
