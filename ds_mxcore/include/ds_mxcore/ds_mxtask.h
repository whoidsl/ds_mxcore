/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/26/18.
//

#ifndef PROJECT_DS_MXTASK_H
#define PROJECT_DS_MXTASK_H

#include <json/json.h>
#include <string>
#include <vector>
#include <boost/uuid/uuid.hpp>
#include <ros/ros.h>
#include <ds_nav_msgs/NavState.h>
#include <ds_mx_msgs/MxMissionStatus.h>
#include <ds_mx_msgs/MxTaskStatus.h>
#include <ds_mx_msgs/MxEvent.h>
#include <ds_mx_msgs/MissionDisplay.h>

#include "ds_mxparameter.h"
#include "ds_mxevent.h"

namespace ds_mx {

// forward declaration.  Handles all necessary overhead for
// name lookups, etc
class MxCompiler;
typedef std::shared_ptr<MxCompiler> MxCompilerPtr;

/// Return code for tasks in the hierarchy
enum TaskReturnCode {
  /// Task is still running
  RUNNING=0,

  /// Task completed successfully
  SUCCESS=1,

  /// Task completed unsuccessfully; may not be an error.
  FAILED=2,
};

/// \brief The base class for all MX tasks
class MxTask {
 public:
  typedef std::shared_ptr<MxTask> Ptr;

  MxTask();
  virtual ~MxTask();

  /// \brief Convert this task back into a JSON value
  virtual Json::Value toJson();

  /// \brief Initialize the given task from a JSON value
  virtual void init(const Json::Value& config, MxCompilerPtr compiler) = 0;

  /// \brief Initialize all ROS-related stuff; used to avoid
  /// unnecessary subscription / publisher setup when not running
  /// as part of the live system
  virtual void init_ros(ros::NodeHandle& nh) = 0;

  void loadParameters(const Json::Value& config, MxCompilerPtr compiler);
  void saveParameters(Json::Value& config);

  /// \brief Convert this task to
  virtual std::string toDot() = 0;

  const std::string& getName() const;
  void setName(const std::string& _n);

  const std::string& getType() const;
  void setType(const std::string& _n);

  bool isRunning() const;

  const ds_mx::ParameterCollection& getParameters() const;
  ds_mx::ParameterCollection& getParameters();

  const std::shared_ptr<ds_mx::ParameterCollection>& getParametersPtr() const;

  const ds_mx::EventHandlerCollection& getEvents() const;
  ds_mx::EventHandlerCollection& getEvents();

  /// \brief This is the core function that gets called to update state internally
  virtual TaskReturnCode tick(const ds_nav_msgs::NavState& state) = 0;

  /// \brief Event handler that gets called when this task switches from inactive to active
  virtual void onStart(const ds_nav_msgs::NavState& state) = 0;

  /// \brief Event handler that gets called when this task switches from active to inactive
  virtual void onStop(const ds_nav_msgs::NavState& state) = 0;

  /// \brief Statically validate this task and any of its subtasks
  ///
  /// \return True if the task tree at and below the queried node is valid, false otherwise
  virtual bool validate() const = 0;

  /// \brief Update the mission status from the given tick; most classes won't need
  /// to override this.
  virtual void updateStatus(ds_mx_msgs::MxMissionStatus& status) const = 0;

  /// \brief Get the status of this task.  Almost every Task _WILL_ need to override this.
  virtual ds_mx_msgs::MxTaskStatus getStatus() const;

  /// \brief Check to see if a timeout has expired
  bool isTimeoutExpired(const ros::Time& now) const;

  /// \brief Reset the timeout to its initial value
  /// CAUTION: Be VERY careful how this gets used!
  virtual void resetTimeout(const ros::Time& now);

  /// \brief Handle a single event coming in from the outside world
  virtual void handleEvent(const ds_mx_msgs::MxEvent& event) = 0;

  std::string getDotId() const;
  const boost::uuids::uuid& getUuid() const;

  /// \brief Reset any of the Task's internal state, as required.  May be called
  /// by parent tasks.
  virtual void resetState();

  /// \brief Given a starting state, compute expected positions as necessary and update
  /// the display
  /// \param state The predicted vehicle state at the start of this task.  Updated to the
  /// predicted position at the end
  /// \param display The current display to be updated
  virtual void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) = 0;

  /// Check to see of the specified task is a subchild of this task.  This function will
  /// return true in any of the following two cases:
  /// 1). query_uuid == this->uuid
  /// 2). query_uuid matches ANY subtask or their subtasks below this node
  /// \param query_uuid The UUID to query
  /// \return True if this node or any of its descendents matches the query_uuid
  virtual bool subtreeContains(const boost::uuids::uuid& query_uuid) const;

  // these functions are intended to help with testing
  ds_mx::TaskReturnCode nextTickReturn() const;
  ds_mx::TaskReturnCode timeoutCode() const;
  const ros::Time& timeoutTime() const;

 protected:

  std::string name;
  std::string type;
  boost::uuids::uuid uuid;
  ros::Time timeout;
  ds_mx::TaskReturnCode next_tick_return_code;
  ds_mx::TaskReturnCode timeout_code;
  std::shared_ptr<ds_mx::ParameterCollection> params;
  ds_mx::EventHandlerCollection events;
  ds_mx::DoubleParam max_timeout_sec;
  bool running;


  /// \brief Callback triggered when a parameter is changed
  /// This function is called during a tick prior to calling the subtask
  /// if any of this task's dynamic parameters have changed
  virtual void parameterChanged(const ds_nav_msgs::NavState& state);

  /// \brief Callback triggered on every tick
  virtual TaskReturnCode onTick(const ds_nav_msgs::NavState& state) = 0;
};


} // ds_mx

#endif //PROJECT_DS_MXTASK_H
