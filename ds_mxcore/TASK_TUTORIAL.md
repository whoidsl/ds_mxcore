# Writing New Tasks

Whether primitives to support a new vehicle or complex tasks to implement new capabilities, writing new Tasks is 
one of the central development task for MX.  All new Tasks must extend the `ds_mx::MxTask` class, typically through
a `ds_mx::MxPrimitiveTask` or `ds_mx::MxComplexTask`.  These classes are the heart of the Task Tree.  There's a lot
going on in these classes.  This tutorial is intended to highlight the essential parts of the TaskTree system with a 
particular emphasis on what developers must implement when writing new tasks.

Before diving into writing a new Task, it is necessary to start with several key components and modules Tasks may use.


Any Task typically has several member variables.  These include:

1. Parameters that are read from the mission file
1. Event handlers that are triggered in response to an external event
1. Shared pointers to one or more subtasks. (Complex Tasks only)
1. Any required ROS publishers, subscribers, service clients, etc (typically Primitive Tasks)
1. Any other member variables as required


Parameters and event handlers include a number of MX-specific functionality to make them easier to use.  They will be
revisited in more detail later.  

## A First Example: ds_mxsim::TaskIdle





# Task Component Details
## MX Parameters

An MX parameter is any value read out of a mission file.  Parameters for Tasks might include timeout duration,
trackline endpoints, depth targets, etc.  The Parameter infrastructure includes support for error checking, editing
parameters via NavG, handling shared parameters, and implementing macros.  New Tasks should use MX parameters 
and avoid other parameter schemes to take advantage of all these features.

Parameters have a name, a type, and may be either static or dynamic.  The parameter name is used when reading the 
mission JSON file.  The set of allowable types is limited to bool, int, double, string, and `ds_mx::GeoPoint` for 
geographic coordinates.  Static parameters are loaded from the mission file during initialization and may not be changed
during the mission.  Dynamic parameters can change during the mission, and will trigger a parameterChanged callback.

Parameters are typically specified as members in a new Task.  In addition, each Task maintains a complete list of all 
its parameters.  

const T& get() const;
void set(const T& v);