# Getting Started with MX

There should be some stuff here

# Installation

## Base OS / ROS install
Here are some basic installation instructions.  Start with a system running Ubuntu 16.04 and [ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu).

## NavG3

If a full Desktop environment is available, also install NavG 3.0 using the navG install instructions.  Source is available on [bitbucket](https://bitbucket.org/whoidsl/navg3).  Once you have a clone and have satisfied the build dependencies, use the following build instructions to install on your system:

```
hg clone https://bitbucket.org/whoidsl/navg3
cd navg3
mkdir build
cd build
cmake ..
make -j 10
sudo make install
```

This will install navg3 to /usr/local and allow the MX Navg3 plugins to build correctly.  You can test the navg3 install by simply running `/usr/local/bin/navg`, or simply `navg` if local bin is in your path.

## ROS Catkin Workspace

Next, you need to create a catkin workspace.  Start with the following.

```
mkdir -p path_to_your_workspace/src
cd path_to_your_workspace
catkin_init_workspace src
```

You'll need to checkout the required repositories.  The core MX install requires the following, but your system may need other repositories as well.  [`wstool`](http://wiki.ros.org/wstool) is strongly recommended to manage multiple package repositories.  The core MX repository URLs are:

* `https://bitbucket.org/whoidsl/ds_base.git`
* `https://bitbucket.org/whoidsl/ds_msgs.git`
* `https://bitbucket.org/whoidsl/ds_mxcore.git`
* (optional) `https://bitbucket.org/whoidsl/ds_mxgui.git`
* (optional) `https://bitbucket.org/whoidsl/ds_navg.git`

Catkin includes the ability to automatically discover and manage dependencies.  Dependencies can be checked with:

```
rosdep check --from-paths src --ignore-src
```

If any dependencies are missing, they can be automatically installed with:

```
rosdep install --from-paths src --ignore-src
```

Finally, actually build as usual with:

```
catkin_make
source devel/setup.bash
```

In addition to the usual stuff, the setup.bash also makes the NavG plugins discoverable.  To use any of the MX NavG plugins, including the planner, navg must be run from a terminal with that setup.bash file already sourced.

### Running Tests

To run all the unit tests in the standard ROS way and check the results, use:

```
catkin_make run_tests
catkin_test_results build/test_results/
```

### Running some simple example commands

Here are a few example commands showing things you can do with missions through `mxinfo`.

* To view plugins:

```
user@compy:~/sentry_mx_dev$ rosrun ds_mxcore mxinfo plugins
```

* To manually generate a TaskTree for the mission `test_survey_2_uuids` as a postscript file:

```
user@compy:~/sentry_mx_dev$ rosrun ds_mxcore mxinfo dot src/ds_mxcore/ds_mxsim/mission/test_survey2_uuids.json  > test_survey2_uuids.dot
user@compy:~/sentry_mx_dev$ dot -Tps test_survey2_uuids.dot test_survey2_uuids.ps
user@compy:~/sentry_mx_dev$ evince test_survey2_uuids.ps
```

* To validate a mission file, run:

```
user@compy:~/sentry_mx_dev$ rosrun ds_mxcore mxinfo validate src/ds_mxcore/ds_mxsim/mission/test_survey2_uuids.json
```
 
* To generate a mission file after parsing, run:

```
user@compy:~/sentry_mx_dev$ rosrun ds_mxcore mxinfo json src/ds_mxcore/ds_mxsim/mission/test_survey2_uuids.json
```

* To run a simple example mission in very crude simulation, run:

```
user@compy:~/sentry_mx_dev$ roslaunch ds_mxsim mxsim.launch
```
 
