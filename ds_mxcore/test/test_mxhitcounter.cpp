/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/16/19.
//

#include <gtest/gtest.h>

#include "ds_mxcore/ds_mxhitcounter.h"

TEST(MxHitCounter, default_creation) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test");

  EXPECT_EQ("test", counter.getName());
  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_hit_threshold"));
  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_good_increment"));
  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_bad_increment"));
  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_decay_increment"));
  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_initial_count"));
}

TEST(MxHitCounter, basicTest) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 3, 1, -1, 0, 0);

  EXPECT_EQ(0, counter.getHits());
  EXPECT_FALSE(counter);

  counter.goodHit();
  EXPECT_EQ(1, counter.getHits());
  EXPECT_FALSE(counter);

  counter.goodHit();
  EXPECT_EQ(2, counter.getHits());
  EXPECT_FALSE(counter);

  counter.badHit();
  EXPECT_EQ(1, counter.getHits());
  EXPECT_FALSE(counter);

  counter.goodHit();
  EXPECT_EQ(2, counter.getHits());
  EXPECT_FALSE(counter);
  EXPECT_FALSE(counter.triggered());

  counter.goodHit();
  EXPECT_EQ(3, counter.getHits());
  EXPECT_TRUE(counter);
  EXPECT_TRUE(counter.triggered());
}

TEST(MxHitCounter, goodHitClamp) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 3, -1, 0, 0, 2);

  EXPECT_EQ(2, counter.getHits());

  counter.goodHit();
  EXPECT_EQ(1, counter.getHits());

  counter.goodHit();
  EXPECT_EQ(0, counter.getHits());

  counter.goodHit();
  EXPECT_EQ(0, counter.getHits());
}

TEST(MxHitCounter, badClamp) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 3, 0, -1, 0, 2);

  EXPECT_EQ(2, counter.getHits());

  counter.badHit();
  EXPECT_EQ(1, counter.getHits());

  counter.badHit();
  EXPECT_EQ(0, counter.getHits());

  counter.badHit();
  EXPECT_EQ(0, counter.getHits());
}

TEST(MxHitCounter, decayClamp) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 3, 0, 0, -1, 2);

  EXPECT_EQ(2, counter.getHits());

  counter.doDecay();
  EXPECT_EQ(1, counter.getHits());

  counter.doDecay();
  EXPECT_EQ(0, counter.getHits());

  counter.doDecay();
  EXPECT_EQ(0, counter.getHits());
}

TEST(MxHitCounter, resetTest) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 3, 1, 0, 0, 2);

  EXPECT_EQ(2, counter.getHits());
  EXPECT_FALSE(counter.triggered());

  counter.goodHit();
  EXPECT_EQ(3, counter.getHits());
  EXPECT_TRUE(counter.triggered());

  counter.reset();
  EXPECT_EQ(2, counter.getHits());
  EXPECT_FALSE(counter.triggered());
}

TEST(MxHitCounter, fullDecayTest) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 5, 2, 1, -1, 0);

  EXPECT_EQ(0, counter.getHits());
  EXPECT_FALSE(counter);

  // first, good hit + decay
  counter.goodHit();
  EXPECT_EQ(2, counter.getHits());
  EXPECT_FALSE(counter);

  counter.doDecay();
  EXPECT_EQ(1, counter.getHits());
  EXPECT_FALSE(counter);

  // next, bad hit + decay
  counter.badHit();
  EXPECT_EQ(2, counter.getHits());
  EXPECT_FALSE(counter);

  counter.doDecay();
  EXPECT_EQ(1, counter.getHits());
  EXPECT_FALSE(counter);

  // now just decay
  counter.doDecay();
  EXPECT_EQ(0, counter.getHits());
  EXPECT_FALSE(counter);

  // Now 5 good hits in a row
  for (size_t i=0; i<3; i++) {
    counter.goodHit();
    EXPECT_EQ(i+2, counter.getHits());
    EXPECT_FALSE(counter);

    counter.doDecay();
    EXPECT_EQ(i+1, counter.getHits());
    EXPECT_FALSE(counter);
  }

  counter.goodHit();
  EXPECT_EQ(5, counter.getHits());
  EXPECT_TRUE(counter);
}

TEST(MxHitCounter, thresholdExceeded) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::HitCounter counter(collection, "test", 2, 1, 0, 0, 0);

  EXPECT_EQ(0, counter.getHits());
  EXPECT_FALSE(counter);

  counter.goodHit();
  EXPECT_EQ(1, counter.getHits());
  EXPECT_FALSE(counter);

  counter.goodHit();
  EXPECT_EQ(2, counter.getHits());
  EXPECT_TRUE(counter);

  counter.goodHit();
  EXPECT_EQ(3, counter.getHits());
  EXPECT_TRUE(counter);

  counter.goodHit();
  EXPECT_EQ(4, counter.getHits());
  EXPECT_TRUE(counter);
}
