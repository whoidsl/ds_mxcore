/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/28/19.
//

#include <gtest/gtest.h>
#include <iostream>

#include <ds_mxcore/ds_mx_paramhelper.h>
#include <ds_mxcore/ds_mxexceptions.h>

// convert a single JSON token to a string
Json::Value loadStrToJson(const std::string& str) {
  Json::Value json;
  std::stringstream stream(str);
  stream >> json;
  return json;
}

std::string extractJsonStr(const Json::Value& json) {
  std::stringstream stream;
  stream <<json;
  return stream.str();
}

// ////////////////////////////////////////////////////////////////////////////
// jsonToValue
// ////////////////////////////////////////////////////////////////////////////

TEST(MxParamHelper_jsonToValue, BoolParam) {
  Json::Value json = loadStrToJson("true");

  bool val = ds_mx::param_helpers::jsonToValue<bool>(json);
  EXPECT_TRUE(val);
}
TEST(MxParamHelper_jsonToValue, IntParam) {
  Json::Value json = loadStrToJson("1");

  int val = ds_mx::param_helpers::jsonToValue<int>(json);
  EXPECT_EQ(1, val);
}
TEST(MxParamHelper_jsonToValue, DoubleParam) {
  Json::Value json = loadStrToJson("1.0");

  double val = ds_mx::param_helpers::jsonToValue<double>(json);
  EXPECT_EQ(1.0, val);
}
TEST(MxParamHelper_jsonToValue, StringParam) {
  Json::Value json = loadStrToJson("\"some string\"");

  std::string val = ds_mx::param_helpers::jsonToValue<std::string>(json);
  EXPECT_EQ("some string", val);
}
TEST(MxParamHelper_jsonToValue, GeoPointParam) {
  Json::Value json = loadStrToJson("\"POINT(10.0 20.0)\"");

  ds_mx::GeoPoint val = ds_mx::param_helpers::jsonToValue<ds_mx::GeoPoint>(json);
  EXPECT_EQ(ds_mx::GeoPoint(10.0, 20.0), val);
}
TEST(MxParamHelper_jsonToValue, KeyValueListParam) {
  Json::Value json = loadStrToJson( R"({
  "param1": 1.0,
  "param2": true,
  "param3": "foo"
})");

  ds_mx::KeyValueList val = ds_mx::param_helpers::jsonToValue<ds_mx::KeyValueList>(json);
  ASSERT_EQ(3, val.dict.size());

  EXPECT_EQ("1", val["param1"]);
  EXPECT_EQ("true", val["param2"]);
  EXPECT_EQ("foo", val["param3"]);
}


// ////////////////////////////////////////////////////////////////////////////
// valueToJson
// ////////////////////////////////////////////////////////////////////////////

TEST(MxParamHelper_valueToJson, BoolParam) {
  Json::Value json  = ds_mx::param_helpers::valueToJson<bool>(true);
  EXPECT_EQ("true", extractJsonStr(json));
}

TEST(MxParamHelper_valueToJson, IntParam) {
  Json::Value json  = ds_mx::param_helpers::valueToJson<int>(1);
  EXPECT_EQ("1", extractJsonStr(json));
}

TEST(MxParamHelper_valueToJson, DoubleParam) {
  Json::Value json  = ds_mx::param_helpers::valueToJson<double>(2.000000000000001);
  // because of limits to floating point representation errors, these don't
  // EXACTLY match-- but its good to 15 digits, which is "good enough (TM)"
  EXPECT_EQ("2.0000000000000009", extractJsonStr(json));
}

TEST(MxParamHelper_valueToJson, StringParam) {
  Json::Value json  = ds_mx::param_helpers::valueToJson<std::string>("some string");
  EXPECT_EQ("\"some string\"", extractJsonStr(json));
}

TEST(MxParamHelper_valueToJson, GeoPointParam) {
  Json::Value json  = ds_mx::param_helpers::valueToJson(ds_mx::GeoPoint(10.0, 20.0));
  EXPECT_EQ("\"POINT (10.000000000 20.000000000)\"", extractJsonStr(json));
}

TEST(MxParamHelper_valueToJson, KeyValueListParam) {
  ds_mx::KeyValueList param;
  param.dict["param1"] = "1.0";
  param.dict["param2"] = "true";
  param.dict["param3"] = "foo";

  Json::Value json = ds_mx::param_helpers::valueToJson<ds_mx::KeyValueList>(param);
  EXPECT_EQ("{\n\t\"param1\" : \"1.0\",\n\t\"param2\" : \"true\",\n\t\"param3\" : \"foo\"\n}", extractJsonStr(json));
}

// ////////////////////////////////////////////////////////////////////////////
// valueToString
// ////////////////////////////////////////////////////////////////////////////
TEST(MxParamHelper_valueToString, BoolParam) {
  std::string str = ds_mx::param_helpers::valueToString<bool>(true);
  // TODO: This should maybe get washed through JSON?
  EXPECT_EQ("1", str);
}

TEST(MxParamHelper_valueToString, IntParam) {
  std::string str = ds_mx::param_helpers::valueToString<int>(1);
  EXPECT_EQ("1", str);
}

TEST(MxParamHelper_valueToString, DoubleParam) {
  std::string str = ds_mx::param_helpers::valueToString<double>(2.0);
  EXPECT_EQ("2", str);
}

TEST(MxParamHelper_valueToString, StringParam) {
  std::string str = ds_mx::param_helpers::valueToString<std::string>("some string");
  EXPECT_EQ("some string", str);
}

TEST(MxParamHelper_valueToString, GeoPointParam) {
  std::string str = ds_mx::param_helpers::valueToString<ds_mx::GeoPoint>(ds_mx::GeoPoint(10.0, 20.0));
  EXPECT_EQ("POINT (10.000000000 20.000000000)", str);
}

TEST(MxParamHelper_valueToString, KeyValueListParam) {
  ds_mx::KeyValueList param;
  param.dict["param1"] = "1.0";
  param.dict["param2"] = "true";
  param.dict["param3"] = "foo";

  std::string str = ds_mx::param_helpers::valueToString<ds_mx::KeyValueList>(param);
  EXPECT_EQ("{\n\t\"param1\" : \"1.0\",\n\t\"param2\" : \"true\",\n\t\"param3\" : \"foo\"\n}", str);
}

// ////////////////////////////////////////////////////////////////////////////
// stringToValue
// ////////////////////////////////////////////////////////////////////////////
TEST(MxParamHelper_stringToValue, BoolParam) {
  bool val;
  val = ds_mx::param_helpers::stringToValue<bool>("true");
  EXPECT_TRUE(val);

  val = ds_mx::param_helpers::stringToValue<bool>("false");
  EXPECT_FALSE(val);

  val = ds_mx::param_helpers::stringToValue<bool>("1");
  EXPECT_TRUE(val);

  val = ds_mx::param_helpers::stringToValue<bool>("0");
  EXPECT_FALSE(val);
}

TEST(MxParamHelper_stringToValue, IntParam) {
  int val = ds_mx::param_helpers::stringToValue<int>("1");
  EXPECT_EQ(1, val);
}

TEST(MxParamHelper_stringToValue, DoubleParam) {
  double val = ds_mx::param_helpers::stringToValue<double>("2.0");
  EXPECT_EQ(2, val);
}

TEST(MxParamHelper_stringToValue, StringParam) {
  std::string val = ds_mx::param_helpers::stringToValue<std::string>("some string");
  EXPECT_EQ("some string", val);
}

TEST(MxParamHelper_stringToValue, GeoPointParam) {
  ds_mx::GeoPoint val = ds_mx::param_helpers::stringToValue<ds_mx::GeoPoint>("POINT (10.0 20.0)");
  EXPECT_EQ(ds_mx::GeoPoint(10.0, 20.0), val);
}

TEST(MxParamHelper_stringToValue, KeyValueListParam) {
  ds_mx::KeyValueList val = ds_mx::param_helpers::stringToValue<ds_mx::KeyValueList>(
      "{\n\t\"param1\" : \"1.0\",\n\t\"param2\" : \"true\",\n\t\"param3\" : \"foo\"\n}");

  ds_mx::KeyValueList expected;
  expected.dict["param1"] = "1.0";
  expected.dict["param2"] = "true";
  expected.dict["param3"] = "foo";

  EXPECT_EQ(expected, val);
}


// error generation!
TEST(MxParamHelper_stringToValue, BoolParamErrors) {
  EXPECT_THROW({
                 ds_mx::param_helpers::stringToValue<bool>("some string");
               }, ds_mx::ParameterTypeError);
}

TEST(MxParamHelper_stringToValue, IntParamErrors) {
  EXPECT_THROW({
                 ds_mx::param_helpers::stringToValue<int>("some string");
               }, ds_mx::ParameterTypeError);
}

TEST(MxParamHelper_stringToValue, DoubleParamErrors) {
  EXPECT_THROW({
                 ds_mx::param_helpers::stringToValue<double>("some string");
               }, ds_mx::ParameterTypeError);
}

TEST(MxParamHelper_stringToValue, GeoPointParamErrors) {
  EXPECT_THROW({
                 ds_mx::param_helpers::stringToValue<ds_mx::GeoPoint>("some string");
               }, ds_mx::ParameterTypeError);
}

TEST(MxParamHelper_stringToValue, KeyValueListErrors) {
    EXPECT_THROW({
                   ds_mx::param_helpers::stringToValue<ds_mx::KeyValueList>("some string");
                 }, ds_mx::ParameterTypeError);
}

// ////////////////////////////////////////////////////////////////////////////
// canConvertStringToValue
// ////////////////////////////////////////////////////////////////////////////

TEST(MxParamHelper_canConvertStringToValue, BoolParam) {
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<bool>("true"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<bool>("false"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<bool>("1"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<bool>("2.0"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<bool>("some string"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<bool>("POINT (10.0 20.0)"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<bool>("{\n\t\"param1\" : \"1.0\"\n}"));
}

TEST(MxParamHelper_canConvertStringToValue, IntParam) {
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<int>("true"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<int>("false"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<int>("1"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<int>("2.0"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<int>("some string"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<int>("POINT (10.0 20.0)"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<int>("{\n\t\"param1\" : \"1.0\"\n}"));
}

TEST(MxParamHelper_canConvertStringToValue, DoubleParam) {
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<double>("true"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<double>("false"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<double>("1"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<double>("2.0"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<double>("some string"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<double>("POINT (10.0 20.0)"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<double>("{\n\t\"param1\" : \"1.0\"\n}"));
}

TEST(MxParamHelper_canConvertStringToValue, StringParam) {
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("true"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("false"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("1"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("2.0"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("some string"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("POINT (10.0 20.0)"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<std::string>("{\n\t\"param1\" : \"1.0\"\n}"));
}
TEST(MxParamHelper_canConvertStringToValue, GeoPointParam) {
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("true"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("false"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("1"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("2.0"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("some string"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("POINT (10.0 20.0)"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::GeoPoint>("{\n\t\"param1\" : \"1.0\"\n}"));
}
TEST(MxParamHelper_canConvertStringToValue, KeyValueListParam) {
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("true"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("false"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("1"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("2.0"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("some string"));
  EXPECT_FALSE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("POINT (10.0 20.0)"));
  EXPECT_TRUE(ds_mx::param_helpers::canConvertStringToValue<ds_mx::KeyValueList>("{\n\t\"param1\" : \"1.0\"\n}"));
}
