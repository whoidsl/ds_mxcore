/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/24/21.
//

#include <gtest/gtest.h>

#include <ds_mxcore/ds_mx_action_payload.h>
#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>

// we're going to re-use the mock from the action test primitive stuff
#include <ds_mxcore/test/ds_mock_payload_action_server.h>
#include <ds_mxcore/TestPayloadAction.h>

class ActionTestPayload : public ds_mx::ActionPayload<ds_mxcore::TestPayloadAction, ActionTestPayload> {
 public:
  ActionTestPayload() : ActionPayload<ds_mxcore::TestPayloadAction, ActionTestPayload>("action_payload_test"),
                        value(params, "value", 0, ds_mx::ParameterFlag::DYNAMIC) {
  }
  virtual ~ActionTestPayload() = default;

  bool validate() const override { return true; }

  ds_mxcore::TestPayloadGoal toMsg() const {
    ds_mxcore::TestPayloadGoal ret;
    ret.value = value.get();
    return ret;
  }
  bool resultSuccessful(const actionlib::SimpleClientGoalState& state, const ResultConstPtr& result) override {
    return result->success;
  }

  ds_mx::IntParam value;

  // accessors
  ds_mx::TaskReturnCode next_rc() const { return next_return_code_; }
};

class ActionPayloadRostest : public ds_mx::MockPayloadActionServer<ds_mxcore::TestPayloadAction, ActionTestPayload> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "test_payload",
  "value": 0,
  "subtask": "test_subtask"
})";
  }
};

TEST_F(ActionPayloadRostest, TestSuccess) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(3, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);

  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(ActionPayloadRostest, TestSuccessRestart) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // double-check final state, then reset much of it
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);

  // reset the done flag
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
    test_done = false;
  }

  // /////////////////////////////////////////////////////////////////////////////
  // start all over again, beginning with:
  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(2, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(2, subtask->onStartCalls);
    EXPECT_EQ(3, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(3, test_counter);

    EXPECT_EQ(2, subtask->onStartCalls);
    EXPECT_EQ(3, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(4, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(5, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(2, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(4, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(5, subtask->ticks);
  EXPECT_EQ(2, subtask->onStopCalls);
}

TEST_F(ActionPayloadRostest, TestFailed) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    // do this in a lock to double-check uninitialized values
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_return_failed = true;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(ActionPayloadRostest, TestStop) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // Just call onStop
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}

TEST_F(ActionPayloadRostest, TestImmediateStop) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // Just call onStop WITHOUT waking up the server first
  underTest->onStop(state);
  test_cv.notify_all();
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(0, test_counter);
  }
}

TEST_F(ActionPayloadRostest, TestStopRestart) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // stop
  underTest->onStop(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state, then reset much of it
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  // /////////////////////////////////////////////////////////////////////////////
  // start all over again, beginning with:
  // start call on the on-state method, check what we got
  underTest->onStart(state);
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(4, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(2, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(3, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  EXPECT_EQ(2, subtask->onStartCalls);
  EXPECT_EQ(4, subtask->ticks);
  EXPECT_EQ(2, subtask->onStopCalls);
}

TEST_F(ActionPayloadRostest, TestChangeParam) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_FALSE(static_cast<bool>(last_goal));

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, last_goal->value);
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // change a parameter
  underTest->getParameters().get<ds_mx::IntParam>("value").set(10);

  {
    // do ALL this inside a lock to verify that the uninitialized values are correct
    // -- otherwise test occasionally fails when they get overwritten
    std::unique_lock<std::mutex> lock(test_mutex);
    // parameter change won't take effect until we tick
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
    // these values get reset by the primitive while waiting for the first response
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, last_goal->value);
  }
  test_cv.notify_all();
  ros::Duration(0.45).sleep();

  // double-check final state, then reset much of it
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(2, test_counter);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(10, last_goal->value);
  }

  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(3, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(4, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(5, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(2, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(4, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(5, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(ActionPayloadRostest, TestChangeParamBefore) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_FALSE(static_cast<bool>(last_goal));

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  underTest->getParameters().get<ds_mx::IntParam>("value").set(10);
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  {
    // make sure nothing goes out
    std::unique_lock<std::mutex> lock(test_mutex);
    ASSERT_FALSE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
  }


  // start call on the on-state method, check what we got
  {
    underTest->onStart(state);
  }
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(0, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(10, last_goal->value);
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  {
    std::unique_lock<std::mutex> lock(test_mutex);
    // verify our parameter
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(10, last_goal->value);

  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(1, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

TEST_F(ActionPayloadRostest, TestSubtaskSuccess) {
  // NOTE: the task under test should actually report failure

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // in real life, this will result in an immediate on-stop call
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail); // preempted by on-stop call
    EXPECT_EQ(1, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}

TEST_F(ActionPayloadRostest, TestSubtaskFails) {
  // NOTE: the task under test should actually report failure

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // in real life, this will result in an immediate on-stop call
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail); // preempted by on-stop call
    EXPECT_EQ(1, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}
TEST_F(ActionPayloadRostest, TestNoFinishSubtaskSuccess) {
  // NOTE: the task under test should actually report failure
  underTest->getParameters().get<ds_mx::BoolParam>("finish_on_action_done").setFromString("false");

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // in real life, this will result in an immediate on-stop call
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail); // preempted by on-stop call
    EXPECT_EQ(1, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}

TEST_F(ActionPayloadRostest, TestNoFinishSubtaskFails) {
  // NOTE: the task under test should actually report failure
  underTest->getParameters().get<ds_mx::BoolParam>("finish_on_action_done").setFromString("false");

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // in real life, this will result in an immediate on-stop call
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail); // preempted by on-stop call
    EXPECT_EQ(1, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(2, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}

TEST_F(ActionPayloadRostest, TestNoFinishActionSuccess) {
  // NOTE: the task under test should actually report failure
  underTest->getParameters().get<ds_mx::BoolParam>("finish_on_action_done").setFromString("false");

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  // check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(3, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(4, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(4, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}

TEST_F(ActionPayloadRostest, TestNoFinishActionSuccessSubtaskFails) {
  // NOTE: the task under test should actually report failure
  underTest->getParameters().get<ds_mx::BoolParam>("finish_on_action_done").setFromString("false");

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  // check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(3, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  subtask->defaultTickReturn = ds_mx::TaskReturnCode::FAILED;
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(4, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(4, subtask->ticks);
    EXPECT_EQ(1, subtask->onStopCalls);
  }
}

TEST_F(ActionPayloadRostest, TestNoFinishActionFails) {
  // NOTE: the task under test should actually report failure
  underTest->getParameters().get<ds_mx::BoolParam>("finish_on_action_done").setFromString("false");

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(0, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);

    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(0, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // we should still report running even if no response from the action server has been received
  {
    // do this in a lock to double-check uninitialized values
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_EQ(1, subtask->onStartCalls);
    EXPECT_EQ(1, subtask->ticks);
    EXPECT_EQ(0, subtask->onStopCalls);
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_return_failed = true;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(0, subtask->onStopCalls);

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(3, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_actionprimitive");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}


