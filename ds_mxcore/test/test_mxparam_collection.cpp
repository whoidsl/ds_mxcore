/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/10/19.
//

#include <gtest/gtest.h>

#include "ds_mxcore/ds_mxparameter.h"
#include "ds_mxcore/ds_mxparameter_shared.h"
#include "ds_mxcore/ds_mxexceptions.h"

TEST(MxParamCollection, basicCollectionIndex) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

ds_mx::IntParam param_copy = collection->get<ds_mx::IntParam>(0);
EXPECT_EQ(1, param_copy.get());
EXPECT_EQ("test_int", param_copy.getName());

EXPECT_EQ(param_int.getPrivate().get(), param_copy.getPrivate().get());

param_int.set(2);
EXPECT_EQ(2, param_copy.get());
EXPECT_EQ(2, param_int.get());
EXPECT_EQ(2, param_copy.get());
}

TEST(MxParamCollection, collectionBadNotFound) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

EXPECT_THROW({
ds_mx::IntParam param_copy = collection->get<ds_mx::IntParam>(1);
}, std::out_of_range);
}

TEST(MxParamCollection, collectionBadCast) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

EXPECT_THROW({
ds_mx::DoubleParam param_copy = collection->get<ds_mx::DoubleParam>(0);
}, ds_mx::ParameterTypeError);
}


TEST(MxParamCollection, basicCollectionName) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

ds_mx::IntParam param_copy = collection->get<ds_mx::IntParam>("test_int");
EXPECT_EQ(1, param_copy.get());
EXPECT_EQ("test_int", param_copy.getName());

EXPECT_EQ(param_int.getPrivate().get(), param_copy.getPrivate().get());

param_int.set(2);
EXPECT_EQ(2, param_copy.get());
EXPECT_EQ(2, param_int.get());
EXPECT_EQ(2, param_copy.get());
}

TEST(MxParamCollection, basicCollectionNameNotFound) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

EXPECT_THROW({
ds_mx::IntParam param_copy = collection->get<ds_mx::IntParam>("test_int1");
}, ds_mx::ParameterCollectionNotFound);
}

TEST(MxParamCollection, collectionBadCastByName) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

EXPECT_THROW({
ds_mx::DoubleParam param_copy = collection->get<ds_mx::DoubleParam>("test_int");
}, ds_mx::ParameterTypeError);
}


TEST(MxParamCollection, basicCollectionGenericName) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

ds_mx::Parameter::Ptr param_generic = collection->getGeneric("test_int");
EXPECT_EQ("test_int", param_generic->getName());
EXPECT_EQ("int", param_generic->TypeName());
EXPECT_EQ("1", param_generic->ValueString());

EXPECT_EQ(param_int.getPrivate().get(), param_generic->getPrivate().get());

param_int.set(2);
EXPECT_EQ("2", param_generic->ValueString());
EXPECT_EQ(2, param_int.get());
}

TEST(MxParamCollection, basicCollectionGenericNameNotFound) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

EXPECT_THROW({
ds_mx::Parameter::Ptr param_generic = collection->getGeneric("test_int1");
}, ds_mx::ParameterCollectionNotFound);
}

TEST(MxParamCollection, basicCollectionGenericIdx) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

ds_mx::Parameter::Ptr param_generic = collection->getGeneric(0);
EXPECT_EQ("test_int", param_generic->getName());
EXPECT_EQ("int", param_generic->TypeName());
EXPECT_EQ("1", param_generic->ValueString());

EXPECT_EQ(param_int.getPrivate().get(), param_generic->getPrivate().get());

param_int.set(2);
EXPECT_EQ("2", param_generic->ValueString());
EXPECT_EQ(2, param_int.get());
}

TEST(MxParamCollection, basicCollectionGenericIdxNotFound) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

EXPECT_THROW({
ds_mx::Parameter::Ptr param_generic = collection->getGeneric(1);
}, std::out_of_range);
}

TEST(MxParamCollection, foreach) {
// build a collect with lots-o-stuff
auto collection = std::make_shared<ds_mx::ParameterCollection>();

ds_mx::BoolParam   param_bool(collection, "test_bool", true);
EXPECT_EQ(true, param_bool.get());

ds_mx::IntParam    param_int(collection, "test_int", 1);
EXPECT_EQ(1, param_int.get());

ds_mx::DoubleParam param_double(collection, "test_double", 2.0);
EXPECT_DOUBLE_EQ(2.0, param_double.get());

ds_mx::StringParam param_string(collection, "test_string", "a string");
EXPECT_EQ("a string", param_string.get());

std::vector<std::string> expected = {"test_bool", "test_int", "test_double", "test_string"};
size_t i = 0;
// Alrighty, now loop!
for(ds_mx::Parameter::Ptr param : *collection) {
ASSERT_LT(i, expected.size());
EXPECT_EQ(expected[i++], param->getName());
}
}

TEST(MxParamCollection, staticExceptionOnChange) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::STATIC);
EXPECT_EQ(1, param_int.get());
EXPECT_EQ("test_int", param_int.getName());

ds_mx::Parameter::Ptr param_generic = collection->getGeneric(0);
EXPECT_EQ("test_int", param_generic->getName());
EXPECT_EQ("int", param_generic->TypeName());
EXPECT_EQ("1", param_generic->ValueString());

EXPECT_EQ(param_int.getPrivate().get(), param_generic->getPrivate().get());

EXPECT_THROW({
param_int.set(2);
}, ds_mx::ParameterTypeError);
}

TEST(MxParamCollection, hasParam) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int_static(collection, "test_int_static", 1, ds_mx::ParameterFlag::STATIC);
ds_mx::IntParam param_int_dynamic(collection, "test_int_dynamic", 1, ds_mx::ParameterFlag::DYNAMIC);

EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_int_static"));
EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_int_dynamic"));
EXPECT_FALSE(collection->has<ds_mx::IntParam>("test_int_nonexistant"));
}

TEST(MxParamCollection, hasParamTypes) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int_static(collection, "test_int_static", 1, ds_mx::ParameterFlag::STATIC);
ds_mx::IntParam param_int_dynamic(collection, "test_int_dynamic", 1, ds_mx::ParameterFlag::DYNAMIC);

EXPECT_FALSE(collection->has<ds_mx::BoolParam>("test_int_static"));
EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_int_static"));
EXPECT_FALSE(collection->has<ds_mx::DoubleParam>("test_int_static"));
EXPECT_FALSE(collection->has<ds_mx::StringParam>("test_int_static"));

EXPECT_FALSE(collection->has<ds_mx::BoolParam>("test_int_dynamic"));
EXPECT_TRUE(collection->has<ds_mx::IntParam>("test_int_dynamic"));
EXPECT_FALSE(collection->has<ds_mx::DoubleParam>("test_int_dynamic"));
EXPECT_FALSE(collection->has<ds_mx::StringParam>("test_int_dynamic"));

EXPECT_FALSE(collection->has<ds_mx::BoolParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->has<ds_mx::IntParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->has<ds_mx::DoubleParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->has<ds_mx::StringParam>("test_int_nonexistant"));
}

TEST(MxParamCollection, hasParamStaticTypes) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int_static(collection, "test_int_static", 1, ds_mx::ParameterFlag::STATIC);
ds_mx::IntParam param_int_dynamic(collection, "test_int_dynamic", 1, ds_mx::ParameterFlag::DYNAMIC);

EXPECT_FALSE(collection->hasStatic<ds_mx::BoolParam>("test_int_static"));
EXPECT_TRUE(collection->hasStatic<ds_mx::IntParam>("test_int_static"));
EXPECT_FALSE(collection->hasStatic<ds_mx::DoubleParam>("test_int_static"));
EXPECT_FALSE(collection->hasStatic<ds_mx::StringParam>("test_int_static"));

EXPECT_FALSE(collection->hasStatic<ds_mx::BoolParam>("test_int_dynamic"));
EXPECT_FALSE(collection->hasStatic<ds_mx::IntParam>("test_int_dynamic"));
EXPECT_FALSE(collection->hasStatic<ds_mx::DoubleParam>("test_int_dynamic"));
EXPECT_FALSE(collection->hasStatic<ds_mx::StringParam>("test_int_dynamic"));

EXPECT_FALSE(collection->hasStatic<ds_mx::BoolParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->hasStatic<ds_mx::IntParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->hasStatic<ds_mx::DoubleParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->hasStatic<ds_mx::StringParam>("test_int_nonexistant"));
}

TEST(MxParamCollection, hasParamDynamicTypes) {
auto collection = std::make_shared<ds_mx::ParameterCollection>();
ds_mx::IntParam param_int_static(collection, "test_int_static", 1, ds_mx::ParameterFlag::STATIC);
ds_mx::IntParam param_int_dynamic(collection, "test_int_dynamic", 1, ds_mx::ParameterFlag::DYNAMIC);

EXPECT_FALSE(collection->hasDynamic<ds_mx::BoolParam>("test_int_static"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::IntParam>("test_int_static"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::DoubleParam>("test_int_static"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::StringParam>("test_int_static"));

EXPECT_FALSE(collection->hasDynamic<ds_mx::BoolParam>("test_int_dynamic"));
EXPECT_TRUE(collection->hasDynamic<ds_mx::IntParam>("test_int_dynamic"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::DoubleParam>("test_int_dynamic"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::StringParam>("test_int_dynamic"));

EXPECT_FALSE(collection->hasDynamic<ds_mx::BoolParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::IntParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::DoubleParam>("test_int_nonexistant"));
EXPECT_FALSE(collection->hasDynamic<ds_mx::StringParam>("test_int_nonexistant"));
}

TEST(MxParamCollection, basicLink) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam foreground(collection, "other", 1, ds_mx::STATIC);
  ds_mx::IntParam background(backup_collection, "test", 2, ds_mx::STATIC);
  collection->linkCollection(backup_collection);

  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test"));
  EXPECT_EQ(2, collection->get<ds_mx::IntParam>("test").get());
}

TEST(MxParamCollection, shadowing) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam foreground(collection, "test", 1, ds_mx::STATIC);
  ds_mx::IntParam background(backup_collection, "test", 2, ds_mx::STATIC);
  collection->linkCollection(backup_collection);

  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test"));
  EXPECT_EQ(1, collection->get<ds_mx::IntParam>("test").get());
}

TEST(MxParamCollection, linkedGeneric) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam foreground(collection, "other", 1, ds_mx::STATIC);
  ds_mx::IntParam background(backup_collection, "test", 2, ds_mx::STATIC);
  collection->linkCollection(backup_collection);

  EXPECT_TRUE(collection->hasGeneric("test"));
  EXPECT_TRUE(collection->hasGeneric("other"));
  EXPECT_FALSE(collection->hasGeneric("doesnotexist"));
  EXPECT_EQ("test", collection->getGeneric("test")->getName());
  EXPECT_EQ("other", collection->getGeneric("other")->getName());
  EXPECT_ANY_THROW(
      collection->getGeneric("doesnotexist");
      );
}
TEST(MxParamCollection, linkedTemplated) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam foreground(collection, "other", 1, ds_mx::STATIC);
  ds_mx::IntParam background(backup_collection, "test", 2, ds_mx::STATIC);
  collection->linkCollection(backup_collection);

  EXPECT_TRUE(collection->has<ds_mx::IntParam>("test"));
  EXPECT_TRUE(collection->has<ds_mx::IntParam>("other"));
  EXPECT_FALSE(collection->has<ds_mx::IntParam>("doesnotexist"));
  EXPECT_FALSE(collection->has<ds_mx::DoubleParam>("test"));
  EXPECT_FALSE(collection->has<ds_mx::DoubleParam>("other"));
  EXPECT_EQ("test", collection->get<ds_mx::IntParam>("test").getName());
  EXPECT_EQ("other", collection->get<ds_mx::IntParam>("other").getName());
  EXPECT_ANY_THROW(
      collection->get<ds_mx::IntParam>("doesnotexist");
      );
  EXPECT_ANY_THROW(
      collection->get<ds_mx::DoubleParam>("test");
  );
  EXPECT_ANY_THROW(
      collection->get<ds_mx::DoubleParam>("other");
  );
}

TEST(MxParamCollection, linkedHasStatic) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam fore_static(collection, "fore_static", 1, ds_mx::STATIC);
  ds_mx::IntParam fore_dynamic(collection, "fore_dynamic", 10, ds_mx::DYNAMIC);
  ds_mx::IntParam back_static(backup_collection, "back_static", 2, ds_mx::STATIC);
  ds_mx::IntParam back_dynamic(backup_collection, "back_dynamic", 20, ds_mx::DYNAMIC);
  collection->linkCollection(backup_collection);

  EXPECT_TRUE(collection->hasStatic<ds_mx::IntParam>("fore_static"));
  EXPECT_FALSE(collection->hasStatic<ds_mx::IntParam>("fore_dynamic"));
  EXPECT_TRUE(collection->hasStatic<ds_mx::IntParam>("back_static"));
  EXPECT_FALSE(collection->hasStatic<ds_mx::IntParam>("back_dynamic"));
  EXPECT_FALSE(collection->hasStatic<ds_mx::DoubleParam>("back_static"));
}

TEST(MxParamCollection, linkedHasDynamic) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam fore_static(collection, "fore_static", 1, ds_mx::STATIC);
  ds_mx::IntParam fore_dynamic(collection, "fore_dynamic", 10, ds_mx::DYNAMIC);
  ds_mx::IntParam back_static(backup_collection, "back_static", 2, ds_mx::STATIC);
  ds_mx::IntParam back_dynamic(backup_collection, "back_dynamic", 20, ds_mx::DYNAMIC);
  collection->linkCollection(backup_collection);

  EXPECT_FALSE(collection->hasDynamic<ds_mx::IntParam>("fore_static"));
  EXPECT_TRUE(collection->hasDynamic<ds_mx::IntParam>("fore_dynamic"));
  EXPECT_FALSE(collection->hasDynamic<ds_mx::IntParam>("back_static"));
  EXPECT_TRUE(collection->hasDynamic<ds_mx::IntParam>("back_dynamic"));
  EXPECT_FALSE(collection->hasDynamic<ds_mx::DoubleParam>("back_dynamic"));
}

TEST(MxParamCollection, linkedIterate) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam fore_static(collection, "fore_static", 1, ds_mx::STATIC);
  ds_mx::IntParam fore_dynamic(collection, "fore_dynamic", 10, ds_mx::DYNAMIC);
  ds_mx::IntParam back_static(backup_collection, "back_static", 2, ds_mx::STATIC);
  ds_mx::IntParam back_dynamic(backup_collection, "back_dynamic", 20, ds_mx::DYNAMIC);
  collection->linkCollection(backup_collection);

  EXPECT_EQ(2, collection->size());
  for (std::shared_ptr<ds_mx::Parameter> p : *collection) {
    EXPECT_NE("back_static", p->getName());
    EXPECT_NE("back_dynamic", p->getName());
  }
}

TEST(MxParamCollection, linkedChangeNotification) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  auto backup_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam fore_static(collection, "fore_static", 1, ds_mx::STATIC);
  ds_mx::IntParam fore_dynamic(collection, "fore_dynamic", 10, ds_mx::DYNAMIC);
  ds_mx::IntParam back_static(backup_collection, "back_static", 2, ds_mx::STATIC);
  ds_mx::IntParam back_dynamic(backup_collection, "back_dynamic", 20, ds_mx::DYNAMIC);
  collection->linkCollection(backup_collection);

  // minitest #1: change foreground
  EXPECT_FALSE(collection->anyChanged());
  EXPECT_FALSE(backup_collection->anyChanged());

  collection->get<ds_mx::IntParam>("fore_dynamic").set(-100);

  EXPECT_TRUE(collection->anyChanged());
  EXPECT_FALSE(backup_collection->anyChanged());

  collection->resetChanged();

  // minitest #2: change background
  EXPECT_FALSE(collection->anyChanged());
  EXPECT_FALSE(backup_collection->anyChanged());

  backup_collection->get<ds_mx::IntParam>("back_dynamic").set(-200);

  EXPECT_FALSE(collection->anyChanged());
  EXPECT_TRUE(backup_collection->anyChanged());

  backup_collection->resetChanged();

  // minitest #3: change from foreground
  EXPECT_FALSE(collection->anyChanged());
  EXPECT_FALSE(backup_collection->anyChanged());

  collection->get<ds_mx::IntParam>("back_dynamic").set(2000);

  EXPECT_FALSE(collection->anyChanged());
  EXPECT_TRUE(backup_collection->anyChanged());

  collection->resetChanged(); // DOES NOT propagate!

  EXPECT_FALSE(collection->anyChanged());
  EXPECT_TRUE(backup_collection->anyChanged());

  backup_collection->resetChanged();

  // double-check reset
  EXPECT_FALSE(collection->anyChanged());
  EXPECT_FALSE(backup_collection->anyChanged());
}