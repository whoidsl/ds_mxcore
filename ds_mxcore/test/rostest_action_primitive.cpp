/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/19/20.
//

#include <gtest/gtest.h>

#include <ds_mxcore/test/ds_mock_action_server.h>

// we'll build this test using Idle actions as an example; however, the point of the test is NOT
// to test the Idle test-- it's to test the underlying Action primitive
#include <ds_mxcore/TestPrimitiveAction.h>


class ActionTestPrimitive : public ds_mx::ActionPrimitive<ds_mxcore::TestPrimitiveAction, ActionTestPrimitive> {
 public:
  ActionTestPrimitive() : ActionPrimitive<ds_mxcore::TestPrimitiveAction, ActionTestPrimitive>("action_test"),
      value(params, "value", 0, ds_mx::ParameterFlag::DYNAMIC) {
  }
  virtual ~ActionTestPrimitive() = default;

  void init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) override { /* do nothing */}
  bool validate() const override { return true; }

  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) override { /* do nothing */ }

  ds_mxcore::TestPrimitiveGoal toMsg() const {
    ds_mxcore::TestPrimitiveGoal ret;
    ret.value = value.get();
    return ret;
  }

  ds_mx::IntParam value;

  // accessors
  ds_mx::TaskReturnCode next_rc() const { return next_return_code_; }
};

class ActionPrimitiveRostest : public ds_mx::MockActionServer<ds_mxcore::TestPrimitiveAction, ActionTestPrimitive> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "stdprim_idle",
  "value" : 0
})";
  }
};

TEST_F(ActionPrimitiveRostest, TestSuccess) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
    EXPECT_TRUE(std::isnan(underTest->timeToGo()));
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(2, underTest->distanceToGo());
  EXPECT_EQ(2, underTest->timeToGo());

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

TEST_F(ActionPrimitiveRostest, TestSuccessRestart) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(2, underTest->distanceToGo());
  EXPECT_EQ(2, underTest->timeToGo());

  // double-check final state, then reset much of it
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);

  // reset the done flag
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_done = false;
  }

  // /////////////////////////////////////////////////////////////////////////////
  // start all over again, beginning with:
  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
    EXPECT_TRUE(std::isnan(underTest->timeToGo()));
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(3, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(3, underTest->distanceToGo());
  EXPECT_EQ(3, underTest->timeToGo());

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(4, underTest->distanceToGo());
  EXPECT_EQ(4, underTest->timeToGo());

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(2, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(4, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

TEST_F(ActionPrimitiveRostest, TestFailed) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  {
    // do this in a lock to double-check uninitialized values
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
    EXPECT_TRUE(std::isnan(underTest->timeToGo()));
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_return_failed = true;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(2, underTest->distanceToGo());
  EXPECT_EQ(2, underTest->timeToGo());

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

TEST_F(ActionPrimitiveRostest, TestStop) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
  EXPECT_TRUE(std::isnan(underTest->timeToGo()));

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // Just call onStop
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);
  }
}

TEST_F(ActionPrimitiveRostest, TestImmediateStop) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
  EXPECT_TRUE(std::isnan(underTest->timeToGo()));

  // Just call onStop WITHOUT waking up the server first
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(0, test_counter);
  }
}

TEST_F(ActionPrimitiveRostest, TestStopRestart) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // stop
  underTest->onStop(state);
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state, then reset much of it
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  // /////////////////////////////////////////////////////////////////////////////
  // start all over again, beginning with:
  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
  EXPECT_TRUE(std::isnan(underTest->timeToGo()));

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(2, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, underTest->distanceToGo());
  EXPECT_EQ(2, underTest->timeToGo());

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(3, underTest->distanceToGo());
  EXPECT_EQ(3, underTest->timeToGo());

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(2, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(3, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

TEST_F(ActionPrimitiveRostest, TestCancel) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // we should still report running even if no response from the action server has been received
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
    EXPECT_TRUE(std::isnan(underTest->timeToGo()));
  }

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(1, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // Issue a cancel event
  ds_mx_msgs::MxEvent event;
  event.eventid = "/cancel/primitive";
  underTest->handleEvent(event);

  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // should actually stop task
  underTest->onStop(state);

  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(1, test_counter);
  }
}

TEST_F(ActionPrimitiveRostest, TestChangeParam) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_FALSE(static_cast<bool>(last_goal));
  }

  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, last_goal->value);
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // change a parameter
  underTest->getParameters().get<ds_mx::IntParam>("value").set(10);

  {
    // do ALL this inside a lock to verify that the uninitialized values are correct
    // -- otherwise test occasionally fails when they get overwritten
    std::unique_lock<std::mutex> lock(test_mutex);
    // parameter change won't take effect until we tick
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    // these values get reset by the primitive while waiting for the first response
    EXPECT_TRUE(std::isnan(underTest->timeToGo()));
    EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, last_goal->value);
  }
  test_cv.notify_all();
  ros::Duration(0.45).sleep();

  // double-check final state, then reset much of it
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(2, test_counter);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(10, last_goal->value);
  }

  // we should still report running even if no response from the action server has been received
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, underTest->distanceToGo());
  EXPECT_EQ(2, underTest->timeToGo());

  // ok, let's actually change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  // This *should* have changed the state of the
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(3, test_counter);
  }

  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(3, underTest->distanceToGo());
  EXPECT_EQ(3, underTest->timeToGo());

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));
  EXPECT_EQ(4, underTest->distanceToGo());
  EXPECT_EQ(4, underTest->timeToGo());

  // double-check final state
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(2, callback_finished);
    EXPECT_EQ(1, test_fail);
    EXPECT_EQ(4, test_counter);
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

TEST_F(ActionPrimitiveRostest, TestChangeParamBefore) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_FALSE(static_cast<bool>(last_goal));
  }

  underTest->getParameters().get<ds_mx::IntParam>("value").set(10);
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  {
    // make sure nothing goes out
    std::unique_lock<std::mutex> lock(test_mutex);
    ASSERT_FALSE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
  }


  // start call on the on-state method, check what we got
  {
    underTest->onStart(state);
    EXPECT_TRUE(std::isnan(underTest->timeToGo()));
    EXPECT_TRUE(std::isnan(underTest->distanceToGo()));
  }
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(10, last_goal->value);
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(2, underTest->distanceToGo());
  EXPECT_EQ(2, underTest->timeToGo());

  {
    std::unique_lock<std::mutex> lock(test_mutex);
    // verify our parameter
    ASSERT_TRUE(static_cast<bool>(last_goal));
    EXPECT_EQ(10, last_goal->value);

  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_actionprimitive");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}










