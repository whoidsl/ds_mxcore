/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/6/19.
//

#include <gtest/gtest.h>
#include <iostream>
#include <ds_mxcore/ds_mxcore.h>
#include <ds_mxcore/ds_mxexceptions.h>

bool empty_callback(ds_mx_msgs::MxEvent evt) {/* do nothing */}

TEST(ds_mxevent, creation) {
  ds_mx::EventHandlerCollection collection;

  ds_mx::EventHandler event(collection, "test_event", "/event_default", empty_callback);

  EXPECT_EQ("test_event", event.getName());

  EXPECT_EQ(1, event.getMatcherStrings().size());
  EXPECT_EQ("/event_default", event.getMatcherStrings()[0]);
}

const char* TEST_EVENT_JSON = R"({
  "simple_test_event": "/event1",
  "array_test_event": [ "/event1", "/event2", "/event3" ],
  "empty_array_test_event": [ ],
  "bad_event_string": "/event!"
})";

class ds_mxevent_parser : public ::testing::Test {
 protected:

  void SetUp() override {
    reader.parse(TEST_EVENT_JSON, json);
    collection.clear();
  }

  Json::Value json;
  Json::Reader reader;
  ds_mx::EventHandlerCollection collection;
};

TEST_F(ds_mxevent_parser, json_parsing_basic) {
  ds_mx::EventHandler event(collection, "simple_test_event", "/event_default", empty_callback);

  event.loadJson(json["simple_test_event"]);

  ASSERT_EQ(1, event.getMatcherStrings().size());
  EXPECT_EQ("/event1", event.getMatcherStrings()[0]);
}

TEST_F(ds_mxevent_parser, json_parsing_array) {
  ds_mx::EventHandler event(collection, "array_test_event", "/event_default", empty_callback);

  event.loadJson(json["array_test_event"]);

  ASSERT_EQ(3, event.getMatcherStrings().size());
  EXPECT_EQ("/event1", event.getMatcherStrings()[0]);
  EXPECT_EQ("/event2", event.getMatcherStrings()[1]);
  EXPECT_EQ("/event3", event.getMatcherStrings()[2]);
}

TEST_F(ds_mxevent_parser, json_parsing_empty_array) {
  ds_mx::EventHandler event(collection, "empty_test_event", "/event_default", empty_callback);

  event.loadJson(json["empty_array_test_event"]);

  EXPECT_EQ(0, event.getMatcherStrings().size());
}

TEST_F(ds_mxevent_parser, json_bad_default) {
  EXPECT_THROW({
    ds_mx::EventHandler event(collection, "bad_event_string", "/event!", empty_callback);
  }, ds_mx::JsonParsingError);
}

TEST_F(ds_mxevent_parser, json_parsing_bad_string) {
  ds_mx::EventHandler event(collection, "bad_event_string", "/event_default", empty_callback);

  EXPECT_THROW({
    event.loadJson(json["bad_event_string"]);
  }, ds_mx::JsonParsingError);
}


TEST_F(ds_mxevent_parser, json_output) {
  ds_mx::EventHandler simple_event(collection, "simple_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler array_event(collection, "array_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler empty_event(collection, "empty_array_test_event", "/event_default", empty_callback);

  simple_event.loadJson(json["simple_test_event"]);
  array_event.loadJson(json["array_test_event"]);
  empty_event.loadJson(json["empty_test_event"]);

  // write a JSON object again
  EXPECT_EQ("\"/event1\"\n", simple_event.saveJson().toStyledString());
  EXPECT_EQ("[ \"/event1\", \"/event2\", \"/event3\" ]\n",
      array_event.saveJson().toStyledString());
  EXPECT_EQ("[]\n", empty_event.saveJson().toStyledString());
}

TEST_F(ds_mxevent_parser, collection_parsing) {
  ds_mx::EventHandler simple_event(collection, "simple_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler array_event(collection, "array_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler empty_event(collection, "empty_array_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler default_event(collection, "default_event", "/event_default", empty_callback);

  collection.loadJson(json);

  ASSERT_EQ(1, simple_event.getMatcherStrings().size());
  EXPECT_EQ("/event1", simple_event.getMatcherStrings()[0]);

  ASSERT_EQ(3, array_event.getMatcherStrings().size());
  EXPECT_EQ("/event1", array_event.getMatcherStrings()[0]);
  EXPECT_EQ("/event2", array_event.getMatcherStrings()[1]);
  EXPECT_EQ("/event3", array_event.getMatcherStrings()[2]);

  EXPECT_EQ(0, empty_event.getMatcherStrings().size());

  ASSERT_EQ(1, default_event.getMatcherStrings().size());
  EXPECT_EQ("/event_default", default_event.getMatcherStrings()[0]);
}

TEST_F(ds_mxevent_parser, collection_json_output) {
  ds_mx::EventHandler simple_event(collection, "simple_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler array_event(collection, "array_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler empty_event(collection, "empty_array_test_event", "/event_default", empty_callback);
  ds_mx::EventHandler default_event(collection, "default_event", "/event_default", empty_callback);

  collection.loadJson(json);

  Json::Value result(Json::objectValue);
  collection.saveJson(result);

  // Output appears to be listed alphabetically, not in order
  EXPECT_EQ(R"({
   "array_test_event" : [ "/event1", "/event2", "/event3" ],
   "default_event" : "/event_default",
   "empty_array_test_event" : [],
   "simple_test_event" : "/event1"
}
)", result.toStyledString());
}

TEST(ds_mxevent_matcher, basic_matching) {
  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler event(collection, "event", "/default_event", empty_callback);

  EXPECT_FALSE(event.eventMatches("/default_even"));
  EXPECT_TRUE(event.eventMatches("/default_event"));
  EXPECT_FALSE(event.eventMatches("/default_event "));
}

TEST(ds_mxevent_matcher, glob_matching) {
  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler event(collection, "event", "/begin*end", empty_callback);

  EXPECT_FALSE(event.eventMatches("/begin"));
  EXPECT_TRUE(event.eventMatches("/beginend"));
  EXPECT_TRUE(event.eventMatches("/beginaend"));
  EXPECT_TRUE(event.eventMatches("/begin/end"));
  EXPECT_TRUE(event.eventMatches("/begin1end"));
  EXPECT_FALSE(event.eventMatches("/beginenda"));
}

TEST(ds_mxevent_validator, basic) {
  EXPECT_TRUE(ds_mx::eventStringValid("/-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));

  EXPECT_FALSE(ds_mx::eventStringValid("/abc!"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc@"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc#"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc$"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc%"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc^"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc&"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc*"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc("));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc)"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc["));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc]"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc;"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc:"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc'"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc\""));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc\\"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc?"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc>"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc<"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc{"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc}"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc|"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc+"));
  EXPECT_FALSE(ds_mx::eventStringValid("/abc="));
}

TEST(ds_mxevent_validator, matcher) {
  EXPECT_TRUE(ds_mx::eventMatcherStringValid("/-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*"));

  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc!"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc@"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc#"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc$"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc%"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc^"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc&"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc("));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc)"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc["));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc]"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc;"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc:"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc'"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc\""));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc\\"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc?"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc>"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc<"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc{"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc}"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc|"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc+"));
  EXPECT_FALSE(ds_mx::eventMatcherStringValid("/abc="));
}

// This is a minimal example showing how to use the EventHandler in the context
// of a Task class.  Note that in a real task the EventHandlerCollection is held by the
// base class, MxTask
class EventTestClass {
 public:
  EventTestClass():event_count(0),
                   handler(events, "test_event", "/test", &EventTestClass::callback, this) {}

  bool callback(ds_mx_msgs::MxEvent evt) {
    event_count++;
    return true;
  }
  size_t event_count;

  ds_mx::EventHandlerCollection events; // part of MxTask by default
  ds_mx::EventHandler handler;
};

size_t event_test_global_int;

bool event_test_increment_global(ds_mx_msgs::MxEvent evt) {
  event_test_global_int++;
  return true;
}

TEST(ds_mxevent, basic_callback_execution) {
  event_test_global_int = 0;

  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler event_handler(collection, "event", "/test", event_test_increment_global);
  ds_mx_msgs::MxEvent event;
  event.eventid = "/test";
  EXPECT_TRUE(event_handler.eventMatches(event.eventid));

  EXPECT_EQ(0, event_test_global_int);
  EXPECT_TRUE(event_handler.handleEvent(event));
  EXPECT_EQ(1, event_test_global_int);
}

TEST(ds_mxevent, bound_callback_execution) {

  EventTestClass test_class;
  ds_mx_msgs::MxEvent event;
  event.eventid = "/test";
  EXPECT_TRUE(test_class.handler.eventMatches(event.eventid));

  EXPECT_EQ(0, test_class.event_count);
  EXPECT_TRUE(test_class.handler.handleEvent(event));
  EXPECT_EQ(1, test_class.event_count);
}

TEST(ds_mxevent_collection, hasEvent) {
  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler test_event(collection, "test_event", "/event_default", empty_callback);

  EXPECT_TRUE(collection.has("test_event"));
  EXPECT_FALSE(collection.has("missing_event"));

}

bool return_true(ds_mx_msgs::MxEvent evt) { return true; }
bool return_false(ds_mx_msgs::MxEvent evt) { return false; }

TEST(ds_mxevent_collection, trueevent) {
  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler simple_event(collection, "true_event", "/event_default", return_true);

  ds_mx_msgs::MxEvent event;
  event.eventid = "/event_default";
  EXPECT_TRUE(collection.handleEvent(event));
}

TEST(ds_mxevent_collection, falseevent) {
  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler array_event(collection, "false_event", "/event_default", return_false);

  ds_mx_msgs::MxEvent event;
  event.eventid = "/event_default";
  EXPECT_FALSE(collection.handleEvent(event));
}

TEST(ds_mxevent_collection, multievents) {
  ds_mx::EventHandlerCollection collection;
  ds_mx::EventHandler simple_event(collection, "true_event", "/event_default", return_true);
  ds_mx::EventHandler array_event(collection, "false_event", "/event_default", return_false);

  ds_mx_msgs::MxEvent event;
  event.eventid = "/event_default";
  EXPECT_FALSE(collection.handleEvent(event));
}

