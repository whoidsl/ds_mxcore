/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/7/19.
//

#include <gtest/gtest.h>
#include <iostream>
#include <vector>
#include <boost/geometry/io/wkt/read.hpp>

#include "ds_mxcore/ds_mxparameter.h"
#include "ds_mxcore/ds_mxparameter_shared.h"
#include "ds_mxcore/ds_mxexceptions.h"

#include <cxxabi.h>

TEST(MxParam, default_creation) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();

  ds_mx::BoolParam   param_bool(collection, "test_bool");
  EXPECT_EQ(false, param_bool.get());

  ds_mx::IntParam    param_int(collection, "test_int");
  EXPECT_EQ(0, param_int.get());

  ds_mx::DoubleParam param_double(collection, "test_double");
  EXPECT_DOUBLE_EQ(0.0, param_double.get());

  ds_mx::StringParam param_string(collection, "test_string");
  EXPECT_EQ("", param_string.get());

  ds_mx::GeoPointParam param_point(collection, "test_point");
  EXPECT_EQ(0.0, param_point.get().x);
  EXPECT_EQ(0.0, param_point.get().y);

  ds_mx::KeyValueListParam param_list(collection, "test_list");
  EXPECT_EQ(0, param_list.get().dict.size());

  ds_mx::GeoPointListParam param_gpointlist(collection, "test_geopointlist");
  EXPECT_EQ(0, param_gpointlist.get().points.size());
}

TEST(MxParam, nonstandard_creation) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();

  ds_mx::BoolParam   param_bool(collection, "test_bool", true);
  EXPECT_EQ(true, param_bool.get());

  ds_mx::IntParam    param_int(collection, "test_int", 1);
  EXPECT_EQ(1, param_int.get());

  ds_mx::DoubleParam param_double(collection, "test_double", 2.0);
  EXPECT_DOUBLE_EQ(2.0, param_double.get());

  ds_mx::StringParam param_string(collection, "test_string", "a string");
  EXPECT_EQ("a string", param_string.get());

  ds_mx::GeoPointParam param_point(collection, "test_point", ds_mx::GeoPoint(1.0,2.0));
  EXPECT_EQ(1.0, param_point.get().x);
  EXPECT_EQ(2.0, param_point.get().y);

  ds_mx::KeyValueListParam param_list(collection, "test_list", ds_mx::KeyValueList(std::make_pair("param1", "test")));
  ASSERT_EQ(1, param_list.get().dict.size());
  EXPECT_EQ("test", param_list.get()["param1"]);

  std::vector<ds_mx::GeoPoint> gpoints;
  gpoints.push_back(ds_mx::GeoPoint(1.0,2.0));
  gpoints.push_back(ds_mx::GeoPoint(4.5,6.1));
  ds_mx::GeoPointListParam param_gpointlist(collection, "test_geopoints", gpoints);
  EXPECT_EQ(1.0, param_gpointlist.get().points.at(0).x);
  EXPECT_EQ(6.1, param_gpointlist.get().points.at(1).y);
}

TEST(MxParam, copy) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam param_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
  EXPECT_EQ(1, param_int.get());
  EXPECT_EQ("test_int", param_int.getName());

  ds_mx::IntParam param_copy(param_int);
  EXPECT_EQ(1, param_copy.get());
  EXPECT_EQ("test_int", param_copy.getName());

  EXPECT_EQ(param_int.getPrivate().get(), param_copy.getPrivate().get());

  param_int.set(2);
  EXPECT_EQ(2, param_copy.get());
  EXPECT_EQ(2, param_int.get());
  EXPECT_EQ(2, param_copy.get());
}

TEST(MxParam, nanChangeDetection) {
  // This one's from a real bug that really happened!
  // NaN comparisons are a little screwy sometimes, so we added explicity handling for them.  And
  // an explicity test to make sure it worked.
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::DoubleParam param(collection, "test_double", std::numeric_limits<double>::quiet_NaN(), ds_mx::ParameterFlag::DYNAMIC);

  // General test format:  Set a value, check if "hasChanged" works right, then
  // call "resetChanged" to update the "prev" value in the underlying variable

  // this one tests valueChanged(NaN, NaN)
  EXPECT_FALSE(param.hasChanged());

  // now let's check valueChanged (NaN, 1)
  param.resetChanged();
  param.set(1.0);
  EXPECT_TRUE(param.hasChanged());

  // now let's check all-NaN no-change: valueChanged (1, 1);
  param.resetChanged();
  param.set(1.0);
  EXPECT_FALSE(param.hasChanged());

  // now let's check all-NaN changed: valueChanged (1, 2);
  param.resetChanged();
  param.set(2.0);
  EXPECT_TRUE(param.hasChanged());

  // now let's check all-NaN changed: valueChanged (2, NaN)
  param.resetChanged();
  param.set(std::numeric_limits<double>::quiet_NaN());
  EXPECT_TRUE(param.hasChanged());
}


TEST(MxParam, canSetFromStringDynamic) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::BoolParam param_bool(collection, "test_bool", false, ds_mx::ParameterFlag::STATIC);
  ds_mx::IntParam param_int(collection, "test_int", 0, ds_mx::ParameterFlag::STATIC);
  ds_mx::DoubleParam param_double(collection, "test_double", 0.0, ds_mx::ParameterFlag::STATIC);
  ds_mx::StringParam param_string(collection, "test_string", "", ds_mx::ParameterFlag::STATIC);
  ds_mx::GeoPointParam param_point(collection, "test_point", ds_mx::GeoPoint(), ds_mx::ParameterFlag::STATIC);
  ds_mx::KeyValueListParam param_list(collection, "test_list", ds_mx::KeyValueList(), ds_mx::ParameterFlag::STATIC);
  ds_mx::GeoPointListParam param_gpointlist(collection, "test_gpointlist", ds_mx::GeoPointList(), ds_mx::ParameterFlag::STATIC);

  EXPECT_TRUE(param_bool.canSetFromString("true"));
  EXPECT_TRUE(param_bool.canSetFromString("1"));
  EXPECT_TRUE(param_bool.canSetFromString("1.0"));
  EXPECT_FALSE(param_bool.canSetFromString("some_string"));
  EXPECT_FALSE(param_bool.canSetFromString("POINT(1.0 2.0)"));
  EXPECT_FALSE(param_bool.canSetFromString("{\"param1\": \"some str\"}"));

  EXPECT_TRUE(param_int.canSetFromString("true"));
  EXPECT_TRUE(param_int.canSetFromString("1"));
  EXPECT_TRUE(param_int.canSetFromString("1.0"));
  EXPECT_FALSE(param_int.canSetFromString("some_string"));
  EXPECT_FALSE(param_int.canSetFromString("POINT(1.0 2.0)"));
  EXPECT_FALSE(param_int.canSetFromString("{\"param1\": \"some str\"}"));

  EXPECT_TRUE(param_double.canSetFromString("true"));
  EXPECT_TRUE(param_double.canSetFromString("1"));
  EXPECT_TRUE(param_double.canSetFromString("1.0"));
  EXPECT_FALSE(param_double.canSetFromString("some_string"));
  EXPECT_FALSE(param_double.canSetFromString("POINT(1.0 2.0)"));
  EXPECT_FALSE(param_double.canSetFromString("{\"param1\": \"some str\"}"));

  EXPECT_TRUE(param_string.canSetFromString("true"));
  EXPECT_TRUE(param_string.canSetFromString("1"));
  EXPECT_TRUE(param_string.canSetFromString("1.0"));
  EXPECT_TRUE(param_string.canSetFromString("some_string"));
  EXPECT_TRUE(param_string.canSetFromString("POINT(1.0 2.0)"));
  EXPECT_TRUE(param_string.canSetFromString("{\"param1\": \"some str\"}"));

  EXPECT_FALSE(param_point.canSetFromString("true"));
  EXPECT_FALSE(param_point.canSetFromString("1"));
  EXPECT_FALSE(param_point.canSetFromString("1.0"));
  EXPECT_FALSE(param_point.canSetFromString("some_string"));
  EXPECT_TRUE(param_point.canSetFromString("POINT(1.0 2.0)"));
  EXPECT_FALSE(param_point.canSetFromString("{\"param1\": \"some str\"}"));

  EXPECT_FALSE(param_list.canSetFromString("true"));
  EXPECT_FALSE(param_list.canSetFromString("1"));
  EXPECT_FALSE(param_list.canSetFromString("1.0"));
  EXPECT_FALSE(param_list.canSetFromString("some_string"));
  EXPECT_FALSE(param_list.canSetFromString("POINT(1.0 2.0)"));
  EXPECT_TRUE(param_list.canSetFromString("{\"param1\": \"some str\"}"));

  EXPECT_TRUE(param_gpointlist.canSetFromString("LINESTRING (2.5 2.45,3.23 12)"));
  EXPECT_FALSE(param_gpointlist.canSetFromString("LINEWEFWESTRING (2.5 2.45,3.23 12)"));
}

TEST(MxParam, setFromStringDynamic) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::BoolParam param_bool(collection, "test_bool", false, ds_mx::ParameterFlag::STATIC);
  ds_mx::IntParam param_int(collection, "test_int", 0, ds_mx::ParameterFlag::STATIC);
  ds_mx::DoubleParam param_double(collection, "test_double", 0.0, ds_mx::ParameterFlag::STATIC);
  ds_mx::StringParam param_string(collection, "test_string", "", ds_mx::ParameterFlag::STATIC);
  ds_mx::GeoPointParam param_point(collection, "test_point", ds_mx::GeoPoint(), ds_mx::ParameterFlag::STATIC);
  ds_mx::KeyValueListParam param_list(collection, "test_list", ds_mx::KeyValueList(), ds_mx::ParameterFlag::STATIC);
  ds_mx::GeoPointListParam param_gpointlist(collection, "test_gpointlist", ds_mx::GeoPointList(), ds_mx::ParameterFlag::STATIC);

  param_bool.setFromString("true");
  EXPECT_TRUE(param_bool.get());
  param_bool.setFromString("1");
  EXPECT_TRUE(param_bool.get());
  param_bool.setFromString("1.0");
  EXPECT_TRUE(param_bool.get());
  EXPECT_THROW({
                 param_bool.setFromString("some_string");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_bool.setFromString("POINT(1.0 2.0)");
               }, ds_mx::ParameterTypeError);

  param_int.setFromString("true");
  EXPECT_EQ(1, param_int.get());
  param_int.setFromString("1");
  EXPECT_EQ(1, param_int.get());
  param_int.setFromString("1.0");
  EXPECT_EQ(1, param_int.get());
  EXPECT_THROW({
                 param_int.setFromString("some_string");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_int.setFromString("POINT(1.0 2.0)");
               }, ds_mx::ParameterTypeError);

  param_double.setFromString("true");
  EXPECT_EQ(1.0, param_double.get());
  param_double.setFromString("1");
  EXPECT_EQ(1.0, param_double.get());
  param_double.setFromString("1.0");
  EXPECT_EQ(1.0, param_double.get());
  EXPECT_THROW({
                 param_double.setFromString("some_string");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_double.setFromString("POINT(1.0 2.0)");
               }, ds_mx::ParameterTypeError);

  param_string.setFromString("true");
  EXPECT_EQ("true", param_string.get());
  param_string.setFromString("1");
  EXPECT_EQ("1", param_string.get());
  param_string.setFromString("1.0");
  EXPECT_EQ("1.0", param_string.get());
  param_string.setFromString("some_string");
  EXPECT_EQ("some_string", param_string.get());
  param_string.setFromString("POINT(1.0 2.0)");
  EXPECT_EQ("POINT(1.0 2.0)", param_string.get());

  EXPECT_THROW({
                  param_point.setFromString("true");
  }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_point.setFromString("1");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_point.setFromString("1.0");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_point.setFromString("some string");
               }, ds_mx::ParameterTypeError);
  param_point.setFromString("POINT(1.0 2.0)");
  EXPECT_EQ(ds_mx::GeoPoint(1.0, 2.0), param_point.get());


  EXPECT_THROW({
                  param_list.setFromString("true");
  }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_list.setFromString("1");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_list.setFromString("1.0");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_list.setFromString("some string");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_list.setFromString("POINT(1.0 2.0)");
               }, ds_mx::ParameterTypeError);
  param_list.setFromString("{\"param1\": \"some string\"}");
  EXPECT_EQ(ds_mx::KeyValueList(std::make_pair("param1", "some string")), param_list.get());

  //set from string gpointlist testing
  EXPECT_THROW({
                  param_gpointlist.setFromString("true");
  }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_gpointlist.setFromString("1");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_gpointlist.setFromString("1.0");
               }, ds_mx::ParameterTypeError);
  EXPECT_THROW({
                 param_gpointlist.setFromString("some string");
               }, ds_mx::ParameterTypeError);
  param_gpointlist.setFromString("LINESTRING (2.5 2.45,3.23 12)");
  EXPECT_EQ(ds_mx::GeoPoint(2.5, 2.45), param_gpointlist.get().points.at(0));

}

const char* TEST_JSON = R"({
      "type": "example_json",
      "test_bool_true": true,
      "test_bool_false": false,
      "test_int_one": 1,
      "test_int_zero": 0,
      "test_double_two": 2.0,
      "test_double_zero": 0.0,
      "test_string_three": "3.0",
      "test_string_text": "Test 1 2 3",
      "test_point": "POINT (1.0 2.0) ",
      "test_list": {
          "param1": "some str",
          "param2": "another thing"
      },
      "test_gpointlist": "LINESTRING (1.22 5.45,7.21 12) ",
      "test_shared_bool": "#shared_bool_true",
      "test_shared_int": "#shared_int_one",
      "test_shared_double": "#shared_double_two",
      "test_shared_string": "#shared_string_text",
      "test_shared_point": "#shared_point",
      "test_shared_list": "#shared_list",
      "test_shared_gpointlist": "#shared_gpointlist"
    })";

const char* TEST_SHARED = R"({
    "shared_bool_true": {
        "type": "bool",
        "value": true
    },
    "shared_int_one": {
        "type": "int",
        "value": 1
    },
    "shared_double_two": {
        "type": "double",
        "value": 2.0
    },
    "shared_limits": {
        "type": "double",
        "value": 1.0,
        "max": 2.0,
        "min": 0.0
    },
    "shared_string_text": {
        "type": "string",
        "value": "shared text"
    },
    "shared_point" : {
        "type": "geopoint",
        "value": "POINT (30.0 40.0) "
    },
    "shared_gpointlist" : {
        "type": "geopointlist",
        "value": "LINESTRING (2.5 6,2 11) "
    },
    "shared_list" : {
        "type": "keyvaluelist",
        "value": {
            "param1": "yet another str",
            "param2": "test test",
        }
    }
  })";

class MxParamParserTest : public ::testing::Test {
  protected:

  void SetUp() override {
    collection = std::make_shared<ds_mx::ParameterCollection>();
    reader.parse(TEST_JSON, json_root);
  }

  Json::Value json_root;
  Json::Reader reader;

  std::shared_ptr<ds_mx::ParameterCollection> collection;

};

TEST_F(MxParamParserTest, parseBool) {
  ds_mx::BoolParam test_bool(collection, "test_bool", ds_mx::ParameterFlag::STATIC);

  test_bool.loadJson(json_root["test_bool_true"]);
  EXPECT_TRUE(test_bool.get());
  test_bool.loadJson(json_root["test_bool_false"]);
  EXPECT_FALSE(test_bool.get());

  test_bool.loadJson(json_root["test_int_one"]);
  EXPECT_TRUE(test_bool.get());
  test_bool.loadJson(json_root["test_int_zero"]);
  EXPECT_FALSE(test_bool.get());

  test_bool.loadJson(json_root["test_double_two"]);
  EXPECT_TRUE(test_bool.get());
  test_bool.loadJson(json_root["test_double_zero"]);
  EXPECT_FALSE(test_bool.get());

  EXPECT_THROW({
                 test_bool.loadJson(json_root["test_string_three"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_bool.loadJson(json_root["test_point"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_bool.loadJson(json_root["test_list"]);
               }, ds_mx::JsonParsingError);
}

TEST_F(MxParamParserTest, parseInt) {
  ds_mx::IntParam test_int(collection, "test_int", ds_mx::ParameterFlag::STATIC);

  test_int.loadJson(json_root["test_bool_true"]);
  EXPECT_EQ(1, test_int.get());
  test_int.loadJson(json_root["test_bool_false"]);
  EXPECT_EQ(0, test_int.get());

  test_int.loadJson(json_root["test_int_one"]);
  EXPECT_EQ(1, test_int.get());
  test_int.loadJson(json_root["test_int_zero"]);
  EXPECT_EQ(0, test_int.get());

  test_int.loadJson(json_root["test_double_two"]);
  EXPECT_EQ(2, test_int.get());
  test_int.loadJson(json_root["test_double_zero"]);
  EXPECT_EQ(0, test_int.get());

  EXPECT_THROW({
                 test_int.loadJson(json_root["test_string_three"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_int.loadJson(json_root["test_string_text"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_int.loadJson(json_root["test_point"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_int.loadJson(json_root["test_list"]);
               }, ds_mx::JsonParsingError);
}

TEST_F(MxParamParserTest, parseDouble) {
  ds_mx::DoubleParam test_double(collection, "test_double", ds_mx::ParameterFlag::STATIC);

  test_double.loadJson(json_root["test_bool_true"]);
  EXPECT_DOUBLE_EQ(1.0, test_double.get());
  test_double.loadJson(json_root["test_bool_false"]);
  EXPECT_DOUBLE_EQ(0.0, test_double.get());

  test_double.loadJson(json_root["test_int_one"]);
  EXPECT_DOUBLE_EQ(1.0, test_double.get());
  test_double.loadJson(json_root["test_int_zero"]);
  EXPECT_DOUBLE_EQ(0.0, test_double.get());

  test_double.loadJson(json_root["test_double_two"]);
  EXPECT_DOUBLE_EQ(2.0, test_double.get());
  test_double.loadJson(json_root["test_double_zero"]);
  EXPECT_DOUBLE_EQ(0.0, test_double.get());

  EXPECT_THROW({
                 test_double.loadJson(json_root["test_string_three"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_double.loadJson(json_root["test_string_text"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_double.loadJson(json_root["test_point"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_double.loadJson(json_root["test_list"]);
               }, ds_mx::JsonParsingError);
}

TEST_F(MxParamParserTest, parseString) {
  ds_mx::StringParam test_string(collection, "test_string", ds_mx::ParameterFlag::STATIC);

  test_string.loadJson(json_root["test_bool_true"]);
  EXPECT_EQ("true", test_string.get());
  test_string.loadJson(json_root["test_bool_false"]);
  EXPECT_EQ("false", test_string.get());

  test_string.loadJson(json_root["test_int_one"]);
  EXPECT_EQ("1", test_string.get());
  test_string.loadJson(json_root["test_int_zero"]);
  EXPECT_EQ("0", test_string.get());

  test_string.loadJson(json_root["test_double_two"]);
  EXPECT_EQ("2", test_string.get());
  test_string.loadJson(json_root["test_double_zero"]);
  EXPECT_EQ("0", test_string.get());

  test_string.loadJson(json_root["test_string_three"]);
  EXPECT_EQ("3.0", test_string.get());
  test_string.loadJson(json_root["test_string_text"]);
  EXPECT_EQ("Test 1 2 3", test_string.get());

  test_string.loadJson(json_root["test_point"]);
  EXPECT_EQ("POINT (1.0 2.0) ", test_string.get());

  test_string.loadJson(json_root["test_list"]);
  EXPECT_EQ("{\n   \"param1\" : \"some str\",\n   \"param2\" : \"another thing\"\n}\n", test_string.get());
}

TEST_F(MxParamParserTest, parsePoint) {
  ds_mx::GeoPointParam test_point(collection, "test_point", ds_mx::ParameterFlag::STATIC);

  EXPECT_THROW({
                 test_point.loadJson(json_root["test_bool_true"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_point.loadJson(json_root["test_bool_true"]);
               }, ds_mx::JsonParsingError);


  EXPECT_THROW({
                 test_point.loadJson(json_root["test_int_one"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_point.loadJson(json_root["test_int_zero"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_point.loadJson(json_root["test_double_two"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_point.loadJson(json_root["test_double_zero"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_point.loadJson(json_root["test_string_three"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_point.loadJson(json_root["test_string_text"]);
               }, ds_mx::JsonParsingError);

  test_point.loadJson(json_root["test_point"]);
  EXPECT_EQ(ds_mx::GeoPoint(1.0, 2.0), test_point.get());

  EXPECT_THROW({
                 test_point.loadJson(json_root["test_list"]);
               }, ds_mx::JsonParsingError);
}

TEST_F(MxParamParserTest, parsePointList) {
  ds_mx::GeoPointListParam test_gpointlist(collection, "test_gpointlist", ds_mx::ParameterFlag::STATIC);

  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_bool_true"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_bool_true"]);
               }, ds_mx::JsonParsingError);


  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_int_one"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_int_zero"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_double_two"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_double_zero"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_string_three"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_string_text"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_point"]);
               }, ds_mx::JsonParsingError);

  std::vector<ds_mx::GeoPoint> gpoints;
  gpoints.emplace_back(ds_mx::GeoPoint(1.22,5.45));
  gpoints.emplace_back(ds_mx::GeoPoint(7.21,12));

  test_gpointlist.loadJson(json_root["test_gpointlist"]);
  EXPECT_EQ(ds_mx::GeoPointList(gpoints), test_gpointlist.get());

  EXPECT_THROW({
                 test_gpointlist.loadJson(json_root["test_list"]);
               }, ds_mx::JsonParsingError);
}


TEST_F(MxParamParserTest, parseKeyValueList) {
  ds_mx::KeyValueListParam test_list(collection, "test_list", ds_mx::ParameterFlag::STATIC);

  EXPECT_THROW({
                 test_list.loadJson(json_root["test_bool_true"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_list.loadJson(json_root["test_bool_true"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_list.loadJson(json_root["test_int_one"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_list.loadJson(json_root["test_int_zero"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_list.loadJson(json_root["test_double_two"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_list.loadJson(json_root["test_double_zero"]);
               }, ds_mx::JsonParsingError);

  EXPECT_THROW({
                 test_list.loadJson(json_root["test_string_three"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_list.loadJson(json_root["test_string_text"]);
               }, ds_mx::JsonParsingError);
  EXPECT_THROW({
                 test_list.loadJson(json_root["test_point"]);
               }, ds_mx::JsonParsingError);

  test_list.loadJson(json_root["test_list"]);
  EXPECT_EQ(ds_mx::KeyValueList(std::vector<std::pair<std::string, std::string>>{
    std::make_pair("param1", "some str"),
    std::make_pair("param2", "another thing")
  }), test_list.get());

}

TEST(MxParamLinking, basicLink) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam test_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::IntParam linked_int(collection, "linked_int", ds_mx::ParameterFlag::DYNAMIC);

  EXPECT_EQ(1, test_int.get());
  EXPECT_EQ(0, linked_int.get());

  linked_int.linkParameter(test_int);

  EXPECT_EQ(1, test_int.get());
  EXPECT_EQ(1, linked_int.get());
  EXPECT_EQ("test_int", test_int.getName());
  EXPECT_EQ("linked_int", linked_int.getName());

  test_int.set(3);
  EXPECT_EQ(3, test_int.get());
  EXPECT_EQ(3, linked_int.get());

  linked_int.set(4);
  EXPECT_EQ(4, test_int.get());
  EXPECT_EQ(4, linked_int.get());
}

TEST(MxParamLinking, changeDetection) {
  auto collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam test_int(collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::IntParam linked_int(collection, "linked_int", ds_mx::ParameterFlag::DYNAMIC);

  EXPECT_EQ(1, test_int.get());
  EXPECT_FALSE(test_int.hasChanged());
  EXPECT_EQ(0, linked_int.get());
  EXPECT_FALSE(linked_int.hasChanged());

  linked_int.linkParameter(test_int);

  EXPECT_EQ(1, test_int.get());
  EXPECT_FALSE(test_int.hasChanged());
  EXPECT_EQ(1, linked_int.get());
  EXPECT_FALSE(linked_int.hasChanged()); // this is debateable...

  test_int.set(2);

  EXPECT_EQ(2, test_int.get());
  EXPECT_TRUE(test_int.hasChanged());
  EXPECT_EQ(2, linked_int.get());
  EXPECT_TRUE(linked_int.hasChanged());

  test_int.resetChanged();

  EXPECT_EQ(2, test_int.get());
  EXPECT_FALSE(test_int.hasChanged());
  EXPECT_EQ(2, linked_int.get());
  EXPECT_TRUE(linked_int.hasChanged());

  linked_int.resetChanged();

  EXPECT_EQ(2, test_int.get());
  EXPECT_FALSE(test_int.hasChanged());
  EXPECT_EQ(2, linked_int.get());
  EXPECT_FALSE(linked_int.hasChanged());
}

TEST(MxParamLinking, linkTypeError) {
  auto test_collection = std::make_shared<ds_mx::ParameterCollection>();
  auto linked_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::Parameter::Ptr test_double = std::make_shared<ds_mx::DoubleParam>(test_collection,
      "test_double", 1.0, ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::IntParam linked_int(linked_collection, "linked_int", ds_mx::ParameterFlag::DYNAMIC);

  EXPECT_THROW({
    linked_int.linkParameter(test_double);
  }, ds_mx::ParameterTypeError);
}

TEST(MxParamLinking, staticVsDynamicLinking) {
  auto test_collection = std::make_shared<ds_mx::ParameterCollection>();
  auto linked_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam test_static_int(test_collection, "test_int", 1, ds_mx::ParameterFlag::STATIC);
  ds_mx::IntParam linked_static_int(linked_collection, "linked_int", ds_mx::ParameterFlag::STATIC);
  ds_mx::IntParam test_dynamic_int(test_collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::IntParam linked_dynamic_int(linked_collection, "linked_int", ds_mx::ParameterFlag::DYNAMIC);

  EXPECT_THROW({
                 linked_static_int.linkParameter(test_dynamic_int);
               }, ds_mx::ParameterTypeError);

  EXPECT_THROW({
                 linked_dynamic_int.linkParameter(test_static_int);
               }, ds_mx::ParameterTypeError);

  EXPECT_EQ(0, linked_static_int.get());
  linked_static_int.linkParameter(test_static_int);
  EXPECT_EQ(1, linked_static_int.get());

  EXPECT_EQ(0, linked_dynamic_int.get());
  linked_dynamic_int.linkParameter(test_dynamic_int);
  EXPECT_EQ(1, linked_dynamic_int.get());
}

TEST(MxParamLinking, collectionChangeDetection) {
  auto test_collection = std::make_shared<ds_mx::ParameterCollection>();
  auto linked_collection = std::make_shared<ds_mx::ParameterCollection>();
  ds_mx::IntParam test_int(test_collection, "test_int", 1, ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::IntParam linked_int(linked_collection, "linked_int", ds_mx::ParameterFlag::DYNAMIC);

  linked_int.linkParameter(test_int);

  EXPECT_FALSE(test_collection->anyChanged());
  EXPECT_FALSE(linked_collection->anyChanged());

  test_int.set(2);

  EXPECT_TRUE(test_collection->anyChanged());
  EXPECT_TRUE(linked_collection->anyChanged());

  test_collection->resetChanged();

  EXPECT_FALSE(test_collection->anyChanged());
  EXPECT_TRUE(linked_collection->anyChanged());

  linked_collection->resetChanged();

  EXPECT_FALSE(test_collection->anyChanged());
  EXPECT_FALSE(linked_collection->anyChanged());
}

class MxParamSharedParserTest : public ::testing::Test {
  protected:

  void SetUp() override {
    collection.reset(new ds_mx::ParameterCollection());
    registry.reset(new ds_mx::SharedParameterRegistry());

    reader.parse(TEST_JSON, json_root);
    reader.parse(TEST_SHARED, shared_root);
  }

  Json::Value json_root;
  Json::Value shared_root;
  Json::Reader reader;

  std::shared_ptr<ds_mx::ParameterCollection> collection;
  std::shared_ptr<ds_mx::SharedParameterRegistry> registry;
};

TEST_F(MxParamSharedParserTest, parseShared) {
  registry->loadJson(shared_root);

  EXPECT_TRUE(registry->collection().has<ds_mx::BoolParam>("shared_bool_true"));
  EXPECT_TRUE(registry->collection().has<ds_mx::IntParam>("shared_int_one"));
  EXPECT_TRUE(registry->collection().has<ds_mx::DoubleParam>("shared_double_two"));
  EXPECT_TRUE(registry->collection().has<ds_mx::StringParam>("shared_string_text"));
  EXPECT_TRUE(registry->collection().has<ds_mx::GeoPointParam>("shared_point"));
  EXPECT_TRUE(registry->collection().has<ds_mx::KeyValueListParam>("shared_list"));

  EXPECT_TRUE(registry->collection().has<ds_mx::SharedBoolParam>("shared_bool_true"));
  EXPECT_TRUE(registry->collection().has<ds_mx::SharedIntParam>("shared_int_one"));
  EXPECT_TRUE(registry->collection().has<ds_mx::SharedDoubleParam>("shared_double_two"));
  EXPECT_TRUE(registry->collection().has<ds_mx::SharedStringParam>("shared_string_text"));
  EXPECT_TRUE(registry->collection().has<ds_mx::SharedGeoPointParam>("shared_point"));
  EXPECT_TRUE(registry->collection().has<ds_mx::KeyValueListParam>("shared_list"));
  EXPECT_TRUE(registry->collection().has<ds_mx::SharedGeoPointListParam>("shared_gpointlist"));

}

TEST_F(MxParamSharedParserTest, testLimits) {
  registry->loadJson(shared_root);

  ASSERT_TRUE(registry->collection().has<ds_mx::SharedDoubleParam>("shared_limits"));
  ds_mx::SharedDoubleParam sharedLimits = registry->collection().get<ds_mx::SharedDoubleParam>("shared_limits");

  EXPECT_TRUE(sharedLimits.isValidStr("1.0"));
  EXPECT_FALSE(sharedLimits.isValidStr("-0.1"));
  EXPECT_FALSE(sharedLimits.isValidStr("2.1"));

  EXPECT_TRUE(sharedLimits.isValid(1.0));
  EXPECT_FALSE(sharedLimits.isValid(-0.1));
  EXPECT_FALSE(sharedLimits.isValid(2.1));
}

TEST_F(MxParamSharedParserTest, basicShared) {
  registry->loadJson(shared_root);

  ds_mx::BoolParam test_shared_bool(collection, "test_shared_bool", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::BoolParam test_bool(collection, "test_bool_false", ds_mx::ParameterFlag::DYNAMIC);

  ds_mx::IntParam test_shared_int(collection, "test_shared_int", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::IntParam test_int(collection, "test_int_zero", ds_mx::ParameterFlag::DYNAMIC);

  ds_mx::DoubleParam test_shared_double(collection, "test_shared_double", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::DoubleParam test_double(collection, "test_double_two", ds_mx::ParameterFlag::DYNAMIC);

  ds_mx::StringParam test_shared_string(collection, "test_shared_string", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::StringParam test_string(collection, "test_string_text", ds_mx::ParameterFlag::DYNAMIC);

  ds_mx::GeoPointParam test_shared_point(collection, "test_shared_point", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::GeoPointParam test_point(collection, "test_point", ds_mx::ParameterFlag::DYNAMIC);

  ds_mx::KeyValueListParam test_shared_list(collection, "test_shared_list", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::KeyValueListParam test_list(collection, "test_list", ds_mx::ParameterFlag::DYNAMIC);

  ds_mx::GeoPointListParam test_shared_gpointlist(collection, "test_shared_gpointlist", ds_mx::ParameterFlag::DYNAMIC);
  ds_mx::GeoPointListParam test_gpointlist(collection, "test_gpointlist", ds_mx::ParameterFlag::DYNAMIC);

  for (ds_mx::Parameter::Ptr param : *collection) {
    ASSERT_TRUE(json_root.isMember(param->getName()));
    const Json::Value& member = json_root[param->getName()];
    registry->loadOrLink(param, member);
  }

  // check we got the correct result
  EXPECT_EQ(true, test_shared_bool.get());
  EXPECT_EQ(false, test_bool.get());

  EXPECT_EQ(1, test_shared_int.get());
  EXPECT_EQ(0, test_int.get());

  EXPECT_EQ(2.0, test_shared_double.get());
  EXPECT_EQ(2.0, test_double.get());

  EXPECT_EQ("shared text", test_shared_string.get());
  EXPECT_EQ("Test 1 2 3", test_string.get());

  EXPECT_EQ(ds_mx::GeoPoint(30.0, 40.0), test_shared_point.get());
  EXPECT_EQ(ds_mx::GeoPoint(1.0, 2.0), test_point.get());

  EXPECT_EQ(ds_mx::GeoPointList(std::vector<ds_mx::GeoPoint>{ds_mx::GeoPoint(2.5,6), ds_mx::GeoPoint(2,11)}), test_shared_gpointlist.get());
  EXPECT_EQ(ds_mx::GeoPointList(std::vector<ds_mx::GeoPoint>{ds_mx::GeoPoint(1.22,5.45), ds_mx::GeoPoint(7.21,12)}), test_gpointlist.get());

  EXPECT_EQ(ds_mx::KeyValueList({
    std::make_pair("param1", "yet another str"),
    std::make_pair("param2", "test test")
                                }), test_shared_list.get());
  EXPECT_EQ(ds_mx::KeyValueList({
                                    std::make_pair("param1", "some str"),
                                    std::make_pair("param2", "another thing")
                                }), test_list.get());

  // now change stuff
  registry->collection().get<ds_mx::SharedBoolParam>("shared_bool_true").set(false);
  registry->collection().get<ds_mx::SharedIntParam>("shared_int_one").set(10);
  registry->collection().get<ds_mx::SharedDoubleParam>("shared_double_two").set(100.0);
  registry->collection().get<ds_mx::SharedStringParam>("shared_string_text").set("Different Text!");
  registry->collection().get<ds_mx::SharedGeoPointParam>("shared_point").set(ds_mx::GeoPoint(5.0,6.0));
  registry->collection().get<ds_mx::SharedKeyValueListParam>("shared_list").set(ds_mx::KeyValueList(std::make_pair("newparam", "34")));

  // check we got the correct result (again)
  EXPECT_EQ(false, test_shared_bool.get());
  EXPECT_EQ(false, test_bool.get());
  EXPECT_EQ("#shared_bool_true", test_shared_bool.saveJson().asString());

  EXPECT_EQ(10, test_shared_int.get());
  EXPECT_EQ(0, test_int.get());
  EXPECT_EQ("#shared_int_one", test_shared_int.saveJson().asString());

  EXPECT_EQ(100.0, test_shared_double.get());
  EXPECT_EQ(2.0, test_double.get());
  EXPECT_EQ("#shared_double_two", test_shared_double.saveJson().asString());

  EXPECT_EQ("Different Text!", test_shared_string.get());
  EXPECT_EQ("Test 1 2 3", test_string.get());
  EXPECT_EQ("#shared_string_text", test_shared_string.saveJson().asString());

  EXPECT_EQ(ds_mx::GeoPoint(5.0, 6.0), test_shared_point.get());
  EXPECT_EQ(ds_mx::GeoPoint(1.0, 2.0), test_point.get());
  EXPECT_EQ("#shared_point", test_shared_point.saveJson().asString());

  EXPECT_EQ(ds_mx::KeyValueList(
                                    std::make_pair("newparam", "34")
                                ), test_shared_list.get());
  EXPECT_EQ(ds_mx::KeyValueList({
                                    std::make_pair("param1", "some str"),
                                    std::make_pair("param2", "another thing")
                                }), test_list.get());
  EXPECT_EQ("#shared_list", test_shared_list.saveJson().asString());
}
