/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/20.
//


#include <gtest/gtest.h>

#include <ds_mxcore/test/ds_mock_action_server.h>
#include <ds_mxcore/ds_mx_action_duration_primitive.h>

// we'll build this test using Idle actions as an example; however, the point of the test is NOT
// to test the Idle test-- it's to test the underlying Action primitive
#include <ds_mxcore/TestPrimitiveAction.h>

class ActionDurationTestPrimitive : public ds_mx::ActionDurationPrimitive<ds_mxcore::TestPrimitiveAction, ActionDurationTestPrimitive> {
 public:
  ActionDurationTestPrimitive() : ActionDurationPrimitive<ds_mxcore::TestPrimitiveAction, ActionDurationTestPrimitive>("action_test"),
      value(params, "value", 0, ds_mx::ParameterFlag::DYNAMIC) {
  }
  virtual ~ActionDurationTestPrimitive() = default;

  bool validate() const override { return true; }

  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay &display) override { /* do nothing */ }

  ds_mxcore::TestPrimitiveGoal toMsg() const {
    ds_mxcore::TestPrimitiveGoal ret;
    ret.value = value.get();
    ret.finish_at = getEndTime();
    return ret;
  }

  ds_mx::IntParam value;

  // accessors
  ds_mx::TaskReturnCode next_rc() const { return next_return_code_; }
};

class ActionDurationPrimitiveRostest : public ds_mx::MockActionServer<ds_mxcore::TestPrimitiveAction, ActionDurationTestPrimitive> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "mxcore_test",
  "duration": 8,
  "timeout_padding": 6,
  "value" : 0
})";
  }
};

// the goal of these tests is to validate timeout stuff, NOT to duplicate the stuff in ActionTest

TEST_F(ActionDurationPrimitiveRostest, TestSuccess) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  EXPECT_DOUBLE_EQ(8.0, underTest->getParameters().get<ds_mx::DoubleParam>("duration").get());
  EXPECT_DOUBLE_EQ(6.0, underTest->getParameters().get<ds_mx::DoubleParam>("timeout_padding").get());

  // start call on the on-state method, check what we got
  state.header.stamp = ros::Time::now();
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    // verify both timeouts set correctly
    EXPECT_DOUBLE_EQ(8.0, (last_goal->finish_at - state.header.stamp).toSec());
    EXPECT_DOUBLE_EQ(14.0, (underTest->timeoutTime() - state.header.stamp).toSec());
  }

  // none of this should break anythign
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  underTest->onStop(state);
}

TEST_F(ActionDurationPrimitiveRostest, TestSuccessRestart) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
  }

  // start call on the on-state method, check what we got
  ros::Time orig_time = ros::Time::now();
  state.header.stamp = orig_time;
  underTest->onStart(state);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(1, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_fail);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    // verify both timeouts set correctly
    EXPECT_DOUBLE_EQ(8.0, (last_goal->finish_at - orig_time).toSec());
    EXPECT_DOUBLE_EQ(14.0, (underTest->timeoutTime() - orig_time).toSec());
  }

  // signify that we'd like to be done
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    test_done = true;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();

  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

 // call on-stop.  This shouldn't break anything
  underTest->onStop(state);

  // reset the done flag
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_done = false;
  }

  // /////////////////////////////////////////////////////////////////////////////
  // start all over again, beginning with:
  // start call on the on-state method, check what we got
  ros::Time new_time = orig_time + ros::Duration(100);
  state.header.stamp = new_time;
  underTest->onStart(state);
  ros::Duration(0.1).sleep();
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(2, callback_started);
    EXPECT_EQ(1, callback_finished);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(2, test_counter);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    // verify both timeouts set correctly
    EXPECT_DOUBLE_EQ(8.0, (last_goal->finish_at - new_time).toSec());
    EXPECT_DOUBLE_EQ(14.0, (underTest->timeoutTime() - new_time).toSec());
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

TEST_F(ActionDurationPrimitiveRostest, TestChangeParam) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_FALSE(static_cast<bool>(last_goal));
  }

  // start call on the on-state method, check what we got
  ros::Time orig_time = ros::Time::now();
  state.header.stamp = orig_time;
  underTest->onStart(state);
  ros::Duration(0.1).sleep();

  // change the state of the server
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    // verify both timeouts set correctly
    EXPECT_DOUBLE_EQ(8.0, (last_goal->finish_at - orig_time).toSec());
    EXPECT_DOUBLE_EQ(14.0, (underTest->timeoutTime() - orig_time).toSec());
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
  EXPECT_EQ(1, underTest->distanceToGo());
  EXPECT_EQ(1, underTest->timeToGo());

  // change a parameter
  underTest->getParameters().get<ds_mx::DoubleParam>("duration").set(17.0);

  state.header.stamp += ros::Duration(5.0); // for this test to be real, the change must happen
                                               // AFTER we started

  {
    // do ALL this inside a lock to verify that the uninitialized values are correct
    // -- otherwise test occasionally fails when they get overwritten
    std::unique_lock<std::mutex> lock(test_mutex);
    // parameter change won't take effect until we tick
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    // values will only get updated during this ste
    test_counter++;

    // MX timeout updated by this step, but we'll have to do a notify/wait before
    // checking if the changes propagated to the actionserver
    EXPECT_DOUBLE_EQ(23.0, (underTest->timeoutTime() - orig_time).toSec());
  }
  test_cv.notify_all();
  ros::Duration(0.3).sleep();

  {
    std::unique_lock<std::mutex> lock(test_mutex);
    ASSERT_TRUE(static_cast<bool>(last_goal));
    // verify both timeouts set correctly-- relative to ORIGINAL time, not time
    // when change occurred
    EXPECT_DOUBLE_EQ(17.0, (last_goal->finish_at - orig_time).toSec());
    EXPECT_DOUBLE_EQ(23.0, (underTest->timeoutTime() - orig_time).toSec());
  }

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}


TEST_F(ActionDurationPrimitiveRostest, TestChangeParamBefore) {

  // start by checking our initial assumptions
  {
    std::unique_lock<std::mutex> lock(test_mutex);
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
    EXPECT_EQ(0, test_done);
    EXPECT_EQ(0, test_fail);
    EXPECT_EQ(0, test_counter);
    ASSERT_FALSE(static_cast<bool>(last_goal));
  }

  underTest->getParameters().get<ds_mx::DoubleParam>("duration").set(17.0);

  {
    std::unique_lock<std::mutex> lock(test_mutex);
    test_counter++;
  }
  test_cv.notify_all();
  ros::Duration(0.1).sleep();
  {
    // make sure nothing goes out
    std::unique_lock<std::mutex> lock(test_mutex);
    ASSERT_FALSE(static_cast<bool>(last_goal));
    EXPECT_EQ(0, callback_started);
    EXPECT_EQ(0, callback_finished);
  }

  ros::Time orig_time = ros::Time::now();
  state.header.stamp = orig_time;
  // start call on the on-state method, check what we got
  underTest->onStart(state);
  ros::Duration(0.1).sleep();

  {
    // do ALL this inside a lock to verify that the uninitialized values are correct
    // -- otherwise test occasionally fails when they get overwritten
    std::unique_lock<std::mutex> lock(test_mutex);
    // parameter change won't take effect until we tick
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));
    // these values get reset by the primitive while waiting for the first response
    test_counter++;
    ASSERT_TRUE(static_cast<bool>(last_goal));
    // verify both timeouts set correctly-- relative to ORIGINAL time, not time
    // when change occurred
    EXPECT_EQ(17.0, (last_goal->finish_at - orig_time).toSec());
    EXPECT_EQ(23.0, (underTest->timeoutTime() - orig_time).toSec());
  }
  test_cv.notify_all();
  ros::Duration(0.15).sleep();

  // call on-stop.  This shouldn't break anything
  underTest->onStop(state);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_actiondurationprimitive");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}
