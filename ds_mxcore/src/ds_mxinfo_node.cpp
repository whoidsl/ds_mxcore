/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/29/18.
//
//
// This is a little test program to list available classes for tasks

#include <iostream>
#include <string>
#include <algorithm>

#include <pluginlib/class_loader.h>
#include "ds_mxcore/ds_mxtask.h"
#include "ds_mxcore/ds_mxcompiler.h"
#include "ds_mxcore/ds_mxeventlog.h"

#include <boost/program_options.hpp>
#include <ros/time.h>

namespace po = boost::program_options;

int plugin_main(po::variables_map& vm, const std::shared_ptr<ds_mx::MxCompiler>& compiler) {
  if (vm.count("help")) {
    std::cout <<"\n";
    std::cout <<"The \"plugins\" command checks your MX install for all available\n";
    std::cout <<"TASKs and MACROs and prints them out so you can debug your install." <<std::endl;
    return -1;
  }

  std::cout <<"\n\n";
  std::cout <<"Listing available plugins for TASKS...\n";
  std::cout <<"\n\n";

  pluginlib::ClassLoader<ds_mx::MxTask> mxtask_loader("ds_mxcore", "ds_mx::MxTask");
  std::vector<std::string> task_classes = mxtask_loader.getDeclaredClasses();

  std::cout <<"\n";
  std::cout <<"TASKS:\n";
  std::cout <<"\t" <<std::setw(15) <<std::left <<"Key";
  std::cout <<"\t" <<std::setw(15) <<std::left <<"Package";
  std::cout <<"\t" <<std::setw(40) <<std::left <<"Classname";
  std::cout <<"\t" <<std::setw(40) <<std::left <<"Description" <<"\n";

  for (const auto &c : task_classes) {
    std::cout <<"\t" <<std::setw(15) <<std::left <<c;
    std::cout <<"\t" <<std::setw(15) <<std::left <<mxtask_loader.getClassPackage(c);
    std::cout <<"\t" <<std::setw(40) <<std::left <<mxtask_loader.getClassType(c);
    std::cout <<"\t" <<std::setw(40) <<std::left <<mxtask_loader.getClassDescription(c) <<"\n";
  }
  std::cout <<std::endl;

  std::cout <<"\n";
  std::cout <<"MACROS:\n";
  std::cout <<"\t** NOT IMPLEMENTED **\n";
  std::cout <<std::endl;

  return 0;
}

std::shared_ptr<ds_mx::MxRootTask> load_tasktree(po::variables_map& vm,
                                                 const std::shared_ptr<ds_mx::MxCompiler>& compiler) {

  boost::filesystem::path missionfile;

  // TODO: Load macros and stuff
  std::shared_ptr<ds_mx::MxRootTask> ret;

  if (!vm.count("missionargs")) {
    std::cerr <<"\tMUST specify a mission file!" <<std::endl;
    return ret;
  } else {
    missionfile = boost::filesystem::path(vm["missionargs"].as<std::string>());
    std::cerr <<"Loading mission file " <<missionfile <<std::endl;
    if (! boost::filesystem::exists(missionfile)) {
      std::cerr <<"Mission file " <<missionfile <<" does not exist!" <<std::endl;
      return ret;
    }
  }

  Json::Value json_root = ds_mx::parseFile(missionfile);
  ret = compiler->compile(json_root);

  return ret;
}

int dot_main(po::variables_map& vm, const std::shared_ptr<ds_mx::MxCompiler>& compiler) {
  if (vm.count("help")) {
    std::cout <<"\n";
    std::cout <<"The \"dot\" command builds a TaskTree for your mission and has\n";
    std::cout <<"it output a dot file ready to convert into graphviz.\n";
    return -1;
  }

  // avoid any ros console output while creating the task tree
  ros::console::shutdown();  

  // build the task tree
  auto root = load_tasktree(vm, compiler);
  if (!root || !compiler) {
    return -1;
  }

  // convert to dot
  std::cout <<root->toDot() <<std::endl;

  return 0;
}

int json_main(po::variables_map& vm, const std::shared_ptr<ds_mx::MxCompiler>& compiler) {
  if (vm.count("help")) {
    std::cout <<"\n";
    std::cout <<"The \"json\" command builds a TaskTree for your mission and has\n";
    std::cout <<"it output a json file again so you can see if JSON generation works\n";
    return -1;
  }

  // avoid any ros console output while creating the JSON
  ros::console::shutdown();

  // build the task tree
  auto root = load_tasktree(vm, compiler);
  if (!root || !compiler) {
    return -1;
  }

  // convert to dot
  Json::Value mission_json = compiler->save(root);
  std::cout <<mission_json <<std::endl;

  return 0;
}

int validate_main(po::variables_map& vm, const std::shared_ptr<ds_mx::MxCompiler>& compiler) {
  if (vm.count("help")) {
    std::cout <<"\n";
    std::cout <<"The \"validate\" command builds a TaskTree for your mission and\n";
    std::cout <<"runs the static validator on it\n";
    return -1;
  }

  console_bridge::setLogLevel(console_bridge::LogLevel::CONSOLE_BRIDGE_LOG_DEBUG);
  if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
    ros::console::notifyLoggerLevelsChanged();
  }

  // build the task tree
  auto root = load_tasktree(vm, compiler);
  if (!root || !compiler) {
    return -1;
  }

  if (root->validate()) {
    std::cout <<"Mission is VALID." <<std::endl;
    return 0;
  }

  std::cout <<"Mission is INVALID-- see above output" <<std::endl;
  return -1;
}

int main (int argc, char* argv[]) {


  // initialize rostime to spare us the annoyances of
  // any call to ros::Time::now() in an init function throwing
  // an error
  ros::Time::init();

  // disable mxeventlog
  ds_mx::eventlog_disable();

  po::options_description desc("MX Info options");
  desc.add_options()
      ("help", "Produce help message")
      ("command", po::value<std::string>(), "Command to run; [plugin|dot|json]")
      ("missionargs", "Mission file arguments (if necessary)")
  ;

  po::positional_options_description p;
  p.add("command", 1);
  p.add("missionargs", -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::shared_ptr<ds_mx::MxCompiler> compiler = ds_mx::MxCompiler::create();

  if (vm.count("command")) {
    std::string command = vm["command"].as<std::string>();
    std::transform(command.begin(), command.end(), command.begin(), ::tolower);

    if (command == "plugins") {
      return plugin_main(vm, compiler);
    } else if (command == "validate") {
      return validate_main(vm, compiler);
    } else if (command == "dot") {
      return dot_main(vm, compiler);
    } else if (command == "json") {
      return json_main(vm, compiler);
    } else {
      std::cerr <<"Unknown command \"" <<command <<"\"";
    }
  } else {
    std::cerr << "MUST Specify a command!  Valid commands are:" << std::endl;
    std::cerr <<"plugins: List available plugins" <<std::endl;
    std::cerr <<"validate: Validate the mission" <<std::endl;
    std::cerr <<"dot: Create dotfile for use with graphviz" <<std::endl;
    std::cerr <<"json: Add UUIDs and convert mission file to JSON" <<std::endl;
  }

  std::cerr <<"Unable to carry out your request!" <<std::endl;
  return -1;
}
