/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/19.
//

#include "ds_mxcore/test/ds_mock_compiler.h"

namespace ds_mx {

MxMockCompiler::MxMockCompiler() {
  // do nothing
}

std::shared_ptr<MxMockCompiler> MxMockCompiler::create() {
  return std::shared_ptr<MxMockCompiler>(new MxMockCompiler());
}

void MxMockCompiler::addSubtask(const std::string &token, const std::shared_ptr<ds_mx::MxTask> &subtask) {
  auto iter = subtaskRegistry.find(token);
  if (iter == subtaskRegistry.end()) {
    subtaskRegistry[token] = subtask;
  } else {
    throw MxMockException("Mock Compiler already has subtask " + token);
  }
}

std::shared_ptr<MxTask> MxMockCompiler::loadTask(const std::string& name, const Json::Value &ast) {

  auto iter = subtaskRegistry.find(ast.asString());
  if (iter == subtaskRegistry.end()) {
    throw MxMockException("Mock compiler has no subtask named \"" + ast.asString()
    + "\" required for subtask named " + name);
  }

  std::shared_ptr<MxTask> ret = iter->second;
  if (ret) {
    ret->init(ast, shared_from_this());
    ret->setName(name);
    ret->setType("mock");
  }
  return ret;
};

} // namespace ds_mx