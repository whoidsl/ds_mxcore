/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/29/18.
//

#include "ds_mxcore/ds_mxtask.h"

#include <ds_mxcore/ds_mxcompiler.h>

#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <ds_mxcore/ds_mxeventlog.h>
#include <cxxabi.h>

namespace ds_mx {

// ///////////////////////////////////////////////////////////////////////// //
// MxTask -- The Base Class to Rule Them All
// ///////////////////////////////////////////////////////////////////////// //
MxTask::MxTask() : params(new ds_mx::ParameterCollection()), max_timeout_sec(params, "max_timeout", 0.0, ParameterFlag::DYNAMIC | ParameterFlag::OPTIONAL) {
  uuid = boost::uuids::random_generator()();
  next_tick_return_code = ds_mx::TaskReturnCode::RUNNING;
  timeout_code = ds_mx::TaskReturnCode::FAILED;
  running = false;
}

MxTask::~MxTask() = default;

/// \brief Get the name of this Task node
const std::string &MxTask::getName() const {
  return name;
};

/// \brief Set the name of this Task node
void MxTask::setName(const std::string &_n) {
  name = _n;
};

/// \brief Get the type ID string used when creating this object
const std::string& MxTask::getType() const {
  return type;
}

/// \brief Set the type ID string used to create this object
void MxTask::setType(const std::string& _n) {
  type = _n;
}

bool MxTask::isRunning() const {
  return running;
}

Json::Value MxTask::toJson() {
  Json::Value obj(Json::ValueType::objectValue);

  // overhead is just the type field, mostly
  obj["type"] = Json::Value(type);
  obj["uuid"] = Json::Value(to_string(uuid));

  // Name?  Methinks not, but still
  // we could add a tag though!

  return obj;
}

/// \brief Get access to this tasks's parameters
const ds_mx::ParameterCollection &MxTask::getParameters() const {
  return *params;
};

/// \brief Get access to this tasks's parameters
ds_mx::ParameterCollection &MxTask::getParameters() {
  return *params;
};

const std::shared_ptr<ds_mx::ParameterCollection>& MxTask::getParametersPtr() const {
  return params;
}

const ds_mx::EventHandlerCollection& MxTask::getEvents() const {
  return events;
}

ds_mx::EventHandlerCollection& MxTask::getEvents() {
  return events;
}

/// Load all the parameters for this task from a JSON file
/// Parameters are loaded in the order they are specified in the task definition.
/// Parameters are converted to the type specified by the
/// task definition.  This may produce a runtime_error if jsoncpp is unable to
/// convert the JSON text to the correct type, or an invalid_argument exception
/// if the
///
/// \throws runtime_error If jsoncpp is unable to convert a type
/// \throws invalid_argument If the passed config isn't a JSON object
/// \param config The config file to load from
/// \param compiler The Task Compiler to use to perform name lookups, etc
void MxTask::loadParameters(const Json::Value &config, MxCompilerPtr compiler) {
  if (!config.isObject()) {
    throw std::invalid_argument("JSON block for task " + name + " is not an object!");
  }

  for (ds_mx::Parameter::Ptr param : *params) {
    // handle no the case of no JSON value
    if (!config.isMember(param->getName())) {
      // optional parameters use default
      if (param->isOptional()) {
        continue;
      }
      throw std::invalid_argument("Task \"" + type + "\" / \""
      + name + "\" is missing parameter " + param->getName());
    }

    // actually load
    const Json::Value& member = config[param->getName()];
    compiler->getSharedParams()->loadOrLink(param, member);
  }

  // read in a UUID, if there is one
  if (config.isMember("uuid")) {
    boost::uuids::string_generator gen;
    uuid = gen(config["uuid"].asString());
  }
}

/// Save this tasks's parameters as a JSON object.
/// This function is written to make it easy to add parameters to an existing
/// Json::Value object.  For example:
///
/// Json::Value obj(Json::ValueType::objectValue);
/// saveParameters(obj);
///
/// \throws invalid_argument The provided output object must be a JSON Object,
/// or an invalid_argument excpetion is throw.
/// \param output The Json::Value to add our parameters to
void MxTask::saveParameters(Json::Value &output) {
  if (!output.isObject()) {
    throw std::invalid_argument("Attempting to save JSON for parameters of block " + name
    + ", but provided Json::Value is not an object!");
  }

  for (ds_mx::Parameter::Ptr param : *params) {
    output[param->getName()] = param->saveJson();
  }
}

ds_mx_msgs::MxTaskStatus MxTask::getStatus() const {
  ds_mx_msgs::MxTaskStatus ret;

  ret.name = name;
  ret.type = type;
  ret.timeout = timeout;
  if (timeout.isValid() && !timeout.isZero()) {
    ret.timeout_left = timeout - ros::Time::now();
  } else {
    ret.timeout_left = ros::Duration();
  }
  std::copy(uuid.begin(), uuid.end(), std::begin(ret.uuid));

  return ret;
}

bool MxTask::isTimeoutExpired(const ros::Time& now) const {
  if (timeout.isValid() && !timeout.isZero() && now >= timeout) {
      ROS_WARN("Task %s has expired!", name.c_str());
      return true;
  }
  return false;
}

void MxTask::onStart(const ds_nav_msgs::NavState& state) {

  // before starting, check to see if we should invoke the parameters changed
  if (params->anyChanged()) {
    parameterChanged(state);
    params->resetChanged();
  }

  // actually start
  MX_LOG_EVENT(this, MX_LOG_TASK_START);
  next_tick_return_code = ds_mx::TaskReturnCode::RUNNING;
  resetTimeout(state.header.stamp);
  running = true;
}

void MxTask::onStop(const ds_nav_msgs::NavState& state) {
  MX_LOG_EVENT(this, MX_LOG_TASK_STOP);
  running = false;
}

void MxTask::parameterChanged(const ds_nav_msgs::NavState &state) {
  MX_LOG_EVENT(this, MX_LOG_PARAM_CHANGED);
}

void MxTask::resetTimeout(const ros::Time& now) {
  if (max_timeout_sec.get() > 0) {
    timeout = now + ros::Duration(max_timeout_sec.get());
  } else {
    timeout = ros::Time();
  }
}

ds_mx::TaskReturnCode MxTask::nextTickReturn() const {
  return next_tick_return_code;
}

ds_mx::TaskReturnCode MxTask::timeoutCode() const {
  return timeout_code;
}

const ros::Time& MxTask::timeoutTime() const {
  return timeout;
}

/// Get a unique node name for each task based on its UUID
/// \return A unique UUID-based name for a node
std::string MxTask::getDotId() const {
  std::string uuidstr = to_string(uuid);
  std::replace(uuidstr.begin(), uuidstr.end(), '-', '_');
  return "n" + uuidstr;
}

/// Get the globally unique UUID for this Task
/// \return The UUID for this task
const boost::uuids::uuid& MxTask::getUuid() const {
  return uuid;
}

/// Reset any internal Task state
void MxTask::resetState() {
  // do nothing
}

bool MxTask::subtreeContains(const boost::uuids::uuid &query_uuid) const {
  // The simple task can check only itself
  return uuid == query_uuid;
}


}