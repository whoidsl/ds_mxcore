/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/3/19.
//

#include <json/json.h>
#include <sstream>
#include <ds_mxcore/ds_mxexceptions.h>
#include "ds_mxcore/ds_mxparam_keyvaluelist.h"

namespace ds_mx {

KeyValueList::KeyValueList() {}

KeyValueList::KeyValueList(const std::pair<std::string, std::string>& element) {
  dict.insert(element);
}

KeyValueList::KeyValueList(const std::vector<std::pair<std::string, std::string>>& elements) {
  for (const auto& e : elements) {
    dict.insert(e);
  }
}
const std::string& KeyValueList::operator[](std::string key) const {
  return dict.at(key);
}

std::string& KeyValueList::operator[](std::string key) {
  return dict[key];
}

bool KeyValueList::operator >= (const KeyValueList& other) const {
  return dict >= other.dict;
}

bool KeyValueList::operator <= (const KeyValueList& other) const {
  return dict <= other.dict;
}

bool KeyValueList::operator != (const KeyValueList& other) const {
  // std::map defines inequality and equality as a pair-wise comparison, as expected
  return dict != other.dict;
}

bool KeyValueList::operator == (const KeyValueList& other) const {
  return dict == other.dict;
}

KeyValueList KeyValueList::FromString(const std::string& s) {
  Json::Value json;
  std::stringstream sstream(s);
  try {
    sstream >> json;
  } catch (Json::Exception) {
    throw ds_mx::ParameterTypeError("Unable to convert string JSON object to KeyListValue");
  }

  return FromJson(json);
}

Json::Value KeyValueList::toJson() const {
  Json::Value ret;

  for (auto iter : dict) {
    ret[iter.first] = iter.second;
  }

  return ret;
}

KeyValueList KeyValueList::FromJson(const Json::Value& json) {
  KeyValueList ret;
  if (json.isObject()) {
    for (auto itr = json.begin(); itr != json.end(); itr++) {
      // there's really no way to sanity check this safely without knowing
      // what to expect
      ret.dict[itr.name()] = itr->asString();
    }
  } else {
    throw ds_mx::ParameterTypeError("Unable to convert string JSON object to KeyListValue");
  }

  return ret;
}

std::ostream & operator << (std::ostream &out, const KeyValueList& p) {
  Json::Value json = p.toJson();
  out << json;
  return out;
}

} // namespace ds_mx