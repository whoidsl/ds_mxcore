/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/16/19.
//

#include "ds_mxcore/ds_mxhitcounter.h"

namespace ds_mx {

HitCounter::HitCounter(std::shared_ptr<ds_mx::ParameterCollection> &col, const std::string &_name)
    : HitCounter(col, _name, 5, 1, -1, 0, 0) {}

HitCounter::HitCounter(std::shared_ptr<ds_mx::ParameterCollection> &col, const std::string &_name, int _threshold)
    : HitCounter(col, _name, _threshold, 1, -1, 0, 0) {}

HitCounter::HitCounter(std::shared_ptr<ds_mx::ParameterCollection> &col, const std::string& _name,
                       int _threshold, int _good_increment, int _bad_increment, int _decay_increment, int _initial):
    hits(_initial), name(_name),
    hit_threshold(col, name + "_hit_threshold", _threshold, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    good_increment(col, name + "_good_increment", _good_increment, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    bad_increment(col, name + "_bad_increment", _bad_increment, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    decay_increment(col, name + "_decay_increment", _decay_increment, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL),
    initial_count(col, name + "_initial_count", _initial, ds_mx::ParameterFlag::DYNAMIC | ds_mx::ParameterFlag::OPTIONAL)
{ /* do nothing */ }

/// \brief Reset the counter to an initial state
void HitCounter::reset() {
  hits = initial_count.get();
}

/// \brief The externally-run test has passed a threshold;
/// increment the counter using the "good_increment" value, clamping to hits >= 0
void HitCounter::goodHit() {
  hits += good_increment.get();
  if (hits < 0) {
    hits = 0;
  }
}

/// \brief The externally-run test did not pass the threshold;
/// increment the counter using the "bad_increment" value, clamping to hits >= 0
void HitCounter::badHit() {
  hits += bad_increment.get();
  if (hits < 0) {
    hits = 0;
  }
}

/// \brief Data is missing, so the test could neither succeed nor fail.
/// increment by the value of decay_increment, clamping to hits >= 0
void HitCounter::doDecay() {
  hits += decay_increment.get();
  if (hits < 0) {
    hits = 0;
  }
}

/// \brief Test if the hit counter has passed its threshold
bool HitCounter::triggered() const {
  return hits >= hit_threshold.get();
}

/// \brief Test if the hit counter has passed its threshold
/// You can use this as:
/// if (hit_counter) { /* counter > threshold */ } else { /* it isn't */ }
HitCounter::operator bool() const {
  return triggered();
}

/// \brief Get the name assigned to this counter
const std::string& HitCounter::getName() const {
  return name;
}

/// \brief Return the current value of the hit counter.
/// This isn't recommended for use in actual tasks, as tasks shouldn't change their behavior
/// until the hit counter is actully triggered.  That's the whole point!  But its
/// invaluable for testing, so here it is.
int HitCounter::getHits() const {
  return hits;
}

/// \brief Get the current hit threshold.
/// Not recommended for real tasks, but useful for testing and debug messages
int HitCounter::getHitThreshold() const {
  return hit_threshold.get();
}

/// \brief Get the "good increment" value
/// Not recommended for real tasks, but useful for testing and debug messages
int HitCounter::getGoodIncrement() const {
  return good_increment.get();
}

/// \brief Get the "bad increment" value
/// Not recommended for real tasks, but useful for testing and debug messages
int HitCounter::getBadIncrement() const {
  return bad_increment.get();
}

/// \brief Get the "decay increment" value
/// Not recommended for real tasks, but useful for testing and debug messages
int HitCounter::getDecayIncrement() const {
  return decay_increment.get();
}

/// \brief Get the "initial count" value applied at reset
/// Not recommended for real tasks, but useful for testing and debug messages
int HitCounter::getInitialCount() const {
  return initial_count.get();
}

} // namespace ds_mx