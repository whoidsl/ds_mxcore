/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 4/23/19.
//

#ifndef DS_MXPARAMETER_SHAREDPRIVATE_H
#define DS_MXPARAMETER_SHAREDPRIVATE_H

#include <ds_mxcore/ds_mxparameter_shared.h>
#include <ds_mxcore/ds_mxparameter.h>
#include "ds_mxparameterPrivate.h"

#include <boost/optional.hpp>

namespace ds_mx {

template <typename T>
class SharedParameterPrivateT : public ParameterPrivateT<T> {
 private:
  SharedParameterPrivateT(const SharedParameterPrivateT<T>&) = delete;
  SharedParameterPrivateT<T>& operator=(const SharedParameterPrivateT<T>&) = delete;


 public:

  boost::optional<T> bound_lower;
  boost::optional<T> bound_upper;

  using ParameterPrivateT<T>::shared_from_this;

  SharedParameterPrivateT(const std::string& name, const std::shared_ptr<ParameterCollection>& col, ParameterFlag stat,
      const T& defaultValue)
      : ParameterPrivateT<T>(name, col, stat, defaultValue) {
    // do nothing
  }

  bool isValid(const T& test) const {
    return ( (!bound_lower || test >= *bound_lower)
      && (!bound_upper || test <= *bound_upper) );
  }

  typename SharedParameterT<T>::Ptr getView() {
    typename SharedParameterT<T>::Ptr ret(new SharedParameterT<T>(shared_this()));
    return ret;
  }

  // Even though this function is IDENTICAL to the one in the superclass,
  // get View is STATICALLY bound-- we need a DYNAMICALLY-bounded version to
  // get the correct underlying view type, and can't dynamically-bind the
  // getView version.  It's all very annoying.  Yay C++?
  Parameter::Ptr getGenericView() override {
    return getView();
  }

 private:
  std::shared_ptr<SharedParameterPrivateT<T> > shared_this() {
    // this is a static downcast, which is generally not a great idea.  However, can be pretty certain
    // that this points to a class of this object's type.
    return std::static_pointer_cast<SharedParameterPrivateT<T> >(shared_from_this());
  }
  std::shared_ptr<const SharedParameterPrivateT<T> > shared_this() const {
    return std::static_pointer_cast<const SharedParameterPrivateT<T> >(shared_from_this());
  }
};

}

#endif //DS_MXPARAMETER_SHAREDPRIVATE_H
