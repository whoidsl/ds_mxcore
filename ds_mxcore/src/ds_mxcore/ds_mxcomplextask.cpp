/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//

#include <ds_mxcore/ds_mxcompiler.h>

#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <ds_mxcore/ds_mxeventlog.h>

#include "ds_mxcore/ds_mxcomplextask.h"

namespace ds_mx {

MxComplexTask::~MxComplexTask() = default;

// ///////////////////////////////////////////////////////////////////////// //
// MxComplexTask -- Common task with subtasks
// ///////////////////////////////////////////////////////////////////////// //
Json::Value  MxComplexTask::toJson() {
  Json::Value obj = MxTask::toJson();

  saveParameters(obj);
  events.saveJson(obj);
  for (std::shared_ptr<MxTask>& subtask : subtasks) {
    obj[subtask->getName()] = subtask->toJson();
  }

  return obj;
}

void MxComplexTask::init(const Json::Value& config, MxCompilerPtr compiler) {
  loadParameters(config, compiler);
  events.loadJson(config);

  // Individual tasks should override this and load their own subtasks

}

/// By default, complex tasks don't use ROS and simply don't do anything on
/// startup; just initialize all subtask
/// \param nh
void MxComplexTask::init_ros(ros::NodeHandle& nh) {
  for (const auto& subtask : subtasks) {
    subtask->init_ros(nh);
  }
}

ds_mx::TaskReturnCode MxComplexTask::tick(const ds_nav_msgs::NavState& state) {

  // make sure we have an active subtask
  assertActiveSubtask();

  // check to see if the timeout has expired
  if (isTimeoutExpired(state.header.stamp)) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Timeout");
    return timeout_code;
  }

  // check for any parameter changes and respond accordingly
  if (params->anyChanged()) {
    parameterChanged(state);
    params->resetChanged();
  }

  // check for stop-on-next-tick
  if (next_tick_return_code != ds_mx::TaskReturnCode::RUNNING) {
    return next_tick_return_code;
  }

  // tick our subtask
  ds_mx::TaskReturnCode rc = active_subtask->tick(state);

  if (rc == ds_mx::TaskReturnCode::RUNNING) {
    // subtask is still running, so call our
    // own handler and return its result
    return onTick(state);
  }

  return subtaskDone(state, rc);
}

ds_mx::TaskReturnCode MxComplexTask::onTick(const ds_nav_msgs::NavState& state) {
  // by default, just return "keep going"
  return ds_mx::TaskReturnCode::RUNNING;
}

/// Output a dot string for this tasks that references all its subtasks
/// \return A dot string representing this task and all its subtasks
std::string MxComplexTask::toDot() {

  std::stringstream ret;

  // add this node
  ret << getDotId() << " [ shape=ellipse,label=\"" + type + "\" ];\n";

  for (std::shared_ptr<MxTask>& subtask : subtasks) {
    // add child node
    ret <<subtask->toDot();

    // add edge between this node and child node
    ret <<getDotId() <<" -> " <<subtask->getDotId() <<" [ label=\"" + subtask->getName() + "\" ];\n";
  }

  return ret.str();
}

/// Get a reference to the vector of all this tasks's subtasks
/// \return
const std::vector<std::shared_ptr<MxTask> > & MxComplexTask::getSubtasks() const {
  return subtasks;
}

/// Get a reference to the vector of all this tasks's subtasks
/// \return
std::vector<std::shared_ptr<MxTask> >& MxComplexTask::getSubtasks() {
  return subtasks;
}

std::shared_ptr<MxTask> MxComplexTask::loadSubtask(ds_mx::MxCompilerPtr compiler,
                                                   const std::string& name, const Json::Value& config) {

  // create the task
  std::shared_ptr<MxTask> ret = compiler->loadTask(name, config);

  // save a copy
  subtasks.push_back(ret);

  // actually return
  return ret;
}

void MxComplexTask::startSubtask(const ds_nav_msgs::NavState& state, const std::shared_ptr<MxTask>& newSubtask) {
  // switchSubtasks automatically stops a running subtask; we just complain if one is already running
  if (active_subtask) {
    ROS_ERROR_STREAM("Task \"" << name << "\" already has a running subtask, yet was asked to start."
                               << " Automatically stopping running subtask...");
  }
  switchSubtasks(state, newSubtask);
}

void MxComplexTask::switchSubtasks(const ds_nav_msgs::NavState& state, const std::shared_ptr<MxTask>& newSubtask) {
  if (active_subtask) {
    active_subtask->onStop(state);
  }
  active_subtask = newSubtask;
  if (! active_subtask) {
    ROS_ERROR_STREAM("Task \"" <<name <<"\" was asked to switch to a NULL subtask... will crash next tick");
    return;
  }

  active_subtask->onStart(state);
}

void MxComplexTask::stopActiveSubtask(const ds_nav_msgs::NavState& state) {
  if (active_subtask) {
    active_subtask->onStop(state);
    active_subtask.reset();
  } else {
    ROS_WARN_STREAM("Task \"" <<name <<"\" was asked to stop its currently-active "
                              << "subtask, but no subtask is running.  This is likely real bad.");
  }

}

void MxComplexTask::assertActiveSubtask() const {
  if (! active_subtask ) {
    throw std::domain_error("Calling TICK on a node " + name + " without a valid active subtask!");
  }
}

bool MxComplexTask::validateSubtasks() const {
  for (const auto& subtask : subtasks) {
    if (!subtask->validate()) {
      ROS_ERROR_STREAM("Error when validating subtask " <<subtask->getName());
      return false;
    }
  }
  return true;
}

void MxComplexTask::onStart(const ds_nav_msgs::NavState& state) {
  MxTask::onStart(state);
}

void MxComplexTask::onStop(const ds_nav_msgs::NavState& state) {
  if (active_subtask) {
    active_subtask->onStop(state);
    active_subtask.reset();
  }
  MxTask::onStop(state);
}

void MxComplexTask::updateStatus(ds_mx_msgs::MxMissionStatus &status) const {
  status.active_tasks.push_back(getStatus());
  if (!active_subtask) {
    ROS_ERROR_STREAM("No active subtask in a task asked for its status; tree may be invalid!");
  } else {
    active_subtask->updateStatus(status);
  }
}

void MxComplexTask::handleEvent(const ds_mx_msgs::MxEvent &event) {
  ROS_WARN_STREAM("Task " <<name <<" handling event " <<event.eventid);
  if (events.handleEvent(event)) {
    if (active_subtask) {
      active_subtask->handleEvent(event);
    } else {
      ROS_WARN_STREAM("Event delivered to complex task " << name << " without active subtask!");
    }
  } else {
    // don't propagate this event
  }
}

bool MxComplexTask::subtreeContains(const boost::uuids::uuid& query_uuid) const {
  // DO NOT call base class version, just override

  // check ourself
  if (uuid == query_uuid) {
    return true;
  }

  // check all our children
  for (auto subtask : subtasks) {
    if (subtask->subtreeContains(query_uuid)) {
      return true;
    }
  }

  // haven't found it yet, return false
  return false;
}

} // namespace ds_mx
