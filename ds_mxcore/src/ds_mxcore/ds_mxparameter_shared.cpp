/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 4/23/19.
//

#include "ds_mxparameter_sharedPrivate.h"
#include <ds_mxcore/ds_mxexceptions.h>
#include <ds_mxcore/ds_mx_paramhelper.h>

#include <iostream>
#include <ros/console.h>
#include <rosconsole/macros_generated.h>

namespace ds_mx {

// ///////////////////////////////////////////////////////////////////////// //
// SharedParameterT
// ///////////////////////////////////////////////////////////////////////// //
template<typename T>
SharedParameterT<T>::SharedParameterT(const std::shared_ptr<SharedParameterPrivateT<T> >& _impl) : ParameterT<T>(_impl) {
  // do nothing
}

template<typename T>
SharedParameterT<T>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, const T& defaultValue,
    ParameterFlag isStatic)
    : ParameterT<T>(std::shared_ptr<ParameterPrivateT<T> >(new SharedParameterPrivateT<T>(name, col, isStatic, defaultValue))) {

  // forciby add to collection
  col->addParameter(*this);
}

template<>
SharedParameterT<bool>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
                                      : SharedParameterT<bool>(col, name, false, isStatic) { /* do nothing */ }

template<>
SharedParameterT<int>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
    : SharedParameterT<int>(col, name, 0, isStatic) { /* do nothing */ }

template<>
SharedParameterT<double>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
    : SharedParameterT<double>(col, name, 0.0, isStatic) { /* do nothing */ }

template<>
SharedParameterT<std::string>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
    : SharedParameterT<std::string>(col, name, "", isStatic) { /* do nothing */ }

template<>
SharedParameterT<ds_mx::GeoPoint>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
    : SharedParameterT<ds_mx::GeoPoint>(col, name, ds_mx::GeoPoint(), isStatic) { /* do nothing */ }

template<>
SharedParameterT<ds_mx::KeyValueList>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
    : SharedParameterT<ds_mx::KeyValueList>(col, name, ds_mx::KeyValueList(), isStatic) { /* do nothing */ }

template<>
SharedParameterT<ds_mx::GeoPointList>::SharedParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag isStatic)
    : SharedParameterT<ds_mx::GeoPointList>(col, name, ds_mx::GeoPointList(), isStatic) { /* do nothing */ }

#define PT_D auto d = SharedParameterT<T>::d_func();

template<typename T>
const boost::optional<T> & SharedParameterT<T>::getMin() const {
  PT_D;
  return d->bound_lower;
}

template<typename T>
const boost::optional<T> & SharedParameterT<T>::getMax() const {
  PT_D;
  return d->bound_upper;
}

template<typename T>
bool SharedParameterT<T>::isValid(const T& test) const {
  PT_D;
  return d->isValid(test);
}

template <typename T>
bool SharedParameterT<T>::isValidStr(const std::string& str) const {
  if (ParameterT<T>::canSetFromString(str)) {
    T value = param_helpers::stringToValue<T>(str);
    return isValid(value);
  }
  return false;
}

template<>
bool SharedParameterT<std::string>::isValidStr(const std::string &str) const {
  return SharedParameterT<std::string>::isValid(str);
}

template<typename T>
std::string SharedParameterT<T>::GuiString(Role role) const {
  PT_D;

  switch(role) {
    case RoleValue:
      return ParameterT<T>::GuiString(RoleValue);
    case RoleMin:
      if (d->bound_lower) {
        return param_helpers::valueToString(*(d->bound_lower));
      }
    case RoleMax:
      if (d->bound_upper) {
        return param_helpers::valueToString(*(d->bound_upper));
      }
    default:
      ROS_WARN_STREAM("Requested string for variable "
                          <<this->TypeName() <<" " <<this->getName() <<" has unknown role " <<role);
  }
  return "";

}

template<typename T>
void SharedParameterT<T>::setFromString(const std::string& str, Role role) {
  T newValue;
  PT_D;
  try {
    newValue = param_helpers::stringToValue<T>(str);
  } catch (Json::Exception& err) {
    throw JsonParsingError("Unable to parse parameter \"" + d->name
                               + "\" from string \"" + str + "\" because: " + err.what());
  }

  switch (role) {
    case RoleValue:
      d->value = newValue;
      d->prev_value = d->value;
      break;
    case RoleMin:
      d->bound_lower = newValue;
      break;
    case RoleMax:
      d->bound_upper = newValue;
      break;
  }

}

// we specialize the template because JSON requires quotes around strings--
// which are likely not present.  Therefore, we just specialze the template
// and stuff the result in there.
template<>
void SharedParameterT<std::string>::setFromString(const std::string& str, Role role) {
  auto d = SharedParameterT<std::string>::d_func();
  switch (role) {
    case RoleValue:
      d->value = str;
      d->prev_value = d->value;
      break;

    case RoleMin:
      d->bound_lower = str;
      break;

    case RoleMax:
      d->bound_upper = str;
      break;
  }
}

template<typename T>
bool SharedParameterT<T>::canSetFromString(const std::string& str, Role role) const {
  return param_helpers::canConvertStringToValue<T>(str);
}

template<typename T>
void SharedParameterT<T>::loadJson(const Json::Value& config) {
  PT_D;
  if (config.isMember("min")) {
    d->bound_lower = param_helpers::jsonToValue<T>(config["min"]);
  }
  if (config.isMember("max")) {
    d->bound_upper = param_helpers::jsonToValue<T>(config["max"]);
  }
  if (!config.isMember("value")) {
    throw JsonParsingError("Shared Parameter " + d->name + " does not have VALUE field!");
  }
  d->value = param_helpers::jsonToValue<T>(config["value"]);
  d->prev_value = d->value;
}

template<typename T>
Json::Value SharedParameterT<T>::saveJson() const {
  PT_D;

  Json::Value ret(Json::ValueType::objectValue);

  ret["type"] = this->TypeName();
  ret["value"] = param_helpers::valueToJson(d->value);
  if (d->bound_lower) {
    ret["min"] = param_helpers::valueToJson(*(d->bound_lower));
  }
  if (d->bound_upper) {
    ret["max"] = param_helpers::valueToJson(*(d->bound_upper));
  }

  return ret;
}

template<typename T>
inline auto SharedParameterT<T>::d_func() noexcept -> SharedParameterPrivateT<T>* {
  return static_cast<SharedParameterPrivateT<T>*>(Parameter::d_ptr_.get());
}

template<typename T>
inline auto SharedParameterT<T>::d_func() const noexcept -> SharedParameterPrivateT<T> const* {
  return static_cast<SharedParameterPrivateT<T> const*>(Parameter::d_ptr_.get());
}

// ///////////////////////////////////////////////////////////////////////// //
// SharedParameterRegistry
// ///////////////////////////////////////////////////////////////////////// //

SharedParameterRegistry::SharedParameterRegistry() : collection_(new ParameterCollection()) {}

/// \brief Load a block of shared variables
void SharedParameterRegistry::loadJson(const Json::Value &config) {
  if (!config.isObject()) {
    throw JsonParsingError("Unable to load shared parameter registry because it isn't an object!");
  }

  for (Json::Value::const_iterator it = config.begin(); it!=config.end(); ++it) {

    std::string namestr = it.key().asString();

    if (! it->isMember("type")) {
      throw JsonParsingError("Shared parameter " + namestr + " has no type field!");
    }
    std::string typestr = it->operator[]("type").asString();

    // note that creating a parameter automagically adds it to the collection; although the
    // parameter VIEW goes out out-of-scope at the end of this function, the parameter
    // itself remains in collection_
    if (typestr == "bool") {
      ds_mx::SharedBoolParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

    } else if (typestr == "int") {
      ds_mx::SharedIntParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

    } else if (typestr == "double") {
      ds_mx::SharedDoubleParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

    } else if (typestr == "string") {
      ds_mx::SharedStringParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

    } else if (typestr == "geopoint") {
      ds_mx::SharedGeoPointParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

    } else if (typestr == "keyvaluelist") {
      ds_mx::SharedKeyValueListParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

      } else if (typestr == "geopointlist") {
      ds_mx::SharedGeoPointListParam toAdd(collection_, namestr, ds_mx::ParameterFlag::DYNAMIC);
      toAdd.loadJson(*it);

    } else {
      throw JsonParsingError("Shared parameter " + namestr + " has unknown type " + typestr);
    }
  }
}

/// \brief Save all shared variables as a JSON block
Json::Value SharedParameterRegistry::saveJson() {
  Json::Value ret(Json::ValueType::objectValue);

  for (ds_mx::Parameter::Ptr param : *collection_) {
    ret[param->getName()] = param->saveJson();
  }

  return ret;
}

/// \brief Load a single variable field into a parameter; if its a literal value, just load it.
/// If it references a shared variable, link the shared variable
void SharedParameterRegistry::loadOrLink(const ds_mx::Parameter::Ptr& param, const Json::Value& json) {
  if (!json.isConvertibleTo(Json::ValueType::stringValue)) {
    // shared parameter tags are definitely strings.  If this isn't convertible, then
    // simply parse the JSON and return.
    param->loadJson(json);
    return;
  }

  std::string valuestr = json.asString();

  if (valuestr[0] == '#') {
    std::string sharedname = valuestr.substr(1);
    // shared variable; link to value in registry
    if (collection_->hasGeneric(sharedname)) {
      param->linkParameter(collection_->getGeneric(sharedname));
    } else {
      throw JsonParsingError("Unable to find referenced shared parameter \"" +sharedname + "\"" );
    }

  } else {
    // standard variable, just load
    param->loadJson(json);
  }
}

void SharedParameterRegistry::loadRos(const ds_mx_msgs::MxSharedParams &rosmsg) {

  bool will_succeed = true;
  Parameter::Ptr col_param;
  for ( auto msg_param : rosmsg.values ) {
    if (col_param = collection_->getGeneric(msg_param.key)) {
      // we have to split CAN and DO to make these atomic
      if (! col_param->isValidStr(msg_param.value) ) {
        throw ParameterTypeError("Cannot set \"" + col_param->TypeName()
        + "\" param \"" + col_param->getName() + "\" from string \"" + msg_param.value + "\"");
      }
    } // else { throw NotFoundException }
  }

  // at this point, things are guaranteed to succeed; go ahead and update
  for ( auto msg_param : rosmsg.values ) {
    Parameter::Ptr col_param = collection_->getGeneric(msg_param.key);
    col_param->setFromString(msg_param.value);
  }

}

ds_mx_msgs::MxSharedParams SharedParameterRegistry::saveRos() {
  ds_mx_msgs::MxSharedParams ret;
  for (auto param : *collection_) {
    ds_core_msgs::KeyString value;
    value.key = param->getName();
    value.value = param->ValueString();
    ret.values.push_back(value);
  }
  return ret;
}

const ParameterCollection& SharedParameterRegistry::collection() const {
  return *collection_;
}

ParameterCollection& SharedParameterRegistry::collection() {
  return *collection_;
}

// ////////////////////////////////////////////////////////////////////////////
// Explicit instantiations
// ONLY these types can be used
template class SharedParameterT<bool>;
template class SharedParameterT<int>;
template class SharedParameterT<double>;
template class SharedParameterT<std::string>;
template class SharedParameterT<GeoPoint>;
template class SharedParameterT<KeyValueList>;
template class SharedParameterT<GeoPointList>;

}

