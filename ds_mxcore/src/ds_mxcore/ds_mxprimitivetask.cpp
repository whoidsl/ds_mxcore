/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//



#include <ds_mxcore/ds_mxcompiler.h>

#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <ds_mxcore/ds_mxeventlog.h>

#include "ds_mxcore/ds_mxprimitivetask.h"

namespace ds_mx {


// ///////////////////////////////////////////////////////////////////////// //
// MxPrimitiveTask -- Common primitive task functions with no subtasks
// ///////////////////////////////////////////////////////////////////////// //

MxPrimitiveTask::~MxPrimitiveTask() = default;

ds_mx::TaskReturnCode MxPrimitiveTask::tick(const ds_nav_msgs::NavState &state) {
  // check to see if the timeout has expired
  if (isTimeoutExpired(state.header.stamp)) {
    MX_LOG_EVENT_STREAM(this, MX_LOG_TASK_FAILED, "Timeout");
    return timeout_code;
  }

  // check for any parameter changes and respond accordingly
  if (params->anyChanged()) {
    parameterChanged(state);
    params->resetChanged();
  }

  // check for stop-on-next-tick
  if (next_tick_return_code != ds_mx::TaskReturnCode::RUNNING) {
    return next_tick_return_code;
  }

  // actually tick
  return onTick(state);
}

Json::Value MxPrimitiveTask::toJson() {
  // as we have no subtasks, our JSON description is just our parameters
  Json::Value obj = MxTask::toJson();

  saveParameters(obj);
  events.saveJson(obj);

  return obj;
}

void MxPrimitiveTask::init(const Json::Value& config, MxCompilerPtr compiler) {
  // as we have no subtasks, our JSON description is just our parameters
  loadParameters(config, compiler);
  events.loadJson(config);

  MX_LOG_EVENT(this, MX_LOG_TASK_INIT);
}

void MxPrimitiveTask::onStart(const ds_nav_msgs::NavState& state) {
  MxTask::onStart(state);
}

void MxPrimitiveTask::onStop(const ds_nav_msgs::NavState& state) {
  MxTask::onStop(state);
}

std::string MxPrimitiveTask::toDot() {
  return getDotId() + " [shape=box,label=\"" + type + "\"];\n";
}

void MxPrimitiveTask::updateStatus(ds_mx_msgs::MxMissionStatus &status) const {
  status.active_tasks.push_back(getStatus());
}

void MxPrimitiveTask::handleEvent(const ds_mx_msgs::MxEvent &event) {
  // nowhere to propagate to, so who cares about return codes?
  events.handleEvent(event);
}

} // namespace ds_mx