/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//

#include <ds_mxcore/ds_mxcompiler.h>

#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <ds_mxcore/ds_mxeventlog.h>

#include "ds_mxcore/ds_mxroottask.h"

namespace ds_mx {

// ///////////////////////////////////////////////////////////////////////// //
// MxRootTask -- Common root task that never returns
// ///////////////////////////////////////////////////////////////////////// //

MxRootTask::MxRootTask() : MxComplexTask(), launch_pt(params, "launch_pt", ParameterFlag::STATIC) {
  // do nothing
}

MxRootTask::~MxRootTask() = default;

std::string MxRootTask::toDot() {
  std::stringstream ret;
  ret << "digraph mission {\n";
  ret << MxComplexTask::toDot();
  ret <<"}\n";

  return ret.str();
}

void MxRootTask::onStop(const ds_nav_msgs::NavState& state) {
  // do NOT call superclass!

  ROS_FATAL_STREAM("Task \"" <<name <<"\": " <<type <<"::OnStop, but the root task should never return!");
}

ds_nav_msgs::NavState MxRootTask::getLaunchState() const {
  ds_nav_msgs::NavState ret;

  // time automagically initialized to zero
  ds_mx::GeoPoint pt = launch_pt.get();
  ret.lon = pt.x;
  ret.lat = pt.y;

  return ret;
}

} // namespace ds_mx