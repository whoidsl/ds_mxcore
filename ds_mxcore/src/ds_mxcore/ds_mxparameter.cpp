/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/4/19.
//

//#include <ds_mxcore/ds_mxparameter.h>
#include <ds_mxcore/ds_mx_paramhelper.h>
#include "ds_mxparameterPrivate.h"
#include "ds_mxparameter_sharedPrivate.h"
#include <ds_mxcore/ds_mxexceptions.h>
#include <ros/console.h>
#include <sstream>

namespace ds_mx {

// ////////////////////////////////////////////////////////////////////////////
// Parameter (base class)

/// Create a parameter base class
/// \param name
Parameter::Parameter(const std::shared_ptr<ParameterPrivate>& _impl) : d_ptr_(_impl) {
  // do nothing
}

/// Access the underlying private pointer.  Only used internally by the library.
/// \return
const std::shared_ptr<ParameterPrivate>& Parameter::getPrivate() const {
  return d_ptr_;
}

const std::string& Parameter::getName() const {
  return d_func()->name;
}

bool Parameter::isStatic() const {
  return d_func()->isStatic();
}

bool Parameter::isDynamic() const {
  return d_func()->isDynamic();
}

bool Parameter::isOptional() const {
  return d_func()->isOptional();
}

bool Parameter::hasChanged() const {
  return d_func()->hasChanged();
}

void Parameter::resetChanged() {
  d_func()->resetChanged();
}

auto Parameter::d_func() noexcept -> ParameterPrivate* {
  return d_ptr_.get();
}
auto Parameter::d_func() const noexcept -> ParameterPrivate const* {
  return d_ptr_.get();
}

// ////////////////////////////////////////////////////////////////////////////
// ParameterT (child classes)

// this is used by child classes ONLY
template<typename T>
ParameterT<T>::ParameterT(const std::shared_ptr<ParameterPrivateT<T> > &_impl) : Parameter(_impl) {
  // do nothing
}

// we'll do these one at a time to get sensible defaults
template<>
ParameterT<bool>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
  : ParameterT<bool>(col, name, false, stat) { /* do nothing */ }

template<>
ParameterT<int>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
    : ParameterT<int>(col, name, 0, stat) { /* do nothing */ }

template<>
ParameterT<double>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
    : ParameterT<double>(col, name, 0.0, stat) { /* do nothing */ }

template<>
ParameterT<std::string>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
    : ParameterT<std::string>(col, name, "", stat) { /* do nothing */ }

template<>
ParameterT<GeoPoint>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
    : ParameterT<GeoPoint>(col, name, GeoPoint(), stat) { /* do nothing */ }

template<>
ParameterT<KeyValueList>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
    : ParameterT<KeyValueList>(col, name, KeyValueList(), stat) { /* do nothing */ }

template<>
ParameterT<GeoPointList>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, ParameterFlag stat)
    : ParameterT<GeoPointList>(col, name, GeoPointList(), stat) { /* do nothing */ }

// back to our regularly-scheduled templates: this is the one that actually does stuff
template<typename T>
ParameterT<T>::ParameterT(const std::shared_ptr<ParameterCollection>& col, const std::string& name, const T& defaultValue, ParameterFlag stat)
: Parameter(std::shared_ptr<ParameterPrivate>(new ParameterPrivateT<T>(name, col, stat, defaultValue))) {

  // now that we've created everything forcibly add this object to the collection
  col->addParameter(*this);
}

#define PT_D auto d = ParameterT<T>::d_func();

/// Get the value of this parameter as a string
/// \return A string representation of the current parameter value
template<typename T>
std::string ParameterT<T>::ValueString() const {
  PT_D;
  return param_helpers::valueToString(d->value);
}

template<typename T>
std::string ParameterT<T>::GuiString(Role role) const {
  PT_D;

  if (role != RoleValue) {
    return "";
  }
  if (d->linkedParam) {
    return "#" + d->linkedParam->name;
  }
  return ValueString();
}

template<typename T>
void ParameterT<T>::setFromString(const std::string& str, Role role) {
  PT_D;

  if (role != RoleValue) {
    ROS_ERROR_STREAM("Attempt to set parameter role other than value!  Role=" <<static_cast<int>(role));
    return;
  }

  d->value = param_helpers::stringToValue<T>(str);
  // reset the previous counter
  d->prev_value = d->value;
}

template<typename T>
bool ParameterT<T>::canSetFromString(const std::string& str, Role role) const {
  if (role != RoleValue) {
    return false;
  }
  return param_helpers::canConvertStringToValue<T>(str);
}

template <typename T>
bool ParameterT<T>::isValidStr(const std::string& str) const {
  return ParameterT<T>::canSetFromString(str);
}

/// Get the value of this parameter
/// \return
template<typename T>
const T& ParameterT<T>::get() const {
  PT_D;
  if (d->linkedParam) {
    return d->linkedParam->value;
  }
  return d->value;
}

/// Set the value of this parameter
/// \param v The new value of this parameter
template<typename T>
void ParameterT<T>::set(const T& v) {
  PT_D;

  if (this->isStatic()) {
    throw ParameterTypeError("Attempted to change static MX parameter" + getName());
  }

  d->value = v;
  if (d->linkedParam) {
    d->linkedParam->value = v;
  }
}


template <typename T>
void ParameterT<T>::loadJson(const Json::Value& config) {
  PT_D;
  try {
    d->value = param_helpers::jsonToValue<T>(config);
    d->prev_value = d->value;
  } catch (Json::Exception& err) {
    if (config.isConvertibleTo(Json::ValueType::stringValue)) {
      throw JsonParsingError("Unable to parse parameter \"" + d->name
                                 + "\" from string \"" + config.toStyledString()
                                 + "\" because: " + err.what());
    } else {
      throw JsonParsingError("Unable to parse parameter \"" + d->name
                                 + "\" because: " + err.what());
    }
  } catch (ds_mx::ParameterTypeError err){
    if (config.isConvertibleTo(Json::ValueType::stringValue)) {
      throw JsonParsingError("Unable to parse parameter \"" + d->name
                                 + "\" from string \"" + config.toStyledString()
                                 + "\" because: " + err.what());
    } else {
      throw JsonParsingError("Unable to parse parameter \"" + d->name
                                 + "\" because: " + err.what());
    }
  }


}

template<typename T>
Json::Value ParameterT<T>::saveJson() const {
  PT_D;
  if (d->linkedParam) {
    return Json::Value( "#" + d->linkedParam->name);
  }
  return param_helpers::valueToJson(d->value);
}

template<typename T>
void ParameterT<T>::linkParameter(const ds_mx::Parameter::Ptr &other) {
  PT_D;

  // we can only access the protected member d_ptr_ if "other" is definitely
  // the same type of object we are.  Therefore, check for that first.
  auto ptr = std::dynamic_pointer_cast<ds_mx::ParameterT<T> >(other);
  if (! ptr) {
    throw ParameterTypeError("Type mismatch when linking parameter "
                               + getName() + " to " + other->getName());
  }

  d->linkedParam = std::dynamic_pointer_cast<ParameterPrivateT<T> >(ptr->d_ptr_);
  if (! d->linkedParam) {
    throw ParameterTypeError("Type mismatch when linking parameter "
                               + getName() + " to " + other->getName());
  }

  if (d->linkedParam->isDynamic() != d->isDynamic()) {
    d->linkedParam.reset();
    throw ParameterTypeError("Static vs. dynamic mismatch when linking parameter "
                               + getName() + " to " + other->getName());
  }

  d->value = d->linkedParam->value;
  d->prev_value = d->linkedParam->value;
}

template<typename T>
void ParameterT<T>::linkParameter(const ds_mx::ParameterT<T> &other) {
  PT_D;

  d->linkedParam = std::dynamic_pointer_cast<ParameterPrivateT<T> >(other.d_ptr_);
  if (! d->linkedParam) {
    throw ParameterTypeError("Type mismatch when linking parameter "
                               + getName() + " to " + other.getName());
  }
  if (d->linkedParam->isDynamic() != d->isDynamic()) {
    d->linkedParam.reset();
    throw ParameterTypeError("Static vs. dynamic mismatch when linking parameter "
                                 + getName() + " to " + other.getName());
  }

  d->value = d->linkedParam->value;
  d->prev_value = d->linkedParam->value;
}

template<typename T>
void ParameterT<T>::unlinkParameter() {
  PT_D;

  // set our current value to whatever the linked parameter had
  if (d->linkedParam) {
    d->value = d->linkedParam->value;
  }
  d->linkedParam.reset();
}

template<typename T>
inline auto ParameterT<T>::d_func() noexcept -> ParameterPrivateT<T>* {
  return static_cast<ParameterPrivateT<T>*>(Parameter::d_ptr_.get());
}

template<typename T>
inline auto ParameterT<T>::d_func() const noexcept -> ParameterPrivateT<T> const* {
  return static_cast<ParameterPrivateT<T> const*>(Parameter::d_ptr_.get());
}

// now we're going to do some specializations
template <>
std::string ParameterT<bool>::TypeName() const { return "bool"; }
template <>
std::string ParameterT<int>::TypeName() const { return "int"; }
template <>
std::string ParameterT<double>::TypeName() const { return "double"; }
template <>
std::string ParameterT<std::string>::TypeName() const { return "string"; }
template <>
std::string ParameterT<GeoPoint>::TypeName() const { return "geopoint"; }
template <>
std::string ParameterT<KeyValueList>::TypeName() const { return "keyvaluelist"; }
template <>
std::string ParameterT<GeoPointList>::TypeName() const { return "geopointlist"; }

// ////////////////////////////////////////////////////////////////////////////
// ParameterEnum (enum class)
// TODO

// ////////////////////////////////////////////////////////////////////////////
// ParameterCollection

/// \brief All parameters keep a pointer to their parent collection; if the collection gets
/// destroyed, set all their collection pointers to NULL so that at least there
/// are no dangling-pointer segfaults
ParameterCollection::~ParameterCollection() {
  for (std::shared_ptr<ParameterPrivate> x : _param_ptrs_) {
    x->collection.reset();
  }
}

void ParameterCollection::addParameter(const Parameter& p) {
  _param_ptrs_.push_back(p.getPrivate());
}

void ParameterCollection::linkCollection(const std::shared_ptr<ParameterCollection> &other) {
  _linked_collection = other;
}

std::shared_ptr<Parameter> ParameterCollection::getGeneric(const std::string& name) const {
  std::shared_ptr<ParameterPrivate> impl;
  for (std::shared_ptr<ParameterPrivate> x : _param_ptrs_) {
    if (x->name == name) {
      impl = x;
      break;
    }
  }

  if (_linked_collection) {
    if (_linked_collection->hasGeneric(name)) {
      return _linked_collection->getGeneric(name);
    }
  }
  if (!impl) {
    throw ParameterCollectionNotFound("Collection has no parameter named " + name);
  }

  return impl->getGenericView();
}

std::shared_ptr<Parameter> ParameterCollection::getGeneric(size_t i) const {
  if (i >= _param_ptrs_.size()) {
    throw ParameterCollectionNotFound("Attempt to read index past end of collection size");
  }
  std::shared_ptr<ParameterPrivate> impl = _param_ptrs_[i];

  return impl->getGenericView();
}

bool ParameterCollection::hasGeneric(const std::string& name) const {
  std::shared_ptr<ParameterPrivate> impl;
  for (std::shared_ptr<ParameterPrivate> x : _param_ptrs_) {
    if (x->name == name) {
      impl = x;
      break;
    }
  }

  if (static_cast<bool>(impl)) {
    return true;
  }

  if (_linked_collection) {
    return _linked_collection->hasGeneric(name);
  }

  return false;
}

bool ParameterCollection::hasGeneric(size_t i) const {
  return i < _param_ptrs_.size();
}

std::shared_ptr<Parameter> ParameterCollection::operator[](size_t i) {
  return _param_ptrs_[i]->getGenericView();
}
std::shared_ptr<Parameter> ParameterCollection::operator[](size_t i) const {
  return _param_ptrs_[i]->getGenericView();
}
size_t ParameterCollection::size() const {
  return _param_ptrs_.size();
}

std::shared_ptr<ParameterPrivate> ParameterCollection::_get_inner(const std::string&name) const {
  std::shared_ptr<ParameterPrivate> impl;

  for (std::shared_ptr<ParameterPrivate> x : _param_ptrs_) {
    if (x->name == name) {
      impl = x;
      return impl;
    }
  }

  if (_linked_collection) {
    return _linked_collection->_get_inner(name);
  }

  return impl;
}

template<typename T>
T ParameterCollection::get(const std::string& name) const {
  std::shared_ptr<ParameterPrivate> impl = _get_inner(name);

  if (!impl) {
    throw ParameterCollectionNotFound("Collection has no parameter named " + name);
  }

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  if (!impl_derived) {
    throw ParameterTypeError("bad cast for " + impl->name);
  }

  return T(impl_derived);

}

template<typename T>
T ParameterCollection::get(size_t i) const {
  if (i >= _param_ptrs_.size()) {
    throw ParameterCollectionNotFound("Attempt to read index past end of collection size");
  }
  // ok, getting the implementation is easy
  std::shared_ptr<ParameterPrivate> impl = _param_ptrs_[i];

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  if (!impl_derived) {
    throw ParameterTypeError("bad cast for " + impl->name);
  }

  return T(impl_derived);
}

template<typename T>
bool ParameterCollection::has(const std::string& name) const {
  static_assert(std::is_base_of<Parameter, T>::value, "Type T must derive from Parameter");
  static_assert(std::is_base_of<ParameterPrivate, typename T::PrivateType>::value, "Type T must derive from Parameter");
  std::shared_ptr<ParameterPrivate> impl = _get_inner(name);

  if (!impl) {
    return false;
  }

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  return static_cast<bool>(impl_derived);
}

template<typename T>
bool ParameterCollection::hasStatic(const std::string& name) const {
  std::shared_ptr<ParameterPrivate> impl = _get_inner(name);

  if (!impl) {
    return false;
  }

  if (!impl->isStatic()) {
    return false;
  }

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  return static_cast<bool>(impl_derived);
}

template<typename T>
bool ParameterCollection::hasDynamic(const std::string& name) const {
  std::shared_ptr<ParameterPrivate> impl = _get_inner(name);

  if (!impl) {
    return false;
  }

  if (!impl->isDynamic()) {
    return false;
  }

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  return static_cast<bool>(impl_derived);
}

template<typename T>
bool ParameterCollection::has(size_t i) const {
  if (i >= _param_ptrs_.size()) {
    return false;
  }
  // ok, getting the implementation is easy
  std::shared_ptr<ParameterPrivate> impl = _param_ptrs_[i];

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  return static_cast<bool>(impl_derived);
}

template<typename T>
bool ParameterCollection::hasStatic(size_t i) const {
  if (i >= _param_ptrs_.size()) {
    return false;
  }
  // ok, getting the implementation is easy
  std::shared_ptr<ParameterPrivate> impl = _param_ptrs_[i];

  if (!(impl && impl->isStatic())) {
    return false;
  }

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  return static_cast<bool>(impl_derived);
}

template<typename T>
bool ParameterCollection::hasDynamic(size_t i) const {
  if (i >= _param_ptrs_.size()) {
    return false;
  }
  // ok, getting the implementation is easy
  std::shared_ptr<ParameterPrivate> impl = _param_ptrs_[i];

  if (!(impl && impl->isDynamic())) {
    return false;
  }

  // now for the tricky bit:
  // we have to verify we're creating the correct view
  // for this type
  std::shared_ptr<typename T::PrivateType> impl_derived
                      = std::dynamic_pointer_cast<typename T::PrivateType>(impl);
  return static_cast<bool>(impl_derived);
}

void ParameterCollection::resetChanged() {
  for (auto& param : _param_ptrs_) {
    param->resetChanged();
  }
}

bool ParameterCollection::anyChanged() const {
  for (auto& param : _param_ptrs_) {
    if (param->hasChanged()) {
      return true;
    };
  }
  return false;
}

ParameterCollection::iterator::iterator(std::vector<std::shared_ptr<ds_mx::ParameterPrivate>>::iterator i) {
  iter = i;
}
std::shared_ptr<Parameter> ParameterCollection::iterator::operator*() {
  return (*iter)->getGenericView();
}
bool ParameterCollection::iterator::operator!=(const ParameterCollection::iterator& rhs) {
  return iter != rhs.iter;
}
bool ParameterCollection::iterator::operator==(const ParameterCollection::iterator& rhs) {
  return iter == rhs.iter;
}
void ParameterCollection::iterator::operator++() {
  iter++;
}
ParameterCollection::iterator ParameterCollection::begin() {
  return ParameterCollection::iterator(_param_ptrs_.begin());
}

ParameterCollection::iterator ParameterCollection::end() {
  return ParameterCollection::iterator(_param_ptrs_.end());
}


// ////////////////////////////////////////////////////////////////////////////
// Explicit instantiations
// ONLY these types can be used
template class ParameterT<bool>;
template BoolParam ParameterCollection::get(const std::string& name) const;
template BoolParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<BoolParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<BoolParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<BoolParam>(const std::string& name) const;
template bool ParameterCollection::has<BoolParam>(size_t i) const;
template bool ParameterCollection::hasStatic<BoolParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<BoolParam>(size_t i) const;

template class ParameterT<int>;
template IntParam ParameterCollection::get(const std::string& name) const;
template IntParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<IntParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<IntParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<IntParam>(const std::string& name) const;
template bool ParameterCollection::has<IntParam>(size_t i) const;
template bool ParameterCollection::hasStatic<IntParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<IntParam>(size_t i) const;

template class ParameterT<double>;
template DoubleParam ParameterCollection::get(const std::string& name) const;
template DoubleParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<DoubleParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<DoubleParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<DoubleParam>(const std::string& name) const;
template bool ParameterCollection::has<DoubleParam>(size_t i) const;
template bool ParameterCollection::hasStatic<DoubleParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<DoubleParam>(size_t i) const;

template class ParameterT<std::string>;
template StringParam ParameterCollection::get(const std::string& name) const;
template StringParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<StringParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<StringParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<StringParam>(const std::string& name) const;
template bool ParameterCollection::has<StringParam>(size_t i) const;
template bool ParameterCollection::hasStatic<StringParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<StringParam>(size_t i) const;

template class ParameterT<GeoPoint>;
template GeoPointParam ParameterCollection::get(const std::string& name) const;
template GeoPointParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<GeoPointParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<GeoPointParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<GeoPointParam>(const std::string& name) const;
template bool ParameterCollection::has<GeoPointParam>(size_t i) const;
template bool ParameterCollection::hasStatic<GeoPointParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<GeoPointParam>(size_t i) const;

template class ParameterT<KeyValueList>;
template KeyValueListParam ParameterCollection::get(const std::string& name) const;
template KeyValueListParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<KeyValueListParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<KeyValueListParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<KeyValueListParam>(const std::string& name) const;
template bool ParameterCollection::has<KeyValueListParam>(size_t i) const;
template bool ParameterCollection::hasStatic<KeyValueListParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<KeyValueListParam>(size_t i) const;

template class ParameterT<GeoPointList>;
template GeoPointListParam ParameterCollection::get(const std::string& name) const;
template GeoPointListParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<GeoPointListParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<GeoPointListParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<GeoPointListParam>(const std::string& name) const;
template bool ParameterCollection::has<GeoPointListParam>(size_t i) const;
template bool ParameterCollection::hasStatic<GeoPointListParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<GeoPointListParam>(size_t i) const;

template SharedBoolParam ParameterCollection::get(const std::string& name) const;
template SharedBoolParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedBoolParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedBoolParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedBoolParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedBoolParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedBoolParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedBoolParam>(size_t i) const;

template SharedIntParam ParameterCollection::get(const std::string& name) const;
template SharedIntParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedIntParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedIntParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedIntParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedIntParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedIntParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedIntParam>(size_t i) const;

template SharedDoubleParam ParameterCollection::get(const std::string& name) const;
template SharedDoubleParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedDoubleParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedDoubleParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedDoubleParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedDoubleParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedDoubleParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedDoubleParam>(size_t i) const;

template SharedStringParam ParameterCollection::get(const std::string& name) const;
template SharedStringParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedStringParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedStringParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedStringParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedStringParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedStringParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedStringParam>(size_t i) const;

template SharedGeoPointParam ParameterCollection::get(const std::string& name) const;
template SharedGeoPointParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedGeoPointParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedGeoPointParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedGeoPointParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedGeoPointParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedGeoPointParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedGeoPointParam>(size_t i) const;

template SharedKeyValueListParam ParameterCollection::get(const std::string& name) const;
template SharedKeyValueListParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedKeyValueListParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedKeyValueListParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedKeyValueListParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedKeyValueListParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedKeyValueListParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedKeyValueListParam>(size_t i) const;

template SharedGeoPointListParam ParameterCollection::get(const std::string& name) const;
template SharedGeoPointListParam ParameterCollection::get(size_t i) const;
template bool ParameterCollection::has<SharedGeoPointListParam>(const std::string& name) const;
template bool ParameterCollection::hasStatic<SharedGeoPointListParam>(const std::string& name) const;
template bool ParameterCollection::hasDynamic<SharedGeoPointListParam>(const std::string& name) const;
template bool ParameterCollection::has<SharedGeoPointListParam>(size_t i) const;
template bool ParameterCollection::hasStatic<SharedGeoPointListParam>(size_t i) const;
template bool ParameterCollection::hasDynamic<SharedGeoPointListParam>(size_t i) const;

}