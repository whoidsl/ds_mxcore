/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/26/19.
//
#include <boost/geometry/io/wkt/read.hpp>
#include <ds_mxcore/ds_mx_paramhelper.h>
#include <ds_mxcore/ds_mxexceptions.h>

#include <cxxabi.h>

const char* currentExceptionTypeName()
{
    int status;
    return abi::__cxa_demangle(abi::__cxa_current_exception_type()->name(), 0, 0, &status);
}


namespace ds_mx {

namespace param_helpers {

// ///////////////////////////////////////////////////////////////////////// //
// jsonToValue
// ///////////////////////////////////////////////////////////////////////// //
template<>
bool jsonToValue(const Json::Value &val) {
  return val.asBool();
}

template<>
int jsonToValue(const Json::Value &val) {
  return val.asInt();
}

template<>
double jsonToValue(const Json::Value &val) {
  return val.asDouble();
}

template<>
std::string jsonToValue(const Json::Value &val) {
  if (val.isConvertibleTo(Json::ValueType::stringValue)) {
    return val.asString();
  } else {
    // fancy objects are harder.  We'll pretty-print
    return val.toStyledString();
  }
}

template<>
ds_mx::GeoPoint jsonToValue(const Json::Value& val) {
  ds_mx::GeoPoint ret;
  try {
    ret = ds_mx::GeoPoint::FromString(val.asString());
  } catch (boost::geometry::read_wkt_exception e) {
    throw ds_mx::JsonParsingError("String \"" + val.asString() + "\" cannot be parsed as a Point");
  }
  return ret;
}

template <>
ds_mx::KeyValueList jsonToValue(const Json::Value& val) {
  ds_mx::KeyValueList ret;
  try {
    ret = ds_mx::KeyValueList::FromJson(val);
  } catch (ds_mx::ParameterTypeError e) {
    throw ds_mx::JsonParsingError("String \"" + val.asString() + "\" cannot be parsed as a Key Value List");
  }

  return ret;
}

template<>
ds_mx::GeoPointList jsonToValue(const Json::Value& val) {
  ds_mx::GeoPointList ret;
  try {
    ret = ds_mx::GeoPointList::FromString(val.asString());
  } catch (boost::geometry::read_wkt_exception e) {
    throw ds_mx::JsonParsingError("String \"" + val.asString() + "\" cannot be parsed as a PointList");
  }
  return ret;
}

// ///////////////////////////////////////////////////////////////////////// //
// valueToJson
// ///////////////////////////////////////////////////////////////////////// //
template<typename T>
Json::Value valueToJson(T value) {
  return Json::Value(value);
}

template Json::Value valueToJson<bool>(bool value);
template Json::Value valueToJson<int>(int value);
template Json::Value valueToJson<double>(double value);
template Json::Value valueToJson<std::string>(std::string value);

template<>
Json::Value valueToJson(ds_mx::GeoPoint value) {
  std::stringstream sstream;
  sstream << value;
  return Json::Value(sstream.str());
}

template <>
Json::Value valueToJson(ds_mx::KeyValueList value) {
  return value.toJson();
}

// ///////////////////////////////////////////////////////////////////////// //
// valueToString
// ///////////////////////////////////////////////////////////////////////// //
template<typename T>
std::string valueToString(const T& value) {
  std::stringstream ss;
  ss << value;
  return ss.str();
}

template<>
Json::Value valueToJson(ds_mx::GeoPointList value) {
  std::stringstream sstream;
  sstream << value;
  return Json::Value(sstream.str());
}

template std::string valueToString<bool>(const bool& value);
template std::string valueToString<int>(const int& value);
template std::string valueToString<double>(const double& value);
template std::string valueToString<std::string>(const std::string& value);
template std::string valueToString<ds_mx::GeoPoint>(const ds_mx::GeoPoint& value);
template std::string valueToString<ds_mx::KeyValueList>(const ds_mx::KeyValueList& value);
template std::string valueToString<ds_mx::GeoPointList>(const ds_mx::GeoPointList& value);

// ///////////////////////////////////////////////////////////////////////// //
// stringToValue
// ///////////////////////////////////////////////////////////////////////// //
template<typename T>
T stringToValue(const std::string& str) {
  // this is gonna seem weird, but FIRST convert to JSON
  // so we can use the JSON parser's handling of weird things
  // like true vs 1 vs 1.0 and all that
  std::stringstream ss(str);
  Json::Value json;

  try {
    ss >> json;
  } catch (Json::Exception) {
    // do nothing
    throw ds_mx::ParameterTypeError("String \"" + str + "\" cannot be parsed to the requested type");
  }

  return jsonToValue<T>(json);
}

// only use this default case for the easy ones
template bool stringToValue<bool>(const std::string& str);
template int stringToValue<int>(const std::string& str);
template double stringToValue<double>(const std::string& str);

// MUCH simplier explicit specialization for string to handle not needing quotes
template<>
std::string stringToValue(const std::string& str) {
  return str;
}

// Slightly simplier explicit specialization for GeoPoint
template<>
ds_mx::GeoPoint stringToValue(const std::string& str) {
  ds_mx::GeoPoint ret;
  try {
    ret = ds_mx::GeoPoint::FromString(str);
  } catch (boost::geometry::read_wkt_exception e) {
    throw ds_mx::ParameterTypeError("String \"" + str + "\" cannot be parsed to the requested type");
  }
  return ret;
}

template<>
ds_mx::GeoPointList stringToValue(const std::string& str) {
  ds_mx::GeoPointList ret;
  try {
    ret = ds_mx::GeoPointList::FromString(str);
  } catch (boost::geometry::read_wkt_exception e) {
    throw ds_mx::ParameterTypeError("String \"" + str + "\" cannot be parsed to the requested type");
  }
  return ret;
}

template<>
ds_mx::KeyValueList stringToValue(const std::string& str) {
  return ds_mx::KeyValueList::FromString(str);
}

// ///////////////////////////////////////////////////////////////////////// //
// canConvertStringToValue
// ///////////////////////////////////////////////////////////////////////// //

// again, the strategy here is to use the JSON parser to validate for us.

// first, we need this purly-internal helper function
template<typename T>
bool canConvertJsonValue(const Json::Value& val);

template<>
bool canConvertJsonValue<bool>(const Json::Value &val) {
  return !val.isNull() && val.isConvertibleTo(Json::ValueType::booleanValue);
}

template<>
bool canConvertJsonValue<int>(const Json::Value &val) {
  return !val.isNull() && val.isConvertibleTo(Json::ValueType::intValue);
}

template<>
bool canConvertJsonValue<double>(const Json::Value &val) {
  return !val.isNull() && val.isConvertibleTo(Json::ValueType::realValue);
}

// let's start this off with a semi-generic version for bool, int and double
template <typename T>
bool canConvertStringToValue(const std::string& str) {
  Json::Value json;
  std::stringstream ss(str);

  // convert to JSON
  try {
    ss >> json;
  } catch (Json::Exception) {
    return false;
  }

  return canConvertJsonValue<T>(json);
}

// explicit instantiations
template bool canConvertStringToValue<bool>(const std::string& str);
template bool canConvertStringToValue<int>(const std::string& str);
template bool canConvertStringToValue<double>(const std::string& str);

// ok, now we specialize for string.  This one is easy, a string is always a string
template <>
bool canConvertStringToValue<std::string>(const std::string& str) {
  return true;
}

// finally, specialize for GeoPoint.
template <>
bool canConvertStringToValue<ds_mx::GeoPoint>(const std::string& str) {
  // the only way to check this is to try it and catch the
  // possibly-resulting exception
  try {
    ds_mx::GeoPoint pt = ds_mx::GeoPoint::FromString(str);
  } catch (boost::geometry::read_wkt_exception e) {
    return false;
  }
  return true;
}

// finally, specialize for GeoPointList
template <>
bool canConvertStringToValue<ds_mx::GeoPointList>(const std::string& str) {
  // the only way to check this is to try it and catch the
  // possibly-resulting exception
  try {
    ds_mx::GeoPointList pts = ds_mx::GeoPointList::FromString(str);
  } catch (boost::geometry::read_wkt_exception e) {
    return false;
  }
  return true;
}

template<>
bool canConvertStringToValue<ds_mx::KeyValueList>(const std::string& str) {
  // again, the only way to do this, really, is ot try it and catch any resulting exception.
  try {
    ds_mx::KeyValueList pt = ds_mx::KeyValueList::FromString(str);
  } catch (...) {
    return false;
  }
  return true;
}

} // namespace param_helpers
} // namespace ds_mx