/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/3/19.
//

#include <ds_mxcore/ds_mxexceptions.h>
#include "ds_mxeventPrivate.h"

#define EH_D auto d = EventHandler::d_func();

namespace ds_mx {

bool eventStringValid(const std::string& eventstr) {
  boost::regex regex{"[-_a-zA-Z0-9/]*"};
  return boost::regex_match(eventstr, regex);
}


bool eventMatcherStringValid(const std::string& eventstr) {
  boost::regex regex{"[-_a-zA-Z0-9/*]*"};
  return boost::regex_match(eventstr, regex);
}

// ///////////////////////////////////////////////////////////////////////// //
// EventHandler class
// ///////////////////////////////////////////////////////////////////////// //
EventHandler::EventHandler(const std::shared_ptr<EventHandlerPrivate>& _impl) : d_ptr_(_impl) {
  // do nothing
}

EventHandler::EventHandler(EventHandlerCollection& col, const std::string& name,
      const std::string& default_event, std::function<bool(ds_mx_msgs::MxEvent)> _callback)
      : d_ptr_(new EventHandlerPrivate(col, name, default_event, _callback)) {
  col.addHandler(*this);
}

const std::string& EventHandler::getName() const {
  return d_func()->name;
}
std::vector<std::string> EventHandler::getMatcherStrings() const {
  std::vector<std::string> ret;

  for (auto event : d_func()->event_matchers) {
    ret.push_back(event.eventStr);
  }

  return ret;
}

void EventHandler::addMatcherString(const std::string& eventstr) {
  if (hasMatcherString(eventstr)) {
    // don't add duplicates!
    return;
  }
  d_func()->event_matchers.push_back(EventMatcher(eventstr));
}

/// Check to see if a SPECIFIC event string is included
/// \param eventstr
/// \return
bool EventHandler::hasMatcherString(const std::string& matcherstr) const {
  const EH_D;

  for (auto iter=d->event_matchers.begin(); iter != d->event_matchers.end(); ++iter) {
    if (iter->eventStr == matcherstr) {
      return true;
    }
  }
  return false;
}

void EventHandler::removeMatcherString(const std::string& matcherstr) {
  EH_D;
  for (auto iter=d->event_matchers.begin(); iter != d->event_matchers.end(); ++iter) {
    if (iter->eventStr == matcherstr) {
      d->event_matchers.erase(iter);
      break;
    }
  }
}

void EventHandler::removeMatcherString(size_t i) {
  d_func()->event_matchers.erase(d_func()->event_matchers.begin()+i);
}

void EventHandler::loadJson(const Json::Value& config) {
  EH_D;
  if (hasMatcherString(d->default_event)) {
    removeMatcherString(d->default_event);
  }

  if (config.isArray()) {
    for (const auto eventstr : config) {
      d->event_matchers.push_back(EventMatcher(eventstr.asString()));
    }

  } else if (! config.isNull()) {
    d->event_matchers.push_back(EventMatcher(config.asString()));
  }
}

Json::Value EventHandler::saveJson() const {
  EH_D;

  // if we have a single item, just return
  if (d->event_matchers.size() == 1) {
    return Json::Value(d->event_matchers.front().eventStr);
  }

  // ok, we have nothing; go ahead and make it
  Json::Value ret(Json::ValueType::arrayValue);
  for (const auto& event_match : d->event_matchers) {
    ret.append(Json::Value(event_match.eventStr));
  }

  return ret;
}

bool EventHandler::eventMatches(const std::string &eventstr) const {
  EH_D;
  return d->eventMatches(eventstr);
}

bool EventHandler::handleEvent(const ds_mx_msgs::MxEvent &event) {
  EH_D;
  return d->handleEvent(event);
}

const std::shared_ptr<EventHandlerPrivate>& EventHandler::getPrivate() const {
  return d_ptr_;
}

auto EventHandler::d_func() noexcept -> EventHandlerPrivate* {
  return d_ptr_.get();
}

auto EventHandler::d_func() const noexcept -> EventHandlerPrivate const* {
  return d_ptr_.get();
}

// ///////////////////////////////////////////////////////////////////////// //
// EventHandlerCollection class
// ///////////////////////////////////////////////////////////////////////// //

bool EventHandlerCollection::has(const std::string& _name) {
  for (const auto& handler : _event_handlers) {
    if (handler->name == _name) {
      return true;
    }
  }
  return false;
}

const std::shared_ptr<EventHandler> EventHandlerCollection::operator[](const std::string& str) const {
  for (const auto& handler : _event_handlers) {
    if (handler->name == str) {
      return std::shared_ptr<EventHandler>(new EventHandler(handler));
    }
  }

  // return nullptr
  return std::shared_ptr<EventHandler>();
}

const std::shared_ptr<EventHandler> EventHandlerCollection::operator[](size_t i) const {
  return std::shared_ptr<EventHandler>(new EventHandler(_event_handlers[i]));
}

size_t EventHandlerCollection::size() const {
  return _event_handlers.size();
}

void EventHandlerCollection::clear() {
  _event_handlers.clear();
}

EventHandlerCollection::iterator::iterator(std::vector<std::shared_ptr<ds_mx::EventHandlerPrivate> >::iterator i) {
  iter = i;
}

std::shared_ptr<EventHandler> EventHandlerCollection::iterator::operator*() {
  return std::shared_ptr<EventHandler>(new EventHandler(*iter));
}
void EventHandlerCollection::iterator::operator++() {
  iter++;
}
bool EventHandlerCollection::iterator::operator!=(const EventHandlerCollection::iterator& rhs) {
  return iter != rhs.iter;
}

bool EventHandlerCollection::iterator::operator==(const EventHandlerCollection::iterator& rhs) {
  return iter == rhs.iter;
}

/// Check to see if any of the event handlers will accept this event.  Return the
/// return value of the events that got called.  If multiple events are called,
/// AND all the return values.  If none are called, return true.
/// \param event
/// \return
bool EventHandlerCollection::handleEvent(const ds_mx_msgs::MxEvent &event) {

  bool rc = true;

  for (auto handler : _event_handlers) {
    rc = handler->handleEvent(event) && rc;
  }

  return rc;
}

void EventHandlerCollection::addHandler(const ds_mx::EventHandler &handler) {
  _event_handlers.push_back(handler.getPrivate());
}

EventHandlerCollection::iterator EventHandlerCollection::begin() {
  return EventHandlerCollection::iterator(_event_handlers.begin());
}

EventHandlerCollection::iterator EventHandlerCollection::end() {
  return EventHandlerCollection::iterator(_event_handlers.end());
}

void EventHandlerCollection::loadJson(const Json::Value& config) {
  if (!config.isObject()) {
    throw JsonParsingError("Unable to parse event from JSON: \"" + config.toStyledString() + "\"");
  }
  for (auto handler : _event_handlers) {
    if (config.isMember(handler->name)) {
      EventHandler full_handler(handler);
      full_handler.loadJson(config[handler->name]);
    }
  }
}

void EventHandlerCollection::saveJson(Json::Value& config) const {
  if (!config.isObject()) {
    throw std::runtime_error("Unable to save EventHandlerCollection because passed JSON object was not an object!");
  }

  for (auto handler : _event_handlers) {
    EventHandler full_handler(handler);
    config[full_handler.getName()] = full_handler.saveJson();
  }
}

};
