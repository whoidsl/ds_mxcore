/**
* Copyright 2022 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by tmjoyce on 10/18/22.
//

#include <json/json.h>
#include <sstream>
#include <ds_mxcore/ds_mxexceptions.h>
#include "ds_mxcore/ds_mxparam_geopointlist.h"
#include <ds_libtrackline/WktUtil.h>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry.hpp>

namespace ds_mx {

GeoPointList::GeoPointList(){};

GeoPointList::GeoPointList(const GeoPoint &element) {
  points.emplace_back(element);
}
GeoPointList::GeoPointList(const std::vector<GeoPoint> &elements) {
  for (auto pt : elements) {
    //don't want to create these as references anyway
    points.emplace_back(pt);
  }
}
GeoPoint& GeoPointList::operator[](int position) {
  //return a reference to the geopoint, this could be manipulated
  return points.at(position);
}

bool GeoPointList::operator >= (const GeoPointList& other) const {
  return points.size() >= other.points.size();
}

bool GeoPointList::operator <= (const GeoPointList& other) const {
  return points.size() <= other.points.size();
}

bool GeoPointList::operator != (const GeoPointList& other) const {

  return points != other.points;
}

bool GeoPointList::operator == (const GeoPointList& other) const {
  return points == other.points;
}

std::ostream & operator << (std::ostream &out, const GeoPointList& p) {
    std::vector<std::pair<double,double>> points;
    for (auto single : p.points){
        
        points.push_back(std::pair<double,double>(single.x, single.y));
    }
    out << ds_trackline::wktLineStringLL(points);
    return out;
}

GeoPointList GeoPointList::FromString(const std::string &str) {

  typedef boost::geometry::model::d2::point_xy<double> point_type;
  boost::geometry::model::linestring<point_type> ls;
  boost::geometry::read_wkt(str, ls);
  
  
  std::vector<GeoPoint> ret;
  for (auto pt : ls){
   double x = pt.x();
   double y = pt.y();
   ret.emplace_back(GeoPoint(x,y));
  }
  
  return GeoPointList(ret);
}

} // namespace ds_mx