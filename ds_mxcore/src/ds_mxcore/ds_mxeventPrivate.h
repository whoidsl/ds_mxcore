/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/3/19.
//

#ifndef DS_MXEVENTPRIVATE_H
#define DS_MXEVENTPRIVATE_H

#include <ds_mxcore/ds_mxevent.h>
#include <ds_mxcore/ds_mxexceptions.h>
#include <memory>
#include <string>
#include <map>
#include <utility>

#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace ds_mx {

class EventMatcher {
 public:
  EventMatcher(const std::string& eventstr) : eventStr(eventstr),
  regex(boost::replace_all_copy(eventstr, "*", ".*")){
    if (!eventMatcherStringValid(eventstr)) {
      throw ds_mx::JsonParsingError("Invalid event matching string \"" + eventstr + "\"");
    }
  }

  std::string eventStr;
  boost::regex regex;
};

class EventHandlerPrivate : public std::enable_shared_from_this<EventHandlerPrivate> {
  // make noncopyable
 private:
  EventHandlerPrivate(const EventHandlerPrivate&) = delete;
  EventHandlerPrivate& operator=(const EventHandlerPrivate&) = delete;

 public:
  EventHandlerPrivate(EventHandlerCollection& col, const std::string& _n,
      const std::string& _default_event, std::function<bool(ds_mx_msgs::MxEvent)> _callback)
      : name(_n), default_event(_default_event), callback(_callback) {
    event_matchers.push_back(EventMatcher(default_event));
  }
  virtual ~EventHandlerPrivate() = default;


  bool eventMatches(const std::string &eventstr) const {
    for (const auto &matcher : event_matchers) {
      if (boost::regex_match(eventstr, matcher.regex)) {
        return true;
      }
    }
    return false;
  }

  bool handleEvent(const ds_mx_msgs::MxEvent &event) {
    if (!eventMatches(event.eventid)) {
      return true; // pass on the event
    }

    // call the callback
    return callback(event);
  }

  std::string name;
  std::string default_event;
  std::vector<EventMatcher> event_matchers;
  std::function<bool(ds_mx_msgs::MxEvent)> callback;
};

}

#endif //DS_MXEVENTPRIVATE_H
