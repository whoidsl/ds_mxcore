/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/8/19.
//

#include <boost/filesystem.hpp>
#include "ds_mxcore/ds_mxexec.h"

#include "ds_mxcore/ds_mxcompiler.h"

#include <std_msgs/String.h>
#include <ds_mxcore/ds_mxeventlog.h>
#include <std_msgs/String.h>

namespace ds_mx {

MxExecProcess::MxExecProcess() : ds_base::DsProcess() {
  started = false;
}

MxExecProcess::MxExecProcess(int argc, char *argv[], const std::string &name)
: ds_base::DsProcess(argc, argv, name) {
  started = false;
}

MxExecProcess::~MxExecProcess() {
  eventlog_destroy();
}

void MxExecProcess::setupParameters() {
  ds_base::DsProcess::setupParameters();
  auto nh = nodeHandle();

  if (! nh.getParam(ros::this_node::getName() + "/mission_file", mission_filename)) {
    ROS_FATAL("No mission file specified!");
    ROS_BREAK();
  }
  if (mission_filename.empty()) {
    ROS_FATAL("Mission filename is empty! Cannot proceed...");
    ROS_BREAK();
  }
}

void MxExecProcess::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();
  auto nh = nodeHandle();
  std::string nav_state_topic;

  if (! nh.getParam(ros::this_node::getName() + "/nav_state_topic", nav_state_topic)) {
    ROS_FATAL("No nav_state_topic tag present in MX private namespace!");
    ROS_BREAK();
  }
  nav_state_sub = nh.subscribe(nav_state_topic, 10, &MxExecProcess::navstate_callback, this);

  std::string mx_event_topic;
  if (! nh.getParam(ros::this_node::getName() + "/mx_events", mx_event_topic)) {
    ROS_FATAL("mx_events topic tag present in MX private namespace!");
    ROS_BREAK();
  }
  mx_event_sub = nh.subscribe(mx_event_topic, 10, &MxExecProcess::mx_event_callback, this);

}

void MxExecProcess::setupPublishers() {
  ds_base::DsProcess::setupPublishers();
  auto nh = nodeHandle();

  std::string mx_state_topic;
  if (! nh.getParam(ros::this_node::getName() + "/mx_state_topic", mx_state_topic)) {
    ROS_FATAL("No mx_state_topic tag present in MX private namespace!");
    ROS_BREAK();
  }
  status_pub = nh.advertise<ds_mx_msgs::MxMissionStatus>(mx_state_topic, 10);

  std::string shared_param_topic;
  if (! nh.getParam(ros::this_node::getName() + "/mx_shared_param_topic", shared_param_topic)) {
    ROS_FATAL("No shared_param_topic tag present in MX private namespace!");
    ROS_BREAK();
  }
  shared_param_pub = nh.advertise<ds_mx_msgs::MxSharedParams>(shared_param_topic, 10);

  std::string mission_pub_topic;
  if (! nh.getParam(ros::this_node::getName() + "/mx_mission_topic", mission_pub_topic)) {
    ROS_FATAL("No mx_mission_topic tag present in MX private namespace!");
    ROS_BREAK();
  }
  mission_pub = nh.advertise<std_msgs::String>(mission_pub_topic, 2, true);

  std::string eventlog_topic;
  if (! nh.getParam(ros::this_node::getName() + "/eventlog_topic", eventlog_topic)) {
    ROS_FATAL("No eventlog_topic tag present in MX private namespace!");
    ROS_BREAK();
  }
  eventlog_init(nh, eventlog_topic);
}

void MxExecProcess::setupServices() {
  ds_base::DsProcess::setupServices();
  auto nh = nodeHandle();

  std::string update_param_service;
  if (! nh.getParam(ros::this_node::getName() + "/mx_shared_param_update", update_param_service)) {
    ROS_FATAL("No shared_param_update tag present in MX private namespace!");
    ROS_BREAK();
  }

  shared_param_update_client = nh.advertiseService(update_param_service,
      &MxExecProcess::shared_param_update_callback, this);

}

void MxExecProcess::setupTimers() {
  ds_base::DsProcess::setupTimers();
  auto nh = nodeHandle();

  status_timer = nh.createTimer(ros::Duration(1), &MxExecProcess::status_timer_callback, this);

}

void MxExecProcess::setup() {
  ds_base::DsProcess::setup();

  // loads the mission from rosparam and executes
  setupMission();

  // allow a small delay on startup for connections to get made, etc
  ros::WallDuration setup_delay(ros::param::param("~/startup_delay", 3.0));
  if (setup_delay.toSec() > 0) {
    ROS_WARN_STREAM("Watering TaskTree for " <<setup_delay.toSec() <<" seconds before executing mission...");
    setup_delay.sleep();
  }
  MX_LOG_EVENT(root_task.get(), MX_LOG_MISSION_START);
}

void MxExecProcess::setupMission() {

  // create the compiler
  compiler = ds_mx::MxCompiler::create();

  // setup macros
  // TODO

  // parse the mission file into JSON
  boost::filesystem::path mission_path(mission_filename);
  if (! boost::filesystem::exists(mission_path)) {
    ROS_FATAL_STREAM("Mission path \"" << mission_filename <<"\" does not exist! mxexec cannot proceed");
    ROS_BREAK();
  }

  // load the actual mission
  Json::Value json_root;
  try {

    json_root = ds_mx::parseFile(mission_path);
    // Compile the parsed mission file into a task tree
    root_task = compiler->compile(json_root);

  } catch (std::exception& err) {
    ROS_FATAL_STREAM("Unable to load mission because:\n" <<err.what());
    ROS_BREAK();
  }

  // grab a pointer to our shared parameter repo
  shared_params = compiler->getSharedParams();


  // initilize all the ROS hooks inside each mission node
  auto nh = nodeHandle();
  root_task->init_ros(nh);

  // always run the static validation on the mission; if it fails, break
  if (root_task->validate()) {
    ROS_WARN("Mission validated; ready to proceed");
  } else {
    ROS_FATAL("INVALID MISSION! It is strongly recommended to verify the mission with \"mxinfo validate\" prior to starting mxexec");
    ROS_BREAK();
  }

  // write the mission out-- WITH the correct UUIDs added-- and publish
  std::stringstream mission_text;
  mission_text <<compiler->save(root_task);
  std_msgs::String mission_msg;
  mission_msg.data = mission_text.str();
  mission_pub.publish(mission_msg);

  // log start-of-mission
  MX_LOG_EVENT(root_task.get(), MX_LOG_MISSION_LOADED);
}

void MxExecProcess::tick(const ds_nav_msgs::NavState &state) {
  if (! root_task) {
    ROS_ERROR("No root task defined in mission executive!");
    return;
  }

  // this should never return
  ds_mx::TaskReturnCode rc = root_task->tick(state);
  if (rc != ds_mx::RUNNING) {
    ROS_FATAL("Root task returned rc!=RUNNING.  This should never happen!");
    ROS_BREAK(); // this will crash this process and eventually tickle the abort deadman
    // TODO Make this fail more gracefully
  }
}

void MxExecProcess::publishState(const std_msgs::Header& hdr) const {
  if (status_pub) {
    ds_mx_msgs::MxMissionStatus status;
    status.header = hdr;
    root_task->updateStatus(status);
    status_pub.publish(status);
  }
}

void MxExecProcess::publishSharedParams(const std_msgs::Header& hdr) const {
  if (shared_param_pub) {
    // fill in the message
    ds_mx_msgs::MxSharedParams msg = shared_params->saveRos();

    shared_param_pub.publish(msg);
  }
}

void MxExecProcess::navstate_callback(const ds_nav_msgs::NavState& state) {
  // TODO: maybe downsample or something?

  // handle the first-time onStart
  if (!started) {
    root_task->onStart(state);
    started = true;
  }

  // TICK!
  tick(state);

  publishState(state.header);
}

void MxExecProcess::mx_event_callback(const ds_mx_msgs::MxEvent& event) {

  // log event to eventlog
  MX_LOG_EVENT_TEXT(root_task.get(), MX_LOG_EVENT_RECEIVED, event.eventid);

  if (!ds_mx::eventStringValid(event.eventid)) {
    ROS_ERROR_STREAM("Got INVALID event string \"" <<event.eventid <<"\"");
    return;
  }

  // actually handle the event
  root_task->handleEvent(event);
}

bool MxExecProcess::shared_param_update_callback(
    ds_mx_msgs::MxUpdateSharedParamsRequest &req,
    ds_mx_msgs::MxUpdateSharedParamsResponse &res) {

  // log eventlog event
  std::stringstream logstr;
  for (auto param : req.requested.values) {
    logstr <<"\"" <<param.key <<"\"=\"" <<param.value <<"\" ";
  }
  MX_LOG_EVENT_TEXT(root_task.get(), MX_LOG_SHARED_PARAM_INIT, logstr.str());

  // attempt to load variables
  bool rc;
  try {
    shared_params->loadRos(req.requested);
    res.response = "success";
    MX_LOG_EVENT_TEXT(root_task.get(), MX_LOG_SHARED_PARAM_SUCCEED, logstr.str());
    rc = true;

  } catch (std::exception &error) {
    ROS_ERROR_STREAM("Unable to set requested shared parameters because " <<error.what());
    res.response = error.what();
    MX_LOG_EVENT_TEXT(root_task.get(), MX_LOG_SHARED_PARAM_FAILED, error.what());
    rc = true; // always return true so the response goes back
  }

  // publish the updated changes
  std_msgs::Header hdr;
  hdr.stamp = ros::Time::now();
  publishSharedParams(hdr);

  return rc;
}

void MxExecProcess::status_timer_callback(const ros::TimerEvent& evt) {

  std_msgs::Header hdr;
  hdr.stamp = evt.current_real;
  //publishState(hdr); // happens every tick
  publishSharedParams(hdr);
}


}
