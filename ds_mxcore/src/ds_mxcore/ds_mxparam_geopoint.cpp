/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/26/19.
//

#include "ds_mxcore/ds_mxparam_geopoint.h"

#include <ds_libtrackline/WktUtil.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

namespace ds_mx {

// ////////////////////////////////////////////////////////////////////////////
// GeoPoint (point class)

GeoPoint::GeoPoint() : x(0), y(0) {}
GeoPoint::GeoPoint(double _x, double _y) : x(_x), y(_y) {}

bool GeoPoint::operator >= (const GeoPoint& other) const {
  return x >= other.x && y >= other.y;
}

bool GeoPoint::operator <= (const GeoPoint& other) const {
  return x <= other.x && y <= other.y;
}

bool GeoPoint::operator != (const GeoPoint& other) const {
  return x != other.x && y != other.y;
}

bool GeoPoint::operator == (const GeoPoint& other) const {
  return x == other.x && y == other.y;
}

std::ostream & operator << (std::ostream &out, const GeoPoint& p) {
  return out <<ds_trackline::wktPointLL(p.x, p.y);
}

GeoPoint GeoPoint::FromString(const std::string &str) {
  GeoPoint ret;
  boost::geometry::model::d2::point_xy<double> geom;
  boost::geometry::read_wkt(str, geom);

  ret.x = geom.x();
  ret.y = geom.y();

  return ret;
}

} // namespace ds_mx
