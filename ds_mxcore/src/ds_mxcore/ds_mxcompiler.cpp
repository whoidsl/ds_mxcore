/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/3/19.
//

#include <ds_mxcore/ds_mxcompiler.h>
#include <fstream>

namespace ds_mx {

/// \brief Parse a mission file and return its parsed objects
/// \param filename A valid path to the JSON file to read
/// \return A JSON Value containing the parsed result of the file
Json::Value parseFile(const boost::filesystem::path& filename) {
  std::ifstream ifs(filename.c_str());
  Json::Value ret;

  // setup our JSON reader
  // 2020-05-14 SS - enabled strict mode, to require a configuration
  //                 strictly compatible with the JSON specification
  Json::Reader reader(Json::Features::strictMode());

  // do the parse & return
  reader.parse(ifs, ret);
  return ret;
}

MxCompiler::MxCompiler() : mxtask_loader("ds_mxcore", "ds_mx::MxTask"),
                           shared_params(new ds_mx::SharedParameterRegistry) {
  // do nothing-- for now!
}

std::shared_ptr<MxCompiler> MxCompiler::create() {
  return std::shared_ptr<MxCompiler> (new MxCompiler());
}

std::shared_ptr<MxRootTask> MxCompiler::compile(const Json::Value& ast) {

  loadSharedParams(ast);
  // TODO: Load macros
  return loadMission(ast);
}

std::shared_ptr<MxRootTask> MxCompiler::loadMission(const Json::Value& ast) {
  if (! (ast.isObject() && ast.isMember("root")) ) {
    std::cerr <<"JSON root object not found!" <<std::endl;
    return std::shared_ptr<MxRootTask>();
  }

  Json::Value rootObj = ast["root"];
  std::shared_ptr<MxTask> task = loadTask("root", rootObj);

  std::shared_ptr<MxRootTask> ret = std::dynamic_pointer_cast<MxRootTask>(task);
  if (!ret) {
    std::cerr <<"Root Task does not inherit from MxRootTask!" <<std::endl;
  }

  return ret;
}

void MxCompiler::loadSharedParams(const Json::Value &ast) {
  if (! (ast.isObject() && ast.isMember("shared")) ) {
    throw ds_mx::JsonParsingError("No shared variable block in mission file!");
  }

  shared_params->loadJson(ast["shared"]);
}

Json::Value MxCompiler::save(const std::shared_ptr<ds_mx::MxRootTask> &root) const {
  Json::Value ret(Json::objectValue);
  ret["shared"] = shared_params->saveJson();
  ret["root"] = root->toJson();

  return ret;
}

void MxCompiler::loadMacro(const Json::Value& ast) {
  // TODO
}

/// Load an individal Task and all its subtasks from a JSON description
/// \param name The name of the task to load
/// \param ast The JSON description of the mission file
/// \throws std::invalid_argument When the Task description is not available
/// \return The returned Task
std::shared_ptr<MxTask> MxCompiler::loadTask(const std::string& name, const Json::Value &ast) {
  if (ast.isNull()) {
    throw std::invalid_argument("No JSON for subtask \"" + name + "\"");
  }
  if (! ast.isObject() ) {
    throw std::invalid_argument("JSON for task named " + name + " is not an object");
  }
  if (! ast.isMember("type") ) {
    throw std::invalid_argument("Attemped to load a Task that does not have a type!");
  }

  std::string typestr = ast["type"].asString();
  
  std::shared_ptr<MxTask> ret(mxtask_loader.createUnmanagedInstance(typestr));

  // initialize the object and all its subtasks
  if (ret) {
    // set name/typestr FIRST so error messages make sense
    ret->setName(name);
    ret->setType(typestr);
    ret->init(ast, shared_from_this());
  }
  else {std::cout << "Failed to make ret ret task";}

  return ret;
}

/// Accessor for our underlying pluginlib classloader for MxTask objects
/// \return The class loader for MxTasks
pluginlib::ClassLoader<ds_mx::MxTask>& MxCompiler::getTaskLoader() {
  return mxtask_loader;
}

/// Accessor for our underlying pluginlib classloader for MxTask objects
/// \return The class loader for MxTasks
const pluginlib::ClassLoader<ds_mx::MxTask>& MxCompiler::getTaskLoader() const {
  return mxtask_loader;
}

const std::shared_ptr<ds_mx::SharedParameterRegistry>& MxCompiler::getSharedParams() const {
  return shared_params;
}

}
