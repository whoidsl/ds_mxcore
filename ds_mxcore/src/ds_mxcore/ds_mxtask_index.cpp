/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 6/4/19.
//

#include "ds_mxcore/ds_mxtask_index.h"

namespace ds_mx {

TaskIndex::TaskIndex() {}

TaskIndex::TaskIndex(const std::shared_ptr<ds_mx::MxRootTask> &_root) {
  setRoot(_root);
}

std::shared_ptr<ds_mx::MxTask> TaskIndex::operator[](const boost::uuids::uuid &id) {
  TaskIndex_t::const_iterator iter = index.find(id);
  // if its not found, go ahead and rebuild the index
  if (iter == index.end()) {
    reindex();
    iter = index.find(id);
  }

  if (iter != index.end()) {
    return iter->second;
  }

  return std::shared_ptr<ds_mx::MxTask>();
}

const std::shared_ptr<ds_mx::MxRootTask> &TaskIndex::getRoot() const {
  return root;
}

void TaskIndex::setRoot(const std::shared_ptr<ds_mx::MxRootTask> &_root) {
  root = _root;
  reindex();
}

bool TaskIndex::has(const boost::uuids::uuid &id) {
  TaskIndex_t::const_iterator iter = index.find(id);
  // if its not found, go ahead and rebuild the index
  if (iter == index.end()) {
    reindex();
    iter = index.find(id);
  }

  return iter != index.end();
}

size_t TaskIndex::numTasks() const {
  return index.size();
}

void TaskIndex::reindex() {

  // clear the existing index
  index.clear();

  // this routine must obviously be recursive.  So we're going to call an inner function.
  if (root) {
    indexSubtree(root);
  }
}

void TaskIndex::indexSubtree(std::shared_ptr<ds_mx::MxTask> task) {
  // add the current task
  index[task->getUuid()] = task;

  // check if we need to recurse
  auto complexTask = std::dynamic_pointer_cast<ds_mx::MxComplexTask>(task);
  if (complexTask) {
    for (auto subtask : complexTask->getSubtasks()) {
      indexSubtree(subtask);
    }
  }
  // otherwise, we had a primitive, and we're done.
}

} // namespace ds_mx