/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/8/19.
//
#include <ds_mxcore/ds_mxeventlog.h>

#include <ros/ros.h>
#include <ds_mx_msgs/MxEventLog.h>

#include <boost/uuid/uuid_io.hpp>

namespace ds_mx {

// TODO: This needs to go into a class to ensure proper order at destruction-- right now,
// there's an issue where not calling eventlog_destroy at shutdown can cause
// various exceptions to get thrown as ros shuts down dirty.
// Keeping a local copy of the node handle and guaranteeing shutdown order
// may help (?).  Otherwise, we need to see what ros::console does and borrow that.

static ros::Publisher eventlog_pub;

// Flag to warn if there's no publisher
static bool eventlog_warn_nopub = true;

/// Initialize the MX event log.  Note that all events are copied to ROSCONSOLE at the "info"
/// level.
/// \param node_handle The node handle to use when advertising a publisher
/// \param topicname The topicname to use when publishing MX EventLog events
void eventlog_init(ros::NodeHandle& node_handle, const std::string& topicname) {
  eventlog_pub = node_handle.advertise<ds_mx_msgs::MxEventLog>(topicname, 10, false);
}

void eventlog_disable() {
  eventlog_warn_nopub = false;
}

void eventlog_destroy() {
  eventlog_pub.shutdown();
}

void eventlog_log(ds_mx::MxTask *task, const std::string &type, const std::string &text) {
  // prepare output structure
  ds_mx_msgs::MxEventLog msg;
  msg.header.stamp = ros::Time::now();
  msg.event_type = type;
  msg.event_text = text;

  // build the free text
  std::string task_uuid_str;
  if (task != nullptr) {
    msg.task_name = task->getName();
    msg.task_type = task->getType();
    msg.task_id = boost::uuids::to_string(task->getUuid());
  }

  // publish if possible
  if (eventlog_pub) {
    eventlog_pub.publish(msg);
  } else if (eventlog_warn_nopub) {
    ROS_ERROR_STREAM("No MX EVENTLOG publisher is available, but tried to send event text\""
                         << msg.event_text <<"\"");
  }
  ROS_INFO_STREAM("MxEvent \"" <<msg.event_type <<"\": \"" <<msg.event_text <<"\"");
}

}