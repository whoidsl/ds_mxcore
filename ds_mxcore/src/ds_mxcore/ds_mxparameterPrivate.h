/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/4/19.
//

#ifndef DS_MXPARAMETERPRIVATE_H
#define DS_MXPARAMETERPRIVATE_H

#include <ds_mxcore/ds_mxparameter.h>
#include <string>
#include <memory>
#include <cmath>

namespace ds_mx {

// ////////////////////////////////////////////////////////////////////////////
// ParameterPrivate (base class)
 class ParameterPrivate : public std::enable_shared_from_this<ParameterPrivate> {
  // make non-copyable
 private:
  ParameterPrivate(const ParameterPrivate&) = delete;
  ParameterPrivate& operator=(const ParameterPrivate&) = delete;

 public:
  ParameterPrivate(const std::string& _n, const std::shared_ptr<ParameterCollection>& col, ParameterFlag _stat)
  : name(_n), collection(col), parameterType(_stat) {
    // do nothing
  }
  virtual ~ParameterPrivate() = default;

  virtual Parameter::Ptr getGenericView() = 0;
  virtual bool hasChanged() const=0;
  virtual void resetChanged()=0;

  std::string name;
  ParameterFlag parameterType;
  std::weak_ptr<ParameterCollection> collection; // managed elsewhere

  bool isStatic() const {
    return (parameterType & 0x01) == ParameterFlag::STATIC;
  };

  bool isDynamic() const {
    return (parameterType & 0x01) == ParameterFlag::DYNAMIC;
  };

  bool isOptional() const {
    return (parameterType & 0x02) == ParameterFlag::OPTIONAL;
  }

  private:
   // we want a shared_from_this throughout the class hierarchy,
   // but want each member to always get the right derived type.
   // So we steal the same approach used by d_func() in the view class
   std::shared_ptr<ParameterPrivate> shared_this() {
     return shared_from_this();
   }
   std::shared_ptr<const ParameterPrivate> shared_this() const {
     return shared_from_this();
   }

};

// Checking if a value has changed is hard because NaN != NaN returns true.
// In order to explicity check NaN, we need a template specialization for double.  But
// you can't specialize single templated functions in a class definition, so we define
// this OTHER templated function here and just specialize that.
template <typename T>
bool valueChanged(T prev, T value) {
  return prev != value;
}

template <>
inline bool valueChanged(double prev, double value) {
  // if they're both NaN, nothing's changed
  if (std::isnan(prev) && std::isnan(value)) {
    return false;
  }

  // if one is NaN, but by implication of the previous statement at least one isn't,
  // something's changed
  if (std::isnan(prev) || std::isnan(value)) {
    return true;
  }

  // Nothing is NaN, so:
  return prev != value;
}

// ////////////////////////////////////////////////////////////////////////////
// ParameterPrivateT (per-type template class)
template<typename T>
 class ParameterPrivateT : public ParameterPrivate {
  // make noncopyable
 private:
  ParameterPrivateT(const ParameterPrivateT<T>&) = delete;
  ParameterPrivateT<T>& operator=(const ParameterPrivateT<T>&) = delete;

 public:
  typedef T ValueType;
  T value;
  T prev_value;

  /// \Parameters can be linked to another parameter instance, useful
  /// when implementing shared parameters and macros
  std::shared_ptr<ParameterPrivateT<T> > linkedParam;

  ParameterPrivateT(const std::string& name, std::shared_ptr<ParameterCollection> col, ParameterFlag stat,
      const T& defaultValue)
  : ParameterPrivate(name, col, stat), value(defaultValue), prev_value(defaultValue)
  {
    // do nothing
  }

  typename ParameterT<T>::Ptr getView() {
    typename ParameterT<T>::Ptr ret(new ParameterT<T>(shared_this()));
    return ret;
  }

  virtual Parameter::Ptr getGenericView() {
    return getView();
  }

  bool hasChanged() const override {
    // NaNs make this kinda annoying
    if (linkedParam) {
      return valueChanged<T>(prev_value, linkedParam->value);
    }
    return valueChanged<T>(prev_value, value);
  }

  void resetChanged() override {
    if (linkedParam) {
      value = linkedParam->value;
    }
    prev_value = value;
  }

  private:
   std::shared_ptr<ParameterPrivateT<T> > shared_this() {
    // this is a static downcast, which is generally not a great idea.  However, can be pretty certain
    // that this points to a class of this object's type.
    return std::static_pointer_cast<ParameterPrivateT<T> >(shared_from_this());
   }
   std::shared_ptr<const ParameterPrivate> shared_this() const {
     return std::static_pointer_cast<const ParameterPrivateT<T> >(shared_from_this());
   }
};

}
#endif //DS_MXPARAMETERPRIVATE_H
