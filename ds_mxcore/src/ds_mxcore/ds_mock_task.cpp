/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/2/19.
//

#include "ds_mxcore/test/ds_mock_task.h"

namespace ds_mx {

MxMockTask::MxMockTask() {
  initCalls = 0;
  onStartCalls = 0;
  onStopCalls = 0;
  validateCalls = 0;
  validateReturn = true;
  ticks = 0;
  defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
}

void MxMockTask::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  initCalls++;
}

void MxMockTask::init_ros(ros::NodeHandle& nh) {
  // does nothing
}

bool MxMockTask::validate() const {
  validateCalls++;
  return validateReturn;
}

void MxMockTask::resetTimeout(const ros::Time& now) {
  timeout = now;
}

void MxMockTask::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  std::copy(uuid.begin(), uuid.end(), std::begin(displayElement.task_uuid));
  display.elements.push_back(displayElement);
}

ds_mx::TaskReturnCode MxMockTask::tick(const ds_nav_msgs::NavState& state) {
  ds_mx::TaskReturnCode ret = defaultTickReturn;

  if (tickReturns.size() > 0) {
    ret = tickReturns.front();
    tickReturns.pop_front();
  }

  ticks++;

  return ret;
}

void MxMockTask::onStart(const ds_nav_msgs::NavState& state) {
  onStartCalls++;
}

void MxMockTask::onStop(const ds_nav_msgs::NavState& state) {
  onStopCalls++;
}

ds_mx::TaskReturnCode MxMockTask::onTick(const ds_nav_msgs::NavState &state) {
  // never called, doesn't do anything, doesn't matter.  But must be defined.
  return ds_mx::TaskReturnCode::RUNNING;
}

} // namespace ds_mx