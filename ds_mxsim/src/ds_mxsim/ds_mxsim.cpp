/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/29/18.
//

#include "ds_mxsim/ds_mxsim.h"
#include <cmath>
#include <algorithm>
#include <Eigen/Geometry>

namespace ds_mxsim {

MxSim::MxSim() {
  // do nothing
}

MxSim::MxSim(int argc, char* argv[], const std::string &name) : ds_base::DsProcess(argc, argv, name) {
  // do nothing
}

MxSim::~MxSim() {
  // do nothing special
}

void MxSim::setupParameters() {

  // load max turn rate, in deg/s
  turn_rate_max = ros::param::param<double>("~max_turn_rate", 15)*M_PI/180.0;

  // Load max speeds, in m/s
  surge_min = ros::param::param<double>("~surge_min", -1.0);
  surge_max = ros::param::param<double>("~surge_max",  1.0);

  sway_min  = ros::param::param<double>("~sway_min" , -0.5);
  sway_max  = ros::param::param<double>("~sway_max" ,  0.5);

  heave_min = ros::param::param<double>("~heave_min", -0.7);
  heave_max = ros::param::param<double>("~heave_max",  0.7);

  update_period_ = ros::Duration(1.0/ros::param::param<double>("~update_rate_hz", 10.0));

  // Load some topic names
  desiredInputTopic = ros::param::param<std::string>("~desired_input", ros::this_node::getName() + "/desired_input");
  stateInputTopic = ros::param::param<std::string>("~state_input", ros::this_node::getName() + "/state_input");
  navaggOutputTopic = ros::param::param<std::string>("~navagg_output", ros::this_node::getName() + "/navagg");

  double origin_lat = ros::param::param<double>("origin_lat", 0.0);
  double origin_lon = ros::param::param<double>("origin_lon", 0.0);
  ROS_INFO_STREAM("Setting origin: " <<origin_lat <<" " <<origin_lon);
  projection.reset(new ds_trackline::Projection(origin_lon, origin_lat));

}

void MxSim::setupSubscriptions() {
  auto nh = nodeHandle();

  desired_input = nh.subscribe(desiredInputTopic, 3, &MxSim::setDesiredState, this);
  state_input = nh.subscribe(stateInputTopic, 3, &MxSim::setCurrentState, this);

}

void MxSim::setupPublishers() {
  auto nh = nodeHandle();

  pub_sim_state = nh.advertise<ds_nav_msgs::NavState>(ros::this_node::getName() + "/sim_state", 10);
  pub_des_state = nh.advertise<ds_nav_msgs::NavState>(ros::this_node::getName() + "/desired_state", 10);

  pub_navagg_state = nh.advertise<ds_nav_msgs::AggregatedState>(navaggOutputTopic, 10);
}

void MxSim::setupTimers() {
  auto nh = nodeHandle();

  // both out.  Probably at 10Hz.
  update_timer = nh.createTimer(update_period_, &MxSim::update, this);
}

#define NA_COPY_FIELD(na,ss,fname) \
  (na).fname.valid = true; \
  (na).fname.value = (ss).fname;

#define NA_INVALID_FIELD(na, fname) \
  (na).fname.valid = false; \
  (na).fname.value = 0.0;

/// Called periodically to update the sim state based on the desired
/// state
void MxSim::update(const ros::TimerEvent& event) {
  ros::Duration step = event.current_expected - event.last_expected;
  double dt = step.toSec();

  // rotation from body -> global
  Eigen::Rotation2Dd R(M_PI/2.0-sim_state.heading);

  // Assume perfect position control

  Eigen::Vector2d uv;
  uv <<sim_state.surge_u, sim_state.sway_v;

  Eigen::Vector2d en = R * uv;

  sim_state.easting  += en(0)*dt;
  sim_state.northing += en(1)*dt;
  sim_state.down = 0;

  ds_trackline::Projection::VectorEN nav_en;
  nav_en(0) = sim_state.easting;
  nav_en(1) = sim_state.northing;

  // This conversion will output degrees-- exactly as we're expecting
  ds_trackline::Projection::VectorLL nav_ll;
  nav_ll = projection->projected2lonlat(nav_en);
  sim_state.lon = nav_ll(0);
  sim_state.lat = nav_ll(1);

  // --------------------------------------------------------------------------------
  // Update the heading while respecting the max rate
  double delta = desired_state.heading - sim_state.heading;
  // unwrap
  if (delta > M_PI) {
    delta -= (2.0*M_PI);
  } else if (delta < -M_PI) {
    delta += (2.0*M_PI);
  }
  // limit to max turn velocity as necessary
  double limit_hdg = turn_rate_max * dt;
  delta = copysign(std::min<double>(fabs(delta), limit_hdg), delta);

  // fix limits
  sim_state.heading += delta;
  if (sim_state.heading < 0) {
    sim_state.heading += (2.0*M_PI);
  } else if (sim_state.heading > 2.0*M_PI) {
    sim_state.heading -= (2.0*M_PI);
  }

  // set velocity
  sim_state.surge_u = std::max(std::min(desired_state.surge_u, surge_max), surge_min);
  sim_state.sway_v  = std::max(std::min(desired_state.sway_v,  sway_max ), sway_min );
  sim_state.heave_w = std::max(std::min(desired_state.heave_w, heave_max), heave_min);

  // Just pin roll/pitch at 0
  sim_state.roll = 0;
  sim_state.pitch = 0;

  // update timestamps
  sim_state.header.stamp = event.current_expected;

  // publish some stuff
  pub_sim_state.publish(sim_state);

  // Build an actual navagg to send
  ds_nav_msgs::AggregatedState navagg;
  navagg.header = sim_state.header;
  navagg.ds_header = sim_state.ds_header;

  // we need to copy this into a navagg so we can publish it
  NA_COPY_FIELD(navagg, sim_state, easting);
  NA_COPY_FIELD(navagg, sim_state, northing);
  NA_COPY_FIELD(navagg, sim_state, down);
  NA_COPY_FIELD(navagg, sim_state, roll);
  NA_COPY_FIELD(navagg, sim_state, pitch);
  NA_COPY_FIELD(navagg, sim_state, heading);
  NA_COPY_FIELD(navagg, sim_state, surge_u);
  NA_COPY_FIELD(navagg, sim_state, sway_v);
  NA_COPY_FIELD(navagg, sim_state, heave_w);
  NA_INVALID_FIELD(navagg, p);
  NA_INVALID_FIELD(navagg, q);
  NA_INVALID_FIELD(navagg, r);
  NA_INVALID_FIELD(navagg, du_dt);
  NA_INVALID_FIELD(navagg, dv_dt);
  NA_INVALID_FIELD(navagg, dw_dt);
  NA_INVALID_FIELD(navagg, dp_dt);
  NA_INVALID_FIELD(navagg, dq_dt);
  NA_INVALID_FIELD(navagg, dr_dt);

  pub_navagg_state.publish(navagg);
}

void MxSim::resetState() {
  sim_state.northing = 0;
  sim_state.easting = 0;
  sim_state.down = 0;
  sim_state.roll = 0;
  sim_state.pitch = 0;
  sim_state.heading = 0;
}

const ds_nav_msgs::NavState& MxSim::getCurrentState() const {
  return sim_state;
}

const ds_nav_msgs::NavState& MxSim::getDesiredState() const {
  return desired_state;
}

void MxSim::setCurrentState(const ds_nav_msgs::NavState& _curr_state) {
  sim_state = _curr_state;
}

void MxSim::setDesiredState(const ds_nav_msgs::NavState& _des_state) {
  desired_state = _des_state;
}

}