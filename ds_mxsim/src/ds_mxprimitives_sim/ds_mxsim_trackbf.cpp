/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/4/19.
//

#include <ds_libtrackline/WktUtil.h>
#include <pluginlib/class_list_macros.h>

#include <ds_mxcore/ds_mxevent.h>
#include <ds_mxcore/ds_mxtask.h>

#include <ds_mxprimitives_sim/ds_mxsim_trackbf.h>

namespace ds_mxsim {

TaskTracklineBf::TaskTracklineBf() :
  start_pt(params, "start_pt", ds_mx::ParameterFlag::DYNAMIC),
  end_pt(params, "end_pt", ds_mx::ParameterFlag::DYNAMIC),
  speed(params, "speed", ds_mx::ParameterFlag::DYNAMIC),
  altitude(params, "altitude", ds_mx::ParameterFlag::DYNAMIC),
  depth_floor(params, "depth_floor", ds_mx::ParameterFlag::DYNAMIC),
  timeout_multiplier(params, "timeout_multiplier", 2.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
  timeout_increment(params, "timeout_increment", 60.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
  trackline(0,0,0,0) {

  // re-initialize
}

void TaskTracklineBf::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);

  // initialize our trackline
  ds_mx::GeoPoint start = start_pt.get();
  ds_mx::GeoPoint end = end_pt.get();
  trackline = ds_trackline::Trackline(start.x, start.y, end.x, end.y);
}

void TaskTracklineBf::init_ros(ros::NodeHandle& nh) {
  pub_desired = nh.advertise<ds_nav_msgs::NavState>(ros::this_node::getName() + "/desired_state", 10);
}

void TaskTracklineBf::parameterChanged(const ds_nav_msgs::NavState &state) {
  updateTrackline();
}

ds_mx::TaskReturnCode TaskTracklineBf::onTick(const ds_nav_msgs::NavState& state) {

  // check for doneness
  ds_trackline::Trackline::VectorEN along_across = trackline.lonlat_to_trackframe(state.lon, state.lat);
  if (along_across[0] > 0) {
    // We're done!
    //ROS_WARN_STREAM("Finished trackline!");
    return ds_mx::SUCCESS;
  }

  // 0-length tracklines are buggy-- heading is ill-defined-- and should return immediately
  if (trackline.getLength() < 0.001) {
    ROS_ERROR_STREAM("Trackline too short (<1mm), skipping...");
    return ds_mx::SUCCESS;
  }

  double delta = std::copysign(std::min<double>(M_PI/3.0, fabs(along_across[1]) * kappa), along_across[1]);

  ds_nav_msgs::NavState desired;

  desired.header.stamp = state.header.stamp;
  desired.heading = trackline.getCourseRad() - delta;
  if (desired.heading > 2.0*M_PI) {
    desired.heading -= (2.0*M_PI);
  } else if (desired.heading < 0.0) {
    desired.heading += (2.0*M_PI);
  }
  desired.down = depth_floor.get();
  desired.surge_u = speed.get();
  desired.sway_v = 0.0;

  /*
  ROS_INFO_STREAM("TRACK: " <<-along_across[0] <<" to go, oline=" <<along_across[1]
                            <<" veh: (" <<state.lon <<"," <<state.lat <<"), ==> HDG="
                            << desired.heading*180.0/M_PI <<"deg @" <<desired.surge_u <<" m/s");
                            */

  pub_desired.publish(desired);

  return ds_mx::RUNNING;
}

bool TaskTracklineBf::validate() const {
  // TODO not sure what to do here
  return true;
}

void TaskTracklineBf::resetTimeout(const ros::Time &now) {
  if (params->anyChanged()) {
    updateTrackline();
    params->resetChanged();
  }
  auto start = trackline.getStartLL();
  auto end = trackline.getStartLL();
  //ROS_INFO_STREAM("Trackline: " <<start(0) <<" " <<start(1)
  //                    <<" --> " <<end(0) <<" " <<end(1));
  double expected_time = trackline.getLength() / speed.get();

  //ROS_INFO_STREAM("Expected timeout: " <<expected_time);
  //ROS_INFO_STREAM("Timeout multiplier: " <<timeout_multiplier.get());
  //ROS_INFO_STREAM("Timeout increment: " <<timeout_increment.get());

  timeout = now + ros::Duration(expected_time*timeout_multiplier.get()
      + timeout_increment.get());
}

void TaskTracklineBf::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {

  // access our start/end
  const auto& line_start = trackline.getStartLL();
  const auto& line_end = trackline.getEndLL();

  ds_trackline::Trackline connectingLine(state.lon, state.lat, line_start(0), line_end(0));

  ds_mx_msgs::MissionElementDisplay line1;
  line1.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING;
  line1.wellknowntext = connectingLine.getWktLL();
  std::copy(uuid.begin(), uuid.end(), line1.task_uuid.begin());
  display.elements.push_back(line1);

  // account for time
  state.header.stamp += ros::Duration(connectingLine.getLength() / speed.get());

  // draw the trackline itself
  ds_mx_msgs::MissionElementDisplay line2;
  line2.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE;
  line2.wellknowntext = trackline.getWktLL();
  std::copy(uuid.begin(), uuid.end(), line2.task_uuid.begin());
  display.elements.push_back(line2);

  // update state to nominal location
  state.lon = line_end(0);
  state.lat = line_end(1);
  state.header.stamp += ros::Duration(trackline.getLength() / speed.get());
}

void TaskTracklineBf::updateTrackline() {
  ds_mx::GeoPoint start = start_pt.get();
  ds_mx::GeoPoint end = end_pt.get();
  trackline = ds_trackline::Trackline(start.x, start.y, end.x, end.y);
}

}

PLUGINLIB_EXPORT_CLASS(ds_mxsim::TaskTracklineBf, ds_mx::MxTask)
