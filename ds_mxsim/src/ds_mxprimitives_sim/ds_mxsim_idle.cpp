/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/4/19.
//

#include <pluginlib/class_list_macros.h>

#include <ds_mxcore/ds_mxevent.h>
#include <ds_mxcore/ds_mxtask.h>

#include <ds_mxprimitives_sim/ds_mxsim_idle.h>
#include <ds_libtrackline/WktUtil.h>

namespace ds_mxsim {

void TaskIdle::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);
  timeout_code = ds_mx::TaskReturnCode::SUCCESS;
}

void TaskIdle::init_ros(ros::NodeHandle& nh) {
  pub_desired = nh.advertise<ds_nav_msgs::NavState>(ros::this_node::getName() + "/desired_state", 10);
}

ds_mx::TaskReturnCode TaskIdle::onTick(const ds_nav_msgs::NavState& state) {
  ds_nav_msgs::NavState desired;

  // set state to 0 velocity, our current heading, and current depth
  desired.header.stamp = state.header.stamp;
  desired.heading = state.heading;
  desired.surge_u = 0;
  desired.sway_v = 0;
  desired.heave_w = 0;
  desired.down = state.down;

  pub_desired.publish(desired);

  return ds_mx::TaskReturnCode::RUNNING;
}

bool TaskIdle::validate() const {
  // Idle Task is always OK
  return true;
}

void TaskIdle::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  // increment timeout
  state.header.stamp += ros::Duration(max_timeout_sec.get());

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

}

PLUGINLIB_EXPORT_CLASS(ds_mxsim::TaskIdle, ds_mx::MxTask)
