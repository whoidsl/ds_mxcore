/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/29/18.
//

#ifndef PROJECT_DS_MXSIM_H
#define PROJECT_DS_MXSIM_H

#include <ds_nav_msgs/NavState.h>
#include <ds_nav_msgs/AggregatedState.h>
#include <ds_libtrackline/Projection.h>
#include <ds_base/ds_process.h>

namespace  ds_mxsim {

class MxSim : public ds_base::DsProcess
{
 public:
  explicit MxSim();
  MxSim(int argc, char* argv[], const std::string& name);
  ~MxSim() override;

  DS_DISABLE_COPY(MxSim);

  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupTimers() override;

  const ds_nav_msgs::NavState& getCurrentState() const;
  const ds_nav_msgs::NavState& getDesiredState() const;
  void setCurrentState(const ds_nav_msgs::NavState& _curr_state);
  void setDesiredState(const ds_nav_msgs::NavState& _des_state);

  void update(const ros::TimerEvent& event);
  void resetState();

 protected:
  ds_nav_msgs::NavState sim_state;
  ds_nav_msgs::NavState desired_state;
  ros::Time sim_time;
  // just fix the projection to a transverse mercator about 0,0
  std::unique_ptr<ds_trackline::Projection> projection;

  // maximum turn rate + speed
  double turn_rate_max;

  // max speeds, in body-frame m/s
  double surge_min;
  double surge_max;
  double sway_min;
  double sway_max;
  double heave_min;
  double heave_max;

  ros::Duration update_period_;

  // topic names
  std::string desiredInputTopic;
  std::string stateInputTopic;
  std::string navaggOutputTopic;

  ros::Subscriber desired_input;
  ros::Subscriber state_input;

  ros::Publisher pub_sim_state;
  ros::Publisher pub_des_state;
  ros::Publisher pub_navagg_state;

  ros::Timer update_timer;

};
}

#endif //PROJECT_DS_MXSIM_H
