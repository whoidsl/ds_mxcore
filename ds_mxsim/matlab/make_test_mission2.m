mission_start = [0,0];
mission_waypts = 100*[0,0; 0,1; 1,1; 1,0; 0,0;...
                 -1,0; -1,-1; 0,-1; 0,0];
SPEED = 1.0;
ALTITUDE = 10.0;
DEPTH_FLOOR = 100.0;

orglat = 0;
orglon = 0;

%XY msut be NORTH EAST DOWN
[lat, lon] = xy2ll(mission_waypts(:,1), mission_waypts(:,2), orglat, orglon);

mission_latlon = [lon, lat];

figure(1);
subplot(1,2,1);
hold off;
plot(mission_waypts(:,1), mission_waypts(:,2), 'b');
hold on;
plot(mission_start(1), mission_start(2), 'rx', 'LineWidth', 3);
grid on;
axis equal;
xlabel('Easting [m]');
ylabel('Northing [m]');

subplot(1,2,2);
hold off;
plot(mission_latlon(:,1), mission_latlon(:,2), 'b');
hold on;
plot(orglon, orglat, 'rx', 'LineWidth', 3);
grid on;
axis equal;
xlabel('Longitude [deg]');
ylabel('Latitude [deg]');

figure(2);
hold off;

% Write the output file
fid = fopen('../mission/test2.json', 'w');
fprintf(fid, '{\n');
fprintf(fid, '  "root": {\n');
fprintf(fid, '    "type": "abort_root",\n');
fprintf(fid, '    "mission":\n');
fprintf(fid, '    {\n');
fprintf(fid, '      "type": "sequence",\n');
fprintf(fid, '      "subtasks": [\n');

for i=2:size(mission_latlon,1)
    fprintf(fid, '        {\n');
    fprintf(fid, '          "type": "sim_trackbf",\n');
    fprintf(fid, '          "start_lat": %f,\n', mission_latlon(i-1,2));
    fprintf(fid, '          "start_lon": %f,\n', mission_latlon(i-1,1));
    fprintf(fid, '          "end_lat": %f,\n', mission_latlon(i,2));
    fprintf(fid, '          "end_lon": %f,\n', mission_latlon(i,1));
    fprintf(fid, '          "speed": %f,\n', SPEED);
    fprintf(fid, '          "altitude": %f,\n', ALTITUDE);
    fprintf(fid, '          "depth_floor": %f\n', DEPTH_FLOOR);

    if i==size(mission_latlon,1)
        fprintf(fid, '        }\n');
    else 
        fprintf(fid, '        },\n');
    end
    plot(mission_waypts(i-1:i,1), mission_waypts(i-1:i,2), 'b');
    hold on;
    plot(mission_waypts(i-1,1), mission_waypts(i-1,2), 'go');
    plot(mission_waypts(i,1), mission_waypts(i,2), 'rx');
    
end


fprintf(fid, '      ]\n');        
fprintf(fid, '    },\n');
fprintf(fid, '    "abort": {\n');
fprintf(fid, '      "type": "empty"\n');
fprintf(fid, '    },\n');
fprintf(fid, '    "trap": {\n');
fprintf(fid, '      "type": "sim_idle"\n');
fprintf(fid, '    }\n');
fprintf(fid, '  },\n');
fprintf(fid, '  "shared": {\n');
fprintf(fid, '  }\n');
fprintf(fid, '}\n');
fclose(fid);