#!/usr/bin/env python

import rospy
import math

from ds_nav_msgs.msg import NavState
from sensor_msgs.msg import Joy

class Translator(object):
    def __init__(self):
        rospy.init_node("simjoytest")
        self._nav_state = None
        self._sub_joy = rospy.Subscriber("joy", Joy, self._callback_joy)
        self._sub_state = rospy.Subscriber("sim_state", NavState, self._callback_nav_state)
        self._pub_desired = rospy.Publisher("desired_state", NavState, queue_size=3)

        self.max_speed  = float(rospy.get_param("~max_speed", 1.0))
        self.max_rotate = float(rospy.get_param("~max_rotate", 15.0)) # in degrees

    def _callback_joy(self, msg):
        if self._nav_state is None:
            print 'No nav data yet, continuing...'
            return

        # Read the raw command
        hdg_cmd = msg.axes[3]
        spd_cmd = msg.axes[4]

        # Convert raw commands to real units
        des_hdg = self._nav_state.heading*180.0/math.pi + -hdg_cmd * self.max_rotate
        if des_hdg > 360.0:
            des_hdg -= 360.0
        elif des_hdg < 0:
            des_hdg += 360.0

        des_spd = spd_cmd * self.max_speed

        # Build the actual message to send
        toSend = NavState()
        toSend.header.stamp = msg.header.stamp
        toSend.heading = des_hdg * math.pi/180.0
        toSend.surge_u = des_spd
        toSend.sway_v = 0

        self._pub_desired.publish(toSend)

        #print '%10.3f --> %10.3f | %10.3f --> %10.3f' % (spd_cmd, des_spd, hdg_cmd, des_hdg)

    def _callback_nav_state(self, msg):
        self._nav_state = msg

    def run(self):
        rospy.spin()

if __name__ == '__main__':
    node = Translator()
    node.run()
