#!/usr/bin/env python

import rospy
import math
import numpy as np
import matplotlib.pyplot as plt

from ds_nav_msgs.msg import NavState

class Plotter(object):
    def __init__(self):
        self._plt_fig, self._plt_ax = plt.subplots(1,1)
        self._plt_ax.set_aspect('equal')
        self._plt_ax.set_xlim(-200,200)
        self._plt_ax.set_ylim(-200,200)
        self._plt_ax.hold(True)
        self._plt_history_x = []
        self._plt_history_y = []
        self._last_history = None

        self._history_update_interval = float(rospy.get_param('~history_update_interval_seconds', 1.0))
        self._history_max_len = int(rospy.get_param('~history_max_len', 3600))

        # Go ahead and draw the plot
        plt.show(False)
        plt.draw()
        # Keep a copy of the background for later
        self._plt_background = self._plt_fig.canvas.copy_from_bbox(self._plt_ax.bbox)

        # Add our various things
        self._plt_history = self._plt_ax.plot(0,0,'b.')[0]
        self._plt_pos = self._plt_ax.plot(0,0,'ro')[0]
        self._plt_dir = self._plt_ax.plot([0,0],[0,0],'r')[0]

        rospy.init_node("mxsim_viz", anonymous=True)
        self._sub_state = rospy.Subscriber("sim_state", NavState, self._callback_nav_state)

        # Use this instead of rospy.spin()
        # Callbacks will still execute correctly, because threads
        # Not 100% sure why this works, but it does
        plt.show(block=True)

    def _callback_nav_state(self, msg):
        hdg = math.degrees(msg.heading)
        spd = msg.surge_u
        x = msg.easting
        y = msg.northing

        #print '%10.4f @%10.4f (%10.4f, %10.4f)' % (spd, hdg, x, y)

        if self._last_history is None:
            dt = 1000.0
        else:
            delta = msg.header.stamp - self._last_history
            dt = delta.to_sec()
        #print 'DT: %.3f len(hist)=%d' % (dt, len(self._plt_history_x))

        if dt >= self._history_update_interval:
            #print '** UPDATE **'
            self._plt_history_x.append(x)
            self._plt_history_y.append(y)
            self._last_history = msg.header.stamp

        if len(self._plt_history_x) > self._history_max_len:
            self._plt_history_x = self._plt_history_x[-self._history_max_len:]
            self._plt_history_y = self._plt_history_y[-self._history_max_len:]

        # Update the data
        self._plt_pos.set_data(x, y)
        self._plt_dir.set_data([x, x+10.0*math.sin(math.radians(hdg))],
                               [y, y+10.0*math.cos(math.radians(hdg))])
        self._plt_history.set_data(self._plt_history_x, self._plt_history_y)

        # Restore background by bliting directly
        self._plt_fig.canvas.restore_region(self._plt_background)
        self._plt_ax.draw_artist(self._plt_history)
        self._plt_ax.draw_artist(self._plt_dir)
        self._plt_ax.draw_artist(self._plt_pos)
        self._plt_fig.canvas.blit(self._plt_ax.bbox)

if __name__ == '__main__':
    node = Plotter()
    # We use a blocking call to plt.show to make this work.
